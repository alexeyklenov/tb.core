﻿using System.Runtime.Serialization;

namespace TB.Utils.Enums
{
    [DataContract]
    public enum AccessType
    {
        [EnumMember]
        None = 0,
        [EnumMember]
        ReadWrite = 1,
        [EnumMember]
        ReadOnly = 2,
        [EnumMember]
        Custom = 3
    }
}
