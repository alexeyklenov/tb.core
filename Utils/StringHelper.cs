﻿using System;

namespace TB.Utils
{
    public static class StringHelper
    {
        public static String GetSubstrBySeparator(this String str, char firstSeparator, char lastSeparator, Boolean isTrimed = true)
        {
            var length = str.IndexOf(lastSeparator) - str.IndexOf(firstSeparator) - 1;
            var result = length > 0 ? str.Substring(str.IndexOf(firstSeparator) + 1, length)
                : (str.IndexOf(firstSeparator) != -1 ? str.Substring(str.IndexOf(firstSeparator) + 1) : "");
            if (isTrimed)
                result = result.Trim();
            return result;

        }

        public static String GetSubstrBySeparator(this String str, String firstSeparator, String lastSeparator)
        {
            var length = str.IndexOf(lastSeparator, StringComparison.Ordinal) - str.IndexOf(firstSeparator, StringComparison.Ordinal) - 1;
            return length > 0 ? str.Substring(str.IndexOf(firstSeparator, StringComparison.Ordinal) + 1, length).Trim()
                : (str.IndexOf(firstSeparator, StringComparison.Ordinal) != -1 ? str.Substring(str.IndexOf(firstSeparator, StringComparison.Ordinal) + 1).Trim() : "");

        }

        public static int RsHash(string str)
        {
            const int b = 1;
            var a = 1;
            var hash = 0;

            foreach (var t in str)
            {
                hash = hash * a + t;
                a = a + b;
            }

            return hash;
        }
    }
}
