﻿using System;

namespace Utils
{
    public static class GuidToStringExtension
    {
        public static string ToRawString(this Guid guid)
        {
            var bytes = guid.ToByteArray();
            var result = new char[32];
            byte index = 0;

            for (var i = 0; i < 16; i++)
            {
                var a = (bytes[i] >> 4) & 0xf;
                result[index++] = (char)((a > 9) ? a - 10 + 0x41 : a + 0x30);
                a = bytes[i] & 0xf;
                result[index++] = (char)((a > 9) ? a - 10 + 0x41 : a + 0x30);
            }
            return new string(result, 0, 32);
        }
    }
}
