﻿using System;
using System.Linq;
using System.Threading;

namespace Utils
{
    public class WaitLock : IDisposable
    {
        private readonly object[] _padlocks;
        private readonly bool[] _lockFlags;

        public WaitLock(int milliSecondTimeout, object padlock)
            : this(milliSecondTimeout, new[] { padlock })
        {
        }

        public WaitLock(int milliSecondTimeout, params object[] padlocks)
        {
            _padlocks = padlocks;
            _lockFlags = new bool[_padlocks.Length];
            for (var i = 0; i < _padlocks.Length; i++)
                _lockFlags[i] = Monitor.TryEnter(padlocks[i], milliSecondTimeout);
        }

        public WaitLock(params object[] padlocks)
            : this(1000, padlocks)
        {
        }

        public bool Locked
        {
            get { return _lockFlags.All(s => s); }
        }

        public static void Lock(object[] padlocks, int millisecondTimeout, Action codeToRun)
        {
            using (var bolt = new WaitLock(millisecondTimeout, padlocks))
                if (bolt.Locked)
                    codeToRun();
                else
                    throw new TimeoutException(string.Format("Safe.Lock wasn't able to acquire a lock in {0}ms",
                                                             millisecondTimeout));
        }

        public static void Lock(object padlock, int millisecondTimeout, Action codeToRun)
        {
            Lock(new[] { padlock }, millisecondTimeout, codeToRun);
        }

        #region Implementation of IDisposable

        public void Dispose()
        {
            for (var i = 0; i < _lockFlags.Length; i++)
                if (_lockFlags[i])
                {
                    Monitor.Exit(_padlocks[i]);
                    _lockFlags[i] = false;
                }
        }

        #endregion
    }
}
