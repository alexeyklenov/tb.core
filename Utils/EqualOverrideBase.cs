﻿using System;
using System.Runtime.Serialization;

namespace TB.Utils
{
    [Serializable]
    [DataContract(IsReference = true)]
public abstract class EqualOverrideBase
    {
        public override sealed bool Equals(object value)
        {
            var compareTo = value as EqualOverrideBase;

            return ((compareTo != null) &&
                     HasSameBusinessSignatureAs(compareTo));
        }
        public static bool operator ==(EqualOverrideBase source, EqualOverrideBase target)
        {
            // If both are null, or both are same instance, return true.
            if (ReferenceEquals(source, target))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)source == null) || ((object)target == null))
            {
                return false;
            }

            // Return true if the fields match:
            return source.Equals(target);
        }

        public static bool operator !=(EqualOverrideBase source, EqualOverrideBase target)
        {
            return !(source == target);
        }

        /// <summary>
        /// Должен быть определен для сравнения двух объектов
        /// </summary>
        public abstract override int GetHashCode();

        public virtual bool HasSameBusinessSignatureAs(EqualOverrideBase compareTo)
        {
            Check.Require(compareTo != null, "compareTo may not be null");

            return GetHashCode().Equals(compareTo.GetHashCode());
        }

        /// <summary>
        /// Возвращает Хэш введенных значений
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public virtual int HashOf(params object[] parameters)
        {
            var hashString = GetType().FullName;
            foreach (var parameter in parameters)
            {
                if (parameter != null)
                    hashString = hashString + "|" + parameter;
            }
            return hashString.GetHashCode();
        }
    }
}
