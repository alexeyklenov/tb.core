﻿using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;

namespace Utils
{
    public class UnityContext
    {


        private static readonly object SyncRoot = new object();

        private static IUnityContainer _instance;

        private UnityContext()
        {

        }

        public static IUnityContainer Container
        {
            get
            {
                if (_instance == null)
                {
                    WaitLock.Lock(SyncRoot, 10000, () =>
                    {
                        if (_instance == null)
                        {
                            _instance = new UnityContainer();
                            _instance.LoadConfiguration();
                           /* UnityConfigurationSection section
                                =
                                (UnityConfigurationSection)
                                ConfigurationManager.GetSection("unity");

                            if (section != null)
                                section.Configure(instance); */
                        }
                    });
                }
                return _instance;
            }

        }
    }
}
