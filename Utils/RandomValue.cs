﻿namespace TB.Utils
{
    public static class RandomValue
    {
        private static int _incInt = 10;

        public static int GetIncInt
        {
            get { return _incInt++; }
        }
    }
}
