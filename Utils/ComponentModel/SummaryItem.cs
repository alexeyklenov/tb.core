﻿using System;
using System.ComponentModel;

namespace Utils.ComponentModel
{
    [Serializable]
    public class SummaryItem
    {

        public SummaryItem() : this(string.Empty, string.Empty, null) { }

        public SummaryItem(PropertyDescriptor descriptor, string summaryType, object key)
        {
            Key = key;
            SummaryType = summaryType;
            Descriptor = descriptor.Name;

        }
        public SummaryItem(string descriptor, string summaryType, object key)
        {
            Key = key;
            SummaryType = summaryType;
            Descriptor = descriptor;

        }

        public string SummaryType { get; set; }

        public string Descriptor { get; set; }

        public object Key { get; set; }
    }
}
