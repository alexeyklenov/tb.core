﻿using System;
using System.ComponentModel;

namespace Utils.ComponentModel
{
    [Serializable]
    public class SortDescription
    {
        public string Descriptor { get; set; }

        public ListSortDirection SortOrder { get; set; }

        public SortDescription(string name, ListSortDirection sortOrder)
        {
            Descriptor = name;
            SortOrder = sortOrder;
        }
        public SortDescription(ListSortDescription sortDescription)
        {
            Descriptor = sortDescription.PropertyDescriptor.Name;
            SortOrder = sortDescription.SortDirection;
        }


    }
}
