﻿using System.Collections;
using System.ServiceModel;

namespace TB.Utils.Wcf
{
    public class WcfOperationContext : IExtension<OperationContext>
    {
        private readonly Hashtable _items;

        private WcfOperationContext()
        {
            _items = new Hashtable();
        }

        public Hashtable Items
        {
            get { return _items; }
        }

        public static WcfOperationContext Current
        {
            get
            {
                if (OperationContext.Current == null)
                    return null;
                var context = OperationContext.Current.Extensions.Find<WcfOperationContext>();
                if (context == null)
                {
                    context = new WcfOperationContext();
                    OperationContext.Current.Extensions.Add(context);
                }
                return context;
            }
        }

        public void Attach(OperationContext owner) { }
        public void Detach(OperationContext owner) { }
    }
}