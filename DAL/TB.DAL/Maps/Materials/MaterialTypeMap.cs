﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL.Descriptions;
using TB.BOL.Materials;

namespace TB.DAL.Maps.Materials
{
    public class MaterialTypeMap : ClassMapping<MaterialType>
    {
        public MaterialTypeMap()
        {
            Cache(delegate(ICacheMapper mapper)
            {
                mapper.Include(CacheInclude.All);
                mapper.Region(CacheRegion.References.ToString());
                mapper.Usage(CacheUsage.ReadWrite);
            });

            Table("MATERIAL_TYPE");
            Lazy(false);

            ReferenceMap.Map(this);

            Id(x => x.Id, mapper =>
            {
                mapper.Generator(Generators.Identity);
                mapper.Column("MATERIAL_TYPE_ID");
            });

            Property(x => x.Guid, delegate(IPropertyMapper mapper)
            {
                mapper.Column("MATERIAL_TYPE_GUID");
                mapper.NotNullable(true);
                mapper.Unique(true);
            });

            Set(x => x.Descriptions, delegate(ISetPropertiesMapper<MaterialType, Description<MaterialType>> mapper)
            {
                mapper.Key(keyMapper => keyMapper.Column("MATERIAL_TYPE_ID"));
                mapper.Inverse(true);
                mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
                mapper.Lazy(CollectionLazy.Lazy);
            }, relation => relation.OneToMany());
        }
    }
}
