﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL.Materials;

namespace TB.DAL.Maps.Materials
{
    public class MaterialRelationMap: ClassMapping<MaterialRelation>
    {
        public MaterialRelationMap()
        {
            Table("MATERIAL_RELATION");
            Lazy(true);

            Id(x => x.Id, mapper =>
            {
                mapper.Generator(Generators.Identity);
                mapper.Column("MATERIAL_RELATION_ID");
            });

            Property(x => x.Guid, delegate(IPropertyMapper mapper)
            {
                mapper.Column("MATERIAL_RELATION_GUID");
                mapper.NotNullable(true);
                mapper.Unique(true);
            });

            ManyToOne(x => x.ParentMaterial, delegate(IManyToOneMapper mapper)
            {
                mapper.Column("PARENT_MATERIAL_ID");
                mapper.Lazy(LazyRelation.NoLazy);
                mapper.Fetch(FetchKind.Join);
                mapper.Cascade(Cascade.Persist);
            });

            ManyToOne(x => x.ChildMaterial, delegate(IManyToOneMapper mapper)
            {
                mapper.Column("CHILD_MATERIAL_ID");
                mapper.Lazy(LazyRelation.NoLazy);
                mapper.Fetch(FetchKind.Join);
                mapper.Cascade(Cascade.All);
            });
        }
    }
}
