﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL.Descriptions;
using TB.BOL.Documents;
using TB.BOL.Materials;

namespace TB.DAL.Maps.Materials
{
    public class MaterialMap: ClassMapping<Material>
    {
        public MaterialMap()
        {
            Table("MATERIALS");
            Lazy(true);
            DynamicUpdate(true);

            Id(x => x.Id, mapper =>
            {
                mapper.Generator(Generators.Identity);
                mapper.Column("MATERIAL_ID");
            });

            Property(x => x.Guid, delegate(IPropertyMapper mapper)
            {
                mapper.Column("MATERIAL_GUID");
                mapper.NotNullable(true);
                mapper.Unique(true);
            });

            Property(x => x.MaterialStatus, delegate(IPropertyMapper mapper)
            {
                mapper.Column("STATUS");
                mapper.Type<NHibernate.Type.EnumStringType<MaterialStatus>>();
                mapper.NotNullable(true);
            });

            Property(x => x.No, delegate(IPropertyMapper mapper) { mapper.Column("NUM"); mapper.Index("MATERIALS_NO_IDX"); });
            Property(x => x.Code, delegate(IPropertyMapper mapper) { mapper.Column("CODE"); mapper.Index("MATERIALS_CODE_IDX"); });
            Property(x => x.Name, delegate(IPropertyMapper mapper) { mapper.Column("NAME"); mapper.Index("MATERIALS_NAME_IDX"); });

            ManyToOne(x => x.Parent, delegate(IManyToOneMapper mapper)
            {
                mapper.Column("PARENT_ID");
                mapper.Lazy(LazyRelation.NoProxy);
            });
            ManyToOne(x => x.MaterialType, delegate(IManyToOneMapper mapper)
            {
                mapper.Column("MATERIAL_TYPE_ID");
                mapper.Lazy(LazyRelation.NoLazy);
                mapper.Fetch(FetchKind.Join);
            });

            Set(x => x.Descriptions, delegate(ISetPropertiesMapper<Material, Description<Material>> mapper)
            {
                mapper.Key(keyMapper => keyMapper.Column("MATERIAL_ID"));
                mapper.Inverse(true);
                mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
                mapper.Fetch(CollectionFetchMode.Join);
                mapper.Lazy(CollectionLazy.Extra);
            }, relation => relation.OneToMany());

            Set(x => x.Components, delegate(ISetPropertiesMapper<Material, Material> mapper)
            {
                mapper.Key(keyMapper => keyMapper.Column("PARENT_ID"));
                mapper.Inverse(true);
                mapper.Cascade(Cascade.All);
                mapper.Lazy(CollectionLazy.Lazy);
            }, relation => relation.OneToMany());

            Set(x => x.ParentMaterials, delegate(ISetPropertiesMapper<Material, MaterialRelation> mapper)
            {
                mapper.Key(keyMapper => keyMapper.Column("CHILD_MATERIAL_ID"));
                mapper.Inverse(true);
                mapper.Cascade(Cascade.All);
                mapper.Lazy(CollectionLazy.Lazy);
            }, relation => relation.OneToMany());

            Set(x => x.ChildMaterials, delegate(ISetPropertiesMapper<Material, MaterialRelation> mapper)
            {
                mapper.Key(keyMapper => keyMapper.Column("PARENT_MATERIAL_ID"));
                mapper.Inverse(true);
                mapper.Cascade(Cascade.All);
                mapper.Lazy(CollectionLazy.Lazy);
            }, relation => relation.OneToMany());

            Set(x => x.Documents, delegate(ISetPropertiesMapper<Material, DocumentLine> mapper)
            {
                mapper.Key(keyMapper => keyMapper.Column("MATERIAL_ID"));
                mapper.Inverse(true);
                mapper.Cascade(Cascade.All);
                mapper.Lazy(CollectionLazy.Lazy);
            }, relation => relation.OneToMany());
        }
    }
}
