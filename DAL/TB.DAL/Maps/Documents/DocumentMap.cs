﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL.Descriptions;
using TB.BOL.Documents;

namespace TB.DAL.Maps.Documents
{
    public class DocumentBaseMap:  ClassMapping<DocumentBase>
    {
        public DocumentBaseMap()
        {
            Table("DOCUMENTS");
            Lazy(true);
            Id(x => x.Id, mapper =>
            {
                mapper.Generator(Generators.Identity);
                mapper.Column("DOCUMENT_ID");
            });

            Property(x => x.Guid, delegate(IPropertyMapper mapper)
            {
                mapper.Column("DOCUMENT_GUID");
                mapper.NotNullable(true);
                mapper.Unique(true);
            });

            Property(x=>x.No, mapper => mapper.Column("NUM"));
            Property(x => x.Name, mapper => mapper.Column("NAME"));

            Property(x => x.DocumentStatus, delegate(IPropertyMapper mapper)
            {
                mapper.Column("STATUS");
                mapper.Type<NHibernate.Type.EnumStringType<DocumentStatus>>();
                mapper.NotNullable(true);
            });
            Property(x => x.DocumentDate, delegate(IPropertyMapper mapper)
            {
                mapper.Column("DOCUMENT_DATE");
                mapper.NotNullable(true);
            });

            ManyToOne(x => x.DocumentType, delegate(IManyToOneMapper mapper)
            {
                mapper.Column("DOCUMENT_TYPE_ID");
                mapper.NotNullable(true);
                mapper.Fetch(FetchKind.Join);
            });

            Set(x => x.Lines,
                delegate(ISetPropertiesMapper<DocumentBase, DocumentLine> mapper)
                {
                    mapper.Lazy(CollectionLazy.Lazy);
                    mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
                    mapper.Key(keyMapper => keyMapper.Column("DOCUMENT_ID"));
                }, relation => relation.OneToMany());
        }

    }

    public class DocumentMap : SubclassMapping<Document>
    {
        public DocumentMap()
        {
            Lazy(true);

            ManyToOne(x => x.Parent, delegate(IManyToOneMapper mapper)
            {
                mapper.Column("PARENT_ID");
                mapper.Lazy(LazyRelation.NoProxy);
            });

            Set(x => x.Childs, delegate(ISetPropertiesMapper<Document, DocumentBase> mapper)
            {
                mapper.Key(keyMapper => keyMapper.Column("PARENT_ID"));
                mapper.Inverse(true);
                mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
                mapper.Lazy(CollectionLazy.Lazy);
            }, relation => relation.OneToMany());

            Set(x => x.Descriptions, delegate(ISetPropertiesMapper<Document, Description<Document>> mapper)
            {
                mapper.Key(keyMapper => keyMapper.Column("DOCUMENT_ID"));
                mapper.Inverse(true);
                mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
                mapper.Lazy(CollectionLazy.Lazy);
            }, relation => relation.OneToMany());

            Set(x => x.SourceDocumentRltns, delegate(ISetPropertiesMapper<Document, DocumentRelation> mapper)
            {
                mapper.Key(keyMapper => keyMapper.Column("CHILD_DOCUMENT_ID"));
                mapper.Inverse(true);
                mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
                mapper.Lazy(CollectionLazy.Lazy);
            }, relation => relation.OneToMany());

            Set(x => x.DerivedDocumentRltns, delegate(ISetPropertiesMapper<Document, DocumentRelation> mapper)
            {
                mapper.Key(keyMapper => keyMapper.Column("PARENT_DOCUMENT_ID"));
                mapper.Inverse(true);
                mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
                mapper.Lazy(CollectionLazy.Lazy);
            }, relation => relation.OneToMany());
        }
    }
}
