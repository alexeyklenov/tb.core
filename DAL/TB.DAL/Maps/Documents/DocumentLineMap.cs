﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL.Descriptions;
using TB.BOL.Documents;
using Cascade = NHibernate.Mapping.ByCode.Cascade;

namespace TB.DAL.Maps.Documents
{
    public class DocumentLineMap: ClassMapping<DocumentLine>
    {
        public DocumentLineMap()
        {
            Table("DOCUMENT_LINE");
            Lazy(true);
            Id(x => x.Id, mapper =>
            {
                mapper.Generator(NHibernate.Mapping.ByCode.Generators.Identity);
                mapper.Column("DOCUMENT_LINE_ID");
            }
                );
            Property(x => x.Guid, delegate(IPropertyMapper mapper)
            {
                mapper.Column("DOCUMENT_GUID");
                mapper.NotNullable(true);
                mapper.Unique(true);
            });

            Property(x => x.No, mapper => mapper.Column("NUM"));
            Property(x => x.Name, mapper => mapper.Column("NAME"));

            Property(x => x.DocumentLineType, delegate(IPropertyMapper mapper)
            {
                mapper.Column("TYPE");
                mapper.Type<NHibernate.Type.EnumStringType<DocumentLineType>>();
                mapper.NotNullable(true);
            });
            Property(x => x.DocumentLineStatus, delegate(IPropertyMapper mapper)
            {
                mapper.Column("STATUS");
                mapper.Type<NHibernate.Type.EnumStringType<DocumentLineStatus>>();
                mapper.NotNullable(true);
            });

            ManyToOne(x => x.Document, delegate(IManyToOneMapper mapper)
            {
                mapper.Column("DOCUMENT_ID");
                mapper.Lazy(LazyRelation.NoLazy);
                mapper.Fetch(FetchKind.Join);
                mapper.Cascade(Cascade.Persist);
            });
            ManyToOne(x => x.Material, delegate(IManyToOneMapper mapper)
            {
                mapper.Column("MATERIAL_ID");
                mapper.Lazy(LazyRelation.NoLazy);
                mapper.Fetch(FetchKind.Join);
            });

            Set(x => x.Descriptions, delegate(ISetPropertiesMapper<DocumentLine, Description<DocumentLine>> mapper)
            {
                mapper.Lazy(CollectionLazy.Lazy);
                mapper.Inverse(true);
                mapper.Cascade(Cascade.DeleteOrphans | Cascade.All);
                mapper.Key(keyMapper => keyMapper.Column("DOCUMENT_LINE_ID"));
            }, a => a.OneToMany());
        }
    }
}
