﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL.Documents;

namespace TB.DAL.Maps.Documents
{
    public class DocumentRelationMap : ClassMapping<DocumentRelation>
    {
        public DocumentRelationMap()
        {
            Table("DOCUMENT_RELATION");
            Lazy(true);

            Id(x => x.Id, mapper =>
            {
                mapper.Generator(Generators.Identity);
                mapper.Column("DOCUMENT_RELATION_ID");
            }
                );
            Property(x => x.Guid, delegate(IPropertyMapper mapper)
            {
                mapper.Column("DOCUMENT_RELATION_GUID");
                mapper.NotNullable(true);
                mapper.Unique(true);
            });

            ManyToOne(x => x.SourceDocument, delegate(IManyToOneMapper mapper)
            {
                mapper.Column("PARENT_DOCUMENT_ID");
                mapper.Lazy(LazyRelation.NoProxy);
                mapper.Cascade(Cascade.Persist);
            });

            ManyToOne(x => x.DerivedDocument, delegate(IManyToOneMapper mapper)
            {
                mapper.Column("CHILD_DOCUMENT_ID");
                mapper.Lazy(LazyRelation.NoProxy);
                mapper.Cascade(Cascade.Persist);
            });

            Property(x => x.IsDefault, mapper => mapper.Column("F_DEFAULT"));

        }
    }
}
