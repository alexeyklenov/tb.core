﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL.Documents;

namespace TB.DAL.Maps.Documents
{
    public class DocumentTypeMap: ClassMapping<DocumentType>
    {
        public DocumentTypeMap()
        {
            Cache(delegate(ICacheMapper mapper)
            {
                mapper.Include(CacheInclude.All);
                mapper.Region(CacheRegion.References.ToString());
                mapper.Usage(CacheUsage.ReadWrite);
            });

            Table("DOCUMENT_TYPE");
            //Lazy(false);

            ReferenceMap.Map(this);

            Id(x => x.Id, mapper =>
            {
                mapper.Generator(Generators.Identity);
                mapper.Column("DOCUMENT_TYPE_ID");
            });

            Property(x => x.Guid, delegate(IPropertyMapper mapper)
            {
                mapper.Column("DOCUMENT_TYPE_GUID");
                mapper.NotNullable(true);
                mapper.Unique(true);
            });
            //Set(x => x.Documents, mapper => mapper.Lazy(CollectionLazy.Lazy), relation => relation.OneToMany());

        }
    }
}
