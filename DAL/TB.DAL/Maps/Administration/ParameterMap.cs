﻿
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL.Administration;

namespace TB.DAL.Maps.Administration
{
    public class ParameterMapper : ClassMapping<Parameter>
    {
        public ParameterMapper()
        {
            Table("PARAMETR");
            Lazy(false);
            Id(x => x.Id, mapper =>
            {
                mapper.Generator(Generators.Identity);
                mapper.Column("PARAMETER_ID");
            });
            Property(x => x.Name, mapper => mapper.Column("NAME"));
            Property(x => x.Note, mapper => mapper.Column("NOTE"));
            Property(x => x.DataType, mapper => mapper.Column("DATA_TYPE"));

            ManyToOne(x => x.ReferenceType, delegate(IManyToOneMapper mapper)
            {
                mapper.Column("DESCRIPTION_TYPE_ID");
                mapper.NotFound(NotFoundMode.Ignore);
                mapper.Fetch(FetchKind.Join);
            });

            Set(x => x.Values, delegate(ISetPropertiesMapper<Parameter, ParameterValue> mapper)
            {
                mapper.Key(keyMapper => keyMapper.Column("PARAMETER_ID"));
                mapper.Inverse(true);
                mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
            }, relation => relation.OneToMany());
        }
    }
}
