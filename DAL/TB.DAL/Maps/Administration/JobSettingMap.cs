﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL.Administration;

namespace TB.DAL.Maps.Administration
{
    public class JobSettingMap : ClassMapping<JobSetting>
    {
        public JobSettingMap()
        {
            Table("JOB_SETTINGS");
            Lazy(false);
            Id(x => x.Id, mapper =>
            {
                mapper.Generator(Generators.Identity);
                mapper.Column("JOB_SETTINGS_ID");
            });

            Property(x => x.JobCode, mapper => mapper.Column("JOB_CODE"));
            Property(x => x.JobName, mapper => mapper.Column("JOB_NAME"));
            Property(x => x.AgentCode, mapper => mapper.Column("AGENT_CODE"));
            Property(x => x.AgentName, mapper => mapper.Column("AGENT_NAME"));
            Property(x => x.IsActive, mapper => mapper.Column("IS_ACTIVE"));
            Property(x => x.Period, mapper => mapper.Column("PERIOD"));
        }
    }
}