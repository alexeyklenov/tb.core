﻿using NHibernate;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL.Administration;

namespace TB.DAL.Maps.Administration
{
    public class SettingMap : ClassMapping<Setting>
    {
        public SettingMap()
        {
            Table("SETTING");
            Lazy(false);
            Id(x => x.Id, mapper =>
            {
                mapper.Generator(Generators.Identity);
                mapper.Column("SETTING_ID");
            });

            Property(x => x.Name, mapper => mapper.Column("NAME"));
            Property(x => x.Note, mapper => mapper.Column("NOTE"));
            Property(x => x.No, mapper => mapper.Column("NUM"));
            Property(x => x.Code, mapper => mapper.Column("CODE"));
            Property(x => x.Height, mapper => mapper.Column("HEIGHT"));
            Property(x => x.IsGlobal, mapper => mapper.Column("IS_GLOBAL"));
            Property(x => x.Value, mapper =>
            {
                mapper.Type(NHibernateUtil.StringClob);
                mapper.Column("BIN_VALUE");
            });
            ManyToOne(x => x.Parent, delegate(IManyToOneMapper mapper)
            {
                mapper.Column("PARENT_ID");
                mapper.NotFound(NotFoundMode.Ignore);
                mapper.Fetch(FetchKind.Join);
            });
            ManyToOne(x => x.ParameterValue, delegate(IManyToOneMapper mapper)
            {
                mapper.Column("VALUE_ID");
                mapper.NotFound(NotFoundMode.Ignore);
                mapper.NotNullable(false);
            });

            ManyToOne(x => x.ReferenceValue, delegate(IManyToOneMapper mapper)
            {
                mapper.Column("DESCRIPTION_VALUE_ID");
                mapper.NotFound(NotFoundMode.Ignore);
                mapper.NotNullable(false);
            });

            ManyToOne(x => x.ReferenceType, delegate(IManyToOneMapper mapper)
            {
                mapper.Column("DESCRIPTION_TYPE_ID");
                mapper.NotFound(NotFoundMode.Ignore);
                mapper.NotNullable(false);
            });

            ManyToOne(x=>x.Parameter,mapper => mapper.Column("PARAMETER_ID"));

            ManyToOne(x => x.Role, delegate(IManyToOneMapper mapper)
            {
                mapper.Column("ROLE_ID");
                mapper.NotFound(NotFoundMode.Ignore);
            });
            ManyToOne(x => x.Module, delegate(IManyToOneMapper mapper)
            {
                mapper.Column("MODULE_ID");
                mapper.NotFound(NotFoundMode.Ignore);
            });
            Set(x => x.Children, delegate(ISetPropertiesMapper<Setting, Setting> mapper)
            {
                mapper.Key(keyMapper => keyMapper.Column("PARENT_ID"));
                mapper.Inverse(true);
                mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
            }, relation => relation.OneToMany());
        }
    }
}
