﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL.Administration;

namespace TB.DAL.Maps.Administration
{
    public class ModuleAccessMap: ClassMapping<ModuleAccess>
    {
        public ModuleAccessMap()
        {
            Table("FUNC_ROLE_MODULE");
            Lazy(false);


            Id(x => x.Id, mapper =>
            {
                mapper.Generator(Generators.Identity);
                mapper.Column("FUNC_ROLE_MODULE_ID");
            });


            Cache(delegate(ICacheMapper mapper)
            {
                mapper.Usage(CacheUsage.NonstrictReadWrite);
                mapper.Region("References");
            });

            ManyToOne(x => x.Module, mapper => mapper.Column("MODULE_ID"));
            ManyToOne(x => x.Role, mapper => mapper.Column("FUNC_ROLE_ID"));

            Property(x => x.AccessType, mapper => mapper.Column("ACCESS_TYPE"));

            Set(x => x.ModuleActions, delegate(ISetPropertiesMapper<ModuleAccess, ModuleAction> mapper)
            {
                mapper.Table("FUNC_ROLE_MODULE_ACTION");
                mapper.Key(keyMapper => keyMapper.Column("FUNC_ROLE_MODULE_ID"));
                mapper.Cascade(Cascade.Persist);
                mapper.Cache(delegate(ICacheMapper cacheMapper)
                {
                    cacheMapper.Usage(CacheUsage.NonstrictReadWrite);
                    cacheMapper.Region("References");
                });
            }, relation => relation.ManyToMany(mapper => mapper.Column("MODULE_ACTION_ID")));
        }
    }
}
