﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL.Administration;

namespace TB.DAL.Maps.Administration
{
    public class UserMap: ClassMapping<User>
    {
        public UserMap()
        {
            Table("USERS");
            Lazy(false);

            Cache(delegate(ICacheMapper mapper)
            {
                mapper.Usage(CacheUsage.NonstrictReadWrite);
                mapper.Region("References");
            });

            Id(x => x.Id, mapper =>
            {
                mapper.Generator(Generators.Identity);
                mapper.Column("USER_ID");
            });

            Property(x => x.Login, mapper => mapper.Column("LOGIN"));
            Property(x => x.DisplayName, mapper => mapper.Column("DISPLAY_NAME"));
            Property(x => x.Phone, mapper => mapper.Column("PHONE"));
            Property(x => x.NetworkName, mapper => mapper.Column("NETWORK_NAME"));
            Property(x => x.Email, mapper => mapper.Column("EMAIL"));
            Property(x => x.Note, mapper => mapper.Column("NOTE"));
            Property(x => x.Hash, mapper => mapper.Column("HASH"));
            Property(x => x.BeginDate, mapper => mapper.Column("BEGIN_DATE"));
            Property(x => x.EndDate, mapper => mapper.Column("END_DATE"));
            Property(x => x.Guid, mapper => mapper.Column("USER_GUID"));

            Set(x => x.Roles, delegate(ISetPropertiesMapper<User, Role> mapper)
            {
                mapper.Table("USER_FUNC_ROLE");
                mapper.Inverse(true);
                mapper.Key(keyMapper => keyMapper.Column("USER_ID"));
                mapper.Cascade(Cascade.Persist);
            }, relation => relation.ManyToMany(mapper => mapper.Column("FUNC_ROLE_ID")));

            Set(x => x.Employees, delegate(ISetPropertiesMapper<User, UserEmployee> mapper)
            {
                mapper.Table("USER_EMPLOYEE");
                mapper.Inverse(true);
                mapper.Key(keyMapper => keyMapper.Column("USER_ID"));
                mapper.Cascade(Cascade.Persist);
            }, relation => relation.OneToMany());

        }
    }
}
