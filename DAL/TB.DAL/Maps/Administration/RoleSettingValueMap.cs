﻿using NHibernate;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL.Administration;

namespace TB.DAL.Maps.Administration
{
    public class RoleSettingValueMap : ClassMapping<RoleSettingValue>
    {
        public RoleSettingValueMap()
        {
            Table("ROLE_SETTING_VALUE");
            Lazy(false);

            Id(x => x.Id, mapper =>
            {
                mapper.Generator(Generators.Identity);
                mapper.Column("ROLE_SETTING_VALUE_ID");
            });

            ManyToOne(x => x.Setting, delegate(IManyToOneMapper mapper)
            {
                mapper.Column("SETTING_ID");
                mapper.NotFound(NotFoundMode.Ignore);
            });

            Property(x => x.Value, mapper =>
            {
                mapper.Type(NHibernateUtil.StringClob);
                mapper.Column("BIN_VALUE");
            });

            ManyToOne(x => x.ParameterValue, delegate(IManyToOneMapper mapper)
            {
                mapper.Column("VALUE_ID");
                mapper.NotFound(NotFoundMode.Ignore);
                mapper.NotNullable(false);
            });

            ManyToOne(x => x.ReferenceValue, delegate(IManyToOneMapper mapper)
            {
                mapper.Column("DESCRIPTION_VALUE_ID");
                mapper.NotFound(NotFoundMode.Ignore);
                mapper.NotNullable(false);
            });

            ManyToOne(x => x.Parameter, mapper => mapper.Column("PARAMETER_ID"));

            ManyToOne(x => x.Role, delegate(IManyToOneMapper mapper)
            {
                mapper.Column("ROLE_ID");
                mapper.NotFound(NotFoundMode.Ignore);
            });
        }
    }
}
