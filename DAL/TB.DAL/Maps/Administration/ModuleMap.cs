﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL.Administration;

namespace TB.DAL.Maps.Administration
{
    public class ModuleMap:ClassMapping<Module>
    {
        public ModuleMap()
        {
            Table("MODULE");
            Lazy(false);


            Id(x => x.Id, mapper =>
            {
                mapper.Generator(Generators.Identity);
                mapper.Column("MODULE_ID");
            });

            Cache(delegate(ICacheMapper mapper)
            {
                mapper.Usage(CacheUsage.NonstrictReadWrite);
                mapper.Region("References");
            });

            Property(x => x.Name, mapper => mapper.Column("NAME"));
            Property(x => x.Note, mapper => mapper.Column("NOTE"));
            Property(x => x.FileName, mapper => mapper.Column("FILE_NAME"));
            Property(x => x.BeginDate, delegate(IPropertyMapper mapper)
            {
                mapper.Column("BEGIN_DATE");
                mapper.NotNullable(true);
            });

            Property(x => x.EndDate, mapper => mapper.Column("END_DATE"));

            ManyToOne(x => x.Parent, delegate(IManyToOneMapper mapper)
            {
                mapper.Column("PARENT_ID");
                mapper.NotFound(NotFoundMode.Ignore);
                mapper.Fetch(FetchKind.Select);
            });

            Set(x => x.Childs, delegate(ISetPropertiesMapper<Module, Module> mapper)
            {
                mapper.Cascade(Cascade.All);
                mapper.Key(keyMapper => keyMapper.Column("PARENT_ID"));
            }, relation => relation.OneToMany());
            
            Set(x => x.Actions,delegate(ISetPropertiesMapper<Module, ModuleAction> mapper)
            {
                mapper.Key(keyMapper => keyMapper.Column("MODULE_ID"));
                mapper.Inverse(true);
                mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
            }, relation => relation.OneToMany());
            
            Set(x => x.Dependencies, delegate(ISetPropertiesMapper<Module, Module> mapper)
            {
                mapper.Table("MODULE_DEPENDENCY");
                mapper.Key(keyMapper => keyMapper.Column("DEPENDENT_MODULE_ID"));
                mapper.Cascade(Cascade.Persist);
                mapper.Cache(delegate(ICacheMapper cacheMapper)
                {
                    cacheMapper.Usage(CacheUsage.NonstrictReadWrite);
                    cacheMapper.Region("References");
                });
            }, relation => relation.ManyToMany(delegate(IManyToManyMapper mapper)
            {
                mapper.NotFound(NotFoundMode.Ignore);
                mapper.Column("MODULE_ID");
            }));

            Set(x => x.Access, delegate(ISetPropertiesMapper<Module, ModuleAccess> mapper)
            {
                mapper.Table("FUNC_ROLE_MODULE");
                mapper.Key(keyMapper => keyMapper.Column("MODULE_ID"));
                mapper.Cascade(Cascade.Persist);
                mapper.Cache(delegate(ICacheMapper cacheMapper)
                {
                    cacheMapper.Usage(CacheUsage.NonstrictReadWrite);
                    cacheMapper.Region("References");
                });
            }, relation => relation.OneToMany());


        }
    }
}
