﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL.Administration;

namespace TB.DAL.Maps.Administration
{
    public class ActionMap:ClassMapping<Action>
    {
        public ActionMap()
        {
            Table("ACTION");
            Lazy(false);
            Cache(delegate(ICacheMapper mapper)
            {
                mapper.Usage(CacheUsage.NonstrictReadWrite);
                mapper.Region("References");
            });

            Id(x => x.Id, mapper =>
            {
                mapper.Generator(Generators.Identity);
                mapper.Column("ACTION_ID");
            });
            Property(x => x.Name, mapper => mapper.Column("NAME"));
            Property(x => x.Note, mapper => mapper.Column("NOTE"));

            ManyToOne(x => x.ActionType, mapper => mapper.Column("ACTION_TYPE_ID"));
        }
    }
}
