﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL.Administration;

namespace TB.DAL.Maps.Administration
{
    public class SessionContextMap:ClassMapping<SessionContext>
    {
        public SessionContextMap()
        {

            Table("USER_SESSION_CONTEXT");
            Lazy(false);
            Id(x => x.Id, mapper =>
            {
                mapper.Generator(Generators.Identity);
                mapper.Column("USER_SESSION_CONTEXT_ID");
            });

            Property(x => x.Name, mapper => mapper.Column("NAME"));
            Property(x => x.Value, mapper => mapper.Column("TERMINAL"));

            ManyToOne(x => x.Session, delegate(IManyToOneMapper mapper)
            {
                mapper.Column("USER_SESSION_GUID");
                mapper.NotNullable(true);
                mapper.Fetch(FetchKind.Join);
            });
        }
    }
}
