﻿
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL.Administration;

namespace TB.DAL.Maps.Administration
{
    public class EmployeeMap: ClassMapping<Employee>
    {
        public EmployeeMap()
        {
            Table("EMPLOYEE");
            Lazy(true);

            Id(x => x.Id, mapper =>
            {
                mapper.Generator(Generators.Identity);
                mapper.Column("EMPLOYEE_ID");
            });

            Property(x => x.Guid, mapper => mapper.Column("EMPLOYEE_GUID"));
            Property(x => x.Name, mapper => mapper.Column("NAME"));
            Property(x => x.Surname, mapper => mapper.Column("SURNAME"));
            Property(x => x.Patname, mapper => mapper.Column("PATNAME"));
            Property(x => x.BeginDate, mapper => mapper.Column("BEGIN_DATE"));
            Property(x => x.EndDate, mapper => mapper.Column("END_DATE"));
        }
    }
}
