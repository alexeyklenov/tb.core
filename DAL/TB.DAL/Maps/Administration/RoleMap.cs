﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL.Administration;

namespace TB.DAL.Maps.Administration
{
    public class RoleMap : ClassMapping<Role>
    {
        public RoleMap()
        {
            Table("FUNC_ROLE");
            Lazy(false);

            Id(x => x.Id, mapper =>
            {
                mapper.Generator(Generators.Identity);
                mapper.Column("FUNC_ROLE_ID");
            });

            Property(x => x.Name, mapper => mapper.Column("NAME"));
            Property(x => x.Note, mapper => mapper.Column("NOTE"));
            Set(x => x.Modules, delegate(ISetPropertiesMapper<Role, ModuleAccess> mapper)
            {
                mapper.Key(keyMapper => keyMapper.Column("FUNC_ROLE_ID"));
                mapper.Inverse(true);
                mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
                mapper.Cache(delegate(ICacheMapper cacheMapper)
                {
                    cacheMapper.Usage(CacheUsage.NonstrictReadWrite);
                    cacheMapper.Region("References");
                });
            }, relation => relation.OneToMany());

            Set(x => x.Users, delegate(ISetPropertiesMapper<Role, User> mapper)
            {
                mapper.Table("USER_FUNC_ROLE");
                mapper.Key(keyMapper => keyMapper.Column("FUNC_ROLE_ID"));
                mapper.Cascade(Cascade.Persist);
                mapper.Cache(delegate(ICacheMapper cacheMapper)
                {
                    cacheMapper.Usage(CacheUsage.NonstrictReadWrite);
                    cacheMapper.Region("References");
                });
            }, relation => relation.ManyToMany(delegate(IManyToManyMapper mapper)
            {
                mapper.NotFound(NotFoundMode.Ignore);
                mapper.Column("USER_ID");
            }));
        }
    }
}
