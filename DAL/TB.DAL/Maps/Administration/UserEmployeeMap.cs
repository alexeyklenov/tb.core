﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL.Administration;

namespace TB.DAL.Maps.Administration
{
    public class UserEmployeeMap: ClassMapping<UserEmployee>
    {
        public UserEmployeeMap()
        {
            Table("USER_EMPLOYEE");
            Lazy(false);

            Id(x => x.Id, mapper =>
            {
                mapper.Generator(Generators.Identity);
                mapper.Column("USER_EMPLOYEE_ID");
            });

            ManyToOne(x => x.User, delegate(IManyToOneMapper mapper)
            {
                mapper.NotFound(NotFoundMode.Exception);
                mapper.Fetch(FetchKind.Join);
                mapper.Column("USER_ID");
            });

            ManyToOne(x => x.Employee, delegate(IManyToOneMapper mapper)
            {
                mapper.NotFound(NotFoundMode.Exception);
                mapper.Fetch(FetchKind.Join);
                mapper.Column("EMPLOYEE_ID");
            });

            Property(x => x.EmployeeId, mapper => mapper.Column("EMPLOYEE_ID"));
        }
    }
}
