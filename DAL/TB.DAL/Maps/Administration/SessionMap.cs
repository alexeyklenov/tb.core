﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL.Administration;

namespace TB.DAL.Maps.Administration
{
    public class SessionMap : ClassMapping<Session>
    {
        public SessionMap()
        {
            
            Table("USER_SESSION");
            Lazy(false);
            Id(x => x.Id, mapper =>
            {
                mapper.Generator(Generators.Assigned);
                mapper.Column("USER_SESSION_GUID");
            });
            /*Property(x => x.Guid, delegate(IPropertyMapper mapper)
            {
                mapper.Column("USER_SESSION_GUID");
                mapper.Access(Accessor.ReadOnly);
            });*/
            Property(x => x.StartDate, mapper => mapper.Column("START_DATE"));
            Property(x => x.Terminal, mapper => mapper.Column("TERMINAL"));
            Property(x => x.UserId, mapper => mapper.Column("USER_ID"));
            Property(x => x.UserName, mapper => mapper.Column("USER_NAME"));
            Property(x => x.Module, mapper => mapper.Column("MODULE"));
            Property(x => x.Description, mapper => mapper.Column("NOTE"));
            Set(x => x.Context,
                          delegate(ISetPropertiesMapper<Session, SessionContext> mapper)
                          {
                              mapper.Lazy(CollectionLazy.NoLazy);
                              mapper.Key(keyMapper => keyMapper.Column("USER_SESSION_GUID"));
                              mapper.Cascade(Cascade.All);
                          }, relation => relation.OneToMany());
        }
    }
}
