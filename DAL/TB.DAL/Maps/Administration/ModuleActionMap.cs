﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL.Administration;

namespace TB.DAL.Maps.Administration
{
    public class ModuleActionMap:ClassMapping<ModuleAction>
    {
        public ModuleActionMap()
        {
            Table("MODULE_ACTION");
            Lazy(false);

            Id(x => x.Id, mapper =>
            {
                mapper.Generator(Generators.Identity);
                mapper.Column("MODULE_ACTION_ID");
            });

            Cache(delegate(ICacheMapper mapper)
            {
                mapper.Usage(CacheUsage.NonstrictReadWrite);
                mapper.Region("References");
            });

            Property(x => x.MenuItemPath, mapper => mapper.Column("MENU_ITEM_PATH"));
            Property(x => x.Parameters, mapper => mapper.Column("ACTION_PARAMETERS"));

            ManyToOne(x => x.Module, mapper => mapper.Column("MODULE_ID"));
            ManyToOne(x => x.Action, mapper => mapper.Column("ACTION_ID"));
            ManyToOne(x => x.Parent, delegate(IManyToOneMapper mapper)
            {
                mapper.Column("PARENT_ID");
                mapper.NotFound(NotFoundMode.Ignore);
                mapper.Fetch(FetchKind.Select);
            });
        }
    }
}
