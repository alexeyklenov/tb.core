﻿
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL.Administration;

namespace TB.DAL.Maps.Administration
{
    public class ParameterValueMap : ClassMapping<ParameterValue>
    {
        public ParameterValueMap()
        {
            Table("PARAMETER_VALUE");
            Lazy(false);
            Id(x => x.Id, mapper =>
            {
                mapper.Generator(Generators.Identity);
                mapper.Column("PARAMETER_VALUE_ID");
            });
            Property(x => x.Name, mapper => mapper.Column("NAME"));
            Property(x => x.Note, mapper => mapper.Column("NOTE"));
            Property(x => x.Code, mapper => mapper.Column("CODE"));
            ManyToOne(x => x.Parameter, mapper => mapper.Column("PARAMETER_ID"));
        }
    }
}
