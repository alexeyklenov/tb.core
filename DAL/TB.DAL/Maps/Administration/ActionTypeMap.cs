﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL.Administration;

namespace TB.DAL.Maps.Administration
{
    public class ActionTypeMap: ClassMapping<ActionType>
    {
        public ActionTypeMap()
        {
            Table("ACTION_TYPE");
            Lazy(false);
            Mutable(false);// = ReadOnly

            Cache(delegate(ICacheMapper mapper)
            {
                mapper.Usage(CacheUsage.ReadOnly);
                mapper.Region("References");
            });
            
        }
    }
}
