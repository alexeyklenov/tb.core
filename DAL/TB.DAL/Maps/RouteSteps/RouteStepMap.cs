﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL;
using TB.BOL.Descriptions;
using TB.BOL.Interfaces;
using TB.BOL.RouteStep;

namespace TB.DAL.Maps.RouteSteps
{
    public class RouteStepMap<T> where T: DomainObject<long>, IRouteStep<T>
    {
        /// <summary>
        /// Маппинг шагов
        /// </summary>
        /// <param name="mapObject">Класс маппинга объекта предментной области</param>
        /// <param name="tableName">Наименование таблицы</param>
        /// <param name="fieldName">Наименование поля</param>
        /// <param name="cacheRegion"></param>
        public static void Map(ClassMapping<RouteStep<T>> mapObject,
            string tableName, string fieldName, CacheRegion? cacheRegion)
        {
            if (cacheRegion != null)
                mapObject.Cache(delegate(ICacheMapper mapper)
                {
                    mapper.Include(CacheInclude.All);
                    mapper.Region(CacheRegion.References.ToString());
                    mapper.Usage(CacheUsage.ReadWrite);
                });

            mapObject.Table(tableName + "_RS");
            mapObject.Lazy(false);

            mapObject.Id(x => x.Id, mapper =>
            {
                mapper.Generator(Generators.Identity);
                mapper.Column(tableName + "_RS_ID");
            }
               );
            mapObject.Property(x => x.Guid, delegate(IPropertyMapper mapper)
            {
                mapper.Column(tableName + "_RS_GUID");
                mapper.NotNullable(true);
                mapper.Unique(true);
            });

            mapObject.ManyToOne(x => x.RouteStepObject, delegate(IManyToOneMapper mapper)
            {
                mapper.Column(fieldName);
                mapper.NotNullable(true);
                mapper.Lazy(LazyRelation.NoProxy);
            });

            mapObject.Set(x => x.Descriptions, delegate(ISetPropertiesMapper<RouteStep<T>, Description<RouteStep<T>>> mapper)
            {
                mapper.Key(keyMapper => keyMapper.Column(tableName + "_RS_ID"));
                mapper.Inverse(true);
                mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
                mapper.Fetch(CollectionFetchMode.Join);
                mapper.Lazy(CollectionLazy.Extra);
            }, relation => relation.OneToMany());

            mapObject.Property(x => x.SystemOperation, delegate(IPropertyMapper mapper)
            {
                mapper.Column("SYSTEM_OPERATION");
                mapper.Type<NHibernate.Type.EnumStringType<SystemOperation>>();
                mapper.NotNullable(true);
            });

            mapObject.ManyToOne(x=>x.TechOperation, mapper =>
            {
                mapper.Column(tableName+"TECH_OPERATION_ID");
                mapper.Fetch(FetchKind.Join);
                mapper.Lazy(LazyRelation.NoLazy);
            });


        }
    }
}