﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL;

namespace TB.DAL.Maps
{
    /// <summary>
    /// Маппинг справочника
    /// </summary>
    public class ReferenceMap
    {
        /// <summary>
        /// Маппинг справочника
        /// </summary>
        /// <param name="mapObject">Класс маппинга справочника</param>
        public static void Map<T>(ClassMapping<T> mapObject) where T : Reference<long>
        {
            mapObject.Property(x => x.Code, delegate(IPropertyMapper mapper)
            {
                mapper.Column("CODE");
                mapper.NotNullable(true);
            });

            mapObject.Property(x => x.OuterCode, delegate(IPropertyMapper mapper)
            {
                mapper.Column("OUTER_CODE");
                mapper.NotNullable(false);
            });

            mapObject.Property(x => x.ShortName, mapper => mapper.Column("SHORT_NAME"));
            mapObject.Property(x => x.Name, delegate(IPropertyMapper mapper)
            {
                mapper.Column("NAME");
                mapper.NotNullable(true);
            });
            mapObject.Property(x => x.BeginDate, mapper => mapper.Column("BEGIN_DATE"));
            mapObject.Property(x => x.EndDate, mapper => mapper.Column("END_DATE"));
        }
    }
}
