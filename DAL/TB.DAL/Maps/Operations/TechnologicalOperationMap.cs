﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL;
using TB.BOL.Documents;
using TB.BOL.Materials;
using TB.BOL.RouteStep;

namespace TB.DAL.Maps.Operations
{
    public class TechnologicalOperationMap<T> where T : DomainObject<long>
    {
        public static void Map(ClassMapping<TechnologicalOperation<T>> mapObject,
            string tableName, CacheRegion? cacheRegion)
        {
            if (cacheRegion != null)
                mapObject.Cache(delegate(ICacheMapper mapper)
                {
                    mapper.Include(CacheInclude.All);
                    mapper.Region(CacheRegion.References.ToString());
                    mapper.Usage(CacheUsage.ReadWrite);
                });

            mapObject.Table(tableName + "_TECH_OPERATIONS");
            mapObject.Lazy(false);

            mapObject.Id(x => x.Id, mapper =>
            {
                mapper.Generator(Generators.Identity);
                mapper.Column(tableName + "_TO_ID");
            });

            mapObject.Property(x => x.Guid, delegate(IPropertyMapper mapper)
            {
                mapper.Column(tableName + "_TO_GUID");
                mapper.NotNullable(true);
                mapper.Unique(true);
            });

            ReferenceMap.Map(mapObject);
        }
    }

    public class MaterialTechnologicalOperationMap : ClassMapping<TechnologicalOperation<Material>>
    {
        public MaterialTechnologicalOperationMap()
        {
            TechnologicalOperationMap<Material>.Map(this, "MATERIAL", null);
        }
    }

    public class DocumentTechnologicalOperationMap : ClassMapping<TechnologicalOperation<Document>>
    {
        public DocumentTechnologicalOperationMap()
        {
            TechnologicalOperationMap<Document>.Map(this, "DOCUMENT", null);
        }
    }


}