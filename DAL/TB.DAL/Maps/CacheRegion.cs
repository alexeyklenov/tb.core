﻿namespace TB.DAL.Maps
{
    /// <summary>
    ///     Регионы кэширования
    /// </summary>
    public enum CacheRegion
    {
        /// <summary>
        ///     Основные объекты
        /// </summary>
        BasicObjects,

        /// <summary>
        ///     Вспомогательные объекты
        /// </summary>
        AuxObjects,

        /// <summary>
        ///     Справочники
        /// </summary>
        References
    }
}
