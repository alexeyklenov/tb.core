﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL.Descriptions;

namespace TB.DAL.Maps.Descriptions
{
    public class DescriptionTypeRelationMap: ClassMapping<DescriptionTypeRelation>
    {
        public DescriptionTypeRelationMap()
        {
            Cache(delegate(ICacheMapper mapper)
            {
                mapper.Include(CacheInclude.All);
                mapper.Region(CacheRegion.References.ToString());
                mapper.Usage(CacheUsage.ReadWrite);
            });

            Table("DESCRIPTION_TYPE_RELATION");
            Lazy(true);

            Id(x => x.Id, mapper =>
            {
                mapper.Generator(Generators.Identity);
                mapper.Column("DESCRIPTION_TYPE_RELATION_ID");
            });
            Property(x => x.Guid, delegate(IPropertyMapper mapper)
            {
                mapper.Column("DESCRIPTION_TYPE_RELATION_GUID");
                mapper.NotNullable(true);
                mapper.Unique(true);
            });

            Property(x => x.RelationType, delegate(IPropertyMapper mapper)
            {
                mapper.NotNullable(true);
                mapper.Column("TYPE");
            });

            ManyToOne(x => x.ParentDescriptionType, delegate(IManyToOneMapper mapper)
            {
                mapper.Column("PARENT_DESCRIPTION_TYPE_ID");
                mapper.Lazy(LazyRelation.NoProxy);
            });

            ManyToOne(x => x.ChildDescriptionType, delegate(IManyToOneMapper mapper)
            {
                mapper.Column("CHILD_DESCRIPTION_TYPE_ID");
                mapper.Lazy(LazyRelation.NoProxy);
            });
        }
    }
}
