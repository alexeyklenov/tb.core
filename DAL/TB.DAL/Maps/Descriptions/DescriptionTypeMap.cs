﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL.Descriptions;

namespace TB.DAL.Maps.Descriptions
{
    public class DescriptionTypeMap: ClassMapping<DescriptionType>
    {
        public DescriptionTypeMap()
        {
            Cache(delegate(ICacheMapper mapper)
            {
                mapper.Include(CacheInclude.All);
                mapper.Region(CacheRegion.References.ToString());
                mapper.Usage(CacheUsage.ReadWrite);
            });

            Table("DESCRIPTION_TYPE");
            Lazy(true);

            Id(x => x.Id, mapper =>
            {
                mapper.Generator(Generators.Identity);
                mapper.Column("DESCRIPTION_TYPE_ID");
            });

            ReferenceMap.Map(this);

            Property(x => x.Guid, delegate(IPropertyMapper mapper)
            {
                mapper.Column("DESCRIPTION_TYPE_GUID");
                mapper.NotNullable(true);
                mapper.Unique(true);
            });

            Property(x => x.MinValue, mapper => mapper.Column("MIN_VALUE"));
            Property(x => x.MaxValue, mapper => mapper.Column("MAX_VALUE"));

            ManyToOne(x => x.Parent, mapper => mapper.Lazy(LazyRelation.NoProxy));

            Set(x => x.DescriptionTypes, delegate(ISetPropertiesMapper<DescriptionType, DescriptionType> mapper)
            {
                mapper.Inverse(true);
                mapper.Cascade(Cascade.Remove);
                mapper.Lazy(CollectionLazy.Lazy);
                mapper.Key(keyMapper => keyMapper.Column("PARENT_ID"));
            }, relation => relation.OneToMany());

            Property(x => x.DataType, delegate(IPropertyMapper mapper)
            {
                mapper.Column("DATA_TYPE");
                mapper.Type<NHibernate.Type.EnumStringType<DescriptionDataType>>();
                mapper.NotNullable(true);
            });

            Set(x => x.DescriptionValues, delegate(ISetPropertiesMapper<DescriptionType, DescriptionValue> mapper)
            {
                mapper.Inverse(true);
                mapper.Cascade(Cascade.DeleteOrphans);
                mapper.Lazy(CollectionLazy.Lazy);
                mapper.Key(keyMapper => keyMapper.Column("DESCRIPTION_TYPE_ID"));
            }, relation => relation.OneToMany());

            Set(x => x.Descriptions, delegate(ISetPropertiesMapper<DescriptionType, Description<DescriptionType>> mapper)
            {
                mapper.Inverse(true);
                mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
                mapper.Lazy(CollectionLazy.Lazy);
                mapper.Key(keyMapper => keyMapper.Column("PARENT_DESCRIPTION_TYPE_ID"));
            },relation => relation.OneToMany());

            Set(x => x.Childs, delegate(ISetPropertiesMapper<DescriptionType, DescriptionTypeRelation> mapper)
            {
                mapper.Inverse(true);
                mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
                mapper.Lazy(CollectionLazy.Lazy);
                mapper.Key(keyMapper => keyMapper.Column("PARENT_DESCRIPTION_TYPE_ID"));
            },relation => relation.OneToMany());

            Set(x => x.Parents, delegate(ISetPropertiesMapper<DescriptionType, DescriptionTypeRelation> mapper)
            {
                mapper.Inverse(true);
                mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
                mapper.Lazy(CollectionLazy.Lazy);
                mapper.Key(keyMapper => keyMapper.Column("CHILD_DESCRIPTION_TYPE_ID"));
            },relation => relation.OneToMany());

        }
    }
}
