﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL.Descriptions;

namespace TB.DAL.Maps.Descriptions
{
    public class DescriptionValueMap: ClassMapping<DescriptionValue>
    {
        public DescriptionValueMap()
        {
            Cache(delegate(ICacheMapper mapper)
            {
                mapper.Include(CacheInclude.All);
                mapper.Region(CacheRegion.References.ToString());
                mapper.Usage(CacheUsage.ReadWrite);
            });

            Table("DESCRIPTION_VALUE");
            Lazy(true);

            ReferenceMap.Map(this);

            Id(x => x.Id, mapper =>
            {
                mapper.Generator(Generators.Identity);
                mapper.Column("DESCRIPTION_VALUE_ID");
            });

            Property(x => x.Guid, delegate(IPropertyMapper mapper)
            {
                mapper.Column("DESCRIPTION_VALUE_GUID");
                mapper.NotNullable(true);
                mapper.Unique(true);
            });
            
            ManyToOne(x => x.DescriptionType, delegate(IManyToOneMapper mapper) { mapper.Column("DESCRIPTION_TYPE_ID"); mapper.NotNullable(true);});

            Set(x => x.Descriptions,
                delegate(ISetPropertiesMapper<DescriptionValue, Description<DescriptionValue>> mapper)
                {
                    mapper.Key(keyMapper => keyMapper.Column("PARENT_DESCRIPTION_VALUE_ID"));
                    mapper.Inverse(true);
                    mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
                    mapper.Lazy(CollectionLazy.Lazy);
                },relation => relation.OneToMany());
        }
    }
}
