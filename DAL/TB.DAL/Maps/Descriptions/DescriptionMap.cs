﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL;
using TB.BOL.Descriptions;
using TB.BOL.Documents;
using TB.BOL.Interfaces;
using TB.BOL.Materials;

namespace TB.DAL.Maps.Descriptions
{
    /// <summary>
    /// Маппинг описания
    /// </summary>
    /// <typeparam name="T">Описание объекта предметной области</typeparam>
    public class DescriptionMap<T> where T : DomainObject<long>, IDescribable<T> 
    {
        /// <summary>
        /// Маппинг описания
        /// </summary>
        /// <param name="mapObject">Класс маппинга объекта предментной области</param>
        /// <param name="tableName">Наименование таблицы</param>
        /// <param name="fieldName">Наименование поля</param>
        /// <param name="cacheRegion"></param>
        public static void Map(ClassMapping<Description<T>> mapObject,
            string tableName, string fieldName, CacheRegion? cacheRegion)
        {
            if (cacheRegion != null)
                mapObject.Cache(delegate(ICacheMapper mapper)
                {
                    mapper.Include(CacheInclude.All);
                    mapper.Region(CacheRegion.References.ToString());
                    mapper.Usage(CacheUsage.ReadWrite);
                });

            mapObject.Table(tableName + "_DESC");
            mapObject.Lazy(false);

            mapObject.Id(x => x.Id, mapper =>
            {
                mapper.Generator(Generators.Identity);
                mapper.Column(tableName + "_DESC_ID");
            }
                );
            mapObject.Property(x => x.Guid, delegate(IPropertyMapper mapper)
            {
                mapper.Column(tableName + "_DESC_GUID");
                mapper.NotNullable(true);
                mapper.Unique(true);
            });

            mapObject.ManyToOne(x => x.DescribedObject, delegate(IManyToOneMapper mapper)
            {
                mapper.Column(fieldName);
                mapper.NotNullable(true);
                mapper.Lazy(LazyRelation.NoProxy);
            });
            mapObject.ManyToOne(x => x.DescriptionType, delegate(IManyToOneMapper mapper)
            {
                mapper.Column("DESCRIPTION_TYPE_ID");
                mapper.NotNullable(true);
                mapper.Lazy(LazyRelation.NoLazy);
                mapper.Fetch(FetchKind.Join);
            });
            mapObject.ManyToOne(x => x.DescriptionValue, delegate(IManyToOneMapper mapper)
            {
                mapper.Column("DESCRIPTION_VALUE_ID");
                mapper.Lazy(LazyRelation.NoLazy);
                mapper.Fetch(FetchKind.Join);
            });

            mapObject.Property(x => x.IsEqual, mapper => mapper.Column("F_EQUAL"));
            mapObject.Property(x => x.IsDefault, mapper => mapper.Column("F_DEFAULT"));
            mapObject.Property(x => x.IsEditable, mapper => mapper.Column("F_EDITABLE"));
            mapObject.Property(x => x.No, mapper => mapper.Column("NUM"));

            mapObject.Property(x => x.MinValue, mapper => mapper.Column("MIN_VALUE"));
            mapObject.Property(x => x.MaxValue, mapper => mapper.Column("MAX_VALUE"));
            mapObject.Property(x => x.BeginDate, mapper => mapper.Column("BEGIN_DATE"));
            mapObject.Property(x => x.EndDate, mapper => mapper.Column("END_DATE"));
            mapObject.Property(x => x.StringValue, mapper => mapper.Column("STRING_VALUE"));
        }

        public static void Map(ClassMapping<Description<T>> mapObject, string tableName, string fieldName)
        {
            Map(mapObject, tableName, fieldName, null);
        }
        public static void Map(ClassMapping<Description<T>> mapObject, string tableName, CacheRegion? cacheRegion)
        {
            Map(mapObject, tableName, tableName + "_ID", cacheRegion);
        }
        public static void Map(ClassMapping<Description<T>> mapObject, string tableName)
        {
            Map(mapObject, tableName, tableName + "_ID");
        }
    }

    /// <summary>
    /// Мэппинг описания справочного значения
    /// </summary>
    public class DescriptionValueDescriptionMap : ClassMapping<Description<DescriptionValue>>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public DescriptionValueDescriptionMap()
        {
            DescriptionMap<DescriptionValue>.Map(this, "DESCRIPTION_VALUE", "PARENT_DESCRIPTION_VALUE_ID");
        }
    }

    /// <summary>
    /// Мэппинг описания материала
    /// </summary>
    public class MaterialDescriptionMap : ClassMapping<Description<Material>>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public MaterialDescriptionMap()
        {
            DescriptionMap<Material>.Map(this, "MATERIAL");
        }
    }

    /// <summary>
    /// Мэппинг описания типа материала
    /// </summary>
    public class MaterialTypeDescriptionMap : ClassMapping<Description<MaterialType>>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public MaterialTypeDescriptionMap()
        {
            DescriptionMap<MaterialType>.Map(this, "MATERIAL_TYPE");
        }
    }

    /// <summary>
    /// Мэппинг описания документа
    /// </summary>
    public class DocumentDescriptionMap : ClassMapping<Description<Document>>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public DocumentDescriptionMap()
        {
            DescriptionMap<Document>.Map(this, "DOCUMENT");
        }
    }

    /// <summary>
    /// Мэппинг настройки описаний
    /// </summary>
    public class DescriptionTypeDescriptionMap : ClassMapping<Description<DescriptionType>>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public DescriptionTypeDescriptionMap()
        {
            DescriptionMap<DescriptionType>.Map(this, "DESCRIPTION_TYPE", "PARENT_DESCRIPTION_TYPE_ID");
        }
    }

    /// <summary>
    /// Мэппинг описания строки документа
    /// </summary>
    public class DocumentLineDescriptionMap : ClassMapping<Description<DocumentLine>>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public DocumentLineDescriptionMap()
        {
            DescriptionMap<DocumentLine>.Map(this, "DOCUMENT_LINE");
        }
    }
}
