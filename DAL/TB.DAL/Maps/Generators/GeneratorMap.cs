﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using TB.BOL.Generators;

namespace TB.DAL.Maps.PropertyGenerators
{
    public  class GeneratorMap : ClassMapping<Generator>
    {
        public GeneratorMap()
        {
            Table("GENERATOR");
            Lazy(false);
            Id(x => x.Id, delegate(IIdMapper mapper)
            {
                mapper.Column("GENERATOR_ID");
                mapper.Generator(Generators.Identity);
            });

            Property(x => x.Guid, delegate(IPropertyMapper mapper)
            {
                mapper.Column("GENERATOR_GUID");
                mapper.NotNullable(true);
                mapper.Update(false);
                mapper.Unique(true);
            });

            Property(x => x.GeneratorType, delegate(IPropertyMapper mapper)
            {
                mapper.Column("TYPE");
                mapper.Type<NHibernate.Type.EnumStringType<GeneratorType>>();
            });

            Property(x => x.Code, delegate(IPropertyMapper mapper)
            {
                mapper.Column("CODE");
                mapper.NotNullable(true);
            });

            Property(x => x.Name, delegate(IPropertyMapper mapper)
            {
                mapper.Column("NAME");
                mapper.NotNullable(true);
            });
            Property(x => x.ValueTypeName, delegate(IPropertyMapper mapper)
            {
                mapper.Column("VALUE_TYPE");
                mapper.NotNullable(true);
            });
            Discriminator(mapper => mapper.Formula("case" +
                         "  when type = '" + GeneratorType.Sequential + "' and value_type = '" + typeof(long).FullName + "' then 'SequentialNumeric'" +
                         "  when type = '" + GeneratorType.Sequential + "' and value_type = '" + typeof(string).FullName + "' then 'SequentialString'" +
                         "  else null " +
                         "end"));
        }
    }

    public class SequentialGeneratorMap : SubclassMapping<SequentialGenerator>
    {
        public SequentialGeneratorMap()
        {
            Property(x => x.No, delegate(IPropertyMapper mapper)
            {
                mapper.Column("NUM");
                mapper.NotNullable(true);
            });

            Property(x => x.MinNo, delegate(IPropertyMapper mapper)
            {
                mapper.Column("MIN_NO");
                mapper.NotNullable(true);
            });
            Property(x => x.MaxNo, delegate(IPropertyMapper mapper)
            {
                mapper.Column("MAX_NO");
                mapper.NotNullable(false);
            });
            Property(x => x.Step, delegate(IPropertyMapper mapper)
            {
                mapper.Column("STEP");
                mapper.NotNullable(true);
            });
        }
    }

    public class SequentialNumericGeneratorMap : SubclassMapping<SequentialNumericGenerator>
    {
        public SequentialNumericGeneratorMap()
        {
            Lazy(false);
           
            DiscriminatorValue("SequentialNumeric");
        }
    }

    public class SequentialStringGeneratorMap : SubclassMapping<SequentialStringGenerator>
    {

        public SequentialStringGeneratorMap()
        {
            Lazy(false);

            Property(x => x.Prefix, mapper => mapper.Column("PREFIX"));
            Property(x => x.Suffix, mapper => mapper.Column("SUFFIX"));

            DiscriminatorValue("SequentialString");
        }
    }
}
