﻿using System;
using System.Diagnostics;
using System.Reflection;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Mapping.ByCode;
using NHibernate.SqlCommand;
using NHibernate.Tool.hbm2ddl;

namespace TB.DAL
{
    public class SessionHelper
    {
        public static Configuration Config { get; set; }

        private static ISessionFactory _sessionFactory;

        private static ISessionFactory SessionFactory
        {
            get
            {
                if (_sessionFactory == null)
                {
                    var mapper = new ModelMapper();

                    var cfg = Config ?? new Configuration().Configure();

                    cfg.SetInterceptor(new LoggingInterceptor());
                   

                    mapper.AddMappings(Assembly.GetExecutingAssembly().GetExportedTypes());

                    cfg.AddMapping(mapper.CompileMappingForAllExplicitlyAddedEntities());

                    try
                    {
                        new SchemaValidator(cfg).Validate();
                    }
                    catch (Exception)
                    {
                        try
                        {
                            new SchemaUpdate(cfg).Execute(false, true);
                        }
                        catch (Exception)
                        {
                            new SchemaExport(cfg).Execute(true, true, false);
                        }
                    }

                    _sessionFactory = cfg.BuildSessionFactory();
                }
                return _sessionFactory;
            }
        }

        public static ISession OpenSession()
        {
            return SessionFactory.OpenSession();
        }
    }

    public class LoggingInterceptor : EmptyInterceptor
    {
        public override SqlString OnPrepareStatement(SqlString sql)
        {
            Debug.WriteLine(sql);
            return base.OnPrepareStatement(sql);
        }
    }
}
