﻿using NHibernate;
using NHibernate.SqlCommand;

namespace TB.DAL
{
    public class DebugSqlInterceptor : EmptyInterceptor, IInterceptor
    {

            SqlString IInterceptor.OnPrepareStatement(SqlString sql)
            {
                return sql;
            }
        
    }
}
