﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using log4net;
using NHibernate;
using NHibernate.AdoNet.Util;
using NHibernate.Criterion;
using NHibernate.Engine;
using NHibernate.Impl;
using NHibernate.Linq;
using NHibernate.Proxy;
using TB.DAL.Interface;
using Utils.ComponentModel;

namespace TB.DAL.DAO
{
    public class Dao<T> : IDao<T>
    {
        private Type _persitentType = typeof(T);
        protected readonly NHibernate.Cfg.Configuration Config;

        ILog _log = LogManager.GetLogger("Dao");

        public Dao()
            : this(DaoContainer.Config)
        { 
        }
        
        public Dao(NHibernate.Cfg.Configuration config, string mappingAssemblyName)
        {
            Config = config;

            var mappingAssemblies = new List<Assembly>();
            try
            {
                if (!string.IsNullOrEmpty(mappingAssemblyName))
                    mappingAssemblies.Add(Assembly.Load(mappingAssemblyName));
            }
            catch (FileNotFoundException)
            {
                if (mappingAssemblyName != null) mappingAssemblies.Add(Assembly.LoadFrom(mappingAssemblyName));
            }
            catch (Exception ex)
            {
                _log.Warn(string.Format("Loading {0} error", mappingAssemblyName), ex);
            }

            // загрузка сборки с маппингами по-умолчанию
            mappingAssemblies.Add(Assembly.GetExecutingAssembly());
            
          

            SessionManager.Instance.SetSessionFactory(config, mappingAssemblies);
        }

        public Dao(NHibernate.Cfg.Configuration config, IEnumerable<string> mappingAssemblies)
        {
            Config = config;

            var mappingAssembliesList = mappingAssemblies
                .Where(x => !string.IsNullOrEmpty(x))
                .Select(Assembly.Load)
                .ToList();

            // загрузка сборки с маппингами по-умолчанию
            mappingAssembliesList.Add(Assembly.GetExecutingAssembly());

            SessionManager.Instance.SetSessionFactory(config, mappingAssembliesList);
        }

        public Dao(NHibernate.Cfg.Configuration config)
        {
            Config = config;
           
            // загрузка сборки с маппингами по-умолчанию
           var mappingAssembliesList = new List<Assembly> {Assembly.GetExecutingAssembly()};

           SessionManager.Instance.SetSessionFactory(config, mappingAssembliesList);
        }

        /// <summary>
        /// Exposes the ISession used within the DAO.
        /// </summary>
        public ISession NHibernateSession
        {
            get { return SessionManager.Instance.GetSessionFrom(Config); }
        }

        /// <summary>
        /// Получение объекта по его поркси
        /// </summary>
        //TODO при переходе на новый nhibernate проблема с преобразованием proxy к исходному объекту отпала: проверить и исключить данный метод из GetById:
        //TODO проблема по http://stackoverflow.com/questions/7977952/nhibernate-objectproxy-casting-with-lazy-loading
        public T UnProxy(T proxy)
        {
            var hibernateProxy = proxy as INHibernateProxy;
            
            if (hibernateProxy == null) return proxy;

            if (hibernateProxy.HibernateLazyInitializer.IsUninitialized)
                hibernateProxy.HibernateLazyInitializer.Initialize();

            var entity = NHibernateSession.GetSessionImplementation().PersistenceContext.Unproxy(hibernateProxy);
            return entity is T ? (T)entity : default(T);
        }

        /// <summary>
        /// Загружает объекты из БД, используя критерий <see cref="ICriterion" />.
        /// Если  <see cref="ICriterion" /> не определен, возвращает все объекты <see cref="GetAll" />.
        /// </summary>
        public List<T> GetByCriteria(params ICriterion[] criterion)
        {
            return GetByCriteria(false, criterion);
        }

        /// <summary>
        /// Загружает объекты из БД, используя критерий <see cref="ICriterion" />.
        /// Если  <see cref="ICriterion" /> не определен, возвращает все объекты <see cref="GetAll" />.
        /// </summary>
        public List<T> GetByCriteria(bool cachable, params ICriterion[] criterion)
        {
            var criteria = NHibernateSession.CreateCriteria(_persitentType);

            foreach (var criterium in criterion)
            {
                criteria.Add(criterium);
            }
            criteria.SetFetchMode(_persitentType.Name, FetchMode.Select);
            criteria.SetCacheable(cachable);
            var l = criteria.List<T>() as List<T>;

            return l;
        }

        public List<string> GetDirtyPropertiesForEntity(object entity)
        {
            return NHibernateSession.GetDirtyProperties(entity).ToList();
        }

        #region IDao Members

        /// <summary>
        /// Загружает объекты из БД, используя запрос HQL
        /// </summary>
        /// <param name="queryString">Строка запроса</param>
        /// <param name="cacheRegion">Регион кэширования</param>
        /// <param name="paramValues">Список параметров</param>
        /// <returns>Результат запроса</returns>
        public IList<TU> GetByQuery<TU>(string queryString, string cacheRegion, params KeyValuePair<string, object>[] paramValues)
        {
            var query = NHibernateSession.CreateQuery(queryString);

            query.SetCacheable(!string.IsNullOrEmpty(cacheRegion));
            if (!string.IsNullOrEmpty(cacheRegion))
                query.SetCacheRegion(cacheRegion);

            if (paramValues != null)
                foreach (var paramValue in paramValues)
                    if (paramValue.Value is ICollection)
                        query.SetParameterList(paramValue.Key, (ICollection)paramValue.Value);
                    else
                        query.SetParameter(paramValue.Key, paramValue.Value);

            return query.List<TU>();
        }
        public IList<TU> GetByQuery<TU>(string queryString, params KeyValuePair<string, object>[] paramValues)
        {
            return GetByQuery<TU>(queryString, null, paramValues);
        }

        public bool Check()
        {
            try
            {
                var persister = ((SessionFactoryImpl)NHibernateSession.SessionFactory).GetEntityPersister(_persitentType.FullName);
                if (persister != null)
                {
                    return true;
                }
                
                _log.Info("Не найдено " + _persitentType.FullName);

                return false;
            }
            catch (Exception ex)
            {
                _log.Info(_persitentType.FullName, ex);
                return false;
            }
        }

        /// <summary>
        /// Загрузка объекта по типу и идентификатору
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>
        /// <param name="shouldLock"></param>
        /// <returns></returns>
        public object GetById(Type type, object id, bool shouldLock)
        {
            var entity = shouldLock ? NHibernateSession.Get(type, id, LockMode.Upgrade) : NHibernateSession.Get(type, id);

            if (entity == null)
            {
                throw new ObjectNotFoundException(id, type);
            }
            return UnProxy(entity);
        }

        object IDao.GetById(object id, bool shouldLock)
        {
            return GetById(id, shouldLock);
        }

        IList<object> IDao.GetAll()
        {
            var all = GetAll().ConvertAll(c => (object)c);
            return all;
        }

        IList<object> IDao.GetPage(int page, int pageSize)
        {
            return GetPage(page, pageSize).ConvertAll(c => (object)c);
        }

        public List<T> GetByProperties(Dictionary<string, object> propertiesAndValues)
        {
            var criteria = NHibernateSession.CreateCriteria(_persitentType);
            var exampleInstance = Activator.CreateInstance(_persitentType);
            var props = new List<PropertyInfo>(_persitentType.GetProperties());
            foreach (var propertiesAndValue in propertiesAndValues)
            {
                var prop = _persitentType.GetProperty(propertiesAndValue.Key);
                if (prop != null)
                {
                    prop.SetValue(exampleInstance, propertiesAndValue.Value, null);
                    props.Remove(prop);
                }
            }
            var example = Example.Create(exampleInstance);

            foreach (var propertyToExclude in props)
            {
                example.ExcludeProperty(propertyToExclude.Name);
            }
            example.IgnoreCase()             //perform case insensitive string comparisons
                   .EnableLike();             //use like for string comparisons*/

            criteria.Add(example);

            return criteria.List<T>() as List<T>;

        }

        IList<object> IDao.GetByProperties(Dictionary<string, object> propertiesAndValues)
        {
            return GetByProperties(propertiesAndValues).ConvertAll(c => (object)c);
        }

        IList<object> IDao.GetByFilter(List<SortDescription> sortInfo, string filter)
        {
            var hql = "from " + _persitentType;
            if (!string.IsNullOrEmpty(filter))
                hql += " where " + filter;

            var orderby = new List<string>();
            if (sortInfo.Count > 0)
            {
                orderby.AddRange(
                    sortInfo.Select(
                        sort =>
                            sort.SortOrder == ListSortDirection.Ascending
                                ? string.Format("{0} asc", sort.Descriptor)
                                : string.Format("{0} desc", sort.Descriptor)));
            }
            if (orderby.Count > 0)
                hql = hql + " order by " + string.Join(",", orderby.ToArray());
            var qry = NHibernateSession.CreateQuery(hql);


            return qry.List<object>();
        }

        /// <summary>
        /// Сохраняет/изменяет объект
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public object SaveOrUpdate(object entity)
        {
            NHibernateSession.SaveOrUpdate(entity);
            return entity;
        }

        /// <summary>
        /// Удаляет объект
        /// </summary>
        /// <param name="entity"></param>
        public void Delete(object entity)
        {
            NHibernateSession.Delete(entity);
        }

        /// <summary>
        /// Получение объекта по его поркси
        /// </summary>
        public object UnProxy(object proxy)
        {
            if (!(proxy is INHibernateProxy))
                return proxy;

            if (((INHibernateProxy)proxy).HibernateLazyInitializer.IsUninitialized)
                ((INHibernateProxy)proxy).HibernateLazyInitializer.Initialize();

            return NHibernateSession.GetSessionImplementation().PersistenceContext.Unproxy(proxy);
        }

        /// <summary>
        /// Обновление состояния объекта
        /// </summary>
        public object Refresh(object entity)
        {
            NHibernateSession.Refresh(entity);
            return entity;
        }

        public object Merge(object entity)
        {
            return NHibernateSession.Merge(entity);
        }

        /// <summary>
        /// Сохраняет изменения вне зависимости - открыта транзакция или нет
        /// </summary>
        public void CommitChanges()
        {
            if ( !SessionManager.Instance.CommitTransactionOn(Config))
            {
                // If there's no transaction, just flush the changes
                SessionManager.Instance.GetSessionFrom(Config).Flush();
            }
        }

        public int GetCount()
        {
            var count = int.Parse(NHibernateSession.CreateQuery(string.Format("select count(*) from {0}", _persitentType.FullName)).UniqueResult().ToString());
            return count;
        }

        [Obsolete("Использовать ExecuteNonQuery с параметром - строкой")]
        public void ExecuteNonQuery(IDbCommand command)
        {
            command.Connection = NHibernateSession.Connection;
            command.CommandTimeout = 600;
            command.ExecuteNonQuery();
            ((ISessionImplementor)NHibernateSession).Factory.Settings.SqlStatementLogger.LogCommand(command, FormatStyle.Basic);
        }

        public void ExecuteNonQuery(string commandText, Dictionary<string, object> parameters)
        {
            var command = NHibernateSession.Connection.CreateCommand();
            command.CommandTimeout = 600;
            command.CommandText = commandText.Replace('\r', ' ');
            foreach (var p in parameters)
            {
                var pr = command.CreateParameter();
                pr.ParameterName = p.Key;
                //pr.DbType = DbType.String; 
                pr.Value = p.Value;
                command.Parameters.Add(pr);
            }
            ((ISessionImplementor)NHibernateSession).Factory.Settings.SqlStatementLogger.LogCommand(command, FormatStyle.Basic);
            //_log.Debug(GetCommandLineWithParameters(__command));
            command.ExecuteNonQuery();
            // NHibernateSessionManager.Instance.GetSessionFactoryFor(SessionFactoryConfigPath).


        }

        [Obsolete("Использовать ExecuteScalarQuery с параметром - строкой")]
        public object ExecuteScalarQuery(IDbCommand command)
        {
            command.Connection = NHibernateSession.Connection;
            command.CommandTimeout = 600;
            var res = command.ExecuteScalar();
            ((ISessionImplementor)NHibernateSession).Factory.Settings.SqlStatementLogger.LogCommand(command, FormatStyle.Basic);
            return res;
        }

        public object ExecuteScalarQuery(string commandText, Dictionary<string, object> parameters)
        {
            var command = NHibernateSession.Connection.CreateCommand();
            command.CommandTimeout = 600;
            command.CommandText = commandText.Replace('\r', ' ');
            foreach (var p in parameters)
            {
                var pr = command.CreateParameter();
                pr.ParameterName = p.Key;
                //pr.DbType = DbType.String; 
                pr.Value = p.Value;
                command.Parameters.Add(pr);
            }
            ((ISessionImplementor)NHibernateSession).Factory.Settings.SqlStatementLogger.LogCommand(command, FormatStyle.Basic);
            var res = command.ExecuteScalar();
            return res;

        }

        public object GetScalarNamedQueryResult(string queryName, object parameters)
        {
            var result = NHibernateSession.GetNamedQuery(queryName)
                .SetProperties(parameters).UniqueResult();
            return result;
        }

        public void EnableFilter(string filterName, Dictionary<string, object> parameters)
        {
            var filter = NHibernateSession.EnableFilter(filterName);
            foreach (var p in parameters)
                filter.SetParameter(p.Key, p.Value);
        }

        public void DisableFilter(string filterName)
        {
            NHibernateSession.DisableFilter(filterName);
        }

        #endregion

        #region IDao<T> Members

        /// <summary>
        /// Загружает оъект типа T из базы данных по ИД.
        /// </summary>
        public T GetById(object id, bool shouldLock)
        {
            var entity = shouldLock ? NHibernateSession.Get(_persitentType, id, LockMode.Upgrade) : NHibernateSession.Get(_persitentType, id);
            if (entity == null)
                throw new ObjectNotFoundException(id, typeof(T));
            return UnProxy((T)entity);
        }

        /// <summary>
        /// Загружает все объекты 
        /// </summary>
        public List<T> GetAll()
        {
            return GetByCriteria();
        }

        public List<T> GetPage(int page, int pageSize)
        {
            var r = GetSome(page * pageSize, pageSize + 1);
            //_log.Debug("GetPage. Колво=" + __r.Count);
            return r;
        }

        public List<T> GetSome(int startRecord, int maxRecords)
        {
            var criteria = NHibernateSession.CreateCriteria(_persitentType);

            criteria.SetFirstResult(startRecord)
                    .SetMaxResults(maxRecords);

            return criteria.List<T>() as List<T>;
        }

        /// <summary>
        /// Возвращает набор, используя заполненный образец исходного типа
        /// </summary>
        /// <param name="exampleInstance">пример</param>
        /// <param name="propertiesToExclude">свойства, которые не учитываются при поиске</param>
        /// <returns></returns>
        public List<T> GetByExample(T exampleInstance, params string[] propertiesToExclude)
        {
            var criteria = NHibernateSession.CreateCriteria(_persitentType);
            var example = Example.Create(exampleInstance);

            foreach (var propertyToExclude in propertiesToExclude)
            {
                example.ExcludeProperty(propertyToExclude);
            }
            example.IgnoreCase()             //perform case insensitive string comparisons
                   .EnableLike();             //use like for string comparisons*/

            criteria.Add(example);

            return criteria.List<T>() as List<T>;
        }

        /// <summary>
        /// Поиск уникальной записи по образцу. Если записей больше одной вырабатывает исключение
        /// </summary>
        /// <exception cref="NonUniqueResultException" />
        public T GetUniqueByExample(T exampleInstance, params string[] propertiesToExclude)
        {
            var foundList = GetByExample(exampleInstance, propertiesToExclude);

            if (foundList.Count > 1)
            {
                throw new NonUniqueResultException(foundList.Count);
            }

            return foundList.Count > 0 ? foundList[0] : default(T);
        }

        public IQueryable<T> Linq()
        {
            return Linq(false);
        }
        public IQueryable<T> Linq(bool cachable)
        {
            var q = NHibernateSession.Query<T>();
            if (cachable)
                q.Cacheable();

            return q;
        }
        public IQueryable<T> Linq(string cacheRegion)
        {
            var q = NHibernateSession.Query<T>();

            if (string.IsNullOrEmpty(cacheRegion))
                return q;

            q.Cacheable();
            q.CacheRegion(cacheRegion);

            return q;
        }

        public List<T> GetNamedQueryResult(string queryName, object parameters)
        {
            var result = NHibernateSession.GetNamedQuery(queryName)
                                 .SetProperties(parameters).List<T>() as List<T>;
            return result;
        }

        /// <summary>
        /// Для  объектов с присвоенным идентификатором, необходим явный вызов Save для добавления.
        /// See http://www.hibernate.org/hib_docs/reference/en/html/mapping.html#mapping-declaration-id-assigned.
        /// </summary>
        public T Save(T entity)
        {
            NHibernateSession.Save(entity);
            return entity;
        }

        /// <summary>
        /// Для объектов, автоматически генерирующих ID, можно вызвать SaveOrUpdate для добавления записи в БД.
        /// Для изменения сущности можно вызывать SaveOrUpdate, даже если идентификатор присвоен.
        /// </summary>
        public T SaveOrUpdate(T entity)
        {
            NHibernateSession.SaveOrUpdate(entity);
            return entity;
        }

        /// <summary>
        /// Присоеденяет объекты не в сессии к объектам в сессии и сохраняет их.
        /// </summary>
        public T SaveOrUpdateCopy(T entity)
        {
            var e = NHibernateSession.Merge(entity as object);
            return (T)e;
        }

        /// <summary>
        /// Удалить сущность
        /// </summary>
        /// <param name="entity"></param>
        public void Delete(T entity)
        {
            NHibernateSession.Delete(entity);
        }

        /// <summary>
        /// Удалить сущность из кэша
        /// </summary>
        /// <param name="entity"></param>
        public void Evict(T entity)
        {
            NHibernateSession.Evict(entity);
        }

        //Реализация с Criteria - кривая, не удалось преобразовать и подцепить фильтр
        /*
        public List<T> GetPartData(int index, int pageSize, List<SortDescription> sortInfo, string filter)
        {
            _log.DebugFormat("GetPartData: {0}, {1}", index, pageSize);
            ICriteria __criteria = NHibernateSession.CreateCriteria(persitentType);
       
     //       if (!String.IsNullOrEmpty(filter))
     //          __criteria.Add(Expression.Sql(filter));
            if (sortInfo.Count > 0)
            {
                foreach (SortDescription __sort in sortInfo)
                {

                    if (__sort.SortOrder == ListSortDirection.Ascending)
                        __criteria.AddOrder(Order.Asc(__sort.Descriptor));
                    else
                        __criteria.AddOrder(Order.Desc(__sort.Descriptor));
                }
            }
            __criteria.SetFirstResult(index)
                    .SetMaxResults(pageSize+1);


            return __criteria.List<T>() as List<T>;
        }
        */

        /// <summary>
        /// Получить кусок данных, используя фильтр и данные о сортировке
        /// </summary>
        /// <param name="index"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortInfo"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public List<T> GetPartData(int index, int pageSize, List<SortDescription> sortInfo, string filter)
        {
            //_log.DebugFormat("GetPartData: {0}, {1}", index, pageSize);
            var hql = "from " + _persitentType;
            if (!string.IsNullOrEmpty(filter))
                hql += " where " + filter;

            var orderby = new List<string>();
            if (sortInfo.Count > 0)
            {
                orderby.AddRange(
                    sortInfo.Select(
                        sort =>
                            sort.SortOrder == ListSortDirection.Ascending
                                ? string.Format("{0} asc", sort.Descriptor)
                                : string.Format("{0} desc", sort.Descriptor)));
            }
            if (orderby.Count > 0)
                hql = hql + " order by " + string.Join(",", orderby.ToArray());
            var qry = NHibernateSession.CreateQuery(hql)
                    .SetFirstResult(index)
                    .SetMaxResults(pageSize + 1);


            return qry.List<T>() as List<T>;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="summaryInfo"></param>
        /// <returns></returns>
        public Dictionary<object, object> GetTotals(string filter, List<SummaryItem> summaryInfo)
        {
            //  _log.DebugFormat("GetTotals: {0}, {1}",filter,summaryInfo.ToString());
            var result = new Dictionary<object, object>();
            var hql = " select {0} from " + _persitentType;
            if (!string.IsNullOrEmpty(filter))
                hql += " where " + filter;
            if (summaryInfo != null && summaryInfo.Count > 0)
            {
                var criteria = NHibernateSession.CreateCriteria(_persitentType);
                //          if (!String.IsNullOrEmpty(filter))
                //            __criteria.Add(Expression.Sql(filter));
                var agg = new List<string>();

                var pr = Projections.ProjectionList();
                foreach (var item in summaryInfo)
                {
                    result.Add(item.Key, 0);
                    if (item.SummaryType == "Count")
                    {
                        pr.Add(Projections.RowCount(), item.Key.ToString());
                        agg.Add("count(*)");
                    }
                    else
                    {
                        switch (item.SummaryType)
                        {
                            case "Sum":
                                pr.Add(Projections.Sum(item.Descriptor), item.Key.ToString());
                                agg.Add(string.Format("sum({0})", item.Descriptor));
                                break;
                            case "Average":
                                pr.Add(Projections.Avg(item.Descriptor), item.Key.ToString());
                                agg.Add(string.Format("avg({0})", item.Descriptor));
                                break;
                            case "Max":
                                pr.Add(Projections.Max(item.Descriptor), item.Key.ToString());
                                agg.Add(string.Format("max({0})", item.Descriptor));
                                break;
                            case "Min":
                                pr.Add(Projections.Min(item.Descriptor), item.Key.ToString());
                                agg.Add(string.Format("min({0})", item.Descriptor));

                                break;
                            default:
                                pr.Add(Projections.Constant(0), item.Key.ToString());
                                agg.Add("NULL");
                                break;
                        }
                    }
                }
                pr.Add(Projections.RowCount(), "empty");
                agg.Add("count(*)");

                criteria.SetProjection(pr);
                //var __totals = (object[])__criteria.UniqueResult();
                hql = string.Format(hql, string.Join(",", agg.ToArray()));
                var qry = NHibernateSession.CreateQuery(hql);
                var totals = (object[])qry.UniqueResult();
                var i = 0;
                foreach (var item in summaryInfo)
                {
                    result[item.Key] = totals[i];
                    i++;
                }
            }
            return result;
        }

        /// <summary>
        /// Получить уникальные значения поля/колонки
        /// </summary>
        /// <param name="name"></param>
        /// <param name="maxCount"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public object[] GetUniqueColumnValues(string name, int maxCount, string filter)
        {
            const int maxCountConst = 10240;
            var hql = "select distinct {0} from " + _persitentType;
            if (!string.IsNullOrEmpty(filter))
                hql += " where " + filter;

            hql = string.Format(hql, name);
            hql = hql + string.Format(" order by {0} asc", name);
            var qry = NHibernateSession.CreateQuery(hql)
                    .SetMaxResults((maxCount <= 0) ? maxCountConst + 1 : maxCount);



            var result = qry.List();
            if (maxCount <= 0 && result.Count > maxCountConst)
                return new object[0];
            return ExtractObjectsArray(result, true);
        }

        #endregion

        private object[] ExtractObjectsArray(IList selected, bool roundDataTime)
        {
            if (selected.Count == 0)
                return new object[0];
            var result = new ArrayList(selected.Count);
            if (roundDataTime && (selected[0]) is DateTime)
            {
                DateTime? prevDt = null;
                foreach (var row in selected)
                {
                    var value = row as DateTime?;
                    if (value == null)
                        continue;
                    if (value == prevDt)
                        continue;
                    prevDt = value.Value.Date;
                    result.Add(prevDt);
                }
            }
            else
            {
                foreach (var row in selected)
                {
                    var value = row;
                    result.Add(value);
                }
            }
            return result.ToArray();
        }

        /*private string ToSql(string hqlQueryText)
        {
            string sqlText = String.Empty;

            if (!string.IsNullOrEmpty(hqlQueryText))
            {
                IDictionary<string, IFilter> enabledFilters = new Dictionary<string, IFilter>();
                QueryTranslator translator =
                    new QueryTranslator((ISessionFactoryImplementor)SessionManager.Instance.GetSessionFactoryFor(SessionFactoryConfigPath),
                        hqlQueryText,
                        enabledFilters);

                translator.Compile(
                    new Dictionary<String, String>(),
                    false);

                sqlText = translator.SqlString.ToString();
            }

            return sqlText;
        }*/
    }
}
