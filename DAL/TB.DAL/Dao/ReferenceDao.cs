﻿using System;
using System.Collections.Generic;
using System.Linq;
using TB.BOL.Interface;
using TB.DAL.Interface.Dao;
using TB.DAL.Maps;

namespace TB.DAL.DAO
{

    /// <summary>
    ///     Класс доступа к справочникам
    /// </summary>
    /// <typeparam name="T">Тип справочника</typeparam>
    public class ReferenceDao<T> : Dao<T>, IReferenceDao<T>
        where T : IReference
    {
        /// <summary>
        ///     Получение справочного значения по наименованию
        /// </summary>
        /// <param name="name">Имя справочного значения</param>
        /// <returns>Справочное значение</returns>
        public List<T> GetByName(String name)
        {
            return Linq()
                .Where(d => d.Name == name)
                .ToList();
        }

        public List<T> GetByCode(Int64 code)
        {
            return GetByCode(code, false);
        }

        /// <summary>
        ///     Получение справочного значения по коду
        /// </summary>
        /// <param name="code">Код справочного значения</param>
        /// <param name="caching">Кэширование результата</param>
        /// <returns>Справочное значение</returns>
        public List<T> GetByCode(Int64 code, Boolean caching)
        {
            var cacheRegion = caching ? CacheRegion.References.ToString() : null;

            return Linq(cacheRegion)
                .Where(d => d.Code == code)
                .ToList();
        }

        public T GetUniqueByCode(Int64 code)
        {
            return GetUniqueByCode(code, null, false);
        }

        public T GetUniqueByCode(Int64 code, Boolean caching)
        {
            return GetUniqueByCode(code, null, caching);
        }

        /// <summary>
        ///     Получение справочного значения по коду и дате
        /// </summary>
        /// <param name="code">Код справочного значения</param>
        /// <param name="date">Дата действия справочного значения</param>
        /// <param name="caching">Кэширование результата</param>
        /// <returns>Справочное значение</returns>
        public T GetUniqueByCode(Int64 code, DateTime? date, Boolean caching)
        {
            var list = GetByCode(code);

            var dueDate = date.HasValue ? date : DateTime.Now.Date;
            return list.FirstOrDefault(item => (item.BeginDate.HasValue ? item.BeginDate : DateTime.MinValue) <= dueDate &&
                                               (item.EndDate.HasValue ? item.EndDate : DateTime.MaxValue) >= dueDate);
        }

        public List<T> GetAllActive()
        {
            return GetAllActive(false);
        }

        public List<T> GetAllActive(Boolean caching)
        {
            return GetAllActive(null, caching);
        }

        /// <summary>
        ///     Получение списка активных справочных значений
        /// </summary>
        /// <param name="date">Дата действия справочного значения</param>
        /// <param name="caching">Кэширование результата</param>
        /// <returns>Список справочных значение</returns>
        public List<T> GetAllActive(DateTime? date, Boolean caching)
        {
            var cacheRegion = caching ? CacheRegion.References.ToString() : null;

            var list = Linq(cacheRegion)
                .ToList();

            var dueDate = date.HasValue ? date : DateTime.Now.Date;
            return list
                .Where(x => (x.BeginDate.HasValue ? x.BeginDate : DateTime.MinValue) <= dueDate &&
                            (x.EndDate.HasValue ? x.EndDate : DateTime.MaxValue) >= dueDate)
                .ToList();
        }
    }
}
