﻿using System.Collections.Generic;
using System.Linq;
using NHibernate.Cfg;
using NHibernate.Criterion;
using TB.BOL.Descriptions;
using TB.BOL.Documents;
using TB.BOL.Interfaces;

namespace TB.DAL.DAO.Documents
{
    /// <summary>
    /// Класс доступа к документам
    /// </summary>
    public class DocumentDao : Dao<Document>
    {
        public DocumentDao(Configuration config) : base(config)
        {
        }

        /// <summary>
        /// Получение документов по списку идентификаторов
        /// </summary>
        /// <param name="documentsId">Список идентификаторов документов</param>
        /// <returns>Список материалов</returns>
        public List<Document> GetByIdList(List<long> documentsId)
        {
            return (from m in Linq()
                    where documentsId.Contains(m.Id)
                    select m).ToList();
        }

        /// <summary>
        /// Получение списка документов по номеру и коду типа документа
        /// </summary>
        /// <param name="docNo">Номер документа</param>
        /// <param name="docTypeCode">Код типа документа</param>
        /// <returns>Список документов</returns>
        public List<Document> GetByNoAndType(long docNo, long docTypeCode)
        {
            return (from d in Linq()
                    where d.No == docNo && d.DocumentType.Code == docTypeCode
                    select d).ToList();
        }

        /// <summary>
        /// Получение списка документов по наименованию и кодам типов документа
        /// </summary>
        /// <param name="docName">Наименование документа</param>
        /// <param name="typeCodes">Перечень кодов типов документа</param>
        /// <returns>Список документов</returns>
        public List<Document> GetByNameAndType(string docName, params long[] typeCodes)
        {
            return GetByNameAndType(new List<string> { docName }, typeCodes);
        }

        /// <summary>
        /// Получение списка документов по нескольким наименованиям и кодам типов документов
        /// </summary>
        /// <param name="documentNames">Список наименований документов</param>
        /// <param name="paramLongs">Перечень кодов типов документа</param>
        /// <returns>Список документов</returns>
        public List<Document> GetByNameAndType(IEnumerable<string> documentNames, params long[] paramLongs)
        {
            var typeCodes = paramLongs.Length > 0 ? paramLongs.Where(x => x != 0).ToArray() : null;

            if (documentNames == null) return new List<Document>();

            var documentCriteria = NHibernateSession.CreateCriteria<Document>();

            documentCriteria.Add(
                documentNames.Where(x => x != null).Aggregate(Restrictions.Disjunction(),
                    (seed, s) =>
                        (Disjunction)
                            seed.Add(Restrictions.Like("Name", s.Replace("*", ""),
                                s.Contains('*') ? MatchMode.Start : MatchMode.Exact))));

            if (typeCodes != null && typeCodes.Any())
                documentCriteria.CreateCriteria("DocumentType").Add(Expression.In("Code", typeCodes));

            return documentCriteria.List<Document>() as List<Document>;
        }

        /// <summary>
        /// Получение списка документов по коду типа документа и коду шаблона
        /// </summary>
        /// <param name="docTypeCode">Код типа документа</param>
        /// <returns>Список документов</returns>
        public List<Document> GetByType(long docTypeCode)
        {
            return (from d in Linq()
                    where d.DocumentType.Code == docTypeCode 
                    select d).ToList();
        }




        /// <summary>
        /// Получает список производных документов определенных типов
        /// </summary>
        /// <param name="documentId">Идентификатор документа</param>
        /// <param name="documentTypeCodes">Код типов производных документов</param>
        /// <param name="descList">Список примечаний</param>
        /// <returns>Список производных документов</returns>
        public List<Document> GetDerivedDocuments(long documentId, long documentTypeCodes, IList<Description> descList = null)
        {
            var doc = GetById(documentId, false);
            var documents =
                documentTypeCodes != 0
                ? doc.DerivedDocuments.Where(x => documentTypeCodes == x.DocumentType.Code).ToList()
                : doc.DerivedDocuments.ToList();
            if (descList == null || !descList.Any())
            {
                return documents;
            }

            var result = documents.Select(p => new
            {
                document = p,
                rank = p.CompareDescriptions(descList, ComparisonType.Subvalue, true)
            }).ToList();

            var maxRank = result.Any() ? result.Select(p => p.rank).Max() : -1;

            return result.Where(p => p.rank == maxRank && maxRank != -1).Select(p => p.document).Distinct().ToList();
        }


        /// <summary>
        /// Получает список документов-источников определенных типов
        /// </summary>
        /// <param name="documentId">Идентификатор документа</param>
        /// <param name="documentTypeCodes">Код типов документов-источников</param>
        /// <returns>Список документов-источников</returns>
        public List<Document> GetSourceDocuments(long documentId, params long[] documentTypeCodes)
        {
            var doc = GetById(documentId, false);
            return documentTypeCodes.Length > 0
                       ? doc.SourceDocuments.Where(x => documentTypeCodes.Contains(x.DocumentType.Code)).ToList()
                       : doc.SourceDocuments.ToList();
        }
    }
}
