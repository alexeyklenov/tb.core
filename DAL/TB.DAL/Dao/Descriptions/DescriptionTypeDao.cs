﻿using System.Collections.Generic;
using System.Linq;
using TB.BOL.Descriptions;
using TB.DAL.Interface.Dao;

namespace TB.DAL.DAO.Descriptions
{
    public class DescriptionTypeDao : ReferenceDao<DescriptionType>, IDescriptionTypeDao<DescriptionType>
    {
        /// <summary>
        /// Получить дескрипшен по коду
        /// </summary>
        /// <param name="descriptionCode">Код дескрипшена</param>
        /// <returns>Список дескрипшенов</returns>
        public new DescriptionType GetByCode(long descriptionCode)
        {
            var q = (from d in Linq()
                     where d.Code == descriptionCode
                     select d).ToList();
            return (from d in q
                    select d).FirstOrDefault();
        }

        /// <summary>
        /// Получение списка типов описаний по ид значения настройки
        /// TODO: получение по списку описаний
        /// </summary>
        /// <param name="descriptionValueId">ид значения настройки</param>
        /// <returns>список типов описаний</returns>
        public List<DescriptionType> GetByDescriptionValueId(long descriptionValueId)
        {
            return (from d in Linq()
                    where (d.Descriptions != null) &&
                        (d.Descriptions.Count > 0) &&
                        (d.Descriptions.Any(x => x.DescriptionValue != null &&
                                            x.DescriptionValue.Id == descriptionValueId))
                    select d).ToList();
        }

    }
}
