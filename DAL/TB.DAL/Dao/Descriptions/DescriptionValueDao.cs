﻿using System;
using System.Collections.Generic;
using System.Linq;
using TB.BOL.Descriptions;
using TB.BOL.Interface;
using TB.DAL.Interface;
using TB.DAL.Interface.Dao;
using TB.DAL.Maps;

namespace TB.DAL.DAO.Descriptions
{
    public class DescriptionValueDao : ReferenceDao<DescriptionValue>, IDescriptionValueDao<DescriptionValue>
    {
        /// <summary>
        /// Получить дескрипшен по типу
        /// </summary>
        /// <param name="descriptionType">Тип дескрипшена</param>
        /// <param name="caching">Кэширование результата</param>
        /// <returns>Список дескрипшенов</returns>
        public List<DescriptionValue> GetByDescriptionType(IReference descriptionType, Boolean caching)
        {
            return (from d in Linq(caching ? CacheRegion.References.ToString() : null)
                    where d.DescriptionType == ((descriptionType as DescriptionType).Parent ?? descriptionType)
                    //                        && (d.EndDate == null || d.EndDate > DateTime.Today)
                    select d).ToList();
        }
        public List<DescriptionValue> GetByDescriptionType(DescriptionType descriptionType)
        {
            return GetByDescriptionType(descriptionType, false);
        }

        /// <summary>
        /// Получение значения характеристики по типу и коду 
        /// </summary>
        /// <param name="descriptionTypeCode">Код типа характеристики</param>
        /// <param name="predicate">Условие отбора значений характеристик</param>
        public IEnumerable<DescriptionValue> GetByDescriptionType(Int64 descriptionTypeCode, Func<DescriptionValue, Boolean> predicate)
        {
            var descriptionType = DaoContainer.GetDao<DescriptionTypeDao>().GetByCode(descriptionTypeCode);
            return descriptionType != null
                       ? descriptionType.RootDescriptionType.DescriptionValues.Where(predicate)
                       : new List<DescriptionValue>();
        }

        /// <summary>
        /// Получить дескрипшен по типу
        /// </summary>
        /// <param name="descriptionTypeCode">Код типа дескрипшена</param>
        /// <param name="caching">Кэширование результата</param>
        /// <returns>Список дескрипшенов</returns>
        public List<DescriptionValue> GetByDescriptionType(Int64 descriptionTypeCode, Boolean caching)
        {
            var descriptionType = DaoContainer.GetDao<DescriptionTypeDao>().GetByCode(descriptionTypeCode);

            return GetByDescriptionType(descriptionType);
        }
        public List<DescriptionValue> GetByDescriptionType(Int64 descriptionTypeCode)
        {
            return GetByDescriptionType(descriptionTypeCode, false);
        }

        /// <summary>
        /// Получение значения характеристики по типу и наименованию 
        /// </summary>
        /// <param name="descriptionTypeCode">Код типа характеристики</param>
        /// <param name="name">Наименование характеристики</param>
        public DescriptionValue GetByNameAndDescriptionTypeCode(String name, Int64 descriptionTypeCode)
        {
            return (from d in Linq()
                    where d.Name == name &&
                          d.DescriptionType.Code == descriptionTypeCode
                    select d).FirstOrDefault();
        }

        /// <summary>
        /// Получение значения характеристики по типу и краткому наименованию 
        /// </summary>
        /// <param name="descriptionTypeCode">Код типа характеристики</param>
        /// <param name="shortName">Краткое Наименование характеристики</param>
        public DescriptionValue GetByShortNameAndDescriptionTypeCode(String shortName, Int64 descriptionTypeCode)
        {
            return (from d in Linq()
                    where d.ShortName == shortName &&
                          d.DescriptionType.Code == descriptionTypeCode
                    select d).FirstOrDefault();
        }

        /// <summary>
        /// Получение значения характеристики по типу и коду 
        /// </summary>
        /// <param name="descriptionTypeCode">Код типа характеристики</param>
        /// <param name="code">Код характеристики</param>
        public DescriptionValue GetByCodeAndDescriptionTypeCode(Int64 code, Int64 descriptionTypeCode)
        {
            var dv = (from d in Linq()
                                     where d.Code == code &&
                                           d.DescriptionType.Code == descriptionTypeCode
                                     select d).FirstOrDefault();

            if (dv == null)
            {
                var dt = DaoContainer.GetDao<DescriptionTypeDao>().GetByCode(descriptionTypeCode);
                if (dt != null && dt.Parent != null)
                    return GetByCodeAndDescriptionTypeCode(code, dt.Parent.Code);

                return null;
            }

            return dv;
        }

        /// <summary>
        /// Получение значения характеристики по типу и коду 
        /// </summary>
        /// <param name="descriptionTypeCode">Код типа характеристики</param>
        /// <param name="code">Код характеристики</param>
        public DescriptionValue GetByCodeAndDescriptionTypeCodeEx(Int64 code, Int64 descriptionTypeCode)
        {
            return GetByDescriptionType(descriptionTypeCode, x => x.Code == code).FirstOrDefault();
        }

        /// <summary>
        /// Получение значения характеристики по типу и наименованию 
        /// </summary>
        /// <param name="descriptionTypeCode">Код типа характеристики</param>
        /// <param name="name">Наименование характеристики</param>
        public DescriptionValue GetByNameAndDescriptionTypeCodeEx(String name, Int64 descriptionTypeCode)
        {
            return GetByDescriptionType(descriptionTypeCode, x => x.Name == name).FirstOrDefault();
        }
    }
}
