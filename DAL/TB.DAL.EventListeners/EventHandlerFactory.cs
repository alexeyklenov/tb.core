﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Practices.Unity;
using Utils;

namespace TB.DAL.EventListeners
{
    public class EventHandlerFactory
    {
        // private static readonly object _syncRoot = new object();

        #region types for GetEventHandler
        private static readonly Type EhType = typeof(EventHandler<>);
        private static readonly Type IEhType = typeof(IEventHandler);
        #endregion

        EventHandlerFactory()
        {
            if (_isInitialized)
                return;

            foreach (var type in Assembly.GetExecutingAssembly().GetTypes().Where(x => x.GetInterface(IEhType.Name) != null))
                if ((!type.IsGenericType))
                {
                    var gType = EhType.MakeGenericType(type.BaseType.GetGenericArguments());

                    if (!UnityContext.Container.IsRegistered(gType))
                        UnityContext.Container.RegisterType(gType, type, new ContainerControlledLifetimeManager());
                }

            _isInitialized = true;
        }

        private class Nested
        {
            internal static readonly EventHandlerFactory FactoryInstance = new EventHandlerFactory();
        }

        public static EventHandlerFactory Instance
        {
            get
            {
                return Nested.FactoryInstance;
            }
        }

        private readonly bool _isInitialized;
        private readonly Dictionary<Type, IEventHandler> _registred = new Dictionary<Type, IEventHandler>();
        private readonly object _cacheLock = new object();

        /// <summary>
        /// Регистрация обработчика описаний 
        /// </summary>
        private static void RegisterAdditionalEventHandler(Type type, Type interfaceType, Type genericType,
            Type handlerType)
        {
            // Если объект реализует интерфейс interfaceType, то genericType привязываются к обработчику handlerType
            if (type.GetInterface(interfaceType.Name) != null)
            {
                var typeInterface = type.GetInterfaces().FirstOrDefault(x => x.Name == interfaceType.Name);
                if (typeInterface == null) 
                    return;

                var gtype = EhType.MakeGenericType(new[] { genericType.MakeGenericType(typeInterface.GetGenericArguments()) });

                if (UnityContext.Container.IsRegistered(gtype))
                    return;

                UnityContext.Container.RegisterType(gtype,
                    handlerType.MakeGenericType(
                        typeInterface.GetGenericArguments()),
                    new ContainerControlledLifetimeManager());
            }
            else
                // Если объект является genericType, то genericType привязываются к обработчику handlerType
                if (type.IsGenericType && type.GetGenericTypeDefinition() == genericType)
                {
                    var gType = EhType.MakeGenericType(new[] { type });

                    if (!UnityContext.Container.IsRegistered(gType))
                        UnityContext.Container.RegisterType(gType,
                            handlerType.MakeGenericType(type.GetGenericArguments()),
                            new ContainerControlledLifetimeManager());
                }
        }

        /// <summary>
        /// Регистрация обработчиков описаний (выбор обработчика) 
        /// </summary>
        /// <param name="listener"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static IEventHandler GetEventHandler(IEventListener listener, object entity)
        {
            var enttype = entity.GetType();

            IEventHandler handler;

            if (Instance._registred.TryGetValue(enttype, out handler)) return handler;

            lock (Instance._cacheLock)
            {
                if (Instance._registred.TryGetValue(enttype, out handler)) return handler;

                var handlerType = EhType.MakeGenericType(new[] { enttype });

                if (!UnityContext.Container.IsRegistered(handlerType))
                    UnityContext.Container.RegisterType(handlerType, handlerType, new ContainerControlledLifetimeManager());

                Instance._registred.Add(enttype, handler = (IEventHandler)UnityContext.Container.Resolve(handlerType, listener.InitializationParameters));
            }
            return handler;
        }
    }
}

