﻿using Microsoft.Practices.Unity;

namespace TB.DAL.EventListeners
{
    public interface IEventListener
    {
        ResolverOverride[] InitializationParameters { get; }

        event EventListenerEventHandler OnPreFlush;

        event EventListenerEventHandler OnPostFlush;
    }
}
