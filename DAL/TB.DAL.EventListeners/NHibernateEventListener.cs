﻿using System;
using System.Collections.Generic;
using System.Threading;
using log4net;
using Microsoft.Practices.Unity;
using NHibernate;
using NHibernate.AdoNet.Util;
using NHibernate.Engine;
using NHibernate.Event;
using NHibernate.Event.Default;
using TB.BOL.Interface;

namespace TB.DAL.EventListeners
{
    public class NHibernateEventListener : AbstractFlushingEventListener, IEventListener,
     IPreInsertEventListener, IPostInsertEventListener,
     IPreUpdateEventListener, IPostUpdateEventListener,
     IPreDeleteEventListener, IPostDeleteEventListener,
     IPreLoadEventListener, IPostLoadEventListener, ILoadEventListener,
     IFlushEventListener
    {
        private readonly Dictionary<string, Type> _types = new Dictionary<string, Type>();
        private readonly ReaderWriterLockSlim _cacheLock = new ReaderWriterLockSlim();

        private static EventListenerEventHandler _preFlushHandlers;
        private static EventListenerEventHandler _postFlushHandlers;

        private readonly ResolverOverride[] _initializationParameters;

        public NHibernateEventListener()
        {
            _initializationParameters = new ResolverOverride[] { new ParameterOverride("listener", this) };
        }

        private ILog _logger;
        public ILog Logger
        {
            get { return _logger ?? (_logger = LogManager.GetLogger("NHibernateEventListener")); }
        }
        protected void ExecuteNonQuery(ISession session, string commandText, Dictionary<string, object> parameters)
        {
            var command = session.Connection.CreateCommand();
            command.CommandText = commandText.Replace('\r', ' ');

            foreach (var pair in parameters)
            {
                var parameter = command.CreateParameter();
                parameter.ParameterName = pair.Key;
                parameter.Value = pair.Value;
                command.Parameters.Add(parameter);
            }

            ((ISessionImplementor)session).Factory.Settings.SqlStatementLogger.LogCommand(command, FormatStyle.Basic);
            command.ExecuteNonQuery();
        }
        

        public bool OnPreInsert(PreInsertEvent evt)
        {
            var entity = evt.Entity as IDomainObject;
            if (entity == null)
                return false;

            try
            {
                var vetoFlag = EventHandlerFactory.GetEventHandler(this, evt.Entity)
                    .HandlePreInsertEvent(evt.Session, evt.Entity);

                Log("OnPreInsert", entity, vetoFlag);

                return vetoFlag;
            }
            catch (EventHandlingException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Log("OnPreInsert", entity, ex);
            }

            return false;
        }

        public void OnPostInsert(PostInsertEvent evt)
        {
            var entity = evt.Entity as IDomainObject;
            if (entity == null)
                return;

            try
            {
                EventHandlerFactory.GetEventHandler(this, evt.Entity).HandlePostInsertEvent(evt.Session, evt.Entity);
            }
            catch (EventHandlingException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Log("OnPostInsert", entity, ex);
            }
        }

        public bool OnPreUpdate(PreUpdateEvent evt)
        {
            var entity = evt.Entity as IDomainObject;
            if (entity == null)
                return false;

            try
            {
                var vetoFlag = EventHandlerFactory.GetEventHandler(this, evt.Entity)
                    .HandlePreUpdateEvent(evt.Session, evt.Entity);

                Log("OnPreUpdate", entity, vetoFlag);

                return vetoFlag;
            }
            catch (EventHandlingException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Log("OnPreUpdate", entity, ex);
            }

            return false;
        }

        public void OnPostUpdate(PostUpdateEvent evt)
        {
            var entity = evt.Entity as IDomainObject;
            if (entity == null)
                return;

            try
            {
                EventHandlerFactory.GetEventHandler(this, evt.Entity).HandlePostUpdateEvent(evt.Session, evt.Entity);
            }
            catch (EventHandlingException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Log("OnPostUpdate", entity, ex);
            }
        }

        public bool OnPreDelete(PreDeleteEvent evt)
        {
            var entity = evt.Entity as IDomainObject;
            if (entity == null)
                return false;

            try
            {
                var vetoFlag = EventHandlerFactory.GetEventHandler(this, evt.Entity)
                    .HandlePreDeleteEvent(evt.Session, evt.Entity);

                Log("OnPreDelete", entity, vetoFlag);

                return vetoFlag;
            }
            catch (EventHandlingException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Log("OnPreDelete", entity, ex);
            }

            return false;
        }

        public void OnPostDelete(PostDeleteEvent evt)
        {
            var entity = evt.Entity as IDomainObject;
            if (entity == null)
                return;

            try
            {
                EventHandlerFactory.GetEventHandler(this, evt.Entity).HandlePostDeleteEvent(evt.Session, evt.Entity);
            }
            catch (EventHandlingException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Log("OnPostDelete", entity, ex);
            }
        }

        public void OnPreLoad(PreLoadEvent evt)
        {
            var entity = evt.Entity as IDomainObject;
            if (entity == null)
                return;

            try
            {
                EventHandlerFactory.GetEventHandler(this, evt.Entity).HandlePreLoadEvent(evt.Session, evt.Entity);
            }
            catch (EventHandlingException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Log("OnPreLoad", entity, ex);
            }
        }

        public void OnPostLoad(PostLoadEvent evt)
        {
            var entity = evt.Entity as IDomainObject;
            if (entity == null)
                return;

            try
            {
                EventHandlerFactory.GetEventHandler(this, evt.Entity).HandlePostLoadEvent(evt.Session, evt.Entity);
            }
            catch (EventHandlingException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Log("OnPostLoad", entity, ex);
            }
        }



        public void OnLoad(LoadEvent evt, LoadType loadType)
        {
           
        }

        public void OnFlush(FlushEvent evt)
        {
          
        }

        #region IEventListener Members

        public ResolverOverride[] InitializationParameters
        {
            get { return _initializationParameters; }
        }

        public event EventListenerEventHandler OnPreFlush
        {
            add { _preFlushHandlers += value; }
            remove { if (_preFlushHandlers != null) _preFlushHandlers -= value; }
        }

        public event EventListenerEventHandler OnPostFlush
        {
            add { _postFlushHandlers += value; }
            remove { if (_postFlushHandlers != null) _postFlushHandlers -= value; }
        }

        #endregion

        

        private void Log(string methodName, IDomainObject entity, bool vetoFlag)
        {
            if (!Logger.IsDebugEnabled)
                return;

            Logger.DebugFormat("{0}: Entity {1} (Id={2}) - Action {3}permitted",
                               methodName, entity.GetType().Name, entity.ObjectId,
                               vetoFlag ? "NOT " : string.Empty);
        }

        private void Log(string methodName, IDomainObject entity, Exception exception)
        {
            Logger.ErrorFormat("{0}: Entity {3} (Id={4})\nMessage: {1}\nInnerException: {2}",
                               methodName, exception.Message, exception.InnerException,
                               entity.GetType().Name, entity.ObjectId);
        }


    }
    public class CustomSaveOrUpdateEventListener : DefaultSaveOrUpdateEventListener, IEventListener
    {
        private ILog _logger;
        public ILog Logger
        {
            get { return _logger ?? (_logger = LogManager.GetLogger("CustomSaveOrUpdateEventListener")); }
        }

        protected override object PerformSaveOrUpdate(SaveOrUpdateEvent evt)
        {
            var entity = evt.Entity as IDomainObject;
            if (entity != null)
            {
                try
                {
                    EventHandlerFactory.GetEventHandler(this, evt.Entity).HandleSaveOrUpdateEvent(evt.Session, evt.Entity);
                }
                catch (EventHandlingException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    Logger.Error("OnSaveOrUpdate", ex);
                }
            }
            return base.PerformSaveOrUpdate(evt);
        }


        public ResolverOverride[] InitializationParameters { get; private set; }
        public event EventListenerEventHandler OnPreFlush;
        public event EventListenerEventHandler OnPostFlush;
    }
}
