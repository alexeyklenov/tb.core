﻿using NHibernate;
using TB.BOL.Interface;

namespace TB.DAL.EventListeners
{
    /// <summary>
    /// Обработчик событий
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EventHandler<T> : IEventHandler
    {
        #region IEventHandler Members
        public virtual bool HandlePreInsertEvent(ISession session, object entity)
        {
            return false;
        }

        public virtual void HandlePostInsertEvent(ISession session, object entity)
        {
        }

        public virtual bool HandlePreUpdateEvent(ISession session, object entity)
        {
            return false;
        }

        public virtual void HandlePostUpdateEvent(ISession session, object entity)
        {
        }

        public virtual bool HandlePreDeleteEvent(ISession session, object entity)
        {
            return false;
        }

        public virtual void HandlePostDeleteEvent(ISession session, object entity)
        {
        }

        public virtual void HandlePreLoadEvent(ISession session, object entity)
        {
            ((IDomainObject)entity).SetLoading(true);
        }

        public virtual void HandlePostLoadEvent(ISession session, object entity)
        {
            ((IDomainObject)entity).SetLoading(false);
        }

        public virtual void HandleSaveOrUpdateEvent(ISession session, object entity)
        {
        }

        #endregion
    }
}
