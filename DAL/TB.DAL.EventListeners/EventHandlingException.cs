﻿using System;
namespace TB.DAL.EventListeners
{
    /// <summary>
    /// Типизированное исключение при обработки события
    /// </summary>
    public class EventHandlingException : Exception
    {
        public EventHandlingException()
        {
        }
        public EventHandlingException(string message)
            : base(message)
        {
        }
        public EventHandlingException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
