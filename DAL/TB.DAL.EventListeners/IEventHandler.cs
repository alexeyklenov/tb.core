﻿using NHibernate;

namespace TB.DAL.EventListeners
{
    public delegate void EventListenerEventHandler(ISession session);

    /// <summary>
    /// Обработчик событий
    /// </summary>
    public interface IEventHandler
    {
        /// <summary>
        /// Обработка события о подготовке к добавлению объекта
        /// </summary>
        /// <returns>Признак некорректной обработки</returns>
        bool HandlePreInsertEvent(ISession session, object entity);

        /// <summary>
        /// Обработка события на добавление объекта
        /// </summary>
        void HandlePostInsertEvent(ISession session, object entity);

        /// <summary>
        /// Обработка события о подготовке к изменению объекта
        /// </summary>
        /// <returns>Признак некорректной обработки</returns>
        bool HandlePreUpdateEvent(ISession session, object entity);

        /// <summary>
        /// Обработка события на изменение объекта
        /// </summary>
        void HandlePostUpdateEvent(ISession session, object entity);

        /// <summary>
        /// Обработка события о подготовке к удалению объекта
        /// </summary>
        /// <returns>Признак некорректной обработки</returns>
        bool HandlePreDeleteEvent(ISession session, object entity);

        /// <summary>
        /// Обработка события на удаление объекта
        /// </summary>
        void HandlePostDeleteEvent(ISession session, object entity);

        /// <summary>
        /// Обработка события на начало загрузки объекта
        /// </summary>
        void HandlePreLoadEvent(ISession session, object entity);

        /// <summary>
        /// Обработка события на окончание загрузки объекта
        /// </summary>
        void HandlePostLoadEvent(ISession session, object entity);

        /// <summary>
        /// Обработка события на сохранение объекта
        /// </summary>
        void HandleSaveOrUpdateEvent(ISession session, object entity);
    }
}
