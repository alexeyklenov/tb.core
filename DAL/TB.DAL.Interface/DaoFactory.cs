﻿using System;
using System.Linq;

namespace TB.DAL.Interface
{
    public class DaoFactory
    {
        public static IDao CreateInstance<T>(NHibernate.Cfg.Configuration config)
            where T : IDao
        {
            var type = typeof (T);
            if (type.IsInterface)
            {
                type =
                    AppDomain.CurrentDomain.GetAssemblies()
                        .SelectMany(a => a.GetTypes())
                        .First(a => a.GetInterfaces().Contains(type));
            }
            var constructor = type.GetConstructor(new[] {typeof (NHibernate.Cfg.Configuration)});
            if (constructor != null)
                return (IDao) (constructor.Invoke(new object[] {config}));
            
            constructor = type.GetConstructor(new Type[]{});
            if (constructor == null)
                return null;
            return (IDao) (constructor.Invoke(new object[] {}));
        }
    }
}
