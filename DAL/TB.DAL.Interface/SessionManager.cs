﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Web;
using log4net;
using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Unity;
using NHibernate;
using NHibernate.Mapping.ByCode;
using NHibernate.Tool.hbm2ddl;
using TB.Configuration;
using TB.Services.Infrastructure.Attributes.InitData;
using TB.Utils.Wcf;
using Utils;

namespace TB.DAL.Interface
{
    /// <summary>
    /// Handles creation and management of sessions and transactions.  It is a singleton because 
    /// building the initial session factory is very expensive. Inspiration for this class came 
    /// from Chapter 8 of Hibernate in Action by Bauer and King.  Although it is a sealed singleton
    /// you can use TypeMock (http://www.typemock.com) for more flexible testing.
    /// </summary>
    public sealed class SessionManager
    {
        #region Classes

        /// <summary>
        /// Assists with ensuring thread-safe, lazy singleton
        /// </summary>
        private class Nested
        {
            static Nested()
            {
            }

            internal static readonly SessionManager NHibernateSessionManager =
                new SessionManager();
        }

        #endregion

        #region Fields

        private static readonly object SyncRoot = new object();

        private readonly ILog _log = LogManager.GetLogger("SessionManager");

        private const int SessionFactoryAcquireTimeout = 20000;

        private readonly Hashtable _sessionFactories = new Hashtable();

        private const string TransactionKey = "CONTEXT_TRANSACTIONS";
        private const string SessionKey = "CONTEXT_SESSIONS";

        #endregion

        #region Properties

        public static SessionManager Instance
        {
            get { return Nested.NHibernateSessionManager; }
        }

        public static string ConfigString { get; set; }

        /// <summary>
        /// Since multiple databases may be in use, there may be one transaction per database 
        /// persisted at any one time.  The easiest way to store them is via a hashtable
        /// with the key being tied to session factory.  If within a web context, this uses
        /// <see cref="HttpContext" /> instead of the WinForms specific <see cref="CallContext" />.  
        /// Discussion concerning this found at http://forum.springframework.net/showthread.php?t=572
        /// </summary>
        private Hashtable ContextTransactions
        {
            get
            {
                if (IsInWebContext())
                {
                    if (HttpContext.Current.Items[TransactionKey] == null)
                        HttpContext.Current.Items[TransactionKey] = new Hashtable();

                    return (Hashtable)HttpContext.Current.Items[TransactionKey];
                }

                if (CallContext.GetData(TransactionKey) == null)
                    CallContext.SetData(TransactionKey, new Hashtable());

                return (Hashtable)CallContext.GetData(TransactionKey);
            }
        }

        /// <summary>
        /// Since multiple databases may be in use, there may be one session per database 
        /// persisted at any one time.  The easiest way to store them is via a hashtable
        /// with the key being tied to session factory.  If within a web context, this uses
        /// <see cref="HttpContext" /> instead of the WinForms specific <see cref="CallContext" />.  
        /// Discussion concerning this found at http://forum.springframework.net/showthread.php?t=572
        /// </summary>
        private Hashtable ContextSessions
        {
            get
            {
                if (IsInWebContext())
                {
                    if (HttpContext.Current.Items[SessionKey] == null)
                        HttpContext.Current.Items[SessionKey] = new Hashtable();

                    return (Hashtable)HttpContext.Current.Items[SessionKey];
                }

                if (IsInWcfContext())
                {
                    if (WcfOperationContext.Current.Items[SessionKey] == null)
                        WcfOperationContext.Current.Items[SessionKey] = new Hashtable();

                    return (Hashtable)WcfOperationContext.Current.Items[SessionKey];
                }

                if (CallContext.GetData(SessionKey) == null)
                    CallContext.SetData(SessionKey, new Hashtable());

                return (Hashtable)CallContext.GetData(SessionKey);
            }
        }

        private bool IsInWebContext()
        {
            return HttpContext.Current != null;
        }

        private bool IsInWcfContext()
        {
            return WcfOperationContext.Current != null;
        }

        #endregion

        #region Methods

        private SessionManager()
        {
        }

        /// <summary>
        /// This method attempts to find a session factory stored in <see cref="_sessionFactories" />
        /// via its name; if it can't be found it creates a new one and adds it the hashtable.
        /// </summary>
        /// <param name="configuration">конфигурация NHibernate</param>
        public ISessionFactory GetSessionFactoryFor(NHibernate.Cfg.Configuration configuration)
        {
            if (configuration == null && _sessionFactories.Count > 0)
                return (ISessionFactory)_sessionFactories[0];

            if (configuration != null)
            {
                var key = configuration.GetHashCode();
                //  Attempt to retrieve a stored SessionFactory from the hashtable.
                var sessionFactory = (ISessionFactory)_sessionFactories[key];

                //  Failed to find a matching SessionFactory so make a new one.
                if (sessionFactory == null)
                {
                    SetSessionFactory(configuration);
                    sessionFactory = (ISessionFactory)_sessionFactories[key];
                }
                return sessionFactory;
            }
            return null;
        }

        /// <summary>
        /// Создание и настройка фабрики сессий. В случае, если фабрика не найдена, создается новая и конфигурируется с помощью FluentNHibernate.
        /// Инициализация пользовательскими данными происходит на основе классов помеченных <see cref="InitDataClassAttribute" />
        /// </summary>
        /// <param name="configuration">конфигурация NHibernate</param>
        /// <param name="assemblies">Массив сборок, содержащих маппинги</param>
        public void SetSessionFactory(NHibernate.Cfg.Configuration configuration, params Assembly[] assemblies)
        {
            SetSessionFactory(configuration, assemblies.ToList());
        }

        /// <summary>
        /// Создание и настройка фабрики сессий. В случае, если фабрика не найдена, создается новая и конфигурируется с помощью NHibernate.
        /// Инициализация пользовательскими данными происходит на основе классов помеченных <see cref="InitDataClassAttribute" />
        /// </summary>
        /// <param name="configuration">конфигурация NHibernate</param>
        /// <param name="assemblies">Список сборок, содержащих маппинги</param>
        public void SetSessionFactory(NHibernate.Cfg.Configuration configuration, List<Assembly> assemblies)
        {
            WaitLock.Lock(SyncRoot, SessionFactoryAcquireTimeout,
                () =>
                {
                    var key = configuration.GetHashCode();

                    var sessionFactory = (ISessionFactory) _sessionFactories[key];

                    if (sessionFactory == null)
                    {
                        _log.DebugFormat("Создание SessionFactory. Configuration key '{0}'", key);

                        var mapper = new ModelMapper();
                        foreach (var assembly in assemblies)
                        {
                            mapper.AddMappings(assembly.GetExportedTypes());
                        }
                        configuration.AddMapping(mapper.CompileMappingForAllExplicitlyAddedEntities());

                        //TODO - по последним обсуждениям выносим needInit в конфигурацию, чтобы можно было управлять инициализацией на уровень выше и можно было обновлять схему при апдейте софта
                        var needInit = false;

                        try
                        {
                            new SchemaValidator(configuration).Validate();
                        }
                        catch (Exception exValidate)
                        {
                            needInit = true;
                            _log.Error(
                                string.Format(
                                    "Ошибка при валидации схемы. Пробуем обновить схему. Configuration key '{0}'",
                                    key),
                                exValidate);
                            try
                            {
                                var schemaUpdate = new SchemaUpdate(configuration);
                                schemaUpdate.Execute(false, true);
                                if (schemaUpdate.Exceptions.Any())
                                {
                                    schemaUpdate.Exceptions.ForEach(ex =>
                                    _log.Error(
                                    string.Format(
                                        "Ошибка при обновлении схемы. Пробуем создать новую схему. Configuration key '{0}'",
                                        key),
                                    ex));
                                    throw new Exception(string.Format("Число ошибок {0}", schemaUpdate.Exceptions.Count));
                                }
                                
                            }
                            catch (Exception exUpdate)
                            {
                                _log.Error(
                                    string.Format(
                                        "Ошибка при обновлении схемы. Пробуем создать новую схему. Configuration key '{0}'",
                                        key),
                                    exUpdate);
                                try
                                {
                                    new SchemaExport(configuration).Execute(true, true, false);
                                }
                                catch (Exception exExport)
                                {
                                    _log.Error(
                                        string.Format(
                                            "Ошибка при создании схемы. Configuration key '{0}'",
                                            key),
                                        exExport);
                                    throw;
                                }

                            }
                        }
                        try
                        {
                            sessionFactory = configuration.BuildSessionFactory();

                            if (sessionFactory == null)
                            {
                                throw new InvalidOperationException(
                                    "Configuration.BuildSessionFactory() returned null");
                            }
                            _sessionFactories[key] = sessionFactory;
                            _log.DebugFormat("SessionFactory создана. Configuration key '{0}'", key);

                            if (needInit)
                            {
                                // -- инициализируем данные, если имеется соответствующий класс
                                var initDataClasses =
                                    KnownAssemblies.GetKnownTypes(null)
                                        .Where(t => t.GetCustomAttribute(typeof (InitDataClassAttribute)) != null)
                                        .ToList();

                                _log.Debug(string.Format(
                                    "Найдены классы для инициализации данных в количестве {0}",
                                    initDataClasses.Count));

                                foreach (var initDataClass in initDataClasses)
                                {
                                    var method =
                                        (initDataClass.GetCustomAttribute(typeof (InitDataClassAttribute)) as
                                            InitDataClassAttribute).Method;

                                    var initDataClassObj = Activator.CreateInstance(initDataClass, new object[] {});

                                    try
                                    {
                                        initDataClass.GetMethod(method).Invoke(initDataClassObj, new object[] {});

                                        _log.Debug(
                                            string.Format(
                                                "Выполнена инициализация данных классом {0} через метод {1}",
                                                initDataClass.FullName, method));
                                    }
                                    catch (Exception initDataExc)
                                    {
                                        _log.Error(
                                            string.Format(
                                                "Не удалось выполнить инициализацию данных классом {0} через метод {1}",
                                                initDataClass.FullName, method), initDataExc);
                                    }
                                }
                                // --
                            }
                        }
                        catch (TimeoutException ex)
                        {
                            _log.Error(
                                string.Format(
                                    "Ошибка при создании SessionFactory. Configuration key '{0}'",
                                    key),
                                ex);
                            throw;
                        }
                        catch (InvalidOperationException ex)
                        {
                            _log.Error(
                                string.Format(
                                    "Ошибка при создании SessionFactory. Configuration key '{0}'",
                                    key),
                                ex);
                            throw;
                        }

                    }
                });
        }

        public ISession SessionOverride { get; set; }

        /// <summary>
        /// Выполняет создание экземляра зарегестрированного в IoC контейнере Interceptor'а
        /// </summary>
        /// <exception cref="ResolutionFailedException"></exception>
        /// <returns>Экземпляр Interceptor</returns>
        private IInterceptor GetRegisteredInterceptor()
        {
            //return new DebugSqlInterceptor();
            IInterceptor interceptor = null;
            if (UnityContext.Container.IsRegistered<IInterceptor>())
            {
                try
                {
                    interceptor = UnityContext.Container.Resolve<IInterceptor>();
                }
                catch (ResolutionFailedException ex)
                {
                    _log.Error("Не удалось создать экземпляр Interceptor'а. Проверьте конфигурационный файл приложения.", ex);
                    throw;
                }
            }
            return interceptor;
        }

        /// <summary>
        /// Получение сессии с определенным событийным Interceptor
        /// </summary>        
        public ISession GetSessionFrom(NHibernate.Cfg.Configuration configuration, IInterceptor interceptor)
        {
            var configurationKey = configuration.GetHashCode();
            var session = (ISession)ContextSessions[configurationKey];
            if (session == null)
            {
                if (interceptor == null)
                {
                    try
                    {
                        // Если не был передан Interceptor, выполняется поиск зарегистрированного в IoC контейнере Interceptor'а
                        interceptor = GetRegisteredInterceptor();
                    }
                    catch (Exception ex)
                    {
                        _log.Error("Ошибка при создании Interceptor'а", ex);
                        _log.Error("Ошибка при создании Interceptor'а. InnerException", ex.InnerException);
                    }
                }
                session = interceptor != null
                              ? GetSessionFactoryFor(configuration).OpenSession(interceptor)
                              : GetSessionFactoryFor(configuration).OpenSession();

                if (_log.IsDebugEnabled)
                {
                    _log.DebugFormat("Cессия создана. Configuration key '{0}'. SessionId '{1}'",
                        configurationKey,
                        session.GetSessionImplementation().SessionId.ToRawString());

                    if (interceptor != null)
                    {
                        _log.DebugFormat(
                            "Установлен Interceptor для сессии. Configuration key '{0}'. SessionId '{1}'. Interceptor type: {2}",
                            configurationKey,
                            session.GetSessionImplementation().SessionId.ToRawString(), interceptor.GetType().FullName);
                    }
                }
                ContextSessions[configurationKey] = session;
            }
            return session;
        }

        /// <summary>
        /// Получение сессии
        /// </summary> 
        public ISession GetSessionFrom(NHibernate.Cfg.Configuration configuration)
        {
            return GetSessionFrom(configuration, null);
        }


        /// <summary>
        /// Flushes anything left in the session and closes the connection.
        /// </summary>
        public void CloseSessionOn(int configurationkey)
        {
            CloseSessionOn(configurationkey, true);
        }

        /// <summary>
        /// Flushes anything left in the session and closes the connection.
        /// </summary>
        public void CloseSessionOn(int configurationkey, bool flush)
        {
            var session = ContextSessions[configurationkey] as ISession;
            if (session != null && session.IsOpen)
            {
                _log.DebugFormat("Закрытие сессии. Configuration key '{0}'. SessionId '{1}'",
                                     configurationkey,
                                     session.GetSessionImplementation().SessionId);
                if (flush)
                    session.Flush();
                session.Close();
                if (!session.IsOpen)
                {
                    _log.DebugFormat("Cессия закрыта. Configuration key '{0}'. SessionId '{1}'",
                                     configurationkey,
                                     session.GetSessionImplementation().SessionId);
                }
            }
            ContextSessions.Remove(configurationkey);
        }

        /// <summary>
        /// Закрыть все сессии, открытые в контексте
        /// </summary>
        public void CloseAllSessions()
        {
            if (ContextSessions == null || ContextSessions.Count == 0)
                return;

            var keys = new object[ContextSessions.Count];
            ContextSessions.Keys.CopyTo(keys, 0);

            foreach (var key in keys)
                CloseSessionOn((int)key);
        }

        public void AbortSession(ISession session)
        {
            if (session != null && session.IsOpen)
            {
                foreach (DictionaryEntry s in ContextSessions)
                {
                    if (s.Value == session)
                    {
                        _log.DebugFormat("Аварийное завершение сессии. Configuration path '{0}'. SessionId '{1}'",
                                         s.Key, session.GetSessionImplementation().SessionId);

                        ContextSessions.Remove(s.Key);
                        session.Disconnect();
                        session.Dispose();
                        break;
                    }
                }
            }
        }

        public ITransaction BeginTransactionOn(NHibernate.Cfg.Configuration configuration)
        {
            var configurationKey = configuration.GetHashCode();

            var transaction = (ITransaction)ContextTransactions[configurationKey];
            if (transaction == null)
            {
                transaction = GetSessionFrom(configuration).BeginTransaction();
                ContextTransactions.Add(configuration, transaction);
            }
            return transaction;
        }

        public bool CommitTransactionOn(NHibernate.Cfg.Configuration configuration)
        {
            var configurationKey = configuration.GetHashCode();
            var transaction = (ITransaction)ContextTransactions[configurationKey];
            try
            {
                if (HasOpenTransactionOn(configurationKey))
                {
                    transaction.Commit();
                    ContextTransactions.Remove(configurationKey);
                }
                else
                {
                    return false;
                }
            }
            catch (HibernateException)
            {
                RollbackTransactionOn(configurationKey);
                throw;
            }
            return true;
        }

        public bool HasOpenTransactionOn(int configurationKey)
        {
            var transaction = (ITransaction)ContextTransactions[configurationKey];
            return transaction != null && !transaction.WasCommitted && !transaction.WasRolledBack;
        }

        public void RollbackTransactionOn(int configurationKey)
        {
            var transaction = (ITransaction)ContextTransactions[configurationKey];

            try
            {
                if (HasOpenTransactionOn(configurationKey))
                {
                    transaction.Rollback();
                }

                ContextTransactions.Remove(configurationKey);
            }
            finally
            {
                CloseSessionOn(configurationKey);
            }
        }

        #endregion

    }
}
