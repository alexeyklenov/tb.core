﻿using NHibernate.Driver;
using NHibernate.SqlTypes;

namespace TB.DAL.Interface.Drivers
{
    public class ExtFirebirdDriver : FirebirdClientDriver
    {
        protected override void InitializeParameter(System.Data.IDbDataParameter dbParam, string name, SqlType sqlType)
        {
            if (Equals(sqlType, SqlTypeFactory.Boolean)) sqlType = SqlTypeFactory.Int16;

            base.InitializeParameter(dbParam, name, sqlType);
        }
    }
}