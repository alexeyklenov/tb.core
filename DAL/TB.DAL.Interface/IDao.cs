﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Utils.ComponentModel;

namespace TB.DAL.Interface
{
    public interface IDao
    {
        bool Check();
        object GetById(Type type, object id, bool shouldLock);
        object GetById(object id, bool shouldLock);
        IList<object> GetAll();
        IList<object> GetPage(int page, int pageSize);
        IList<object> GetByProperties(Dictionary<string, object> propertiesAndValues);
        IList<object> GetByFilter(List<SortDescription> sortInfo, string filter);
        IList<TU> GetByQuery<TU>(string queryString, string cacheRegion, params KeyValuePair<string, object>[] paramValues);
        IList<TU> GetByQuery<TU>(string queryString, params KeyValuePair<string, object>[] paramValues);

        object SaveOrUpdate(object entity);
        void Delete(object entity);
        object UnProxy(object proxy);
        object Refresh(object entity);
        object Merge(object entity);
        void CommitChanges();
        int GetCount();
        void ExecuteNonQuery(IDbCommand command);
        void ExecuteNonQuery(string commandText, Dictionary<string, object> parameters);
        object ExecuteScalarQuery(IDbCommand command);
        object ExecuteScalarQuery(string commandText, Dictionary<string, object> parameters);
        object GetScalarNamedQueryResult(string queryName, object parameters);
        void EnableFilter(string filterName, Dictionary<string, object> parameters);
        void DisableFilter(string filterName);

        object[] GetUniqueColumnValues(string name, int maxCount, string filter);
    }


    public interface IDao<T> : IDao
    {
        new T GetById(object id, bool shouldLock);
        new List<T> GetAll();
        //GetPartDatadelegate(Int32 index,Int32 windowSize,ListSortDescriptionCollection sortInfo, String filter);
        new List<T> GetPage(int page, int pageSize);
        List<T> GetSome(int startRecord, int maxRecords);
        List<T> GetByExample(T exampleInstance, params string[] propertiesToExclude);
        T GetUniqueByExample(T exampleInstance, params string[] propertiesToExclude);
        new List<T> GetByProperties(Dictionary<string, object> propertiesAndValues);
        IQueryable<T> Linq();
        IQueryable<T> Linq(bool cachable);
        IQueryable<T> Linq(string cacheRegion);
        List<T> GetNamedQueryResult(string queryName, object parameters);

        T Save(T entity);
        T SaveOrUpdate(T entity);
        T SaveOrUpdateCopy(T entity);
        void Delete(T entity);
        void Evict(T entity);

        /// Для реализации ServerMode
        List<T> GetPartData(int index, int pageSize, List<SortDescription> sortInfo, string filter);
        Dictionary<object, object> GetTotals(string filter, List<SummaryItem> summaryInfo);
        new object[] GetUniqueColumnValues(string name, int maxCount, string filter);
    }
}
