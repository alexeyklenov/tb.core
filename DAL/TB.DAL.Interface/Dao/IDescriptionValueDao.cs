﻿using System;
using System.Collections.Generic;
using TB.BOL.Interface;

namespace TB.DAL.Interface.Dao
{
    public interface IDescriptionValueDao<T> : IDao<T> where T:IReference
    {
        List<T> GetByDescriptionType(IReference descriptionType, Boolean caching);
        List<T> GetByDescriptionType(long descriptionTypeCode, Boolean caching);
        T GetByCodeAndDescriptionTypeCodeEx(Int64 code, Int64 descriptionTypeCode);
        T GetByNameAndDescriptionTypeCodeEx(String name, Int64 descriptionTypeCode);
    }
}
