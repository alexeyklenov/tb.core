﻿using System;
using System.Collections.Generic;
using TB.BOL.Interface;

namespace TB.DAL.Interface.Dao
{
    public interface IReferenceDao<T> : IDao<T> where T:IReference
    {
        List<T> GetByName(String name);
        List<T> GetByCode(Int64 code);
        List<T> GetByCode(Int64 code, Boolean caching);
        T GetUniqueByCode(Int64 code);
        T GetUniqueByCode(Int64 code, Boolean caching);
        T GetUniqueByCode(Int64 code, DateTime? date, Boolean caching);
        List<T> GetAllActive();
        List<T> GetAllActive(Boolean caching);
        List<T> GetAllActive(DateTime? date, Boolean caching);
    }
}
