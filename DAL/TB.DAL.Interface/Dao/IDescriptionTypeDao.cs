﻿using System.Collections.Generic;
using TB.BOL.Interface;

namespace TB.DAL.Interface.Dao
{
    public interface IDescriptionTypeDao<T>: IDao<T> where T : IReference
    {
        List<T> GetByDescriptionValueId(long descriptionValueId);
        T GetByCode(long descriptionCode);
    }
}
