﻿using System;
using System.Collections.Generic;
using System.IO;
using FirebirdSql.Data.FirebirdClient;
using Microsoft.Practices.Unity;
using NHibernate;
using NHibernate.Event;
using TB.BOL.Interface;
using TB.DAL.EventListeners;
using TB.DAL.Interface.Dialects;
using TB.DAL.Interface.Drivers;
using Utils;

namespace TB.DAL.Interface
{
    /// <summary>
    /// Хранилище классов доступа к данным
    /// </summary>
    public static class DaoContainer
    {
        private static NHibernate.Cfg.Configuration _config;

        public static NHibernate.Cfg.Configuration Config
        {
            get { return _config; }
        }

        static DaoContainer()
        {
            var path = string.IsNullOrEmpty(AppDomain.CurrentDomain.RelativeSearchPath)
                ? string.Empty
                : AppDomain.CurrentDomain.RelativeSearchPath + Path.DirectorySeparatorChar;

            if (!File.Exists(path + "Data"))
            {
                var builder = new FbConnectionStringBuilder
                {
                    DataSource = "localhost",
                    UserID = "SYSDBA",
                    Password = "1",
                    Database = path + "Data",
                    ServerType = FbServerType.Embedded
                };
                FbConnection.CreateDatabase(builder.ConnectionString);
            }
            _config = new NHibernate.Cfg.Configuration().AddProperties(new Dictionary<string, string>
            {
                {NHibernate.Cfg.Environment.ConnectionDriver,  typeof (ExtFirebirdDriver).AssemblyQualifiedName},
                {NHibernate.Cfg.Environment.Dialect, typeof (ExtFirebirdDialect).AssemblyQualifiedName},
                {
                    NHibernate.Cfg.Environment.ConnectionString,
                    //"Server=localhost;Database=cashierdb;User ID=root;Password=1;"
                    //"Server=localhost; Port=3306; Database=cashierdb; Uid=root; Pwd=1; charset=utf8"
                    "Server=localhost;Database="+ path + "Data;Dialect=3;Charset=UTF8;user=SYSDBA;Password=1;ServerType=1;"
                },
                {NHibernate.Cfg.Environment.ShowSql, "false"},
                {
                    NHibernate.Cfg.Environment.ReleaseConnections,
                    ConnectionReleaseModeParser.ToString(ConnectionReleaseMode.OnClose)
                },
            });
            _config.EventListeners.PreLoadEventListeners = new IPreLoadEventListener[] { new NHibernateEventListener() };
            _config.EventListeners.PostLoadEventListeners = new IPostLoadEventListener[] { new NHibernateEventListener() };
            _config.EventListeners.PreInsertEventListeners = new IPreInsertEventListener[] { new NHibernateEventListener() };
            _config.EventListeners.PostInsertEventListeners = new IPostInsertEventListener[] { new NHibernateEventListener() };
            _config.EventListeners.PreUpdateEventListeners = new IPreUpdateEventListener[] { new NHibernateEventListener() };
            _config.EventListeners.PostUpdateEventListeners = new IPostUpdateEventListener[] { new NHibernateEventListener() };
            _config.EventListeners.PreDeleteEventListeners = new IPreDeleteEventListener[] { new NHibernateEventListener() };
            _config.EventListeners.PostDeleteEventListeners = new IPostDeleteEventListener[] { new NHibernateEventListener() };
        }

        public static void Init(NHibernate.Cfg.Configuration config)
        {
            _config = config;
        }

        public static ISession NHibernateSession
        {
            get { return SessionManager.Instance.GetSessionFrom(_config);}
        }

        public static void CommitChanges()
        {
            NHibernateSession.Flush();
        }

        private static readonly object SyncRoot = new object();

        private static readonly Dictionary<RuntimeTypeHandle, IDao> DaoCache =
            new Dictionary<RuntimeTypeHandle, IDao>();

        /// <summary>
        /// Подбирает реализацию IDao
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetDao<T>() where T : IDao
        {
            IDao result;
            var typeHandle = typeof (T).TypeHandle;
            if (DaoCache.TryGetValue(typeHandle, out result))
                return (T) result;

            lock (SyncRoot)
            {
                if (!DaoCache.TryGetValue(typeHandle, out result))
                    try
                    {
                        result = DaoCache[typeHandle] = UnityContext.Container.Resolve<T>();
                    }
                    catch (Exception)
                    {
                       result = DaoCache[typeHandle] = DaoFactory.CreateInstance<T>(_config);
                    }
            }

            return (T) result;
        }
        
        /// <summary>
        /// Подбирает реализацию Dao для конкретного типа доменного объекта
        /// </summary>
        /// <typeparam name="T">Класс BOL</typeparam>
        /// <returns></returns>
        public static IDao<T> Dao<T>() where T : IDomainObject<long>
        {
            return GetDao<IDao<T>>();
        }

        public static IDao GetDao(Type objectType)
        {
            var mi = typeof(DaoContainer).GetMethod("Dao");
            var genericMethod = mi.MakeGenericMethod(objectType);
            return genericMethod.Invoke(null, null) as IDao;

        }
    }
}
