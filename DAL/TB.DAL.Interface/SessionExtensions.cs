﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Proxy;

namespace TB.DAL.Interface
{
    public static class SessionExtensions
    {
        public static IEnumerable<string> GetDirtyProperties(this ISession session, Object entity)
        {

            var sessionImpl = session.GetSessionImplementation();

            var persistenceContext = sessionImpl.PersistenceContext;

            var oldEntry = persistenceContext.GetEntry(entity);

            var className = oldEntry.EntityName;

            var persister = sessionImpl.Factory.GetEntityPersister(className);




            if (entity is INHibernateProxy)
            {
                var proxy = entity as INHibernateProxy;

                var obj = sessionImpl.PersistenceContext.Unproxy(proxy);

                oldEntry = sessionImpl.PersistenceContext.GetEntry(obj);
            }



            var oldState = oldEntry.LoadedState;

            var currentState = persister.GetPropertyValues(entity, sessionImpl.EntityMode);

            var dirtyPropsInds = persister.FindDirty(currentState, oldState, entity, sessionImpl);


            var dirtyProps = new List<string>();
            for (var i = 0; i < persister.PropertyNames.Count(); i++)
            {
                if (dirtyPropsInds.Contains(i))
                    dirtyProps.Add(persister.PropertyNames[i]);
            }

            return dirtyProps;
        }
    }
}