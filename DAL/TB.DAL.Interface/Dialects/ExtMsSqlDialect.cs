﻿using System.Data;
using NHibernate.Dialect;

namespace TB.DAL.Interface.Dialects
{
    public class ExtMsSqlDialect: MsSql2008Dialect
    {
        public ExtMsSqlDialect()
        {
            RegisterColumnType(DbType.UInt64, "BIGINT");
        }
    }
}