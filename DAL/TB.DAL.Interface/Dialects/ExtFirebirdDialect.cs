﻿using System.Data;
using NHibernate.Dialect;

namespace TB.DAL.Interface.Dialects
{
    public class ExtFirebirdDialect : FirebirdDialect
    {
        public ExtFirebirdDialect()
        {
            RegisterColumnType(DbType.UInt64, "BIGINT");
        }
    }
}