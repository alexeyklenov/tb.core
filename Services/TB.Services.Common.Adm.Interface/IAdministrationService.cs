﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using TB.Services.Common.Dto.Administration;
using TB.Services.Infrastructure.Attributes;
using TB.Utils.Enums;
using Utils.ComponentModel;

namespace TB.Services.Common.Adm.Interface
{
    /// <summary>
    /// Администрирование
    /// </summary>
    [ServiceContract]
    public interface IAdministrationService
    {
        #region Users

        /// <summary>
        /// Создать пользователя
        /// </summary>
        /// <param name="user">Пользователь</param>
        /// <param name="password">Пароль</param>
        /// <returns></returns>
        [OperationContract]
        UserDto CreateUser(UserDto user, string password);

        /// <summary>
        /// Обновить пользователя
        /// </summary>
        /// <param name="user">Пользователь</param>
        /// <returns>Обновленный пользователь</returns>
        [OperationContract]
        [SaveRecord(typeof(UserDto))]
        UserDto UpdateUser(UserDto user);

        /// <summary>
        /// Установить пароль
        /// </summary>
        /// <param name="login">логин</param>
        /// <param name="password">пароль</param>
        [OperationContract]
        void SetPassword(string login, string password);

        /// <summary>
        /// Проверить пользователя и пароль
        /// </summary>
        /// <param name="login">логин</param>
        /// <param name="password">пароль</param>
        /// <returns></returns>
        [OperationContract]
        bool ValidateUser(string login, string password);

        /// <summary>
        /// Закрыть учетную запись 
        /// </summary>
        /// <param name="userId">ид пользователя</param>
        /// <param name="endDate">дата закрытия</param>
        [OperationContract]
        void CloseUser(long userId, DateTime endDate);

        /// <summary>
        /// Получить пользователя 
        /// </summary>
        /// <param name="userId">ИД</param>
        /// <returns>Пользователь</returns>
        [OperationContract]
        [GetOneRecord(typeof(UserDto))]
        UserDto GetById(long userId);

        /// <summary>
        /// Получить пользователя
        /// </summary>
        /// <param name="login">логин</param>
        /// <param name="date">дата</param>
        /// <returns>пользователь</returns>
        [OperationContract]
        UserDto GetUserByLogin(string login, DateTime date);

        /// <summary>
        /// Получить всех пользователей
        /// </summary>
        /// <returns>Список пользователей</returns>
        [OperationContract]
        [GetAllData(typeof(UserDto))]
        List<UserDto> GetAll();

        /// <summary>
        /// Получить всех активных пользователей
        /// </summary>
        /// <returns>Список пользователей</returns>
        [OperationContract]
        [GetAllData(typeof(UserDto))]
        List<UserDto> GetAllActive();

        #region ServerMode grid
        /// <summary>
        /// Часть данных для отображения в гриде в режиме ServerMode
        /// </summary>
        /// <param name="index"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortInfo"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        [OperationContract]
        [GetPartData(typeof(UserDto))]
        List<UserDto> GetUserPartData(int index, int pageSize, List<SortDescription> sortInfo, string filter);

        [OperationContract]
        [GetTotals(typeof(UserDto))]
        Dictionary<object, object> GetUserTotals(string filter, List<SummaryItem> summaryInfo);

        [OperationContract]
        [GetUniqueValues(typeof(UserDto))]
        object[] GetUniqueColumnValues(string name, int maxCount, string filter);
        #endregion

        #endregion

        #region Roles

        /// <summary>
        /// Получить роль 
        /// </summary>
        /// <param name="roleId">ИД роли</param>
        /// <returns></returns>
        [OperationContract]
        [GetOneRecord(typeof(RoleDto))]
        RoleDto GetRoleById(long roleId);

        /// <summary>
        /// Получить роль
        /// </summary>
        /// <param name="roleName">Имя роли</param>
        /// <returns></returns>
        [OperationContract]
        RoleDto GetRoleByName(string roleName);

        /// <summary>
        /// Роли пользователя
        /// </summary>
        /// <param name="userId">ИД пользователя</param>
        /// <returns></returns>
        [OperationContract]
        List<RoleDto> GetUserRoles(long userId);

        /// <summary>
        /// Пользователи, включенные в роль
        /// </summary>
        /// <param name="roleId">ИД роли</param>
        /// <returns>Список пользователей</returns>
        [OperationContract]
        List<UserDto> GetRoleUsers(long roleId);

        /// <summary>
        /// Получить общий список ролей
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [GetAllData(typeof(RoleDto))]
        List<RoleDto> GetRoles();

        /// <summary>
        /// Выдать пользователю роль
        /// </summary>
        /// <param name="userId">ИД пользователя</param>
        /// <param name="roleId">ИД роли</param>
        [OperationContract]
        void SetRoleToUser(long userId, long roleId);

        /// <summary>
        /// Отобрать у пользователя роль
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="roleId"></param>
        [OperationContract]
        void RemoveRoleFromUser(long userId, long roleId);

        /// <summary>
        /// Добавить роль
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        [OperationContract]
        RoleDto InsertRole(RoleDto role);

        /// <summary>
        /// Изменить роль
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        [OperationContract]
        [SaveRecord(typeof(RoleDto))]
        RoleDto UpdateRole(RoleDto role);

        /// <summary>
        /// Удалить роль
        /// </summary>
        /// <param name="roleId"></param>
        [OperationContract]
        [DeleteRecord(typeof(RoleDto))]
        void DeleteRole(long roleId);

        #endregion

        #region Module

        /// <summary>
        /// Получить модуль
        /// </summary>
        /// <param name="moduleId">ИД модуля</param>
        /// <returns></returns>
        [OperationContract]
        [GetOneRecord(typeof(ModuleDto))]
        ModuleDto GetModuleById(long moduleId);

        /// <summary>
        /// Получить все модули
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [GetAllData(typeof(ModuleDto))]
        List<ModuleDto> GetModules();

        [OperationContract]
        List<RoleDto> GetModuleRoles(long moduleId);

        /// <summary>
        /// Модули роли
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [OperationContract]
        List<ModuleDto> GetRoleModules(long roleId);

        /// <summary>
        /// Получить модули пользователя
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [OperationContract]
        List<ModuleDto> GetUserModules(long userId);

        /// <summary>
        /// Получить список модулей, от которых зависит указанный модуль
        /// </summary>
        /// <param name="moduleId">ид модуля</param>
        /// <returns>Зависимости</returns>
        [OperationContract]
        List<ModuleDto> GetDependencies(long moduleId);


        /// <summary>
        /// Добавить новый модуль
        /// </summary>
        /// <param name="module"></param>
        /// <returns></returns>
        [OperationContract]
        ModuleDto InsertModule(ModuleDto module);

        /// <summary>
        /// Изменить модуль
        /// </summary>
        /// <param name="module"></param>
        /// <returns></returns>
        [OperationContract]
        [SaveRecord(typeof(ModuleDto))]
        ModuleDto UpdateModule(ModuleDto module);

        /// <summary>
        /// Установить зависимости модулей
        /// </summary>
        /// <param name="moduleId">целевой модуль</param>
        /// <param name="dependencyModuleId">модуль, от которого зависит целевой</param>
        [OperationContract]
        void SetDependency(long moduleId, long dependencyModuleId);

        /// <summary>
        /// Удалить зависимости модулей
        /// </summary>
        /// <param name="moduleId">целевой модуль</param>
        /// <param name="dependencyModuleId">модуль, от которого зависит целевой</param>
        [OperationContract]
        void RemoveDependency(long moduleId, long dependencyModuleId);

        /// <summary>
        /// Предоставить права роли на задачу
        /// </summary>
        /// <param name="roleId">ИД роли</param>
        /// <param name="moduleId">ИД модуля</param>
        /// <param name="access">Тип доступа</param>
        [OperationContract]
        void SetAccess(long roleId, long moduleId, AccessType access);
        #endregion

        #region Actions

        /// <summary>
        /// Получить список типов действий
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        Dictionary<long, string> GetActionTypes();

        /// <summary>
        /// Получить список всех доступных действий
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<ActionDto> GetAllActions();

        /// <summary>
        /// Получить действие по ИД
        /// </summary>
        /// <param name="actionId">Ид действия</param>
        /// <returns></returns>
        [OperationContract]
        ActionDto GetAction(long actionId);

        /// <summary>
        /// Получить все доступные действия пользователя
        /// </summary>
        /// <param name="userLogin"></param>
        /// <returns></returns>
        [OperationContract]
        List<ActionDto> GetUserActionPermissions(string userLogin);

        /// <summary>
        /// Проверить право пользователя на выполнение конкретной операции 
        /// </summary>
        /// <param name="userLogin">ИД пользователя</param>
        /// <param name="actionName">Название операции</param>
        /// <returns>true, если пользователь имеет право на выполнение операции, false в противном случае</returns>
        [OperationContract]
        bool CheckUserActionAccess(string userLogin, string actionName);

        /// <summary>
        /// Добавить или изменить действие
        /// </summary>
        /// <param name="action">Действие</param>
        /// <returns></returns>
        [OperationContract]
        ActionDto InsertOrUpdateAction(ActionDto action);

        /// <summary>
        /// Удалить действие
        /// </summary>
        /// <param name="action">действие</param>
        [OperationContract]
        void DeleteAction(ActionDto action);


        /// <summary>
        /// Получить действия, привязанные к модулю
        /// </summary>
        /// <param name="moduleId">ИД модуля</param>
        /// <returns></returns>
        [OperationContract]
        List<ActionDto> GetModuleActions(long moduleId);

        /// <summary>
        /// Получить права пользователя на действия модуля (работа оболочки)
        /// </summary>
        /// <param name="moduleId">ИД модуля</param>
        /// <param name="userLogin"></param>
        /// <returns></returns>
        [OperationContract]
        List<ActionDto> GetModuleUserPermissions(long moduleId, string userLogin);

        /// <summary>
        /// Получить права на действия модуля у роли (настройка роли)
        /// </summary>
        /// <param name="moduleId">ИД модуля</param>
        /// <param name="roleId">ИД роли</param>
        /// <returns></returns>
        [OperationContract]
        List<ActionDto> GetModuleRolePermissions(long moduleId, long roleId);

        /// <summary>
        /// Установить привязку модулей
        /// </summary>
        /// <param name="moduleId">ИД модуля</param>
        /// <param name="actions">действия</param>
        [OperationContract]
        void SetModuleActions(long moduleId, IList<ActionDto> actions);


        /// <summary>
        /// Установить права роли на действия модуля
        /// </summary>
        /// <param name="moduleId">ИД модуля</param>
        /// <param name="roleId">ИД роли</param>
        /// <param name="actions">действия</param>
        [OperationContract]
        void SetModuleRolePermissions(long moduleId, long roleId, List<ActionDto> actions);

        #endregion

    }
}
