﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using TB.Services.Common.Dto.Administration;
using TB.Services.Infrastructure.Attributes;

namespace TB.Services.Common.Adm.Interface
{
    [ServiceContract]
    public interface ISettingService
    {
        /// <summary>
        /// Получить список параметров
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<ParameterDto> GetParameters();

        /// <summary>
        /// Получить конкретный параметр
        /// </summary>
        /// <param name="parameterId">ИД</param>
        /// <returns></returns>
        [OperationContract]
        ParameterDto GetParameter(long parameterId);

        /// <summary>
        /// Сохранить параметр
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        [OperationContract]
        ParameterDto SaveParameter(ParameterDto parameter);

        /// <summary>
        /// Получить список значений для параметра
        /// </summary>
        /// <param name="parameterId"></param>
        /// <returns></returns>
        [OperationContract]
        List<ParameterValueDto> GetParameterValues(long parameterId);

        /// <summary>
        /// Созранить значения параметра
        /// </summary>
        /// <param name="parameterId"></param>
        /// <param name="values"></param>
        [OperationContract]
        void SaveParameterValues(long parameterId, List<ParameterValueDto> values);

        [OperationContract]
        List<JobDto> GetJobSettings();

        [OperationContract]
        void AddJob(JobDto job);

        [OperationContract]
        void UpdateJobsPeriodAndActivity(List<JobDto> joblist);


        /// <summary>
        /// Получить все настройки для указанного пользователя в следующем порядке
        ///   1. Общие настройки
        ///   2. Настройки модуля
        ///   3. Настройки роли
        ///   При наличии одинаковых настройке по PathName будут использованы с наибольшим приритетом.
        ///   При наличии одинаковых настроек по PathName с одинаковым приоритетом будут использованы 
        ///   все настройки в понимании ИЛИ
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <returns>Список параметров</returns>
        [OperationContract]
        List<SettingDto> GetSettings(String login);

        [OperationContract]
        SettingDto GetSettingByCode(long code);

        /// <summary>
        /// Получить общие настройки
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [ReferencePreservingDataContractFormat]
        List<SettingDto> GetCommonSettings();

        /// <summary>
        /// Получить настройки модуля
        /// </summary>
        /// <param name="moduleId"></param>
        /// <returns></returns>
        [OperationContract]
        [ReferencePreservingDataContractFormat]
        List<SettingDto> GetModuleSettings(long moduleId);

        /// <summary>
        /// Получить настройки роли
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [OperationContract]
        [ReferencePreservingDataContractFormat]
        List<SettingDto> GetRoleSettings(long roleId);

        /// <summary>
        /// Сохранить общие настройки 
        /// </summary>
        /// <param name="settings"></param>
        [OperationContract]
        //[ReferencePreservingDataContractFormat]
        void UpdateCommonSettings(List<SettingDto> settings);

        /// <summary>
        /// Сохранить настройки модуля
        /// </summary>
        /// <param name="moduleId">ИД модуля</param>
        /// <param name="settings"></param>
        [OperationContract]
        [ReferencePreservingDataContractFormat]
        void UpdateModuleSettings(long moduleId, List<SettingDto> settings);

        /// <summary>
        /// Сохранить настройки роли
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="settings"></param>
        [OperationContract]
        [ReferencePreservingDataContractFormat]
        void UpdateRoleSettings(long roleId, List<SettingDto> settings);
    }
}
