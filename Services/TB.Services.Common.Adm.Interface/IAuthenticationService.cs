﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using TB.Services.Infrastructure.Interface;

namespace TB.Services.Common.Adm.Interface
{
    [ServiceContract]
    public interface IAuthenticationService
    {
        [OperationContract]
        UserContext Authenticate(string userName, string password);

        [OperationContract]
        List<string> GetAllUsers();

        [OperationContract(IsOneWay = true)]
        void LogOff();

        [OperationContract(IsOneWay = true)]
        void SetContext(UserContext context);

        [OperationContract]
        UserContext GetContext(Guid sessionGuid);

        [OperationContract]
        List<UserContext> GetActiveUsers();
    }
}
