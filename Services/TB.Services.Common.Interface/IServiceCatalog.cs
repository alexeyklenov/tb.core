﻿using System.Collections.Generic;
using System.ServiceModel;

namespace TB.Services.Common.Interface
{
    [ServiceContract]
    public interface IServiceCatalog
    {
        [OperationContract]
        void AddService(string type, string endpoint);

        [OperationContract]
        void DeleteService(string type);

        [OperationContract]
        string GetServiceEndpoint(string type);

        [OperationContract]
        Dictionary<string, string> Endpoints();
    }
}
