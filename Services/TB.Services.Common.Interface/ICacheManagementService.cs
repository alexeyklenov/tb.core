﻿using System.Collections.Generic;
using System.ServiceModel;

namespace TB.Services.Common.Interface
{
    /// <summary>
    /// Управление кэшем
    /// </summary>
    [ServiceContract]
    public interface ICacheManagementService
    {
        /// <summary>
        /// Очистить кэш
        /// </summary>
        [OperationContract]
        void ClearAll();

        [OperationContract]
        List<string> GetStatistics(string name, string type);

        /// <summary>
        /// Добавляет в управляющий сервис адреса доменных служб
        /// </summary>
        /// <param name="endpoint"></param>
        [OperationContract]
        void AddService(string endpoint);

        // void ClearRegion(String region);

    }
}
