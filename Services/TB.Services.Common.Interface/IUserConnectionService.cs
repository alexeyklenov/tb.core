﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using TB.Services.Infrastructure.Interface;

namespace TB.Services.Common.Interface
{
    /// <summary>
    /// Предоставляет единое хранилище данных о клиентских сеансах
    /// </summary>
    [ServiceContract]
    public interface IUserConnectionsService
    {
        /// <summary>
        /// Регистрирует в службе клиентский сеанс. Добавляемый контекст замещает существующий контекст для данного клиентского сеанса.
        /// </summary>
        /// <param name="context">Контекст регистрируемого клиентского сеанса</param>
        [OperationContract]
        void Register(UserContext context);

        /// <summary>
        /// Удаляет клиентский сеанс 
        /// </summary>
        /// <param name="context">Контекст удаляемого клиентского сеанса</param>
        [OperationContract]
        void Unregister(UserContext context);

        /// <summary>
        /// Получает контекст клиенского сеанса по идентификатору
        /// </summary>
        /// <param name="sessionId">Идентификатор сеанса</param>
        /// <returns>Контекст клиентского сеанса</returns>
        [OperationContract]
        UserContext GetContext(Guid sessionId);

        /// <summary>
        /// Получает список активных клиентских сеансов
        /// </summary>
        /// <returns>Список контекстов активных клиентских сеансов</returns>
        [OperationContract]
        List<UserContext> GetActiveUsers();
    }
}
