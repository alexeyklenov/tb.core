﻿using System;
using System.ServiceModel;
using TB.BOL.Interface;
using TB.Configuration;

namespace TB.Services.Common.Interface
{
    [ServiceContract]
    [ServiceKnownType("GetKnownTypes", typeof(KnownAssemblies))]
    public interface IExternalChangerService
    {
        [OperationContract]
        Object Change(IExternallyChangeable entity);
    }
}
