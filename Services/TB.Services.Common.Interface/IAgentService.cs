﻿using System.Collections.Generic;
using System.ServiceModel;
using TB.Services.Common.Dto.Administration;

namespace TB.Services.Common.Interface
{
    [ServiceContract]
    public interface IAgentService
    {
        [OperationContract]
        void Refresh();

        [OperationContract]
        void ReloadTimers();

        [OperationContract]
        List<JobDto> GetActiveJobs();

        [OperationContract]
        List<JobDto> GetAllJobs();

        [OperationContract]
        void UpdateJobs(List<JobDto> jobs);
    }
}
