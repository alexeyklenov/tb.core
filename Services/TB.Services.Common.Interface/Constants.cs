﻿namespace TB.Services.Common.Interface
{
    public class Constants
    {
        public const long MaxReceivedMessageSize = int.MaxValue;
        public const string ServerPort = "30001";
    }
}
