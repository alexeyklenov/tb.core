﻿using System.Runtime.Serialization;
using TB.Utils;

namespace TB.Services.Common.Dto.Administration
{
    [DataContract]
    public class SettingValueDto : EqualOverrideBase
    {
        [DataMember]
        public ParameterDto Parameter { get; set; }

        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long No { get; set; }

        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Note { get; set; }

        [DataMember]
        public string Value { get; set; }

        public override int GetHashCode()
        {
            return HashOf(Value);
        }
    }
}
