﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using TB.Utils;

namespace TB.Services.Common.Dto.Administration
{
    /// <summary>
    /// Пользователь системы
    /// </summary>
    [DataContract]
    public class UserDto : EqualOverrideBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Login { get; set; }
        [DataMember]
        public string DisplayName { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string NetworkName { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Note { get; set; }

        [DataMember]
        public List<long> RoleIds { get; set; }

        public long? FirstRoleId
        {
            get { return RoleIds == null || !RoleIds.Any() ? (long?) null : RoleIds.First(); }
            set
            {
                if(!value.HasValue)
                    return;
                if (RoleIds == null || !RoleIds.Any())
                {
                    RoleIds = new List<long> {value.Value};
                    return;
                }
                RoleIds[0] = (long)value;

            }
        }

        [DataMember]
        public long? EmployeeId { get; set; }

        [DataMember]
        public string EmployeeName { get; set; }

        [DataMember]
        public DateTime? BeginDate { get; set; }
        [DataMember]
        public DateTime? EndDate { get; set; }

        public override int GetHashCode()
        {
            return HashOf(Login, BeginDate);
        }
    }
}
