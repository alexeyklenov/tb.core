﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using TB.Utils;

namespace TB.Services.Common.Dto.Administration
{
    [DataContract(IsReference = true)]
    public class SettingDto : EqualOverrideBase
    {
        [DataMember]
        public ParameterDto Parameter { get; set; }

        [DataMember]
        public long Id { get; set; }

        /// <summary>
        /// Порядковый номер настройки
        /// </summary>
        [DataMember]
        public int No { get; set; }

        [DataMember]
        public long Code { get; set; }

        [DataMember]
        public int Height { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Note { get; set; }

        [DataMember]
        public string Value { get; set; }

        [DataMember]
        public ParameterValueDto ParameterValue { get; set; }

        //TODO - сделать descriptionDto
        [DataMember]
        public string ReferenceValue { get; set; }

        [DataMember]
        public long ReferenceId { get; set; }

        [DataMember]
        public long ReferenceTypeId { get; set; }

        [DataMember]
        public long ReferenceTypeCode { get; set; }

        [DataMember]
        public string ReferenceOuterCode { get; set; }

        [DataMember]
        public SettingDto Parent { get; set; }

        [DataMember]
        public List<SettingDto> Children { get; set; }

        [DataMember]
        public Boolean? IsGlobal { get; set; }

        [DataMember]
        public long ModuleId { get; set; }

        [DataMember]
        public long RoleId { get; set; }

        public override int GetHashCode()
        {
            return HashOf(Value);
        }

    }
}
