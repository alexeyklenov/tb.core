﻿using System.ComponentModel;
using System.Runtime.Serialization;
using TB.Utils;

namespace TB.Services.Common.Dto.Administration
{
    [DataContract]
    public class ActionDto : EqualOverrideBase
    {
        public ActionDto()
        {
            //       Childs = new List<ActionDTO>();
        }

        [DataMember]
        [DisplayName("ИД действия")]
        [ReadOnly(true)]
        public long ActionId { get; set; }

        [DataMember]
        [DisplayName("ИД действия в модуле")]
        [ReadOnly(true)]
        public long ModuleActionId { get; set; }

        [DataMember]
        [DisplayName("действие-родитель")]
        public ActionDto Parent { get; set; }

        public long? ParentId
        {
            get { return Parent != null ? Parent.ModuleActionId : (long?)null; }

        }

        [DataMember]
        [DisplayName("ИД модуля")]
        [ReadOnly(true)]
        public long ModuleId { get; set; }

        [DataMember]
        [DisplayName("Тип действия")]
        public long ActionTypeId { get; set; }

        [DataMember]
        [DisplayName("Наименование")]
        public string Name { get; set; }

        [DataMember]
        [DisplayName("Примечание")]
        public string Note { get; set; }

        [DataMember]
        [DisplayName("Путь в меню")]
        public string MenuItemPath { get; set; }

        [DataMember]
        [DisplayName("Параметры")]
        public string Parameters { get; set; }


        // [DataMember]
        // [DisplayName("Подчиненные действия")]
        // public List<ActionDTO> Childs { get; set; }

        public override int GetHashCode()
        {
            return HashOf(ModuleId, Name, MenuItemPath, ParentId);

        }
    }
}
