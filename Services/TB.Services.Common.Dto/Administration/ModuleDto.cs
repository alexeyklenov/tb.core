﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using TB.Utils;
using TB.Utils.Enums;

namespace TB.Services.Common.Dto.Administration
{
    [DataContract]
    public class ModuleDto : EqualOverrideBase
    {
        [DataMember]
        [DisplayName("ИД модуля")]
        [ReadOnly(true)]
        public long Id { get; set; }

        [DataMember]
        [DisplayName("Наименование")]
        public string Name { get; set; }
        [DataMember]
        [DisplayName("Примечание")]
        public string Note { get; set; }
        [DataMember]
        [DisplayName("Файл")]
        public string FileName { get; set; }

        [DataMember]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public AccessType AccessType { get; set; }

        [DataMember]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public IList<ModuleDto> Dependencies { get; set; }

        [DataMember]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public IList<RoleDto> Roles { get; set; }

        [DataMember]
        [DisplayName("Дата нач.")]
        public DateTime BeginDate { get; set; }

        [DataMember]
        [DisplayName("Дата закр.")]
        public DateTime? EndDate { get; set; }

        public override int GetHashCode()
        {
            return HashOf(Name);
        }
    }
}
