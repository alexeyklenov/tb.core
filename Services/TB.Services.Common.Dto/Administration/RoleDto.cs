﻿using System.Runtime.Serialization;
using TB.Utils;

namespace TB.Services.Common.Dto.Administration
{
    [DataContract]
    public class RoleDto : EqualOverrideBase
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Note { get; set; }

        public override int GetHashCode()
        {
            return HashOf(Name);
        }
    }
}
