﻿using System.Runtime.Serialization;
using TB.Utils;

namespace TB.Services.Common.Dto.Administration
{
    [DataContract(IsReference = true)]
    public class ParameterValueDto : EqualOverrideBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long Code { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Note { get; set; }

        //   [DataMember]
        //   public ParameterDTO Parameter { get; set; }


        public override int GetHashCode()
        {
            return HashOf(Id);
        }

    }
}
