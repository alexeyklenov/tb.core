﻿using System;
using System.Runtime.Serialization;

namespace TB.Services.Common.Dto.Administration
{
    [DataContract]
    public class JobDto
    {
        [DataMember]
        public string JobCode { get; set; }
        [DataMember]
        public string AgentCode { get; set; }

        [DataMember]
        public string AgentName { get; set; }

        [DataMember]
        public string JobName { get; set; }

        [DataMember]
        public bool IsActive { get; set; }

        [DataMember]
        public bool IsWorking { get; set; }

        [DataMember]
        public TimeSpan Period { get; set; }
    }
}