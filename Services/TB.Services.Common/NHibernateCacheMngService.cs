﻿using System;
using System.Collections.Generic;
using NHibernate;
using TB.DAL.Interface;
using TB.Services.Common.Interface;

namespace TB.Services.Common
{
    public class NHibernateCacheMngService : ICacheManagementService
    {
        private readonly ISessionFactory _sf = SessionManager.Instance.GetSessionFactoryFor(null);

        #region ICacheManagementService Members

        public void ClearAll()
        {
            _sf.EvictQueries();
            foreach (var collectionMetadata in _sf.GetAllCollectionMetadata())
                _sf.EvictCollection(collectionMetadata.Key);
            foreach (var classMetadata in _sf.GetAllClassMetadata())
                _sf.EvictEntity(classMetadata.Key);
        }

        public List<string> GetStatistics(string name, string type)
        {
            var __st = _sf.Statistics;
            var __b = new List<string>();
            switch (type)
            {
                case "All":
                    {
                        __b.Add("Общая статистика");
                        __b.Add(string.Format("start time: {0}", __st.StartTime.ToString("o")));
                        __b.Add("sessions opened: " + __st.SessionOpenCount);
                        __b.Add("sessions closed: " + __st.SessionCloseCount);
                        __b.Add("transactions: " + __st.TransactionCount);
                        __b.Add("successful transactions: " + __st.SuccessfulTransactionCount);
                        __b.Add("optimistic lock failures: " + __st.OptimisticFailureCount);
                        __b.Add("flushes: " + __st.FlushCount);
                        __b.Add("connections obtained: " + __st.ConnectCount);
                        __b.Add("statements prepared: " + __st.PrepareStatementCount);
                        __b.Add("statements closed: " + __st.CloseStatementCount);
                        __b.Add("second level cache puts: " + __st.SecondLevelCachePutCount);
                        __b.Add("second level cache hits: " + __st.SecondLevelCacheHitCount);
                        __b.Add("second level cache misses: " + __st.SecondLevelCacheMissCount);
                        __b.Add("entities loaded: " + __st.EntityLoadCount);
                        __b.Add("entities updated: " + __st.EntityUpdateCount);
                        __b.Add("entities inserted: " + __st.EntityInsertCount);
                        __b.Add("entities deleted: " + __st.EntityDeleteCount);
                        __b.Add("entities fetched (minimize this): " + __st.EntityFetchCount);
                        __b.Add("collections loaded: " + __st.CollectionLoadCount);
                        __b.Add("collections updated: " + __st.CollectionUpdateCount);
                        __b.Add("collections removed: " + __st.CollectionRemoveCount);
                        __b.Add("collections recreated: " + __st.CollectionRecreateCount);
                        __b.Add("collections fetched (minimize this): " + __st.CollectionFetchCount);
                        __b.Add("queries executed to database: " + __st.QueryExecutionCount);
                        __b.Add("query cache puts: " + __st.QueryCachePutCount);
                        __b.Add("query cache hits: " + __st.QueryCacheHitCount);
                        __b.Add("query cache misses: " + __st.QueryCacheMissCount);
                        __b.Add("max query time: " + __st.QueryExecutionMaxTime.Milliseconds + "ms");
                        break;
                    }

                case "Entity":
                    {
                        __b.Add(string.Format("Статистика по объекту {0}", name));
                        var __ec = __st.GetEntityStatistics(name);
                        if (__ec != null)
                        {
                            __b.Add(string.Format("Статистика по объекту {0}", name));
                            __b.Add("LoadCount: " + __ec.LoadCount);
                            __b.Add("UpdateCount: " + __ec.UpdateCount);
                            __b.Add("InsertCount: " + __ec.InsertCount);
                            __b.Add("DeleteCount: " + __ec.DeleteCount);
                            __b.Add("FetchCount: " + __ec.FetchCount);
                            __b.Add("OptimisticFailureCount: " + __ec.OptimisticFailureCount);
                        }
                        else
                        {
                            __b.Add("Несуществует объект или нет статистики");
                        }
                        break;
                    }
                case "Query":
                    {
                        var __qs = __st.GetQueryStatistics(name);
                        __b.Add(string.Format("Статистика по категории запроса {0}", name));
                        if (__qs != null)
                        {
                            __b.Add("CacheHitCount: " + __qs.CacheHitCount);
                            __b.Add("CacheMissCount: " + __qs.CacheMissCount);
                            __b.Add("CachePutCount: " + __qs.CachePutCount);
                            __b.Add("ExecutionCount: " + __qs.ExecutionCount);
                            __b.Add("ExecutionRowCount: " + __qs.ExecutionRowCount);
                            __b.Add("ExecutionAvgTime: " + __qs.ExecutionAvgTime);
                            __b.Add("ExecutionMaxTime: " + __qs.ExecutionMaxTime);
                            __b.Add("ExecutionMinTime: " + __qs.ExecutionMinTime);
                        }
                        else
                        {
                            __b.Add("Несуществует объект или нет статистики");
                        }
                        break;
                    }
                case "Cache":
                    {
                        var __qs = __st.GetSecondLevelCacheStatistics(name);
                        __b.Add(string.Format("Статистика по кэша региону {0}", name));
                        if (__qs != null)
                        {
                            __b.Add("HitCount: " + __qs.HitCount);
                            __b.Add("MissCount: " + __qs.MissCount);
                            __b.Add("PutCount: " + __qs.PutCount);
                            __b.Add("ElementCountInMemory: " + __qs.ElementCountInMemory);
                            __b.Add("ElementCountOnDisk: " + __qs.ElementCountOnDisk);
                            __b.Add("ElementCountOnDisk: " + __qs.ElementCountOnDisk);
                            __b.Add("SizeInMemory: " + __qs.SizeInMemory);
                        }
                        else
                        {
                            __b.Add("Несуществует объект или нет статистики");
                        }

                        break;
                    }
                case "Collection":
                    {
                        var __qs = __st.GetCollectionStatistics(name);
                        __b.Add(string.Format("Статистика по коллекции {0}", name));
                        if (__qs != null)
                        {
                            __b.Add("LoadCount: " + __qs.LoadCount);
                            __b.Add("FetchCount: " + __qs.FetchCount);
                            __b.Add("UpdateCount: " + __qs.UpdateCount);
                            __b.Add("RemoveCount: " + __qs.RemoveCount);
                            __b.Add("RecreateCount: " + __qs.RecreateCount);
                        }
                        else
                        {
                            __b.Add("Несуществует объект или нет статистики");
                        }

                        break;
                    }
            }
            return __b;
        }

        public void AddService(string endpoint)
        {
            throw new NotImplementedException();
        }

        public void ClearRegion(string region)
        {
            _sf.EvictQueries(region);

        }

        #endregion

    }
}
