﻿using System.Collections.Generic;
using System.ServiceModel;
using log4net;
using TB.Services.Common.Interface;
using TB.Services.Infrastructure.Client;

namespace TB.Services.Common
{
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, InstanceContextMode = InstanceContextMode.Single)]
    public class CacheManagementService : ICacheManagementService
    {
        List<string> _services = new List<string>();


        #region ICacheManagementService Members

        public void ClearAll()
        {
            foreach (var cacheManagementService in _services)
            {
                using (var client = new ClientProxy<ICacheManagementService>(new NetNamedPipeBinding(NetNamedPipeSecurityMode.None), cacheManagementService))
                {
                    client.Channel.ClearAll();
                }
            }
        }

        public List<string> GetStatistics(string name, string type)
        {
            LogManager.GetLogger("CacheMngService").DebugFormat("GetStatistics :{0},{1}", type, name);
            var b = new List<string>();
            foreach (var cacheManagementService in _services)
            {
                LogManager.GetLogger("CacheMngService").DebugFormat("GetStatistics {0}", cacheManagementService);
                using (var client = new ClientProxy<ICacheManagementService>(new NetNamedPipeBinding(NetNamedPipeSecurityMode.None), cacheManagementService))
                {
                    b.Add("Статистика для :" + cacheManagementService);
                    b.AddRange(client.Channel.GetStatistics(name, type));
                }
            }
            return b;
        }

        public void ClearRegion(string region)
        {
            /*
            _sf.EvictQueries(region);
            foreach (var __cacheManagementService in _services)
            {
                using (var __client = new ClientProxy<ICacheManagementService>(__cacheManagementService))
                {
                    __client.Channel.EvictQueries();
                }
            }
             */
        }
        public void AddService(string address)
        {
            LogManager.GetLogger("CacheMngService").DebugFormat("Added {0}", address);

            _services.Add(address);
        }

        #endregion
    }
}
