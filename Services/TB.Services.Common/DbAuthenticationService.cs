﻿using System;
using System.Collections.Generic;
using System.Linq;
using TB.Services.Common.Adm.Interface;
using TB.Services.Infrastructure.Behaviors;
using TB.Services.Infrastructure.Interface;
using Utils;

namespace TB.Services.Common
{
    //[UserContextBehavior]
    [ServerService]
    public class DbAuthenticationService : AuthenticationServiceBase
    {
        readonly IAdministrationService _administrationService = new DbAdministrationService();

        public override UserContext Authenticate(string userName, string password)
        {
            var userContext = new UserContext
            {
                Login = userName,
                IsAuthenticated = false
            };
            if (_administrationService.ValidateUser(userName, password))
            {
                var user = _administrationService.GetUserByLogin(userName, DateTime.Now);
                userContext.Context["UserID"] = user.Id;
                userContext.Description = user.Note;
                userContext.DisplayName = user.DisplayName;
                userContext.Name = user.DisplayName;
                userContext.SessionGuid = Guid.NewGuid();
                userContext.IsAuthenticated = true;
                var roles = _administrationService.GetUserRoles(user.Id);
                userContext.Roles = roles.Select(x => x.Name).ToList();
                userContext.RolesIds = roles.Select(x => x.Id).ToList();

                SetContext(userContext);

                if (Log.IsInfoEnabled)
                    Log.Info(string.Format("Проверка пользователя {0}. вход сессия {1}", userContext.Login,
                        userContext.SessionGuid.ToRawString()));
            }
            return userContext;
        }

        public override List<string> GetAllUsers()
        {
            return _administrationService.GetAllActive().Select(u => u.Login).ToList();
        }
    }
}
