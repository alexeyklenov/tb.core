﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Unity;
using TB.BOL.Administration;
using TB.BOL.Descriptions;
using TB.DAL.Interface;
using TB.Services.Common.Adm.Interface;
using TB.Services.Common.Dto.Administration;
using TB.Services.Infrastructure.Attributes;
using TB.Services.Infrastructure.Behaviors;
using Utils;
using Parameter = TB.BOL.Administration.Parameter;

namespace TB.Services.Common
{
    [UserContextBehavior]
    [ServerService]
    public class SettingService : ISettingService
    {
        private readonly IAdministrationService _adminserv;
        private readonly IDao<Parameter> _paramDao;
        private readonly IDao<ParameterValue> _paramvalDao;
        private readonly IDao<Setting> _settingDao;
        private readonly IDao<Module> _moduleDao;
        private readonly IDao<Role> _roleDao;
        private readonly IDao<JobSetting> _jobSetDao;
        private readonly IDao<DescriptionValue> _descValDao;

        private Module _module;
        private Role _role;
        public SettingService()
        {
            _adminserv = new DbAdministrationService();
            _paramDao = UnityContext.Container.Resolve<IDao<Parameter>>();
            _paramvalDao = UnityContext.Container.Resolve<IDao<ParameterValue>>();
            _settingDao = UnityContext.Container.Resolve<IDao<Setting>>();
            _moduleDao = UnityContext.Container.Resolve<IDao<Module>>();
            _roleDao = UnityContext.Container.Resolve<IDao<Role>>();
            _jobSetDao = UnityContext.Container.Resolve<IDao<JobSetting>>();
            _descValDao = UnityContext.Container.Resolve<IDao<DescriptionValue>>();

            Parameter param = null;

            Mapper.CreateMap<Parameter, ParameterDto>();

            Mapper.CreateMap<ParameterDto, Parameter>()
                .BeforeMap((s, d) =>
                {
                    param = s.Id != 0 ? _paramDao.GetById(s.Id, true) : new Parameter();
                })
                .ForMember(r => r.Values, opt => opt.MapFrom(s => param.Values))
                .ForMember(a => a.Guid, opt => opt.Ignore())
                .ForMember(a=>a.ReferenceType, opt=>opt.Ignore())
                .ForMember(a => a.TypeName, opt => opt.Ignore());

            Mapper.CreateMap<ParameterValue, ParameterValueDto>();

            Mapper.CreateMap<ParameterValueDto, ParameterValue>()
                .ForMember(a => a.Guid, opt => opt.Ignore())
                .ForMember(a => a.Parameter, opt => opt.Ignore())
                .ForMember(a => a.TypeName, opt => opt.Ignore());


            Mapper.CreateMap<Setting, SettingDto>()
                .ForMember(r => r.Id, opt => opt.MapFrom(s => s.Id))
                .ForMember(a => a.ParameterValue, opt => opt.MapFrom(s => s.ParameterValue))
                .ForMember(a => a.ReferenceValue,
                    opt => opt.MapFrom(s => s.ReferenceValue == null ? "" : s.ReferenceValue.ResultShortName))
                .ForMember(a => a.ReferenceId,
                    opt => opt.MapFrom(s => s.ReferenceValue == null ? 0 : s.ReferenceValue.Id))
                .ForMember(a => a.ReferenceOuterCode,
                    opt => opt.MapFrom(s => s.ReferenceValue == null ? "" : s.ReferenceValue.OuterCode))
                .ForMember(a => a.ReferenceTypeId,
                    opt => opt.MapFrom(s => s.ReferenceType == null ? 0 : s.ReferenceType.Id))
                .ForMember(a => a.ReferenceTypeCode,
                    opt => opt.MapFrom(s => s.ReferenceType == null ? 0 : s.ReferenceType.Code));
            
            //Mapper.CreateMap<Setting, SettingValueDto>();

            Mapper.CreateMap<SettingDto, Setting>()
                .ForMember(r => r.Id, opt => opt.MapFrom(s => s.Id))
                .ForMember(r => r.Name, opt => opt.Ignore())
                .ForMember(a => a.Guid, opt => opt.Ignore())
                .ForMember(a => a.Module, opt => opt.MapFrom(s => _module))
                .ForMember(a => a.Role, opt => opt.MapFrom(s => _role))
                .ForMember(a => a.Children, opt => opt.Ignore())
                .ForMember(a => a.Parameter, opt => opt.MapFrom(s => s.Parameter))
                .ForMember(a => a.TypeName, opt => opt.Ignore())
                .ForMember(a => a.ParameterValue, opt => opt.MapFrom(s => s.ParameterValue))
                .ForMember(a => a.ReferenceValue,
                    opt => opt.MapFrom(s => s.ReferenceId == 0 ? null : _descValDao.GetById(s.ReferenceId, false)))
                .ForMember(a => a.ReferenceType, opt => opt.Ignore());

            Mapper.CreateMap<JobSetting, JobDto>()
                .ForMember(a => a.IsWorking, opt => opt.Ignore());

            Mapper.CreateMap<JobDto, JobSetting>().ForMember(a => a.Id, opt => opt.Ignore());

            Mapper.AssertConfigurationIsValid();

        }


        #region ISettingService Members

        public List<ParameterDto> GetParameters()
        {
            return _paramDao.GetAll().ConvertAll(Mapper.Map<Parameter, ParameterDto>);
        }

        public ParameterDto GetParameter(long parameterId)
        {
            var param = _paramDao.GetById(parameterId, false);
            return Mapper.Map<Parameter, ParameterDto>(param);

        }

        public ParameterDto SaveParameter(ParameterDto parameter)
        {
            var p = _paramDao.SaveOrUpdateCopy(Mapper.Map<ParameterDto, Parameter>(parameter));
            _paramDao.CommitChanges();
            return Mapper.Map<Parameter, ParameterDto>(p);

        }

        /// <summary>
        /// Получить список значений для параметра
        /// </summary>
        /// <param name="parameterId"></param>
        /// <returns></returns>
        public List<ParameterValueDto> GetParameterValues(long parameterId)
        {
            var param = _paramDao.GetById(parameterId, false);
            return param.Values.ToList().ConvertAll(Mapper.Map<ParameterValue, ParameterValueDto>);

        }

        /// <summary>
        /// Созранить значения параметра
        /// </summary>
        /// <param name="parameterId"></param>
        /// <param name="values"></param>
        public void SaveParameterValues(long parameterId, List<ParameterValueDto> values)
        {
            var param = _paramDao.GetById(parameterId, false);
            foreach (var v in param.Values)
            {
                _paramvalDao.Evict(v);
            }
            param.Values.Clear();
            foreach (var v in values.Select(Mapper.Map<ParameterValueDto, ParameterValue>))
            {
                v.Parameter = param;
                param.Values.Add(v);
            }
            _paramDao.CommitChanges();
        }


        public List<JobDto> GetJobSettings()
        {
            return _jobSetDao.GetAll().ConvertAll(Mapper.Map<JobSetting, JobDto>);
        }

        public void AddJob(JobDto job)
        {
            var nJob = Mapper.Map<JobDto, JobSetting>(job);
            _jobSetDao.Save(nJob);
            _jobSetDao.CommitChanges();
        }

        public void UpdateJobsPeriodAndActivity(List<JobDto> joblist)
        {
            var jobs = _jobSetDao.GetAll();
            foreach (var jobDto in joblist)
            {
                var job = jobs.FirstOrDefault(j => j.JobCode == jobDto.JobCode && j.AgentCode == jobDto.AgentCode);
                if(job==null)
                    continue;

                job.IsActive = jobDto.IsActive;
                job.Period = jobDto.Period;
            }

            _jobSetDao.CommitChanges();
        }

        public SettingDto GetSettingByCode(long code)
        {
            var set = _settingDao.Linq().FirstOrDefault(s => s.Code == code);
            return set == null ? null : Mapper.Map<Setting, SettingDto>(set);
        }

        public List<SettingDto> GetAllSettings()
        {
            return _settingDao.GetAll().ConvertAll(Mapper.Map<Setting, SettingDto>);
        }

        /// <summary>
        /// Получить все настройки для указанного пользователя в следующем порядке
        ///   1. Общие настройки
        ///   2. Настройки модуля
        ///   3. Настройки роли
        ///   При наличии одинаковых настройке по Name будут использованы с наибольшим приритетом.
        ///   При наличии одинаковых настроек по Name с одинаковым приоритетом будут использованы 
        ///   все настройки в понимании ИЛИ
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <returns>Список параметров</returns>
        public List<SettingDto> GetSettings(string login)
        {
            var settings = new List<SettingDto>();
            var user = _adminserv.GetUserByLogin(login, DateTime.Now);
            var modules = _adminserv.GetUserModules(user.Id);
            var roles = _adminserv.GetUserRoles(user.Id);
            var all = _settingDao.GetAll();

            // Общие настройки
            var commonSettings = (from s in all
                                    where s.Module == null && s.Role == null
                                    select s).ToList();

            // Настройки модулей
            var ms = (
                from s in all
                where modules.Contains(Mapper.Map<Module, ModuleDto>(s.Module))
                select s
                ).ToList();


            // Настройки ролей
            var rs = (
                from s in all
                where roles.Contains(Mapper.Map<Role, RoleDto>(s.Role))
                select s
                ).ToList();

            //Добавляю те настройки модулей, которых нет в настройках ролей
            foreach (var s in ms.GroupBy(s => s.Name).Where(s => !rs.Exists(r => r.Name == s.Key)))
            {
                rs.AddRange((from e in ms
                    where e.Name == s.Key
                    select e));
            }
            //Добавляю те общие настройки, которых нет в настройках ролей и настройках модулей
            foreach (var s in commonSettings.GroupBy(s => s.Name).Where(s => !rs.Exists(r => r.Name == s.Key)))
            {
                rs.AddRange((from e in commonSettings
                    where e.Name == s.Key
                    select e));
            }
            settings.AddRange(rs.ConvertAll(Mapper.Map<Setting, SettingDto>));

            return settings;
        }
        [ReferencePreservingDataContractFormat]
        public List<SettingDto> GetCommonSettings()
        {
            return GetSpecificSettingsDto(0, 0);
        }

        [ReferencePreservingDataContractFormat]
        public List<SettingDto> GetModuleSettings(long moduleId)
        {
            return GetSpecificSettingsDto(moduleId, 0);

        }


        [ReferencePreservingDataContractFormat]
        public List<SettingDto> GetRoleSettings(long roleId)
        {
            return GetSpecificSettingsDto(0, roleId);
        }
        //[ReferencePreservingDataContractFormat]
        public void UpdateCommonSettings(List<SettingDto> settings)
        {
            _module = null;
            _role = null;
            UpdateSettings(settings);
        }

        [ReferencePreservingDataContractFormat]
        public void UpdateModuleSettings(long moduleId, List<SettingDto> settings)
        {
            _module = _moduleDao.GetById(moduleId, false);
            _role = null;
            UpdateSettings(settings);
        }

        //[ReferencePreservingDataContractFormat]
        private void UpdateSettings(IEnumerable<SettingDto> settings)
        {
            UpdateSettings(settings, null);
            _settingDao.CommitChanges();
        }

        /// <summary>
        /// Сохранить настройки
        /// </summary>
        /// <param name="settings">Текущие настройки</param>
        /// <param name="parent"></param>
        private void UpdateSettings(IEnumerable<SettingDto> settings, Setting parent)
        {
            var setIds = settings.Select(sd => sd.Id).ToList();
            var updSettings = _settingDao.Linq().Where(s => setIds.Contains(s.Id)).ToList();
            
            updSettings.ForEach(s => Mapper.Map(settings.First(sd => sd.Id == s.Id), s));

            /*foreach (var s in settings)
            {
                var sObj = Mapper.Map<SettingDto, Setting>(s);
                if (parent == null)
                    updSettings.Add(sObj);
                sObj.Parent = parent;
                sObj.Module = _module;
                sObj.Role = _role;
                if (parent != null)
                    parent.Children.Add(sObj);
                if (s.Children != null && s.Children.Any())
                    UpdateSettings(s.Children, sObj);

            }*/

            if (_role != null)
            {
                var roleBaseSets = updSettings.Where(s => s.IsGlobal == null || !s.IsGlobal.Value).ToList();

                setIds = roleBaseSets.Select(s => s.Id).ToList();
                var roleSets =
                    DaoContainer.Dao<RoleSettingValue>()
                        .Linq()
                        .Where(rsv => rsv.Role.Id == _role.Id && setIds.Contains(rsv.Setting.Id)).ToList();
                foreach (var set in roleBaseSets)
                {
                    var roleSet = roleSets.FirstOrDefault(rsv => rsv.Setting.Id == set.Id) ??
                                  new RoleSettingValue {Setting = set, Role = _role};

                    roleSet.ReferenceValue = set.ReferenceValue;
                    roleSet.Value = set.Value;
                    roleSet.Parameter = set.Parameter;
                    roleSet.ParameterValue = set.ParameterValue;

                    if (roleSet.Id == 0)
                        roleSets.Add(roleSet);
                }

                roleSets.ForEach(rsv => DaoContainer.Dao<RoleSettingValue>().SaveOrUpdateCopy(rsv));
                updSettings.Where(s => s.IsGlobal.HasValue && s.IsGlobal.Value)
                    .ForEach(x => _settingDao.SaveOrUpdateCopy(x));
            }
            else
                updSettings.ForEach(x => _settingDao.SaveOrUpdateCopy(x));

        }

        [ReferencePreservingDataContractFormat]
        public void UpdateRoleSettings(long roleId, List<SettingDto> settings)
        {
            _module = null;
            _role = _roleDao.GetById(roleId, false);
            UpdateSettings(settings);
        }


        #endregion

        private List<SettingDto> GetSpecificSettingsDto(long moduleId, long roleId)
        {
            var settings = new List<SettingDto>();
            settings.AddRange(
                GetSpecificSettings(moduleId, roleId).ConvertAll(Mapper.Map<Setting, SettingDto>));
            return settings;
        }

        private List<Setting> GetSpecificSettings(long moduleId, long roleId)
        {
            var setQuery = _settingDao.Linq();
            setQuery = moduleId != 0
                ? setQuery.Where(s => s.Module == null)
                : setQuery.Where(s => s.Module == null || s.Module.Id == moduleId);

            setQuery = roleId != 0
               ? setQuery.Where(s => s.Role == null)
               : setQuery.Where(s => s.Role== null || s.Role.Id == roleId);

            var settings = setQuery.ToList();

            var commonSetIds = settings.Select(s => s.Id).ToList();

            var roleValues = (from v in DaoContainer.Dao<RoleSettingValue>().Linq()
                where 
                    v.Role.Id == roleId && commonSetIds.Contains(v.Setting.Id)
                select v).ToList();

            settings.ForEach(s =>
            {
                var roleSet = roleValues.FirstOrDefault(v => v.Setting.Id == s.Id);
                if (roleSet != null)
                {
                    s.Value = roleSet.Value;
                    s.Parameter = roleSet.Parameter;
                    s.ParameterValue = roleSet.ParameterValue;
                    s.ReferenceValue = roleSet.ReferenceValue;
                    s.Role = roleSet.Role;
                }
            });

            return settings;
        }
    }
}
