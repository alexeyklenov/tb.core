﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using log4net;
using Microsoft.Practices.Unity;
using NHibernate;
using TB.BOL.Administration;
using TB.DAL.Interface;
using TB.Services.Common.Adm.Interface;
using TB.Services.Common.Interface;
using TB.Services.Infrastructure.Client;
using TB.Services.Infrastructure.Interface;
using Utils;

namespace TB.Services.Common
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession)]
    public abstract class AuthenticationServiceBase : IAuthenticationService
    {
        protected ILog Log = LogManager.GetLogger("AuthService");
        protected IDao<Session> Dao;

        private readonly string _endpointUcs = string.Format("net.tcp://localhost:{0}/IUserConnectionsService",
                                                    AppDomain.CurrentDomain.GetData("ServerPort"));

        public AuthenticationServiceBase()
        {
            Dao = UnityContext.Container.Resolve<IDao<Session>>();
        }

        #region IAuthenticationService Members

        public bool MockAuthenticate(string userName, string password)
        {
            return true;
        }

        public abstract UserContext Authenticate(string userName, string password);

        public abstract List<string> GetAllUsers();

        public void LogOff()
        {
            LogManager.GetLogger("AuthenticationServiceBase").Debug("Logging off");
            using (
                var userConnectionService =
                    new ClientProxy<IUserConnectionsService>(_endpointUcs))
            {
                userConnectionService.Channel.Unregister(UserContext.Current);
            }
        }

        public void SetContext(UserContext context)
        {
            if (context.SessionGuid == Guid.Empty) return;
            if (OperationContext.Current != null)
            {
                context.Terminal =
                    ((RemoteEndpointMessageProperty)
                     OperationContext.Current.IncomingMessageProperties[RemoteEndpointMessageProperty.Name]).Address;
                context.Action = OperationContext.Current.IncomingMessageHeaders.Action;
            }
            // Регистрируем во внутреннем хранилище
            using (
                var userConnectionService =
                    new ClientProxy<IUserConnectionsService>(_endpointUcs))
            {
                userConnectionService.Channel.Register(context);
            }
            try
            {
                if (Log.IsDebugEnabled)
                    Log.DebugFormat("Контекст. {0}", context);

                if (Dao == null)
                {
                    var exc = new Exception("_dao не должен быть null");
                    Log.ErrorFormat(exc.Message);
                    throw exc;
                }
                Session session;
                try
                {
                    session = Dao.GetById(context.SessionGuid, true);
                }
                catch (ObjectNotFoundException)
                {
                    session = new Session { Id = context.SessionGuid, StartDate = DateTime.Now };
                }

                session.Guid = session.Id;
                session.UserId = context.Login;
                session.UserName = context.DisplayName;
                session.Terminal = context.Terminal;
                session.Module = context.ClientModule;
                session.Description = context.Description;

                if (context.Context != null)
                {
                    foreach (DictionaryEntry userContextEntry in context.Context)
                    {
                        SessionContext sessionContext = null;
                        if (session.Context != null)
                            sessionContext =
                                session.Context.FirstOrDefault(
                                    x => string.Equals(x.Name, userContextEntry.Key.ToString(), StringComparison.CurrentCultureIgnoreCase));

                        if (userContextEntry.Value != null)
                        {
                            if (sessionContext != null)
                                sessionContext.Value = userContextEntry.Value.ToString();
                            else
                                session.Context.Add(new SessionContext
                                {
                                    Session = session,
                                    Name = userContextEntry.Key.ToString(),
                                    Value = userContextEntry.Value.ToString()
                                });
                        }
                    }
                }
                Dao.SaveOrUpdate(session);
                Dao.CommitChanges();
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("Ошибка установки контекста для {0}", context.Login), ex);
            }
        }

        public UserContext GetContext(Guid sessionGuid)
        {
            if (sessionGuid == Guid.Empty) return null;

            UserContext userContext;
            try
            {
                using (var userConnectionService = new ClientProxy<IUserConnectionsService>(_endpointUcs))
                {
                    userContext = userConnectionService.Channel.GetContext(sessionGuid);
                }
                if (userContext == null) // Если нет контекста внутри, попробую прочитать из базы данных
                {
                    if (Dao == null)
                    {
                        var exc = new Exception("_dao не должен быть null");
                        Log.ErrorFormat(exc.Message);
                        throw exc;
                    }
                    Session session;
                    try
                    {
                        session = Dao.GetById(sessionGuid, false);
                    }
                    catch (ObjectNotFoundException)
                    {
                        Log.ErrorFormat("Не найден контекст сессии: {0}. Создана заглушка", sessionGuid);
                        session = new Session { Id = sessionGuid, StartDate = DateTime.Now };
                    }
                    userContext = new UserContext
                    {
                        Name = session.UserId,
                        Login = session.UserId,
                        DisplayName = session.UserName,
                        SessionGuid = session.Id,
                        Terminal = session.Terminal,
                        ClientModule = session.Module,
                    };
                    foreach (var sessionContextEntry in session.Context)
                        userContext.Context[sessionContextEntry.Name] = sessionContextEntry.Value;

                    using (var userConnectionService = new ClientProxy<IUserConnectionsService>(_endpointUcs))
                    {
                        userConnectionService.Channel.Register(userContext);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка получения контекста", ex);
                throw;
            }
            return userContext;
        }

        public List<UserContext> GetActiveUsers()
        {
            using (var userConnectionService = new ClientProxy<IUserConnectionsService>(_endpointUcs))
            {
                return userConnectionService.Channel.GetActiveUsers();
            }
        }

        #endregion
    }
}
