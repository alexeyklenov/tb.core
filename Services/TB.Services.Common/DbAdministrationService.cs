﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using log4net;
using Microsoft.Practices.Unity;
using TB.BOL.Administration;
using TB.DAL.Interface;
using TB.Services.Common.Adm.Interface;
using TB.Services.Common.Dto.Administration;
using TB.Services.Infrastructure.Behaviors;
using TB.Utils;
using TB.Utils.Enums;
using Utils;
using Utils.ComponentModel;
using Action = TB.BOL.Administration.Action;
using Module = TB.BOL.Administration.Module;

namespace TB.Services.Common
{
    [UserContextBehavior]
    [ServerService]
    public class DbAdministrationService : IAdministrationService
    {
        private static object _lockobject = new object();
        //ReaderWriterLockSlim rwLock;
        protected IDao<User> Dao = null;
        protected IDao<UserEmployee> DaoUsrEmpl = null;
        protected IDao<Role> RoleDao = null;
        protected IDao<Module> ModDao = null;
        protected IDao<Action> ActionDao = null;
        protected ILog Log = LogManager.GetLogger("DBAdministration");


        public DbAdministrationService()
        {
            Dao = UnityContext.Container.Resolve<IDao<User>>();
            DaoUsrEmpl = UnityContext.Container.Resolve<IDao<UserEmployee>>();
            RoleDao = UnityContext.Container.Resolve<IDao<Role>>();
            ModDao = UnityContext.Container.Resolve<IDao<Module>>();
            ActionDao = UnityContext.Container.Resolve<IDao<Action>>();

            Role role = null;
            Module mod = null;

            lock (_lockobject)
            {
                if (Mapper.FindTypeMapFor<Role, RoleDto>() == null)
                {

                    Mapper.Reset();

                    Mapper.CreateMap<Role, RoleDto>();
                    Mapper.CreateMap<RoleDto, Role>()
                        .BeforeMap((s, d) =>
                        {
                            role = s.Id != 0 ? RoleDao.GetById(s.Id, true) : new Role();
                        })
                        .ForMember(r => r.Modules, opt => opt.MapFrom(s => role.Modules))
                        .ForMember(r => r.Users, opt => opt.MapFrom(s => role.Users))
                        .ForMember(r => r.Guid, opt => opt.Ignore())
                        .ForMember(r=>r.TypeName, opt => opt.Ignore())
                        //.ForMember(r => r.IsLoadingInProgress, opt => opt.Ignore())
                        ;
                    Mapper.CreateMap<Module, ModuleDto>()
                        .ForMember(r => r.AccessType, opt => opt.Ignore())
                        .ForMember(r => r.Roles, opt => opt.MapFrom(s => s.Access.Select(x => x.Role)));

                    // Из ModuleDto в Module
                    Mapper.CreateMap<ModuleDto, Module>()
                        .BeforeMap((s, d) =>
                        {
                            mod = s.Id != 0 ? ModDao.GetById(s.Id, true) : new Module();
                        })
                        .ForMember(m => m.Childs, opt => opt.MapFrom(s => mod.Childs))
                        .ForMember(m => m.Guid, opt => opt.Ignore())
                        .ForMember(r => r.TypeName, opt => opt.Ignore())
                        .ForMember(m => m.Actions, opt => opt.MapFrom(s => mod.Actions))
                        //.ForMember(m => m.IsLoadingInProgress, opt => opt.Ignore())
                        .ForMember(m => m.Parent, opt => opt.MapFrom(s => mod.Parent))
                        .ForMember(m => m.Dependencies, opt => opt.MapFrom(s => mod.Dependencies))
                        .ForMember(m=>m.Access, opt=>opt.Ignore())
                        //.ForMember(m => m.Roles, opt => opt.MapFrom(s => mod.Access.Select(a=>a.Role)))
                        ;
                    // Из Action в ActionDto
                    Mapper.CreateMap<Action, ActionDto>()
                        .ForMember(a => a.ActionId, opt => opt.MapFrom(src => src.Id))
                        .ForMember(a => a.ModuleId, opt => opt.Ignore())
                        .ForMember(a => a.ModuleActionId, opt => opt.Ignore())
                        .ForMember(a => a.MenuItemPath, opt => opt.Ignore())
                        .ForMember(a => a.Parameters, opt => opt.Ignore())
                        .ForMember(a => a.Parent, opt => opt.Ignore());
                    // .ForMember(a => a.Childs, opt => opt.Ignore());

                    Mapper.CreateMap<ActionDto, Action>()
                        .ForMember(a => a.Id, opt => opt.MapFrom(src => src.ActionId))
                        .ForMember(a => a.ActionType, opt =>
                                                      opt.MapFrom(src => ActionType.TypeById(src.ActionTypeId)
                                                          ))
                        .ForMember(a => a.Guid, opt => opt.Ignore())
                        .ForMember(r => r.TypeName, opt => opt.Ignore())
                        //.ForMember(r => r.IsLoadingInProgress, opt => opt.Ignore())
                        ;

                    Mapper.CreateMap<ModuleAction, ActionDto>()
                        .ForMember(a => a.ModuleActionId, opt => opt.MapFrom(src => src.Id))
                        .ForMember(a => a.ActionTypeId, opt => opt.MapFrom(src => src.Action.ActionType.Id))
                        .ForMember(a => a.Name, opt => opt.MapFrom(src => src.Action.Name))
                        .ForMember(a => a.Note, opt => opt.MapFrom(src => src.Action.Note))
                        .ForMember(a => a.ActionId, opt => opt.MapFrom(src => src.Action.Id))
                        .ForMember(a => a.ModuleId, opt => opt.MapFrom(src => src.Module.Id))
                        ;
                    Mapper.CreateMap<ActionDto, ModuleAction>()
                        .BeforeMap((s, d) =>
                        {
                            if (s.ModuleId != 0)
                            {
                                mod = ModDao.GetById(s.ModuleId, false);
                            }
                        })
                        .ForMember(a => a.Id, opt => opt.MapFrom(src => src.ModuleActionId))
                        .ForMember(a => a.Action, opt => opt.MapFrom(src =>
                            new Action
                            {
                                Id = src.ActionId,
                                Name = src.Name,
                                Note = src.Note,
                                ActionType =
                                    ActionType.TypeById(src.ActionTypeId)
                            }
                            ))
                        .ForMember(a => a.Module, opt => opt.MapFrom(dto => mod))
                        .ForMember(a => a.Guid, opt => opt.Ignore())
                        .ForMember(r => r.TypeName, opt => opt.Ignore())
                        //.ForMember(r => r.IsLoadingInProgress, opt => opt.Ignore())
                        ;


                    Mapper.AssertConfigurationIsValid();
                }
            }
            /*
            if (Mapper.FindTypeMapFor<Role, RoleDto>() == null)
            {
                Mapper.Initialize(cfg =>
                                      {
                                          cfg.CreateMap<Role, RoleDto>();
                                          cfg.CreateMap<RoleDto, Role>()
                                              .BeforeMap((s, d) =>
                                                             {
                                                                 if (s.Id != 0)
                                                                 {
                                                                     __role = _roleDAO.GetById(s.Id, true);
                                                                 }
                                                                 else
                                                                 {
                                                                     __role = new Role();
                                                                 }
                                                             })
                                              .ForMember(r => r.Modules, opt => opt.MapFrom(s => __role.Modules))
                                              .ForMember(r => r.Users, opt => opt.MapFrom(s => __role.Users))
                                              .ForMember(r => r.Guid, opt => opt.Ignore())
                                              .ForMember(r => r.IsLoadingInProgress, opt => opt.Ignore())
                                              ;
                                          cfg.CreateMap<Module, ModuleDto>().ForMember(r => r.AccessType,
                                                                                       opt => opt.Ignore());
                                          // Из ModuleDto в Module
                                          cfg.CreateMap<ModuleDto, Module>()
                                              .BeforeMap((s, d) =>
                                                             {
                                                                 if (s.Id != 0)
                                                                 {
                                                                     __mod = _modDAO.GetById(s.Id, true);
                                                                 }
                                                                 else
                                                                 {
                                                                     __mod = new Module();
                                                                 }
                                                             })
                                              .ForMember(m => m.Childs, opt => opt.MapFrom(s => __mod.Childs))
                                              .ForMember(m => m.Guid, opt => opt.Ignore())
                                              .ForMember(m => m.Actions, opt => opt.MapFrom(s => __mod.Actions))
                                              .ForMember(m => m.IsLoadingInProgress, opt => opt.Ignore())
                                              .ForMember(m => m.Parent, opt => opt.MapFrom(s => __mod.Parent))
                                              .ForMember(m => m.Dependencies,
                                                         opt => opt.MapFrom(s => __mod.Dependencies))
                                              ;
                                          // Из Action в ActionDto
                                          cfg.CreateMap<Malahit.Administration.BOL.Action, ActionDto>()
                                              .ForMember(a => a.ActionId, opt => opt.MapFrom(src => src.Id))
                                              .ForMember(a => a.ModuleId, opt => opt.Ignore())
                                              .ForMember(a => a.ModuleActionId, opt => opt.Ignore())
                                              .ForMember(a => a.MenuItemPath, opt => opt.Ignore())
                                              .ForMember(a => a.Parameters, opt => opt.Ignore())
                                              .ForMember(a => a.Parent, opt => opt.Ignore());
                                          // .ForMember(a => a.Childs, opt => opt.Ignore());

                                          cfg.CreateMap<ActionDto, Malahit.Administration.BOL.Action>()
                                              .ForMember(a => a.Id, opt => opt.MapFrom(src => src.ActionId))
                                              .ForMember(a => a.ActionType, opt =>
                                                                            opt.MapFrom(
                                                                                src =>
                                                                                ActionType.TypeById(src.ActionTypeId)
                                                                                ))
                                              .ForMember(a => a.Guid, opt => opt.Ignore())
                                              .ForMember(r => r.IsLoadingInProgress, opt => opt.Ignore());

                                          cfg.CreateMap<ModuleAction, ActionDto>()
                                              .ForMember(a => a.ModuleActionId, opt => opt.MapFrom(src => src.Id))
                                              .ForMember(a => a.ActionTypeId,
                                                         opt => opt.MapFrom(src => src.Action.ActionType.Id))
                                              .ForMember(a => a.Name, opt => opt.MapFrom(src => src.Action.Name))
                                              .ForMember(a => a.Note, opt => opt.MapFrom(src => src.Action.Note))
                                              .ForMember(a => a.ActionId, opt => opt.MapFrom(src => src.Action.Id))
                                              .ForMember(a => a.ModuleId, opt => opt.MapFrom(src => src.Module.Id))
                                              ;
                                          cfg.CreateMap<ActionDto, ModuleAction>()
                                              .BeforeMap((s, d) =>
                                                             {
                                                                 if (s.ModuleId != 0)
                                                                 {
                                                                     __mod = _modDAO.GetById(s.ModuleId, false);
                                                                 }
                                                                 else
                                                                 {
                                                                     //__mod = null;
                                                                 }
                                                             })
                                              .ForMember(a => a.Id, opt => opt.MapFrom(src => src.ModuleActionId))
                                              .ForMember(a => a.Action, opt => opt.MapFrom(src =>
                                                                                           new Malahit.BOL.
                                                                                               Administration.
                                                                                               Action
                                                                                               {
                                                                                                   Id = src.ActionId,
                                                                                                   Name = src.Name,
                                                                                                   Note = src.Note,
                                                                                                   ActionType =
                                                                                                       ActionType.
                                                                                                       TypeById(
                                                                                                           src.
                                                                                                               ActionTypeId)
                                                                                               }
                                                                                   ))
                                              .ForMember(a => a.Module, opt => opt.MapFrom(s =>
                                                                                               {
                                                                                                   return __mod;
                                                                                               }))
                                              .ForMember(a => a.Guid, opt => opt.Ignore())
                                              .ForMember(r => r.IsLoadingInProgress, opt => opt.Ignore());

                                      });
            }*/
            //Mapper.AssertConfigurationIsValId();



        }

        UserDto UserToDTO(User user)
        {
            if (user == null) 
                return null;
            
            var empl = user.Employees.Any() ? user.Employees.First() : null;
            return new UserDto
            {
                BeginDate = user.BeginDate,
                EndDate = user.EndDate,
                NetworkName = user.NetworkName,
                Note = user.Note,
                Phone = user.Phone,
                DisplayName = user.DisplayName,
                Email = user.Email,
                Login = user.Login,
                Id = user.Id,
                EmployeeId = empl == null ? (long?)null : empl.EmployeeId,
                EmployeeName = empl == null ? string.Empty : empl.Employee.Name,
                RoleIds = user.Roles.Select(r=>r.Id).ToList()
            };
        }

        #region IAdministrationService Members
        private User _GetUserByLogin(string login)
        {
            return _GetUserByLogin(login, DateTime.Now);
        }

        private User _GetUserByLogin(string login, DateTime date)
        {
            var usr = (from u in Dao.Linq()
                         where u.Login == login
                            && u.BeginDate <= date
                            && (u.EndDate == null
                                 || u.EndDate != null && u.EndDate >= date)
                         select u).FirstOrDefault();
            return usr;
        }
        public UserDto GetUserByLogin(string login, DateTime date)
        {
            return UserToDTO(_GetUserByLogin(login, date));
        }



        public UserDto CreateUser(UserDto user, string password)
        {
            var usr = _GetUserByLogin(user.Login.ToLower());
            if (usr != null)
            {
                Log.ErrorFormat("Уже существует пользователь с логином {0}", usr.Login);
                throw (new ApplicationException(string.Format("Уже существует пользователь с логином {0}", usr.Login)));
            }
            usr = new User
            {
                BeginDate = user.BeginDate ?? DateTime.Now,
                EndDate = user.EndDate,
                NetworkName = user.NetworkName,
                Note = user.Note,
                Phone = user.Phone,
                DisplayName = user.DisplayName,
                Email = user.Email,

                Login = user.Login,
                Hash = Crypt.Encrypt(password, user.Login)
            };
            var inserted = Dao.Save(usr);
            AttachEmployee(user.EmployeeId, usr);

            Dao.CommitChanges();

            if (user.RoleIds != null)
                user.RoleIds.ForEach(r => SetRoleToUser(inserted.Id, r));

            return UserToDTO(inserted);
        }

        private void AttachEmployee(long? emloyeeId, User usr)
        {
            if (emloyeeId.HasValue)
            {
                if (usr.Employees.Any())
                    usr.Employees.First().EmployeeId = emloyeeId.Value;
                else
                    usr.Employees.Add(new UserEmployee { EmployeeId = emloyeeId.Value, User = usr });
                DaoUsrEmpl.SaveOrUpdate(usr.Employees.First());
            }
            else
            {
                foreach (var empl in usr.Employees)
                {
                    DaoUsrEmpl.Delete(empl);
                }
                usr.Employees.Clear();
            }
        }


        public UserDto UpdateUser(UserDto user)
        {

            var oldusr = Dao.GetById(user.Id, true);
            if (user.BeginDate != null)
                oldusr.BeginDate = (DateTime) user.BeginDate;
            oldusr.EndDate = user.EndDate;
            oldusr.NetworkName = user.NetworkName;
            oldusr.Note = user.Note;
            oldusr.Phone = user.Phone;
            oldusr.DisplayName = user.DisplayName;
            oldusr.Email = user.Email;
            var usr = Dao.SaveOrUpdate(oldusr);
            AttachEmployee(user.EmployeeId, usr);
            Dao.CommitChanges();

            if (user.RoleIds != null)
                user.RoleIds.ForEach(r => SetRoleToUser(usr.Id, r));

            return UserToDTO(usr);


        }

        public void SetPassword(string login, string password)
        {
            var usr = _GetUserByLogin(login);
            //  _log.Debug("SetPassword:" + login + (__usr != null ? __usr.Id.ToString() : "null") + password +Crypt.Encrypt(password, __usr.Login));
            if (usr != null)
            {
                usr.Hash = Crypt.Encrypt(password, usr.Login);
                Dao.SaveOrUpdate(usr);
                Dao.CommitChanges();
            }
        }

        public bool ValidateUser(string login, string password)
        {
            try
            {
                var usr = _GetUserByLogin(login);
                if (usr != null
                    && Crypt.Decrypt(usr.Hash, usr.Login) == password)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка проверки пользователя", ex);
            }
            return false;
        }

        public void CloseUser(long userId, DateTime endDate)
        {
            var oldusr = Dao.GetById(userId, true);
            oldusr.EndDate = endDate;
            Dao.SaveOrUpdate(oldusr);
            Dao.CommitChanges();
        }

        public UserDto GetById(long userId)
        {
            return UserToDTO(Dao.GetById(userId, false));
        }

        public List<UserDto> GetAll()
        {
            return (from u in Dao.GetAll()
                    select UserToDTO(u)).ToList();
        }

        public List<UserDto> GetAllActive()
        {
            return (from u in Dao.GetAll()
                where u.EndDate == null || u.EndDate > DateTime.Now
                select UserToDTO(u)).ToList();
        }

        /// <summary>
        /// Получить роль 
        /// </summary>
        /// <param name="roleId">ИД роли</param>
        /// <returns></returns>
        public RoleDto GetRoleById(long roleId)
        {
            return Mapper.Map<Role, RoleDto>(RoleDao.GetById(roleId, false));
        }

        /// <summary>
        /// Получить роль. Считаю что наименование роли должно быть уникальным
        /// </summary>
        /// <param name="roleName">Имя роли</param>
        /// <returns></returns>
        public RoleDto GetRoleByName(string roleName)
        {
            var rl = (from r in RoleDao.Linq()
                        where r.Name == roleName
                        select r).FirstOrDefault();
            return Mapper.Map<Role, RoleDto>(rl);

        }

        /// <summary>
        /// Роли пользователя
        /// </summary>
        /// <param name="userId">ИД пользователя</param>
        /// <returns></returns>
        public List<RoleDto> GetUserRoles(long userId)
        {
            var usr = Dao.GetById(userId, false);
            return usr.Roles.Select(Mapper.Map<Role, RoleDto>).ToList();
        }

        /// <summary>
        /// Пользователи, включенные в роль
        /// </summary>
        /// <param name="roleId">ИД роли</param>
        /// <returns>Список пользователей</returns>
        public List<UserDto> GetRoleUsers(long roleId)
        {
            var role = RoleDao.GetById(roleId, false);
            return role.Users.Select(UserToDTO).ToList();

        }

        /// <summary>
        /// Получить общий список ролей
        /// </summary>
        /// <returns></returns>
        public List<RoleDto> GetRoles()
        {

            return RoleDao.GetAll().Select(Mapper.Map<Role, RoleDto>).ToList();
        }

        /// <summary>
        /// Выдать пользователю роль
        /// </summary>
        /// <param name="userId">ИД пользователя</param>
        /// <param name="roleId">ИД роли</param>
        public void SetRoleToUser(long userId, long roleId)
        {
            var usr = Dao.GetById(userId, false);
            var role = RoleDao.GetById(roleId, false);
            usr.Roles.Add(role);
            role.Users.Add(usr);
            Dao.CommitChanges();
        }

        public void RemoveRoleFromUser(long userId, long roleId)
        {
            var usr = Dao.GetById(userId, false);
            var role = RoleDao.GetById(roleId, false);
            usr.Roles.Remove(role);
            role.Users.Remove(usr);
            Dao.CommitChanges();
        }

        /// <summary>
        /// Добавить роль
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public RoleDto InsertRole(RoleDto role)
        {
            var roleObj = Mapper.Map<RoleDto, Role>(role);
            roleObj = RoleDao.SaveOrUpdateCopy(roleObj);
            RoleDao.CommitChanges();
            return Mapper.Map<Role, RoleDto>(roleObj);
        }

        /// <summary>
        /// Изменить роль
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public RoleDto UpdateRole(RoleDto role)
        {
            var roleObj = Mapper.Map<RoleDto, Role>(role);
            roleObj = RoleDao.SaveOrUpdateCopy(roleObj);
            RoleDao.CommitChanges();
            return Mapper.Map<Role, RoleDto>(roleObj);
        }

        /// <summary>
        /// Удалить роль
        /// </summary>
        /// <param name="roleId"></param>
        public void DeleteRole(long roleId)
        {
            var role = RoleDao.GetById(roleId, true);
            role.Modules.Clear();
            RoleDao.Delete(role);
            RoleDao.CommitChanges();
        }
        /// <summary>
        /// Получить модуль
        /// </summary>
        /// <param name="moduleId">ИД модуля</param>
        /// <returns></returns>
        public ModuleDto GetModuleById(long moduleId)
        {
            return Mapper.Map<Module, ModuleDto>(ModDao.GetById(moduleId, false));
        }


        /// <summary>
        /// Получить все модули
        /// </summary>
        /// <returns></returns>
        public List<ModuleDto> GetModules()
        {
            return ModDao.GetAll().Select(Mapper.Map<Module, ModuleDto>).ToList();
        }

        /// <summary>
        /// Получить список модулей, от которых зависит указанный модуль
        /// </summary>
        /// <param name="moduleId">ид модуля</param>
        /// <returns>Зависимости</returns>
        public List<ModuleDto> GetDependencies(long moduleId)
        {
            return ModDao.GetById(moduleId, false)
                .Dependencies
                .Select(Mapper.Map<Module, ModuleDto>).ToList();
        }

        /// <summary>
        /// Установить зависимости модулей
        /// </summary>
        /// <param name="moduleId">целевой модуль</param>
        /// <param name="dependencyModuleId">модуль, от которого зависит целевой</param>
        public void SetDependency(long moduleId, long dependencyModuleId)
        {
            var module = ModDao.GetById(moduleId, false);
            var depmodule = ModDao.GetById(dependencyModuleId, false);
            module.Dependencies.Add(depmodule);
            ModDao.CommitChanges();

        }

        /// <summary>
        /// Удалить зависимости модулей
        /// </summary>
        /// <param name="moduleId">целевой модуль</param>
        /// <param name="dependencyModuleId">модуль, от которого зависит целевой</param>
        public void RemoveDependency(long moduleId, long dependencyModuleId)
        {
            var module = ModDao.GetById(moduleId, false);
            var depmodule = ModDao.GetById(dependencyModuleId, false);
            if (module.Dependencies.Contains(depmodule))
                module.Dependencies.Remove(depmodule);
            ModDao.CommitChanges();

        }

        /// <summary>
        /// Модули роли
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public List<ModuleDto> GetRoleModules(long roleId)
        {
            return RoleDao.GetById(roleId, false).Modules
                .Select(t =>
                {
                    var dto = Mapper.Map<Module, ModuleDto>(t.Module);
                    dto.AccessType = t.AccessType;
                    return dto;

                }).ToList();
        }


        /// <summary>
        /// Получить модули пользователя
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<ModuleDto> GetUserModules(long userId)
        {

            var md = new List<ModuleDto>();
            foreach (var r in Dao.GetById(userId, false).Roles)
            {
                md.AddRange(
                               r.Modules
                                       .Select(t =>
                                       {
                                           var dto = Mapper.Map<Module, ModuleDto>(t.Module);
                                           dto.AccessType = t.AccessType;
                                           return dto;
                                       }));
            }
            return md;
        }



        /// <summary>
        /// Добавить новый модуль
        /// </summary>
        /// <param name="module"></param>
        /// <returns></returns>
        public ModuleDto InsertModule(ModuleDto module)
        {
            var mod = Mapper.Map<ModuleDto, Module>(module);
            mod = ModDao.SaveOrUpdateCopy(mod);
            ModDao.CommitChanges();
            return Mapper.Map<Module, ModuleDto>(mod);
        }


        /// <summary>
        /// Изменить модуль
        /// </summary>
        /// <param name="module"></param>
        /// <returns></returns>
        public ModuleDto UpdateModule(ModuleDto module)
        {
            return InsertModule(module);
        }
        /// <summary>
        /// Удалить модуль
        /// </summary>
        /// <param name="moduleId"></param>
        public void DeleteModule(long moduleId)
        {
            var mod = ModDao.GetById(moduleId, true);

            ModDao.Delete(mod);
            ModDao.CommitChanges();
        }

        /// <summary>
        /// Предоставить права роли на задачу
        /// </summary>
        /// <param name="roleId">ИД роли</param>
        /// <param name="moduleId">ИД модуля</param>
        /// <param name="access">Тип доступа</param>
        public void SetAccess(long roleId, long moduleId, AccessType access)
        {
            var role = RoleDao.GetById(roleId, false);
            var mod = ModDao.GetById(moduleId, false);
            var ac = new ModuleAccess
            {
                Role = role,
                Module = mod,
                AccessType = access
            };
            if (role.Modules.Contains(ac))
            {
                var acc = role.Modules
                    .Select(s => s).FirstOrDefault(w => w == ac);

                if (access == AccessType.None)
                    role.Modules.Remove(ac);
                else if (acc != null) 
                    acc.AccessType = ac.AccessType;
            }
            else
            {
                role.Modules.Add(ac);
            }
            RoleDao.CommitChanges();
        }

        public List<RoleDto> GetModuleRoles(long moduleId)
        {
            var roles = new List<RoleDto>();
            var modaccDao = UnityContext.Container.Resolve<IDao<ModuleAccess>>();
            var rls = (from ma in modaccDao.Linq()
                         where ma.Module.Id == moduleId
                         select ma).ToList();
            if (rls.Any())
                roles = (from r in rls
                           select Mapper.Map<Role, RoleDto>(r.Role)).ToList();

            return roles;
        }

        public List<UserDto> GetUserPartData(int index, int pageSize, List<SortDescription> sortInfo, string filter)
        {
            return (from item in Dao.GetPartData(index, pageSize, sortInfo, filter)
                    select UserToDTO(item)).ToList();
        }

        public Dictionary<object, object> GetUserTotals(string filter, List<SummaryItem> summaryInfo)
        {
            return Dao.GetTotals(filter, summaryInfo);
        }

        public object[] GetUniqueColumnValues(string name, int maxCount, string filter)
        {
            return Dao.GetUniqueColumnValues(name, maxCount, filter);
        }
        #endregion

        #region Actions

        /// <summary>
        /// Получить список типов
        /// </summary>
        /// <returns></returns>
        public Dictionary<long, string> GetActionTypes()
        {
            var dao = UnityContext.Container.Resolve<IDao<ActionType>>();
            return dao.GetAll().ToDictionary(s => s.Id, s => s.Name);

        }

        /// <summary>
        /// Получить список всех доступных действий
        /// </summary>
        /// <returns></returns>
        public List<ActionDto> GetAllActions()
        {
            return ActionDao.GetAll().Select(Mapper.Map<Action, ActionDto>).ToList();

        }

        /// <summary>
        /// Получить действие по ИД
        /// </summary>
        /// <param name="actionId">Ид действия</param>
        /// <returns></returns>
        public ActionDto GetAction(long actionId)
        {
            return Mapper.Map<Action, ActionDto>(ActionDao.GetById(actionId, false));
        }

        /// <summary>
        /// Добавить или изменить действие
        /// </summary>
        /// <param name="action">Действие</param>
        /// <returns></returns>
        public ActionDto InsertOrUpdateAction(ActionDto action)
        {
            var ac = Mapper.Map<ActionDto, Action>(action);
            ac = ActionDao.SaveOrUpdateCopy(ac);
            ActionDao.CommitChanges();
            return Mapper.Map<Action, ActionDto>(ac);

        }

        /// <summary>
        /// Удалить действие
        /// </summary>
        /// <param name="action">действие</param>
        public void DeleteAction(ActionDto action)
        {
            var ac = ActionDao.GetById(action.ActionId, true);
            ActionDao.Delete(ac);
            ActionDao.CommitChanges();
        }


        /// <summary>
        /// Получить действия, привязанные к модулю
        /// </summary>
        /// <param name="moduleId">ИД модуля</param>
        /// <returns></returns>
        public List<ActionDto> GetModuleActions(long moduleId)
        {
            var actions = ModDao.GetById(moduleId, false).Actions;
            return actions.Select(Mapper.Map<ModuleAction, ActionDto>).ToList();

        }

        /// <summary>
        /// Получить права пользователя на действия модуля (работа оболочки)
        /// </summary>
        /// <param name="moduleId">ИД модуля</param>
        /// <param name="userLogin">ИД пользователя</param>
        /// <returns></returns>
        public List<ActionDto> GetModuleUserPermissions(long moduleId, string userLogin)
        {
            var usr = _GetUserByLogin(userLogin.ToLower());
            var mod = ModDao.GetById(moduleId, false);
            var actions = usr.Roles.SelectMany(x => x.Modules).Where(x => x.Module == mod).
                SelectMany(x => x.ModuleActions).Select(Mapper.Map<ModuleAction, ActionDto>).Distinct().ToList();
            return actions;
        }

        /// <summary>
        /// Получить все доступные действия пользователя
        /// </summary>
        /// <param name="userLogin">Логин пользователя</param>
        /// <returns></returns>
        public List<ActionDto> GetUserActionPermissions(string userLogin)
        {
            var actions = new List<ActionDto>();

            var usr = _GetUserByLogin(userLogin.ToLower());

            var md = new List<ModuleAction>();
            foreach (var r in usr.Roles)
            {
                foreach (var ma in r.Modules)
                {
                    md.AddRange(ma.ModuleActions);
                }
            }
            actions.AddRange(md.Select(Mapper.Map<ModuleAction, ActionDto>).Distinct().ToList());

            return actions;
        }

        /// <summary>
        /// Проверить право пользователя на выполнение конкретной операции 
        /// </summary>
        /// <param name="userLogin">ИД пользователя</param>
        /// <param name="actionName">Название операции</param>
        /// <returns>true, если пользователь имеет право на выполнение операции, false в противном случае</returns>
        public bool CheckUserActionAccess(string userLogin, string actionName)
        {
            var user = _GetUserByLogin(userLogin.ToLower());
            return user.Roles.SelectMany(role => role.Modules).Any(moduleAccess => moduleAccess.ModuleActions.Any(x => x.Action.Name.Equals(actionName)));
        }

        /// <summary>
        /// Получить права на действия модуля у роли (настройка роли)
        /// </summary>
        /// <param name="moduleId">ИД модуля</param>
        /// <param name="roleId">ИД роли</param>
        /// <returns></returns>
        public List<ActionDto> GetModuleRolePermissions(long moduleId, long roleId)
        {
            var actions = new List<ActionDto>();
            var mod = ModDao.GetById(moduleId, false);
            var modAcc = RoleDao.GetById(roleId, false).Modules.FirstOrDefault(s => s.Module.Id == moduleId);
            if (modAcc != null && modAcc.Module == mod)
            {
                actions = modAcc.ModuleActions.Select(Mapper.Map<ModuleAction, ActionDto>).ToList();
            }
            return actions;

        }

        /// <summary>
        /// Установить привязку модулей
        /// </summary>
        /// <param name="moduleId">ИД модуля</param>
        /// <param name="actions">действия</param>
        public void SetModuleActions(long moduleId, IList<ActionDto> actions)
        {
            var mod = ModDao.GetById(moduleId, false);
            //__mod.Actions.Clear(); //Удалить все имеющиеся привязки
            var moduleActions = actions.ToList().ConvertAll(Mapper.Map<ActionDto, ModuleAction>);
            // Список действий, которые есть у модуля, но их нет во вновь поданном списке
            var todelete = mod.Actions.Where(d => !moduleActions.Contains(d)).ToList();
            //              .Except(__actions,new MyModuleActionComparer()).ToList();
            foreach (var d in todelete)
                mod.Actions.Remove(d);
            foreach (var ac in moduleActions)
            {
                /*   StringBuilder __sb = new StringBuilder();
                   StringWriter __sr = new StringWriter(__sb);
                   ObjectDumper.Write(__ac,2,__sr);
                   _log.Debug(__sb.ToString());*/
                //var __old = __mod.Actions. Where(c => c == __ac).ToList();
                if (mod.Actions.Contains(ac))
                {
                    foreach (var a in mod.Actions)
                        if (a == ac)
                        {
                            a.Parameters = ac.Parameters;
                            a.Parent = ac.Parent;
                            a.MenuItemPath = ac.MenuItemPath;
                        }
                    //__mod.Actions.IndexOf(__ac) Where(c => c == __ac).First();
                    //__old[0] = __ac;
                }
                else
                    mod.Actions.Add(ac);
            }
            ModDao.CommitChanges();
        }

        /// <summary>
        /// Установить права роли на действия модуля
        /// </summary>
        /// <param name="moduleId">ИД модуля</param>
        /// <param name="roleId">ИД роли</param>
        /// <param name="actions">действия</param>
        public void SetModuleRolePermissions(long moduleId, long roleId, List<ActionDto> actions)
        {
            var moduleActions = actions.ToList().ConvertAll(Mapper.Map<ActionDto, ModuleAction>);

            var mod = ModDao.GetById(moduleId, false);
            var modAcc = RoleDao.GetById(roleId, false).Modules.FirstOrDefault(s => s.Module.Id == moduleId);
            if (modAcc != null && modAcc.Module == mod)
            {
                modAcc.ModuleActions.Clear();
                ModDao.CommitChanges();
                foreach (var acc in moduleActions)
                {
                    var ac = mod.Actions.FirstOrDefault(s => s == acc);
                    if (ac != null)
                        modAcc.ModuleActions.Add(ac);
                }
                ModDao.CommitChanges();
                //__actions = __modAcc.ModuleActions.Select(t => Mapper.Map<ModuleAction, ActionDto>(t)).ToList();
            }
        }

        #endregion

    }
}
