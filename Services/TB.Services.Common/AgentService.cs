﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Threading;
using log4net;
using NHibernate.Util;
using TB.Configuration;
using TB.Services.Common.Dto.Administration;
using TB.Services.Common.Interface;
using TB.Services.Infrastructure.Attributes.Agent;
using TB.Services.Infrastructure.Behaviors;

namespace TB.Services.Common
{
    public class SynchroObj
    {
        public int Val;
        public bool IsWorking;
    }

    [ServerService(true)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class AgentService: IAgentService
    {
        private readonly Dictionary<MethodInfo, SynchroObj> _jobList = new Dictionary<MethodInfo, SynchroObj>();
        private readonly Dictionary<MethodInfo,Timer> _jobTimers = new Dictionary<MethodInfo, Timer>();

        protected ILog Log = LogManager.GetLogger("AgentService");

        SettingService _setSrv = new SettingService();

        public AgentService()
        {
            var agentJobs =
                KnownAssemblies.GetKnownTypes(null)
                    .Where(x => x.GetCustomAttributes(typeof (AgentAttribute), false).Any())
                    .SelectMany(x => x.GetMethods())
                    .Where(x => x.GetCustomAttributes(typeof (AgentJobAttribute), false).Any()).ToList();
            foreach (
                var agentJob in
                    agentJobs.Where(agentJob => !_jobList.ContainsKey(agentJob) && agentJob.DeclaringType != null))
            {
               _jobList.Add(agentJob, new SynchroObj());
            }

            ReloadTimers();
        }

        private void Invoke(object job)
        {
            if(!(job is MethodInfo))
                return;

            var method = job as MethodInfo;
            var synchro = _jobList[method];

            if (0 == Interlocked.Exchange(ref synchro.Val, 1))
            {
                try
                {
                    var obj = Activator.CreateInstance(method.DeclaringType, true);

                    synchro.IsWorking = true;

                    Log.Info(string.Format("started job {0}:{1}", method.DeclaringType.Name, method.Name));

                    method.Invoke(obj, new object[] { });

                    Log.Info(string.Format("finished job {0}:{1}", method.DeclaringType.Name, method.Name));

                    synchro.IsWorking = false;
                }
                catch (Exception ex)
                {
                    Log.Error(string.Format("error occured in {0}:{1}", method.DeclaringType.Name, method.Name), ex);
                }
               
                Interlocked.Exchange(ref synchro.Val, 0);
            }
        }

        public List<JobDto> GetActiveJobs()
        {
            var inWorkLst =
                _jobList.Where(j => j.Value.IsWorking)
                    .Select(j => new {JobCode = j.Key.Name, AgentCode = j.Key.DeclaringType.Name});
            return
                _setSrv.GetJobSettings()
                    .Where(j => inWorkLst.Any(i => i.AgentCode == j.AgentCode && i.JobCode == j.JobCode))
                    .ToList();
        }

        public List<JobDto> GetAllJobs()
        {
            return _setSrv.GetJobSettings();
        }

        public void UpdateJobs(List<JobDto> jobs)
        {
            _setSrv.UpdateJobsPeriodAndActivity(jobs);
            ReloadTimers();
        }

        public void ReloadTimers()
        {
            _jobTimers.ForEach(t=>
            {
                t.Value.Change(Timeout.Infinite, Timeout.Infinite);
                t.Value.Dispose();
            });
            _jobTimers.Clear();

            var jobSets = _setSrv.GetJobSettings();

            foreach (var job in _jobList)
            {
                var jobAttr = job.Key.GetCustomAttribute<AgentJobAttribute>();
                var agentAttr = job.Key.DeclaringType.GetCustomAttribute<AgentAttribute>();
                var jobSet = jobSets.FirstOrDefault(j => j.JobCode == job.Key.Name && j.AgentName == agentAttr.Name);
                if (jobSet == null)
                {
                    jobSet = new JobDto
                    {
                        JobCode = job.Key.Name,
                        JobName = jobAttr.Name,
                        AgentName = agentAttr.Name,
                        AgentCode = job.Key.DeclaringType.Name,
                        IsActive = true,
                        Period = jobAttr.DefaultPeriod
                    };
                    _setSrv.AddJob(jobSet);
                }

                if (jobSet.IsActive)
                    _jobTimers.Add(job.Key,
                        new Timer(Invoke, job.Key, TimeSpan.FromSeconds(10), jobSet.Period));
            }

        }

        public void Refresh()
        {
            foreach (var jobTimer in _jobTimers)
            {
                new Timer(Invoke, jobTimer.Key, 0, Timeout.Infinite);
            }
        }
    }
}
