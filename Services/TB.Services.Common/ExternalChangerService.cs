﻿using TB.BOL.Interface;
using TB.DAL.Interface;
using TB.Services.Common.Interface;
using TB.Services.Infrastructure.Behaviors;
using Utils;
using Microsoft.Practices.Unity;

namespace TB.Services.Common
{
    [ServerService(true)]
    public class ExternalChangerService : IExternalChangerService
    {
        private static readonly object SyncRoot = new object();

        public object Change(IExternallyChangeable entity)
        {
            lock (SyncRoot)
            {
                var daoType = typeof(IDao<>).MakeGenericType(new[] { entity.GetType() });
                var entityDao = (IDao)UnityContext.Container.Resolve(daoType);

                entity = (IExternallyChangeable)entityDao.GetById(entity.GetType(), entity.ObjectId, true);

                entity.Change();
                entityDao.SaveOrUpdate(entity);
                entityDao.CommitChanges();

                return entity;
            }
        }
    }
}
