﻿using System.Collections.Generic;
using System.ServiceModel;
using TB.Services.Common.Interface;

namespace TB.Services.Common
{
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, InstanceContextMode = InstanceContextMode.Single)]
    public class ServiceCatalog : IServiceCatalog
    {
        private static readonly Dictionary<string, string> EndpointsDict = new Dictionary<string, string>();

        #region IServiceCatalog Members

        public void AddService(string type, string endpoint)
        {
            lock (EndpointsDict)
            {
                if (!EndpointsDict.ContainsKey(type))
                    EndpointsDict.Add(type, endpoint);
                else
                    EndpointsDict[type] = endpoint;
            }
        }

        public void DeleteService(string type)
        {
            lock (EndpointsDict)
            {
                if (EndpointsDict.ContainsKey(type))
                    EndpointsDict.Remove(type);
            }
        }

        public string GetServiceEndpoint(string type)
        {
            string endpoint;
            return !EndpointsDict.TryGetValue(type, out endpoint) ? null : endpoint;
        }

        public Dictionary<string, string> Endpoints()
        {
            return EndpointsDict;
        }

        #endregion
    }
}
