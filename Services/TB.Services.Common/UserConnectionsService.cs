﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using TB.Services.Infrastructure.Behaviors;
using TB.Services.Infrastructure.Interface;
using Utils;

namespace TB.Services.Common
{
    /// <summary>
    /// Предоставляет единое хранилище данных о клиентских сеансах
    /// </summary>
    [ServerService(true)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple, IncludeExceptionDetailInFaults = true)]
    public class UserConnectionsService : Interface.IUserConnectionsService
    {
        private readonly Dictionary<Guid, UserContext> _clients = new Dictionary<Guid, UserContext>();

        private object _lock = new object();

        private const int DefaultMaxInactiveTime = 20;

        /// <summary>
        /// Регистрирует в хранилище клиентский сеанс. Добавляемый контекст замещает существующий контекст для данного клиентского сеанса.
        /// </summary>
        /// <param name="context">Контекст регистрируемого клиентского сеанса</param>
        public void Register(UserContext context)
        {
            WaitLock.Lock(_lock, 1000, () =>
            {
                context.AliveTime = DateTime.Now;
                if (!_clients.ContainsKey(context.SessionGuid))
                    _clients.Add(context.SessionGuid, context);
                else
                    _clients[context.SessionGuid] = context;
            });
        }

        /// <summary>
        /// Удаляет из хранилища клиентский сеанс 
        /// </summary>
        /// <param name="context">Контекст удаляемого клиентского сеанса</param>
        public void Unregister(UserContext context)
        {
            WaitLock.Lock(_lock, 1000, () =>
            {
                if (_clients.ContainsKey(context.SessionGuid))
                    _clients.Remove(context.SessionGuid);
            });
        }

        /// <summary>
        /// Получает контекст клиенского сеанса по идентификатору
        /// </summary>
        /// <param name="sessionId">Идентификатор сеанса</param>
        /// <returns>Контекст клиентского сеанса</returns>
        public UserContext GetContext(Guid sessionId)
        {
            UserContext context = null;
            WaitLock.Lock(_lock, 1000, () =>
            {
                if (_clients.TryGetValue(sessionId, out context))
                    context.AliveTime = DateTime.Now;
            });
            return context;
        }

        /// <summary>
        /// Получает список активных клиентских сеансов
        /// </summary>
        /// <returns>Список контекстов активных клиентских сеансов</returns>
        public List<UserContext> GetActiveUsers()
        {
            TryRemoveInactive(null);
            var contexts = new List<UserContext>();
            WaitLock.Lock(_lock, 1000, () =>
            {
                contexts = _clients.Values.ToList();
            });
            return contexts;
        }

        private void TryRemoveInactive(object state)
        {
            int inactiveTime;
            inactiveTime = int.TryParse(ConfigurationManager.AppSettings["MaxInactiveTime"], out inactiveTime)
                               ? inactiveTime
                               : DefaultMaxInactiveTime;
            WaitLock.Lock(_lock, 1000, () =>
            {
                foreach (
                    var r in _clients.Where(c => (DateTime.Now - c.Value.AliveTime).TotalMinutes >= inactiveTime)
                        .Select(c => c.Key).ToList())
                    _clients.Remove(r);
            });
        }

    }
}
