﻿using System.Runtime.Serialization;

namespace TB.Services.Infrastructure.Interface
{
    [DataContract]
    public class FaultBase
    {
        [DataMember]
        public string Message { get; set; }

        public FaultBase(string message)
        {
            Message = message;
        }
    }
}
