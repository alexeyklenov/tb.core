﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using Utils;

namespace TB.Services.Infrastructure.Interface
{
    /// <summary>
    /// Контекст пользователя
    /// </summary>
    [DataContract]
    public class UserContext : IExtension<OperationContext>, ICloneable
    {
        [DataMember]
        public string Login { get; set; }

        [DataMember]
        public DateTime AliveTime { get; set; }

        /// <summary>
        /// Имя сервера приложений
        /// </summary>
        [DataMember]
        public string AppServerName { get; set; }

        /// <summary>
        /// Адрес сервера приложений
        /// </summary>
        [DataMember]
        public string AppServerAddress { get; set; }

        [DataMember]
        public Guid SessionGuid { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string DisplayName { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string Terminal { get; set; }

        [DataMember]
        public List<string> Roles { get; set; }

        [DataMember]
        public List<long> RolesIds { get; set; }

        /// <summary>
        /// Модуль клиентской части
        /// </summary>
        [DataMember]
        public string ClientModule { get; set; }

        /// <summary>
        /// Сервис сервера приложений
        /// </summary>
        [DataMember]
        public string Service { get; set; }

        /// <summary>
        /// Метод сервиса
        /// </summary>
        [DataMember]
        public string Action { get; set; }

        [DataMember]
        public bool IsAuthenticated { get; set; }

        [DataMember]
        public bool IgnoreChecks { get; set; }

        [DataMember]
        public Hashtable Context { get; set; }

        public static UserContext Current
        {
            get
            {
                if (OperationContext.Current == null || OperationContext.Current.Extensions.Find<UserContext>() == null)
                    return ClientInstance;
                return OperationContext.Current.Extensions.Find<UserContext>();
            }
        }

        public string ShortName
        {
            get
            {
                var fullName = DisplayName.Split(' ');
                var shortName = "";
                for (var i = 0; i <= fullName.Count() - 1; i++)
                {
                    if (fullName[i].Length > 0)
                        shortName += i == 0 ? fullName[i] + " " : fullName[i].Substring(0, 1) + ".";
                }
                return shortName;
            }
        }

        #region Methods

        public UserContext()
        {
            Context = new Hashtable();
        }

        public void Assign(UserContext source)
        {
            Context = (Hashtable)source.Context.Clone();
            Login = source.Login;
        }

        public override string ToString()
        {
            return string.Format("User: {0} ({1})", Name,
                                 string.Join("; ", Context.Cast<DictionaryEntry>()
                                                       .Select(x => x.Key + " = " + x.Value)
                                                       .ToArray()));
        }

        #endregion

        #region Static instance

        private static readonly object SyncRoot = new object();

        private static UserContext _instance;

        public static UserContext ClientInstance
        {
            get
            {
                if (_instance == null)
                {
                    WaitLock.Lock(SyncRoot, 10000, () =>
                    {
                        if (_instance == null)
                            _instance = new UserContext();
                    });
                }
                return _instance;
            }
            set
            {
                _instance = value;
            }
        }

        #endregion

        #region IExtension<OperationContext> Members

        public void Attach(OperationContext owner)
        {
        }

        public void Detach(OperationContext owner)
        {
        }

        #endregion

        #region ICloneable Members

        public object Clone()
        {
            var cloneUserContext = (UserContext)MemberwiseClone();
            return cloneUserContext;
        }

        #endregion
    }
}
