﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel.Description;
using System.Xml;

namespace TB.Services.Infrastructure.Interface
{
    public class ReferencePreservingDataContractSerializerOperationBehavior
        : DataContractSerializerOperationBehavior
    {
        string _dataContractSurrogateType = string.Empty;
        public ReferencePreservingDataContractSerializerOperationBehavior(
          OperationDescription operationDescription)
            : base(operationDescription) { }

        public ReferencePreservingDataContractSerializerOperationBehavior(
        OperationDescription operationDescription, string dataContractSurrogateType)
            : base(operationDescription)
        {
            _dataContractSurrogateType = dataContractSurrogateType;
        }

        public override XmlObjectSerializer CreateSerializer(
          Type type, string name, string ns, IList<Type> knownTypes)
        {
            return CreateDataContractSerializer(type, name, ns, knownTypes);
        }

        private static XmlObjectSerializer CreateDataContractSerializer(
          Type type, string name, string ns, IList<Type> knownTypes)
        {
            return CreateDataContractSerializer(type, name, ns, knownTypes);
        }

        public override XmlObjectSerializer CreateSerializer(
          Type type, XmlDictionaryString name, XmlDictionaryString ns,
          IList<Type> knownTypes)
        {
            IDataContractSurrogate surrogate = null;
            if (!string.IsNullOrEmpty(_dataContractSurrogateType))
            {
                var t = Type.GetType(_dataContractSurrogateType);
                if (t != null)
                {
                    surrogate = (IDataContractSurrogate)Activator.CreateInstance(t);
                }
            }
            return new DataContractSerializer(type, name, ns, knownTypes,
                int.MaxValue /*maxItemsInObjectGraph*/,
                false/*ignoreExtensionDataObject*/,
                true/*preserveObjectReferences*/,
                surrogate/*dataContractSurrogate*/);
        }
    }
}
