﻿using System;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using TB.Services.Infrastructure.Interface;

namespace TB.Services.Infrastructure.Client
{
    public class UserContextClientMessageInspector : IClientMessageInspector, IParameterInspector
    {
        #region IClientMessageInspector Members

        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
        }

        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            /*            Guid myToken = Guid.NewGuid();
                        MessageHeader<Guid> mhg = new MessageHeader<Guid>(myToken);
                        MessageHeader untyped = mhg.GetUntypedHeader("token", "ns");
                        OperationContext.Current.OutgoingMessageHeaders.Add(untyped);
                        */
            if (UserContext.ClientInstance != null)
            {
                var header = new MessageHeader<Guid>
                {
                    Actor = "Anyone",
                    Content = UserContext.ClientInstance.SessionGuid
                };

                //UserName = System.Threading.Thread.CurrentPrincipal.Identity.Name;

                //Creating an untyped header to add to the WCF context
                var unTypedHeader = header.GetUntypedHeader("UserSessionGuid", "http://TB.Service");

                //Add the header to the current request
                request.Headers.Add(unTypedHeader);
            }
            else
            {
                var header = new MessageHeader<string>
                {
                    Actor = "Anyone",
                    Content = string.Format("Ошибка получения контекста. Terminal: {0}, action: {1}",
                        request.Properties.Any(x => x.Key == RemoteEndpointMessageProperty.Name)
                            ? ((RemoteEndpointMessageProperty) request.Properties[RemoteEndpointMessageProperty.Name])
                                .Address
                            : Dns.GetHostName(),
                        request.Headers.Action)
                };

                //Creating an untyped header to add to the WCF context
                var unTypedHeader = header.GetUntypedHeader("UserSessionError", "http://TB.Service");

                //Add the header to the current request
                request.Headers.Add(unTypedHeader);
            }
            return null;
        }

        #endregion



        #region IParameterInspector Members

        public void AfterCall(string operationName, object[] outputs, object returnValue, object correlationState)
        {

        }

        public object BeforeCall(string operationName, object[] inputs)
        {
            return null;
        }

        #endregion
    }
}
