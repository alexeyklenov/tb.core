﻿
namespace TB.Services.Infrastructure.Client.Interface
{
    public interface IEndpointCatalogFactory
    {
        IEndpointCatalog CreateCatalog();
    }
}
