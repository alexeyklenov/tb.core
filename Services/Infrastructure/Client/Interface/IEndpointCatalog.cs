﻿using System.Net;
using System.ServiceModel.Channels;

namespace TB.Services.Infrastructure.Client.Interface
{
    /// <summary>
    /// An interface that objects can implement to shwo that they are a catalog containing a collection of Endpoints.
    /// </summary>
    public interface IEndpointCatalog
    {
        /// <summary>
        /// Must be implemented to get the endpoints quantity.
        /// </summary>
        int Count { get; }

        /// <summary>
        /// Must be implemented to verify the existance of endpoint information (address and credentials) 
        /// for a given endpoint in the given network name.
        /// </summary>
        /// <param name="endpointName">Name of the endpoint to be searched.</param>
        /// <returns>true if the information has been found. Otherwise false.</returns>
        bool AddressExistsForEndpoint(string endpointName);

        /// <summary>
        /// Must be implemented to provide the credentials for 
        /// a given endpoint in the given network name.
        /// </summary>
        /// <param name="endpointName">Name of the endpoint.</param>
        /// <returns>NetworkCredential corresponding to the given parameters.</returns>
        NetworkCredential GetCredentialForEndpoint(string endpointName);

        /// <summary>
        /// Must be implemented to provide the address for
        /// a given endpoint in the given network name.
        /// </summary>
        /// <param name="endpointName">Name of the endpoint.</param>
        /// <returns>String containing the address for the given parameters.</returns>
        string GetAddressForEndpoint(string endpointName);

        /// <summary>
        /// Must be implemented to add/update an endpoint in the Catalog.
        /// </summary>
        /// <param name="endpoint">Endpoint to be added/updated.</param>
        void SetEndpoint(Endpoint endpoint);

        /// <summary>
        /// Must be implemented to verify the existance of an endpoint in the catalog.
        /// </summary>
        /// <param name="endpointName">Name of the endpoint to be searched.</param>
        /// <returns>True if the endpoint exists in the catalog. Otherwise false.</returns>
        bool EndpointExists(string endpointName);

        IChannelFactory GetChannelFactory(string endpointName);

        void SetChannelFactory(string endpointName, IChannelFactory factory);
    }
}
