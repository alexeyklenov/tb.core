﻿namespace TB.Services.Infrastructure.Client.Interface
{
    public interface IClientProxy<T>
    {
        T Channel { get; }
    }
}
