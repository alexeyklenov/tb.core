﻿using System.Collections.Generic;
using System.Net;
using System.ServiceModel.Channels;
using TB.Services.Infrastructure.Client.Interface;

namespace TB.Services.Infrastructure.Client
{
    public class EndpointCatalog : IEndpointCatalog
    {
        readonly Dictionary<string, Endpoint> _endpoints = new Dictionary<string, Endpoint>();

        #region IEndpointCatalog Members

        public int Count
        {
            get { return _endpoints.Count; }
        }

        public bool AddressExistsForEndpoint(string endpointName)
        {
            return _endpoints.ContainsKey(endpointName);
        }

        public NetworkCredential GetCredentialForEndpoint(string endpointName)
        {
            return new NetworkCredential();
        }

        public string GetAddressForEndpoint(string endpointName)
        {
            return _endpoints.ContainsKey(endpointName) ? _endpoints[endpointName].Uri : string.Empty;
        }

        public void SetEndpoint(Endpoint endpoint)
        {
            _endpoints[endpoint.Name] = endpoint;
        }

        public bool EndpointExists(string endpointName)
        {
            return _endpoints.ContainsKey(endpointName);
        }

        public IChannelFactory GetChannelFactory(string endpointName)
        {
            return _endpoints.ContainsKey(endpointName) ? _endpoints[endpointName].ChannelFactory : null;
        }

        public void SetChannelFactory(string endpointName, IChannelFactory factory)
        {
            if (_endpoints.ContainsKey(endpointName))
                _endpoints[endpointName].ChannelFactory = factory;
        }

        #endregion
    }
}
