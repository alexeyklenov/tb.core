﻿using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace TB.Services.Infrastructure.Client
{
    public class UserContextClientBehavior : UserContextClientBehavior<UserContextClientMessageInspector>
    {
        /*
        #region IEndpointBehavior Members

        public void AddBindingParameters(ServiceEndpoint endpoint, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(new UserContextClientMessageInspector());
            foreach (var op in clientRuntime.Operations)
            {
                op.ParameterInspectors.Add(new UserContextClientMessageInspector());
            }
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
        }

        public void Validate(ServiceEndpoint endpoint)
        {
        }

        #endregion
         */
    }

    public class UserContextClientBehavior<T> : IEndpointBehavior
        where T : IClientMessageInspector, IParameterInspector, new()
    {

        #region IEndpointBehavior Members

        public void AddBindingParameters(ServiceEndpoint endpoint, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(new T());
            foreach (var op in clientRuntime.Operations)
            {
                op.ParameterInspectors.Add(new T());
            }
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
        }

        public void Validate(ServiceEndpoint endpoint)
        {
        }

        #endregion
    }
}
