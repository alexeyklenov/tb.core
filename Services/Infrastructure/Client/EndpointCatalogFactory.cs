﻿using System;
using System.ServiceModel;
using log4net;
using TB.Services.Infrastructure.Client.Interface;
using TB.Services.Common.Interface;

namespace TB.Services.Infrastructure.Client
{
    public class EndpointCatalogFactory : IEndpointCatalogFactory
    {
        private static EndpointCatalog _catalog;
       
        #region IEndpointCatalogFactory Members

        public static IEndpointCatalog Catalog
        {
            get
            {
                if (_catalog == null)
                {
                    _catalog = new EndpointCatalog();
                    var endpoint = string.Format("{0}/{1}", AppDomain.CurrentDomain.GetData("appAddress"),
                                                      "ServiceCatalog");

                    using (var proxy = new ClientProxy<IServiceCatalog>(new NetTcpBinding(SecurityMode.None), endpoint))
                    {
                        try
                        {
                            foreach (var ep in proxy.Get(x => x.Endpoints()))
                                _catalog.SetEndpoint(new Endpoint { Name = ep.Key, Uri = ep.Value });
                        }
                        catch (Exception ex)
                        {
                            LogManager.GetLogger("EndpointCatalogFactory").Error("Error on get endpoints",ex);
                            throw new Exception("Сервер недоступен!");
                        }
                    }
                }
                return _catalog;
            }
        }

        public static void ResetCatalog()
        {
            _catalog = null;
        }

        public IEndpointCatalog CreateCatalog()
        {
            return Catalog;
        }

        #endregion
    }
}
