﻿using System;
using System.Net.Sockets;
using System.ServiceModel;
using System.ServiceModel.Description;
using log4net;
using TB.Services.Common.Interface;
using TB.Services.Infrastructure.Client.Interface;
using Binding = System.ServiceModel.Channels.Binding;


namespace TB.Services.Infrastructure.Client
{
    public class ClientProxy<T> : ClientProxyBase, IDisposable, IClientProxy<T>
    {
        #region Fields

        protected ILog Log = LogManager.GetLogger("Proxy" + typeof(T).Name);
        protected Binding Binding;
        protected string Endpoint;
        protected ChannelFactory<T> ChannelFactory;

        #endregion

        public T Channel { get; protected set; }

        public ClientProxy()
        {

        }

        public ClientProxy(string endpoint)
            : this(GetBinding(), endpoint)
        {
        }

        public ClientProxy(Binding binding, string endpoint)
        {
            Binding = binding;
            Endpoint = endpoint;
            ChannelFactory = new ChannelFactory<T>(Binding, Endpoint);
            SetupChannel();
        }

        public ClientProxy(ChannelFactory<T> factory)
        {
            ChannelFactory = factory;
            SetupChannel();
        }

        protected static Binding GetBinding()
        {
            return new NetTcpBinding(SecurityMode.None)
            {
                MaxReceivedMessageSize = Constants.MaxReceivedMessageSize,
                ReaderQuotas =
                {
                    MaxArrayLength = int.MaxValue,
                    MaxStringContentLength = int.MaxValue,
                    MaxDepth = 1000
                }
            };
        }

        protected virtual void SetupChannel()
        {
            if (!ChannelFactory.Endpoint.Behaviors.Contains(typeof(UserContextClientBehavior)))
                ChannelFactory.Endpoint.Behaviors.Add(new UserContextClientBehavior());

            // Увеличим колчество объектов в десериализуемом графе до потолка
            foreach (var op in ChannelFactory.Endpoint.Contract.Operations)
            {
                var dataContractBehavior =
                    op.Behaviors[typeof(DataContractSerializerOperationBehavior)]
                    as DataContractSerializerOperationBehavior;

                if (dataContractBehavior != null)
                    dataContractBehavior.MaxItemsInObjectGraph = int.MaxValue;
            }

            Channel = ChannelFactory.CreateChannel();
            ((IContextChannel)Channel).OperationTimeout = TimeSpan.FromHours(1);
            ((ICommunicationObject)Channel).Faulted += Faulted;
        }

        void Faulted(object sender, EventArgs e)
        {
            ((ICommunicationObject)sender).Abort();
            if (sender is T)
            {
                ((IDisposable)sender).Dispose();
                Channel = ChannelFactory.CreateChannel();
            }
        }

        public TR Get<TR>(Func<T, TR> action)
        {
            //TODO отрефакторить эту функцию
            var result = default(TR);
            var channel = (IClientChannel)Channel;
            if (channel.State == CommunicationState.Faulted || channel.State == CommunicationState.Closed)
            {
                if (channel.State == CommunicationState.Faulted)
                {
                    Log.Warn("Автоматическое восстановление неисправного клиента");
                    channel.Abort();
                }
                try
                {
                    Channel = ChannelFactory.CreateChannel();
                    ((ICommunicationObject)Channel).Faulted += Faulted;
                }
                catch (Exception)
                {
                    switch (channel.State)
                    {
                        case CommunicationState.Closed:
                        case CommunicationState.Closing:
                        case CommunicationState.Created:
                        case CommunicationState.Faulted:
                            return result;
                        case CommunicationState.Opened:
                        case CommunicationState.Opening:
                            break;
                    }
                }
            }
            try
            {
                result = action(Channel);
            }
            catch (Exception ex)
            {
                if (ex is CommunicationObjectAbortedException || ex is EndpointNotFoundException ||
                    ex is SocketException || ex is TimeoutException ||
                    (ex is CommunicationException && !(ex is FaultException)))
                {
                    Log.Error("Ошибка связи", ex);
                    SetupChannel();
                    result = action(Channel);
                }
                else
                {
                    Log.Error("Ошибка подключения", ex);
                    if (!OnFaultEvent(this, ex))
                        throw;
                }
            }
            return result;
        }

        public void Execute(Action<T> action)
        {
            Get(x =>
            {
                action(x);
                return (object)null;
            });
        }

        #region IDisposable Members

        public void Dispose()
        {
            var channel = (ICommunicationObject)Channel;
            if (channel != null)
                try
                {
                    if (channel.State == CommunicationState.Opened)
                        channel.Close();
                    ((IDisposable)channel).Dispose();
                }
                catch (Exception e)
                {
                    Log.Error("Channel dispose", e);
                }

            if (ChannelFactory != null)
            {
                try
                {
                    ChannelFactory.Close();
                    ((IDisposable)ChannelFactory).Dispose();
                }
                catch (CommunicationException)
                {
                    ChannelFactory.Abort();
                }
                catch (TimeoutException)
                {
                    ChannelFactory.Abort();
                }
                catch (Exception)
                {
                    ChannelFactory.Abort();
                    throw;
                }
            }
        }

        #endregion
    }

    public class ClientProxyBase
    {
        public static event EventHandler<Exception> OnFault;

        protected static bool OnFaultEvent(object sender, Exception e)
        {
            if (OnFault == null) 
                return false;
            
            OnFault(sender, e);
            return true;
        }
    }
}
