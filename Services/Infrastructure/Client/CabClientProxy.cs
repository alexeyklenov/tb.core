﻿using System;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using TB.Services.Infrastructure.Client.Interface;
using TB.Services.Infrastructure.Interface;

namespace TB.Services.Infrastructure.Client
{
    public abstract class BaseCabClientProxy<T> : ClientProxy<T>
    {
        public IEndpointCatalog EndpointCatalog { get; set; }

        protected void SetupFactory(Binding binding)
        {
            if (EndpointCatalog == null)
                EndpointCatalog = EndpointCatalogFactory.Catalog;

            var endpoint = EndpointCatalog.GetAddressForEndpoint(typeof(T).Name);
            if (string.IsNullOrEmpty(endpoint))
                throw new Exception(string.Format("Не найдена служба: {0}", typeof (T).Name));

            ChannelFactory = CreateFactory(binding, endpoint);
            EndpointCatalog.SetChannelFactory(typeof(T).Name, ChannelFactory);
            if (!ChannelFactory.Endpoint.Behaviors.Contains(typeof(UserContextClientBehavior)))
                ChannelFactory.Endpoint.Behaviors.Add(new UserContextClientBehavior());

            var faultTypes =
                  AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.GetTypes())
                       .Where(t => t.BaseType == typeof(FaultBase))
                       .Select(t => new FaultDescription(t.Name) { DetailType = t }).ToList();

            // Увеличим колчество объектов в десериализуемом графе до потолка
            // Добавим кастомные исключения
            foreach (var op in ChannelFactory.Endpoint.Contract.Operations)
            {
                foreach (var faultDesc in faultTypes)
                {
                    op.Faults.Add(faultDesc);
                }

                var dataContractBehavior =
                    op.Behaviors[typeof(DataContractSerializerOperationBehavior)]
                    as DataContractSerializerOperationBehavior;

                if (dataContractBehavior != null)
                    dataContractBehavior.MaxItemsInObjectGraph = int.MaxValue;
            }
        }

        protected abstract ChannelFactory<T> CreateFactory(Binding binding, string endpoint);

        protected void Faulted(object sender, EventArgs e)
        {
            ((ICommunicationObject)sender).Abort();
            if (sender is T)
            {
                ((IDisposable)sender).Dispose();
                ResetFactory();
                Channel = ChannelFactory.CreateChannel();
                ((ICommunicationObject)Channel).Faulted += Faulted;
            }
        }

        private void ResetFactory()
        {
            EndpointCatalog = null;
            EndpointCatalogFactory.ResetCatalog();
            //AppServerCatalog.ReConnect();
            SetupFactory(GetBinding());
        }
    }

    public class CabClientProxy<T> : BaseCabClientProxy<T>
    {
        public CabClientProxy()
            : this(GetBinding())
        {
        }

        public CabClientProxy(Binding binding)
        {
            if (ChannelFactory == null)
                SetupFactory(binding);

            Channel = ChannelFactory.CreateChannel();
            ((IContextChannel)Channel).OperationTimeout = TimeSpan.FromHours(1);
            ((ICommunicationObject)Channel).Faulted += Faulted;
        }

        protected override ChannelFactory<T> CreateFactory(Binding binding, string endpoint)
        {
            return new ChannelFactory<T>(binding, endpoint);
        }

        public static TResult Invoke<TResult>(Func<T, TResult> operation)
        {
            using (var clientProxy = new CabClientProxy<T>())
                return clientProxy.Get(operation);
        }

        public static void Invoke(Action<T> operation)
        {
            using (var clientProxy = new CabClientProxy<T>())
                clientProxy.Execute(operation);
        }
    }

    public class CabDuplexClientProxy<T> : BaseCabClientProxy<T>
    {
        private object _callback;

        public CabDuplexClientProxy(object callback)
            : this(GetBinding(), callback)
        {
        }

        public CabDuplexClientProxy(Binding binding, object callback)
        {
            _callback = callback;

            if (ChannelFactory == null)
                SetupFactory(binding);

            Channel = ChannelFactory.CreateChannel();
            ((IContextChannel)Channel).OperationTimeout = TimeSpan.FromHours(1);
            ((ICommunicationObject)Channel).Faulted += Faulted;
        }

        protected override ChannelFactory<T> CreateFactory(Binding binding, string endpoint)
        {
            return new DuplexChannelFactory<T>(_callback, binding, endpoint);
        }
    }
}
