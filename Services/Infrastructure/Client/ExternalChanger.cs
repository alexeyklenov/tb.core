﻿using System;
using System.Configuration;
using System.ServiceModel;
using log4net;
using TB.BOL.Interface;
using TB.Services.Common.Interface;

namespace TB.Services.Infrastructure.Client
{
    public static class ExternalChanger<T> where T : IExternallyChangeable
    {
        // ReSharper disable StaticFieldInGenericType
        private static readonly object SyncRoot = new object();
        private static String _endPoint = "";
        // ReSharper restore StaticFieldInGenericType

        private static ILog Logger
        {
            get { return LogManager.GetLogger("ExternalChanger"); }
        }

        public static T Change(T entity)
        {
            if (String.IsNullOrEmpty(_endPoint))
            {
                lock (SyncRoot)
                {
                    var serverIp = (string)AppDomain.CurrentDomain.GetData("ServerIP");
                    var port = (string)AppDomain.CurrentDomain.GetData("ServerPort");
                    if (string.IsNullOrEmpty(port))
                        port = Constants.ServerPort;

                    var serviceEndpoint = string.IsNullOrEmpty(serverIp)
                        ? string.Format("{0}ServiceCatalog", ConfigurationManager.AppSettings["ServerEndPoint"])
                        : string.Format("net.tcp://{0}:{1}/{2}", serverIp, port, "ServiceCatalog");

                    using (var serviceCatalog = new ClientProxy<IServiceCatalog>(new NetTcpBinding(SecurityMode.None), serviceEndpoint))
                    {
                        serviceCatalog.Channel.Endpoints()
                            .TryGetValue(typeof(IExternalChangerService).Name, out _endPoint);
                    }
                }
            }

            using (var proxy = new ClientProxy<IExternalChangerService>(new NetTcpBinding(SecurityMode.None), _endPoint))
            {
                try
                {
                    return (T)proxy.Channel.Change(entity);
                }
                catch (Exception ex)
                {
                    Logger.Error("Ошибка при обращении к удаленному методу IExternalChangerService.Change(IExternallyChangeable entity)", ex);
                    return default(T);
                }
            }
        }
    }
}
