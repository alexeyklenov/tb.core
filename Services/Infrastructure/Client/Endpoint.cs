﻿using System.ServiceModel.Channels;

namespace TB.Services.Infrastructure.Client
{
    public class Endpoint
    {
        public string Name { get; set; }
        public string Uri { get; set; }
        public IChannelFactory ChannelFactory { get; set; }
    }
}
