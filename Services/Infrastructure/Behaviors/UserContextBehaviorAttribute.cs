﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using TB.Services.Infrastructure.Inspectors;

namespace TB.Services.Infrastructure.Behaviors
{
    public class UserContextBehaviorAttribute : Attribute, IServiceBehavior
    {
        #region IServiceBehavior Members

        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            foreach (var endpointDispatcher in serviceHostBase.ChannelDispatchers.SelectMany(x => ((ChannelDispatcher)x).Endpoints))
            {
                endpointDispatcher.DispatchRuntime.MessageInspectors.Add(new UserContextMessageInspector());
                // на каждую операцию навешиваем инспектор логирования параметров
               /* foreach (DispatchOperation dispatchOperation in endpointDispatcher.DispatchRuntime.Operations)
                    dispatchOperation.ParameterInspectors.Add(new ParameterInspector());*/
            }
            foreach (ChannelDispatcher channelDispatcher in serviceHostBase.ChannelDispatchers)
            {
                channelDispatcher.ErrorHandlers.Add(new LoggingErrorHandler());
            }
        }

        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }

        #endregion

    }

    class LoggingErrorHandler : IErrorHandler
    {
        public void ProvideFault(Exception error, MessageVersion version, ref Message fault)
        {
        }
        public bool HandleError(Exception error)
        {
            return false;
        }
    }
}
