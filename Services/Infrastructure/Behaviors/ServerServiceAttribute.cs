﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace TB.Services.Infrastructure.Behaviors
{
    /// <summary>
    /// Аттрибут отмечающий класс как сервеную службу
    /// </summary>
    public class ServerServiceAttribute : Attribute, IServiceBehavior
    {
        private bool NoInspectors { get; set; }

        public ServerServiceAttribute(bool noInspectors = false)
        {
            NoInspectors = noInspectors;
        }

        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }

        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints,
            BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            if (NoInspectors)
                return;

            foreach (
                var endpointDispatcher in
                    serviceHostBase.ChannelDispatchers.SelectMany(x => ((ChannelDispatcher) x).Endpoints))
            {
                //endpointDispatcher.DispatchRuntime.MessageInspectors.Add(new DalSessionMessageInspector());
            }
        }
    }
}