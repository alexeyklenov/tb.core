﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

namespace TB.Services.Infrastructure.Dto
{
    [DataContract]
    public class DtoBase : ICloneable
    {

        private Type _type;
        public Type DtoType
        {
            get { return _type ?? (_type = GetType()); }
        }
        [DataMember]
        public long? Id { get; set; }
        /// <summary>
        /// Вспомогательное поле, используемое исключительно на клиенте
        /// </summary>
        public object Tag { get; set; }

        /// <summary>
        /// Словарь динамических полей
        /// </summary>
        public Dictionary<string, DynamicProperty> Properties { get; set; }

        public DtoBase()
        {
            Properties = new Dictionary<string, DynamicProperty>();
        }

        private DynamicProperty GetProperty(string key)
        {
            return Properties != null
                              ? Properties.Where(x => x.Key == key).Select(x => x.Value).FirstOrDefault()
                              : null;
        }

        /// <summary>
        /// Индексатор динамических полей по имени поля
        /// </summary>
        public dynamic this[string key]
        {
            get
            {
                try
                {
                    var prop = GetProperty(key);
                    if (prop != null)
                        return prop.Value;

                    PropertyInfo p = null;
                    object obj = this;
                    if (!key.Contains("."))
                        p = DtoType.GetProperty(key);
                    else
                    {
                        var subProp = DtoType.GetProperty(key.Substring(0, key.IndexOf(".", StringComparison.Ordinal)));
                        if (subProp != null)
                        {
                            obj = subProp.GetValue(this, null);
                            if (obj != null)
                                p = obj.GetType().GetProperty(key.Substring(key.IndexOf(".", StringComparison.Ordinal) + 1));
                        }
                    }
                    if (p != null)
                        return p.GetValue(obj, null);

                    return null;
                }
                catch (Exception ex)
                {
                    throw new Exception(String.Format("Ошибка получения значения свойства {0}", key), ex);
                }
            }
            set
            {
                try
                {
                    var prop = GetProperty(key);
                    if (prop != null)
                    {
                        prop.Value = value;
                    }
                    else
                    {
                        if (!key.Contains("."))
                        {
                            var p = DtoType.GetProperty(key);
                            if (p != null && p.CanWrite)
                                p.SetValue(this, value, null);
                        }
                        else
                        {
                            var subProp = DtoType.GetProperty(key.Substring(0, key.IndexOf(".", StringComparison.Ordinal)));
                            var obj = subProp.GetValue(this, null);
                            if (obj != null)
                            {
                                var p = subProp.PropertyType.GetProperty(key.Substring(key.IndexOf(".", StringComparison.Ordinal) + 1));
                                if (p != null && p.CanWrite)
                                {
                                    p.SetValue(obj, value, null);
                                    subProp.SetValue(this, obj, null);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(String.Format("Ошибка присвоения значения {0} свойству {1}", value, key), ex);
                }
            }
        }

        public Boolean ContainsKey(string key)
        {
            var res = true;
            var prop = GetProperty(key);
            if (prop == null)
            {
                var p = DtoType.GetProperty(key);
                if (p == null)
                    res = false;
            }
            return res;
        }

        public virtual Object Clone()
        {
            var dto = new DtoBase();
            CloneProperties(dto);
            return dto;
        }

        protected void CloneProperties(DtoBase dto)
        {
            if (Properties != null)
            {
                dto.Properties = new Dictionary<string, DynamicProperty>();
                foreach (var dynamicProperty in Properties)
                {
                    dto.Properties.Add(dynamicProperty.Key, dynamicProperty.Value.Clone() as DynamicProperty);
                }
            }
            dto.Id = Id;
        }

        public DynamicProperty AddProperty(DynamicProperty property)
        {
            if (!Properties.ContainsKey(property.Name))
            {

                var p = DtoType.GetProperty(property.Name);
                if (p != null && p.CanWrite)
                    p.SetValue(this, property.Value, null);
                else
                    Properties.Add(property.Name, property);
            }
            else
                return Properties[property.Name];

            return property;
        }

        public DynamicProperty AddProperty(String propname, Type propType, dynamic propValue = null, object nvl = null)
        {
            return AddProperty(new DynamicProperty(propname, propType, propValue, nvl));
        }
    }
}
