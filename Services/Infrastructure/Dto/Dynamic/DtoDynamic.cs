﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

namespace TB.Services.Infrastructure.Dto.Dynamic
{
    [DataContract]
    public class DtoDynamic : DtoBase, ICustomTypeDescriptor
    {

        private static readonly ICustomTypeDescriptor DefaultDescriptor =
            TypeDescriptor.GetProvider(typeof(DtoDynamic)).GetTypeDescriptor(typeof(DtoDynamic));

        internal void Reset(string key)
        {
            Properties[key].Value = null;
        }

        internal bool ShouldSerialize(string key)
        {
            return Properties.ContainsKey(key);
        }

        #region ICustomTypeDescriptor

        string ICustomTypeDescriptor.GetClassName()
        {
            return DefaultDescriptor.GetClassName();
        }

        string ICustomTypeDescriptor.GetComponentName()
        {
            return DefaultDescriptor.GetComponentName();
        }

        TypeConverter ICustomTypeDescriptor.GetConverter()
        {
            return DefaultDescriptor.GetConverter();
        }

        EventDescriptor ICustomTypeDescriptor.GetDefaultEvent()
        {
            return DefaultDescriptor.GetDefaultEvent();
        }

        PropertyDescriptor ICustomTypeDescriptor.GetDefaultProperty()
        {
            return DefaultDescriptor.GetDefaultProperty();
        }

        object ICustomTypeDescriptor.GetEditor(Type editorBaseType)
        {
            return DefaultDescriptor.GetEditor(editorBaseType);
        }

        EventDescriptorCollection ICustomTypeDescriptor.GetEvents(Attribute[] attributes)
        {
            return DefaultDescriptor.GetEvents(attributes);
        }

        EventDescriptorCollection ICustomTypeDescriptor.GetEvents()
        {
            return DefaultDescriptor.GetEvents();
        }

        object ICustomTypeDescriptor.GetPropertyOwner(PropertyDescriptor pd)
        {
            return this; //DefaultDescriptor.GetPropertyOwner(pd);
        }

        AttributeCollection ICustomTypeDescriptor.GetAttributes()
        {
            return DefaultDescriptor.GetAttributes();
        }

        public PropertyDescriptorCollection GetProperties()
        {
            if (Properties != null)
            {
                var instanceProps =
                    DtoType.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(
                        x => x.Name != "Item");
                var customProps = instanceProps.ToList().
                    ConvertAll(x => new CustomPropertyDescriptor(x.Name, x.PropertyType, null));


                customProps.AddRange(
                    Properties.Values.ToList().ConvertAll(x => new CustomPropertyDescriptor(x.Name, x.Type, null)));

                //foreach (var customPropertyDescriptor in customProps)
                //{
                //    customPropertyDescriptor.PropertyChanged += new PropertyChangedEventHandler(customPropertyDescriptor_PropertyChanged);
                //}
                return new PropertyDescriptorCollection(customProps.Cast<PropertyDescriptor>().ToArray());
            }
            else
            {
                return TypeDescriptor.GetProvider(this).GetTypeDescriptor(this).GetProperties();
            }
        }

        public PropertyDescriptorCollection GetProperties(Attribute[] attributes)
        {
            if (Properties != null)
            {
                return GetProperties();
            }
            return TypeDescriptor.GetProvider(this).GetTypeDescriptor(this).GetProperties(attributes);
        }

        #endregion

        private void customPropertyDescriptor_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this[e.PropertyName] = (sender as CustomPropertyDescriptor).GetValue(null);
        }

        public override Object Clone()
        {
            var dto = new DtoDynamic();
            CloneProperties(dto);
            return dto;
        }
    }
}
