﻿using System;
using System.ComponentModel;

namespace TB.Services.Infrastructure.Dto.Dynamic
{
    //Класс переопределяющий методы класса PropertyDescriptor для динамического заполнения свойств объекта
    public class CustomPropertyDescriptor : PropertyDescriptor, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private readonly Type type;

        public CustomPropertyDescriptor(string name, Type type, Attribute[] attributes)
            : base(name, attributes)
        {
            this.type = type;
        }

        public override object GetValue(object component)
        {
            return ((DtoDynamic)component)[Name];
        }

        public override void SetValue(object component, object value)
        {
            ((DtoDynamic)component)[Name] = value;
            OnPropertyChanged(this, new PropertyChangedEventArgs(Name));
        }
        protected void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(sender, e);
        }
        public override void ResetValue(object component)
        {
            ((DtoDynamic)component).Reset(Name);
        }

        public override bool CanResetValue(object component)
        {
            return true;
        }

        public override bool ShouldSerializeValue(object component)
        {
            return ((DtoDynamic)component).ShouldSerialize(Name);
        }

        public override Type PropertyType
        {
            get { return type; }
        }

        public override bool IsReadOnly
        {
            get { return false; }
        }

        public override Type ComponentType
        {
            get { return typeof(DtoDynamic); }
        }
    }
}
