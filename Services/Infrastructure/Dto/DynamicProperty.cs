﻿using System;
using System.Runtime.Serialization;

namespace TB.Services.Infrastructure.Dto
{
    /// <summary>
    /// Тип поля ДТО
    /// </summary>
    public enum FieldType
    {
        /// <summary>
        /// Замапленое свойство бизнес объекта
        /// </summary>
        MappedProperty,

        /// <summary>
        /// Незамапленое свойство бизнес объекта
        /// </summary>
        NotMappedProperty,

        /// <summary>
        /// Вычислимое поле
        /// </summary>
        CalculatedField,

        /// <summary>
        /// Вычислимое в запросе поле 
        /// </summary>
        SqlCalculatedField,

        /// <summary>
        /// SQL запрос
        /// </summary>
        SqlField
    }

    /// <summary>
    /// Класс описывающий динамические свойства
    /// </summary>
    [DataContract]
    public class DynamicProperty : ICloneable
    {
        private string _name;
        private dynamic _propValue;
        private Type _type;
        private Boolean _isLoadingInProgress;
        public DynamicProperty()
        {

        }



        public DynamicProperty(string name, Type type, dynamic value, object nvl = null)
        {
            _isLoadingInProgress = true;
            if (name == null) throw new ArgumentNullException("name");
            Name = name;
            Type = type;
            Nvl = nvl;
            Value = value;
            _isLoadingInProgress = false;
        }




        public DynamicProperty(string name, Type type, string where, String projectionType,
            FieldType fieldType, Boolean editable, String alias, String updateMethod = null, object nvl = null,
            String onChangeValue = null, String ownerTypeName = null, String displayName = null, String propertyTypeName = null)
        {
            _isLoadingInProgress = true;
            if (name == null) throw new ArgumentNullException("name");
            Name = name;
            Type = type;
            WhereClause = where;
            ProjectionType = projectionType;
            FieldType = fieldType;
            Editable = editable;
            Alias = alias.Contains("Alias") || String.IsNullOrEmpty(alias) ? alias : alias + "Alias";
            DisplayName = displayName;
            UpdateMethod = updateMethod;
            OnChangeValue = onChangeValue;
            Nvl = nvl;
            OwnerTypeName = ownerTypeName;
            PropertyTypeName = propertyTypeName;
            _isLoadingInProgress = false;
        }

        [OnDeserializing]
        private void OnDeserializing(StreamingContext ctx)
        {
            _isLoadingInProgress = true;
        }

        [OnDeserialized]
        private void OnDeserialized(StreamingContext ctx)
        {
            _isLoadingInProgress = false;
        }

        public long? OwnerId { get; set; }
        public string OwnerName { get; set; }

        [DataMember]
        public string OwnerTypeName { get; set; }


        [DataMember]
        public string PropertyTypeName { get; set; }

        /// <summary>
        /// Название поля
        /// </summary>
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>
        /// Значение поля
        /// </summary>
        [DataMember]
        public dynamic Value
        {
            get { return _propValue; }
            set
            {
                try
                {
                    if (!_isLoadingInProgress && !IsModified)
                        IsModified = _propValue != value;
                    if (Type == null)
                        _propValue = value;
                    else
                    {

                        if (value == null && Nvl != null)
                            value = Nvl;
                        if (Type == typeof(Boolean))
                            _propValue = value != null && (value.ToString() == "1" || value.ToString().ToLower() == "true");
                        else
                        {
                            if (value is decimal)
                                value = ((Decimal)value) / 1.000000000000000000000000000000000m;

                            var underlyingType = Nullable.GetUnderlyingType(Type);
                            // if underlyingType has null value, it means the original type wasn't nullable
                            if (value != null && value.GetType() != (underlyingType ?? Type))
                            {
                                _propValue = (underlyingType ?? Type).BaseType == typeof(Enum)
                                    ? Enum.Parse((underlyingType ?? Type), value.ToString())
                                    : Convert.ChangeType(value, underlyingType ?? Type);
                            }
                            else if (value == null && (underlyingType ?? Type).BaseType == typeof(Enum) && (underlyingType ?? Type).GetField("None") != null)
                                _propValue = Enum.Parse((underlyingType ?? Type), "None");
                            else
                                _propValue = value;
                        }
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception(String.Format("Ошибка конвертации значения {0} в тип {1} для свойства {2}", value, Type, Name), ex);
                }
            }
        }

        /// <summary>
        /// Тип поля
        /// </summary>
        [DataMember]
        public String TypeName { get; set; }

        /// <summary>
        /// Тип поля
        /// </summary>
        public Type Type
        {
            get
            {
                if (_type == null && !String.IsNullOrEmpty(TypeName))
                {
                    _type = Type.GetType(TypeName);
                }
                return _type;
            }
            set
            {
                _type = value;
                TypeName = value != null
                    ? value.AssemblyQualifiedName
                    : string.Empty;
            }
        }

        /// <summary>
        /// Значение по умолчанию, если в базе null
        /// </summary>
        [DataMember]
        public object Nvl { get; set; }

        /// <summary>
        ///Строка мапинга 
        /// </summary>
        public string WhereClause { get; set; }

        /// <summary>
        ///Тип проекции
        /// </summary>
        public String ProjectionType { get; set; }
        public FieldType FieldType { get; set; }
        public Boolean Editable { get; set; }
        public String Alias { get; set; }

        public String RefCode { get; set; }
        public String UpdateMethod { get; set; }

        public String OnChangeValue { get; set; }

        [DataMember]
        public Boolean IsModified { get; set; }

        [DataMember]
        public String DisplayName { get; set; }

        public Object Clone()
        {
            return MemberwiseClone();
        }

    }
}
