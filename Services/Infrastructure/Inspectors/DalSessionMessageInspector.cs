﻿using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using TB.DAL.Interface;

namespace TB.Services.Infrastructure.Inspectors
{
    public class DalSessionMessageInspector : IDispatchMessageInspector
    {
        public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
        {
            SessionManager.Instance.SessionOverride = SessionManager.Instance.GetSessionFactoryFor(DaoContainer.Config).OpenSession();
            return null;
        }

        public void BeforeSendReply(ref Message reply, object correlationState)
        {
        }
    }
}