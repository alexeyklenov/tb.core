﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Security.Authentication;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using log4net;
using Microsoft.Practices.Unity;
using TB.Services.Common.Adm.Interface;
using TB.Services.Infrastructure.Interface;
using Utils;

namespace TB.Services.Infrastructure.Inspectors
{
    public class UserContextMessageInspector : IDispatchMessageInspector
    {
        private readonly ILog _log = LogManager.GetLogger("UserContextMessageInspector");

        /// <summary>
        /// Содержит статистику по выполнению запроса
        /// </summary>
        private class RequestStatistics
        {
            /// <summary>
            /// Дата/время получения запроса
            /// </summary>
            public DateTime RecieveDate { get; set; }

            /// <summary>
            /// Дата/время отправки ответа на запрос
            /// </summary>
            public DateTime ReplyDate
            {
                get
                {
                    return TotalProcessingTime > default(TimeSpan)
                               ? RecieveDate + TotalProcessingTime
                               : default(DateTime);
                }
            }

            ///// <summary>
            ///// Время затраченное на получение экземпляра пользовательского контекста
            ///// </summary>
            //public TimeSpan UserContextBuildTime
            //{
            //    get { return UserContextBuild != null ? UserContextBuild.Elapsed : default(TimeSpan); }
            //}

            /// <summary>
            /// Время затраченное на выполнение сервисной операции
            /// </summary>
            public TimeSpan ProcessingTime
            {
                get { return TotalProcessingTime /*- UserContextBuildTime*/; }
            }

            /// <summary>
            /// Промежуток времени от получения запроса до отправки ответа
            /// </summary>
            public TimeSpan TotalProcessingTime
            {
                get { return RequestProcessing != null ? RequestProcessing.Elapsed : default(TimeSpan); }
            }

            ///// <summary>
            ///// Таймер для измерения времени, затрачиваемого на создание экземпляра пользовательского контекста
            ///// </summary>
            //public Stopwatch UserContextBuild { get; set; }

            /// <summary>
            /// Таймер для измерения времени, затрачиваемого на обработку запроса
            /// </summary>
            public Stopwatch RequestProcessing { get; set; }
        }

        private string GetRemoteEndpointAddress(Message message)
        {
            return ((RemoteEndpointMessageProperty)message.Properties[RemoteEndpointMessageProperty.Name]).Address;
        }

        #region IDispatchMessageInspector Members

        public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
        {
            var requestStatistics = new RequestStatistics { RecieveDate = DateTime.Now, RequestProcessing = Stopwatch.StartNew() };
            try
            {
                var sessionId = request.Headers.GetHeader<Guid>("UserSessionGuid", "http://TB.Service", "Anyone");
                if (sessionId != Guid.Empty)
                {
                    //requestStatistics.UserContextBuild = Stopwatch.StartNew();  

                    IAuthenticationService authenticationService = null;
                    try
                    {
                        authenticationService = UnityContext.Container.Resolve<IAuthenticationService>();
                    }
                    catch (ResolutionFailedException ex)
                    {
                        _log.Error("Не удалось создать экземпляр IAuthenticationService. Проверьте конфигурационный файл приложения/службы.", ex);
                    }
                    catch (Exception ex)
                    {
                        _log.Error("Ошибка при получении экземпляра IAuthenticationService", ex);
                    }
                    UserContext userContext;
                    if (authenticationService != null)
                        userContext = authenticationService.GetContext(sessionId);
                    else
                        userContext = new UserContext()
                        {
                            SessionGuid = sessionId,
                            Terminal = GetRemoteEndpointAddress(request)
                        };

                    userContext.Action = request.Headers.Action;
                    OperationContext.Current.Extensions.Add(userContext);
                    //requestStatistics.UserContextBuild.Stop();
                }
                else
                {
                    _log.Error("Не авторизованный доступ!");
                    throw (new AuthenticationException("Не авторизованный доступ!"));

                }
            }
            catch (Exception ex)
            {
                if (ex.GetType() == typeof (AuthenticationException))
                    throw new FaultException<ExceptionDetail>(new ExceptionDetail(ex));

                var error = string.Empty;
                if (request.Headers.Any(x => x.Name == "UserSessionError" && x.Namespace == "http://TB.Service" && x.Actor == "Anyone"))
                    error = request.Headers.GetHeader<string>("UserSessionError", "http://TB.Service", "Anyone");
                _log.Error(string.Format("Ошибка выставления контекста {0}. ({1})", GetRemoteEndpointAddress(request), error), ex);
            }
            return requestStatistics;
        }

        public void BeforeSendReply(ref Message reply, object correlationState)
        {
            var requestStatistics = (RequestStatistics)correlationState;
            requestStatistics.RequestProcessing.Stop();

            var userContext = UserContext.Current;
            if (userContext.SessionGuid != Guid.Empty)
            {
                if (reply != null)
                {
                    if (!reply.IsFault)
                    {
                        if (_log.IsInfoEnabled)
                            _log.InfoFormat(
                                "User: {0}, вызов завершен за {1}.",// (UserContextBuildTime = {2}).",
                                userContext.Name, requestStatistics.TotalProcessingTime/*,
                                requestStatistics.UserContextBuildTime*/);
                    }
                    else
                        _log.WarnFormat(
                            "User: {0}, вызов завершен с ошибкой за {1}.",// (UserContextBuildTime = {2}).",
                            userContext.Name, requestStatistics.TotalProcessingTime/*,
                            requestStatistics.UserContextBuildTime*/);
                }
            }
            OperationContext.Current.Extensions.Remove(UserContext.Current);
        }

        #endregion
    }
}
