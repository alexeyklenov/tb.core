﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Impl;
using NHibernate.Persister.Entity;
using NHibernate.SqlCommand;
using NHibernate.Type;
using NHibernate.Util;
using TB.BOL.Descriptions;
using TB.DAL.Interface;
using TB.Services.Infrastructure.Dto;
using TB.Utils;

namespace TB.Services.Infrastructure.Translators
{
    /// <summary>
    ///Класс формирующий projections для ICriteria на основе "динамических свойств" описанных в классе DynamicProperty
    /// </summary>
    public static class DynamicPropertiesParser
    {
        /// <summary>
        /// Возвращает ProjectionList
        /// </summary>
        /// <param name="criteria">критерий</param>
        /// <param name="properties">динамические поля</param>
        /// <returns></returns>
        private static ProjectionList GetProjectionList(ICriteria criteria, IEnumerable<DynamicProperty> properties)
        {
            var cr = criteria.Clone() as ICriteria;
            cr.ClearOrders();

            var criteriaTranslator =
               new HibernateHqlAndCriteriaToSqlTranslator(DaoContainer.NHibernateSession.SessionFactory);
            var criteriaLoader = criteriaTranslator.GetCriteriaLoader(cr);
            var persisterInfo = (DaoContainer.NHibernateSession.SessionFactory.GetClassMetadata(
                            criteria.GetRootEntityTypeIfAvailable()) as SingleTableEntityPersister);
            var projections = Projections.ProjectionList();
            foreach (var dynamicProperty in properties)
            {
                if (dynamicProperty.WhereClause.Contains("get_desc"))
                {
                    var hql = dynamicProperty.WhereClause;
                    var alias = criteria.GetCriteriaByAlias(dynamicProperty.Alias) != null ?
                                  criteriaLoader.Translator.GetSQLAlias(cr.GetCriteriaByAlias(dynamicProperty.Alias)) :
                                  "{alias}";

                    //TODO доделать ConstantHelper
                    //if (hql.Contains("Const"))
                        //hql = ConstantHelper.FillConstantValueInString(hql);
                    hql = hql.Replace("(", "('" + dynamicProperty.Alias.Replace("Alias", "") + "',");
                    hql = "Select " + hql + " from " + dynamicProperty.Alias.Replace("Alias", "") + " m where m.ID=:ID";
                    var sql = criteriaTranslator.ToSql(hql);
                    var id = sql.Split('.').Last();
                    sql = sql.Replace("?", alias + "." + id.Substring(0, id.IndexOf('?') - 1));

                    sql = sql.Replace("select ", "");
                    sql = sql.Substring(0, sql.IndexOf(" as ", StringComparison.Ordinal));
                    if (((CriteriaImpl)criteria).Projection == null
                        || ((CriteriaImpl)criteria).Projection.Aliases.Contains(dynamicProperty.Name) == false)
                        projections.Add(Projections.SqlProjection(sql + " as " + dynamicProperty.Name.Substring(dynamicProperty.Name.IndexOf('$') + 1)
                            , new[] { dynamicProperty.Name.Substring(dynamicProperty.Name.IndexOf('$') + 1) }, new IType[] { NHibernateUtil.String }), dynamicProperty.Name);

                }
                else

                    //Если не Hql запрос
                    if (dynamicProperty.WhereClause.ToLower().IndexOf("from", 0, StringComparison.Ordinal) == -1)
                    {

                        if (dynamicProperty.OwnerTypeName != null && !dynamicProperty.OwnerTypeName.Contains(persisterInfo.EntityType.ReturnedClass.FullName))
                            persisterInfo = (DaoContainer.NHibernateSession.SessionFactory.GetClassMetadata(
                                     Type.GetType(dynamicProperty.OwnerTypeName)) as SingleTableEntityPersister);

                        DetachedCriteria dtCriteria = null;
                        //Конечное свойство объекта
                        String property = dynamicProperty.WhereClause.GetSubstrBySeparator('[', ']');

                        //список бизнес объектов (путь до необходимого свойства)
                        List<String> objs = dynamicProperty.WhereClause.Replace("[" + property + "]", "")
                                                .Split(new[] { ")." }, StringSplitOptions.RemoveEmptyEntries)
                                                .ToList();
                        //если свойство не корневое
                        if (objs.Count > 0)
                        {

                            var objProp = objs[0];
                            if (objs[0].Contains("("))
                                objProp = objs[0].Substring(0, objs[0].IndexOf('('));
                            if (objProp.Contains("."))
                                objProp = objProp.Substring(0, objProp.IndexOf('.'));
                            String alias = "";
                            //если корневое свойство - коллекция, то сформируем подзапрос
                            if (persisterInfo.PropertyTypes[persisterInfo.PropertyNames.ToList().IndexOf(objProp)].IsCollectionType)
                            {
                                Type entityType =
                                    persisterInfo.PropertyTypes[persisterInfo.PropertyNames.ToList().IndexOf(objProp)].
                                        ReturnedClass.GetGenericArguments()[0];
                                if (entityType != null)
                                    dtCriteria = DetachedCriteria.For(entityType, objProp);
                                objs[0] = objs[0].Replace(objProp, "");
                                alias = objProp + ".";
                                dtCriteria.Add(Expression.Sql("rownum = 1"));
                            }

                            //генерация Criteria последовательно по списку бизнес-объектов 
                            for (int i = 0; i < objs.Count; i++)
                            {
                                List<String> objProps = objs[i].Split(new[] { "(", " and " }, StringSplitOptions.RemoveEmptyEntries).ToList();
                                String newAlias = alias;
                                for (int j = 0; j < objProps.Count; j++)
                                {
                                    objProp = objProps[j];
                                    if (dtCriteria != null)
                                    {
                                        newAlias = GenerateCriteria(dtCriteria, newAlias, objProp);
                                        if (i > 0 && j == 0)
                                            alias = newAlias;
                                    }
                                    else
                                    {
                                        if (i == 0)
                                            newAlias = dynamicProperty.Alias + ".";
                                        newAlias = GenerateCriteria(criteria, newAlias, objProp);
                                        if (j == 0)
                                            alias = newAlias;
                                    }
                                }
                            }

                            //Проекция свойства    
                            String projType = dynamicProperty.ProjectionType ?? (property.IndexOf('(') != -1
                                ? property.Substring(0, property.IndexOf('('))
                                : "Property");
                            object projValue = property.IndexOf('(') != -1
                                ? property.GetSubstrBySeparator('(', ')')
                                : property;

                            GetValue(ref projValue);

                            //Получаем тип проекции, если он задан(например SUM() или Constant())
                            var m = typeof(Projections).GetMethods().Where(x => x.Name == projType).ToList();
                            if (dtCriteria != null)
                                dtCriteria.SetProjection(projType == "Constant"
                                    ? Projections.Constant(projValue)
                                    : Projections.Property(alias + projValue));
                            else
                            {
                                IProjection projection = m.FirstOrDefault().Invoke(criteria,
                                    new[]
                                {
                                    projType == "Property" ? alias + projValue : projValue
                                })
                                    as IProjection;
                                if (((CriteriaImpl)criteria).Projection == null || ((CriteriaImpl)criteria).Projection.Aliases.Contains(dynamicProperty.Name) == false)
                                    projections.Add(projection, dynamicProperty.Name);
                            }

                            //Свяжем с основным запросом
                            if (dtCriteria != null)
                            {
                                dtCriteria.Add(Restrictions.EqProperty(
                                                                       (criteria.GetCriteriaByAlias(dynamicProperty.Alias) != null ?
                                                                        criteria.GetCriteriaByAlias(dynamicProperty.Alias).Alias :
                                                                        "{alias}") + ".ID",
                                                                       dtCriteria.Alias + "."
                                                                       + (dtCriteria.GetRootEntityTypeIfAvailable().BaseType != typeof(Description) ?
                                                                            dtCriteria.GetRootEntityTypeIfAvailable().Name :
                                                                            "DescribedObject")
                                                                       + ".ID"));
                                if (((CriteriaImpl)criteria).Projection == null || ((CriteriaImpl)criteria).Projection.Aliases.Contains(dynamicProperty.Name) == false)
                                {
                                    if (projType != "Property" && projType != "Constant")
                                    {
                                        projections.Add(
                                            m.FirstOrDefault(x => x.GetParameters().First().ParameterType == typeof(IProjection))
                                            .Invoke(criteria, new object[] { Projections.SubQuery(dtCriteria) }) as IProjection, dynamicProperty.Name);
                                    }
                                    else
                                        projections.Add(Projections.SubQuery(dtCriteria), dynamicProperty.Name);
                                }
                            }
                        }
                        //если свойство корневое
                        else
                        {
                            //Проекция свойства    
                            var projType = dynamicProperty.ProjectionType ?? (property.IndexOf('(') != -1
                                                  ? property.Substring(0, property.IndexOf('('))
                                                  : "Property");
                            object projValue = property.IndexOf('(') != -1
                                ? property.GetSubstrBySeparator('(', ')')
                                : property;

                            GetValue(ref projValue);

                            //Получаем тип проекции, если он задан(например SUM() или Constant())
                            var projsInfo = typeof(Projections).GetMethods().Where(x => x.Name == projType).ToList();
                            MethodInfo projInfo = null;
                            if (((CriteriaImpl)criteria).Projection == null || ((CriteriaImpl)criteria).Projection.Aliases.Contains(dynamicProperty.Name) == false)
                            {
                                if (!String.IsNullOrEmpty(projValue.ToString()))
                                {
                                    projInfo = projsInfo.FirstOrDefault(x => x.GetParameters().Count() == 1
                                                                             && x.GetParameters().First().ParameterType == projValue.GetType());

                                    projections.Add(projInfo.Invoke(criteria,
                                                            new object[] { dynamicProperty.Alias + "." + projValue }) as IProjection, dynamicProperty.Name);
                                }
                                else
                                {
                                    var a = projsInfo.FirstOrDefault().GetParameters();
                                    if (a.Any() == false)
                                        projInfo = projsInfo.FirstOrDefault(x => x.GetParameters().Any() == false);
                                    projections.Add(projInfo.Invoke(criteria, null) as IProjection, dynamicProperty.Name);
                                }
                            }
                        }

                    }
                    //Если HQL запрос
                    else
                    {
                        var hql = dynamicProperty.WhereClause;
                        var alias = criteria.GetCriteriaByAlias(dynamicProperty.Alias) != null ?
                                       criteriaLoader.Translator.GetSQLAlias(cr.GetCriteriaByAlias(dynamicProperty.Alias)) :
                                       "{alias}";

                        //TODO доделать ConstantHelper
                        //if (hql.Contains("Const"))
                          //  hql = ConstantHelper.FillConstantValueInString(hql);

                        var sql = criteriaTranslator.ToSql(hql);
                        var id = sql.Split('.').Last();
                        var isAgregate = hql.ToLower().Contains("min(") || hql.ToLower().Contains("max(") ||
                                             hql.ToLower().Contains("sum(") || hql.ToLower().Contains("rowcount(") ||
                                             hql.ToLower().Contains("first_value(") || hql.ToLower().Contains("first_value_desc(");
                        sql = " ( " + sql.Replace("?", alias + "." + id.Substring(0, id.IndexOf('?') - 1) + (isAgregate ? " ) " : " and rownum = 1 ) "));
                        if (((CriteriaImpl)criteria).Projection == null
                            || ((CriteriaImpl)criteria).Projection.Aliases.Contains(dynamicProperty.Name) == false)
                            projections.Add(Projections.SqlProjection(sql + " as " + dynamicProperty.Name.Substring(dynamicProperty.Name.IndexOf('$') + 1)
                                , new[] { dynamicProperty.Name.Substring(dynamicProperty.Name.IndexOf('$') + 1) }, new IType[] { NHibernateUtil.String }), dynamicProperty.Name);
                    }
            }

            return projections;
        }

        /// <summary>
        /// Добавляет к criteria дополнительные условия из str
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="alias"></param>
        /// <param name="str"></param>
        /// <returns></returns>
        private static String GenerateCriteria(object criteria, String alias, String str)
        {
            if (!String.IsNullOrEmpty(str))
            {
                if (str.IndexOf(')') != -1 || str.IndexOf('=') != -1)
                {
                    var whereClause = str.GetSubstrBySeparator('(', ')');
                    if (!String.IsNullOrEmpty(whereClause))
                    {
                        var conjCrit = whereClause.Split(new[] { " and " }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        //TODO доделать ConstantHelper
                       /* foreach (var crit in conjCrit)
                        {
                            var localCrit = ConstantHelper.FillConstantValueInString(crit);
                            object val = localCrit.Substring(localCrit.IndexOf('=') + 1);
                            GetValue(ref val);

                            if (criteria is DetachedCriteria)
                                (criteria as DetachedCriteria).Add(Restrictions.Eq(alias + localCrit.Substring(0, localCrit.IndexOf('=')), val));
                            else
                                (criteria as ICriteria).Add(Restrictions.Eq(alias + localCrit.Substring(0, localCrit.IndexOf('=')), val));
                        }*/
                    }
                }
                else
                {
                    var objProp = str.IndexOf('(') != -1
                                         ? str.Substring(0, str.IndexOf('('))
                                         : str;

                    if (criteria is DetachedCriteria)
                        (criteria as DetachedCriteria).CreateCriteria(alias + objProp, objProp);
                    else
                        if ((criteria as ICriteria).GetCriteriaByPath(alias + objProp) == null && (criteria as ICriteria).GetCriteriaByPath(objProp) == null)
                            (criteria as ICriteria).CreateCriteria(alias + objProp, objProp, JoinType.LeftOuterJoin);

                    alias = objProp + ".";
                }
            }

            return alias;
        }

        private static void GetValue(ref object value)
        {
            Boolean b;
            Int64 i;
            if (Boolean.TryParse(value.ToString(), out b))
                value = b;
            else if (Int64.TryParse(value.ToString(), out i))
                value = i;
        }

        /// <summary>
        /// формирует строку sql запроса 
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="projectionDict"> список динамических полей</param>
        /// <param name="hqlFilter">дополнительные условия фильтрации</param>
        /// <param name="aliases"> Список алиасов</param>
        /// <returns></returns>
        public static SqlString GetSqlString(ICriteria criteria, List<DynamicProperty> projectionDict, String hqlFilter, out Dictionary<String, String> aliases, String hint = null)
        {
            //Замапленные поля
            var mappedProjections = projectionDict.FindAll(x => x.FieldType == FieldType.MappedProperty);
            //Вычислимые в запросе поля
            var sqlCalcProjections = projectionDict.FindAll(x => x.FieldType == FieldType.SqlCalculatedField);

            //добавим поля из hqlFilter в проекции
            if (!String.IsNullOrEmpty(hqlFilter))
            {
                var fieldList =
                    hqlFilter.Split(new[] { "[" }, StringSplitOptions.None).Where(x => !String.IsNullOrEmpty(x) && x.IndexOf(']') != -1)
                    .Select(x => x.Substring(0, x.IndexOf("]", StringComparison.Ordinal)).Replace("$", ".")).ToList();
                var hqlProjections = ProjectionProvider.GetProjections(criteria.GetRootEntityTypeIfAvailable(), fieldList);
                hqlProjections.ForEach(x =>
                {
                    x.Name = x.Name.Replace(".", "$");
                });

                hqlProjections.FindAll(x => !mappedProjections.Exists(y => y.Name == x.Name)).ForEach(mappedProjections.Add);
            }

            //добавим поля из сортировки

            var orderFields = (criteria as CriteriaImpl).IterateOrderings().ToList()
                .FindAll(x => !x.Order.ToString().StartsWith("ID "))
                .ConvertAll(x => x.ToString().Substring(0, x.ToString().IndexOf(" ", StringComparison.Ordinal)));
            if (orderFields.Count > 0)
            {
                var ordrerProjections = ProjectionProvider.GetProjections(criteria.GetRootEntityTypeIfAvailable(),
                    orderFields);
                ordrerProjections.ForEach(x => { x.Name = x.Name.Replace(".", "$"); });
                ordrerProjections.FindAll(x => !mappedProjections.Exists(y => y.Name == x.Name))
                    .ForEach(mappedProjections.Add);
            }

            var projections = GetProjectionList(criteria, mappedProjections);

            if (((CriteriaImpl)criteria).Projection != null && ((CriteriaImpl)criteria).Projection.Aliases.Any())
                projections.Add(((CriteriaImpl)criteria).Projection);
            else
                if (!mappedProjections.Any())
                    projections.Add(Projections.Constant(1), "EmptySql");

            criteria.SetProjection(projections);

            var criteriaTranslator =
            new HibernateHqlAndCriteriaToSqlTranslator(DaoContainer.NHibernateSession.SessionFactory);
            var criteriaLoader = criteriaTranslator.GetCriteriaLoader(criteria);

            //сформируем словарь алиасов
            var al = criteriaLoader.Translator.ProjectedAliases.Where(x => x != null).ToList();
            aliases = al.ToDictionary(x => x, x => criteriaLoader.Translator.ProjectedColumnAliases[al.IndexOf(x)]);

            var calcStr = "SELECT ";
            //добавим sqlCalcProjection поля
            foreach (var sqlCalcProjection in sqlCalcProjections)
            {
                while (sqlCalcProjection.WhereClause.Contains("["))
                {
                    var prop = sqlCalcProjection.WhereClause.GetSubstrBySeparator('[', ']');
                    var projAlias = prop.Contains(".") ? prop : (criteria.Alias != sqlCalcProjection.Alias ? sqlCalcProjection.Alias + "$" + prop : prop);
                    sqlCalcProjection.WhereClause = sqlCalcProjection.WhereClause.Replace("[" + prop + "]", aliases[projAlias]);
                }
                if (!aliases.ContainsKey(sqlCalcProjection.Name))
                    aliases.Add(sqlCalcProjection.Name, sqlCalcProjection.Name);
                calcStr += sqlCalcProjection.WhereClause + " as " + sqlCalcProjection.Name + ",";
            }
            //  (criteriaLoader.SqlString as object[])[0] +=  hint + " ";
            var sqll = criteriaLoader.SqlString.Insert(0, calcStr + " t.* FROM (");
            sqll = sqll.Append(")t");

            //добавим условия hqlFilter
            if (!String.IsNullOrEmpty(hqlFilter))
            {
                while (hqlFilter.Contains("["))
                {
                    var prop = hqlFilter.GetSubstrBySeparator('[', ']');
                    hqlFilter = hqlFilter.Replace("[" + prop + "]",
                                                   aliases[prop]);
                }
                sqll = sqll.Append(" where " + hqlFilter);
            }

            return sqll;
        }

        /// <summary>
        /// формирует sql запрос 
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="sqlString">строка запроса</param>
        /// <returns></returns>
        public static ISQLQuery GetSqlQuery(ICriteria criteria, SqlString sqlString)
        {
            var maxResult = (criteria as CriteriaImpl).MaxResults;
            var firstResult = (criteria as CriteriaImpl).FirstResult;

            var criteriaTranslator =
                new HibernateHqlAndCriteriaToSqlTranslator(DaoContainer.NHibernateSession.SessionFactory);
            var criteriaLoader = criteriaTranslator.GetCriteriaLoader(criteria);


            var parameterValues = criteriaLoader.Translator.GetQueryParameters().PositionalParameterValues;
            var parametersTypes = criteriaLoader.Translator.GetQueryParameters().PositionalParameterTypes;
            var sqlQuery = DaoContainer.NHibernateSession.CreateSQLQuery(sqlString.ToString());

            for (var i = 0; i < parametersTypes.Count(); i++)
                sqlQuery.SetParameter(i, parameterValues[i], parametersTypes[i]);

            var namedParameters = criteriaLoader.Translator.GetQueryParameters().NamedParameters;
            var j = 0;
            if (namedParameters.Any())
                namedParameters.ForEach(x => sqlQuery.SetParameter(j++, x.Value.Value, x.Value.Type));

            if (maxResult > 0)
            {
                sqlQuery.SetFirstResult(firstResult);
                sqlQuery.SetMaxResults(maxResult);
            }

            return sqlQuery;
        }
    }
}
