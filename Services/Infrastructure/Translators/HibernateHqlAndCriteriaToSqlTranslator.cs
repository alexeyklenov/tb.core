﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Engine;
using NHibernate.Hql.Ast.ANTLR;
using NHibernate.Impl;
using NHibernate.Loader.Criteria;
using NHibernate.Persister.Entity;

namespace TB.Services.Infrastructure.Translators
{
    /// <summary>
    ///Транслирует Hql и ICriteria в Sql
    /// </summary>
    public class HibernateHqlAndCriteriaToSqlTranslator
    {
        public HibernateHqlAndCriteriaToSqlTranslator(ISessionFactory sessionFactory)
        {
            SessionFactory = sessionFactory;
        }

        public ISessionFactory SessionFactory { get; set; }

        /// <summary>
        /// Транслирует ICriteria в Sql
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns>Sql строка</returns>
        public string ToSql(ICriteria criteria)
        {
            var c = (CriteriaImpl)criteria;
            var s = (SessionImpl)c.Session;
            var factory = (ISessionFactoryImplementor)s.SessionFactory;
            var implementors = factory.GetImplementors(c.EntityOrClassName);

            var loader = new CriteriaLoader(
               (IOuterJoinLoadable)factory.GetEntityPersister(implementors[0]),
               factory,
               c,
               implementors[0],
               s.EnabledFilters);

            return loader.SqlString.ToString();
        }

        public string GetSqlAlias(CriteriaLoader loader, ICriteria criteria, String alias)
        {
            return loader.Translator.GetSQLAlias(criteria.GetCriteriaByAlias(alias));
        }

        public CriteriaLoader GetCriteriaLoader(ICriteria criteria)
        {
            var c = (CriteriaImpl)criteria;
            var s = (SessionImpl)c.Session;
            var factory = (ISessionFactoryImplementor)s.SessionFactory;
            var implementors = factory.GetImplementors(c.EntityOrClassName);
            return new CriteriaLoader(
                  (IOuterJoinLoadable)factory.GetEntityPersister(implementors[0]),
                  factory,
                  c,
                  implementors[0],
                  s.EnabledFilters);
        }

        public string ToSql(DetachedCriteria criteria, ISession session)
        {
            var c = (CriteriaImpl)criteria.GetExecutableCriteria(session);
            var s = (SessionImpl)c.Session;
            var factory = (ISessionFactoryImplementor)s.SessionFactory;
            var implementors = factory.GetImplementors(c.EntityOrClassName);
            var loader = new CriteriaLoader(
                (IOuterJoinLoadable)factory.GetEntityPersister(implementors[0]),
                factory,
                c,
                implementors[0],
                s.EnabledFilters);

            var sql = loader.SqlString.ToString();
            //Подставим параметры напрямую
            foreach (var param in loader.Translator.GetQueryParameters().PositionalParameterValues)
            {
                sql = sql.Substring(0, sql.IndexOf("?", StringComparison.Ordinal)) + param +
                      sql.Substring(sql.IndexOf("?", StringComparison.Ordinal) + 1);
            }
            return sql;
        }
        /// <summary>
        /// Транслирует hql в Sql
        /// </summary>
        /// <param name="hqlQueryText"></param>
        /// <returns>Sql строка</returns>
        public string ToSql(string hqlQueryText)
        {

            if (!String.IsNullOrEmpty(hqlQueryText))
            {
                var translatorFactory = new ASTQueryTranslatorFactory();
                var factory = (ISessionFactoryImplementor)SessionFactory;
                var translator = translatorFactory.CreateQueryTranslators(
                    hqlQueryText,
                    null,
                    true,
                    new Dictionary<String, IFilter>(),
                    factory)[0];
                translator.Compile(new Dictionary<String, String>(), false);
                return translator.SQLString;
            }

            return null;
        }
    } 
}
