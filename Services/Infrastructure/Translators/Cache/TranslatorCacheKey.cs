﻿using System;
using TB.BOL;

namespace TB.Services.Infrastructure.Translators.Cache
{
    /// <summary>
    /// Тип представляющий ключ в кэше трансляции
    /// </summary>
    public sealed class TranslatorCacheKey
    {
        #region Fields

        private readonly DomainObject<long> _entity;
        private readonly Type _dtoType;

        #endregion

        #region Properties

        /// <summary>
        /// Бизнес-объект
        /// </summary>
        public DomainObject<long> Entity
        {
            get { return _entity; }
        }

        /// <summary>
        /// Тип DTO представляющего бизнес-объект
        /// </summary>
        public Type DtoType
        {
            get { return _dtoType; }
        }

        #endregion

        #region Methods

        public TranslatorCacheKey(DomainObject<long> entity, Type dtoType)
        {
            _entity = entity;
            _dtoType = dtoType;
        }

        public override Boolean Equals(Object obj)
        {
            return obj != null && Equals(obj as TranslatorCacheKey);
        }

        public Boolean Equals(TranslatorCacheKey key)
        {
            if (key == null) return false;
            return Entity.Equals(key.Entity) && DtoType == key.DtoType;
        }

        public override Int32 GetHashCode()
        {
            unchecked
            {
                return ((_entity != null ? _entity.GetHashCode() : 0) * 397) ^ (_dtoType != null ? _dtoType.GetHashCode() : 0);
            }
        }

        #endregion
    }
}
