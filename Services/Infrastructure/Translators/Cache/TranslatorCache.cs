﻿using System;
using System.ServiceModel;

namespace TB.Services.Infrastructure.Translators.Cache
{
    /// <summary>
    /// Тип для работы с кэшем трансляции
    /// </summary>
    public static class TranslatorCache
    {
        /// <summary>
        /// Позволяет определить включено ли использование кэша трансляции в контексте текущей операции
        /// </summary>
        public static Boolean IsEnabled
        {
            get { return Container != null; }
        }

        /// <summary>
        /// Контейнер кэша трансляции. Содержит перечень сформированных DTO и бизнес объектов, 
        /// на основнии которых были созданы эти DTO
        /// </summary>
        public static TranslatorContainer Container
        {
            get { return OperationContext.Current != null ? OperationContext.Current.Extensions.Find<TranslatorContainer>() : null; }
        }
    }
}
