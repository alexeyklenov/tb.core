﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using TB.Services.Infrastructure.Dto;

namespace TB.Services.Infrastructure.Translators.Cache
{
    /// <summary>
    /// Словарь для хранения пар значений бизнес-объекта и его DTO
    /// </summary>
    public sealed class TranslatorContainer : Dictionary<TranslatorCacheKey, DtoBase>, IExtension<OperationContext>
    {
        public Boolean GatherStatisticsEnabled { get; private set; }

        public Dictionary<Type, Int32> Statistics { get; private set; }

        public Int32 HitCount
        {
            get { return Statistics.Sum(x => x.Value); }
        }

        public TranslatorContainer(Boolean gatherStatistics)
        {
            GatherStatisticsEnabled = gatherStatistics;
            Statistics = new Dictionary<Type, Int32>();
        }

        public new Boolean TryGetValue(TranslatorCacheKey key, out DtoBase value)
        {
            Boolean result = base.TryGetValue(key, out value);
            if (GatherStatisticsEnabled && result)
            {
                if (Statistics.ContainsKey(key.DtoType))
                    Statistics[key.DtoType]++;
                else
                    Statistics.Add(key.DtoType, 1);
            }
            return result;
        }

        public void Attach(OperationContext owner)
        {
        }

        public void Detach(OperationContext owner)
        {
        }
    }
}
