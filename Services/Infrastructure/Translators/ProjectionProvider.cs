﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using NHibernate.Persister.Entity;
using TB.BOL;
using TB.DAL.Interface;
using TB.Services.Infrastructure.Dto;
using TB.Utils;

namespace TB.Services.Infrastructure.Translators
{
    public static class ProjectionProvider
    {
        private static XmlDocument _doc;
        public static XmlDocument Doc
        {
            get
            {
                if (_doc == null)
                {
                    _doc = new XmlDocument();

                    _doc.LoadXml(File.ReadAllText("Translator.xml"));
                }
                return _doc;
            }
            set
            {
                _doc = value;
            }
        }
        private static List<XmlElement> _fields;

        public static List<DynamicProperty> GetProjections(Type persistentType, List<String> projs)
        {
            var projectionDict = new List<DynamicProperty>();
            var entity = Doc.GetElementById(persistentType.Name);
            if (projs != null && projs.Count > 0 && entity != null)
            {
                Int16 i = 0;
                _fields = new List<XmlElement>();
                foreach (XmlElement field in entity.GetElementsByTagName("field"))
                    _fields.Add(field);

                foreach (var proj in projs)
                {
                    if (proj != null && proj.IndexOf("Select", 0, StringComparison.Ordinal) == -1)
                    {
                        var projType = proj.IndexOf('(') != -1
                                            ? proj.Substring(0, proj.IndexOf('('))
                                            : null;

                        var projValue = proj.IndexOf('(') != -1
                                        ? proj.GetSubstrBySeparator('(', ')')
                                        : proj;

                        var element = GetElement(projValue);

                        if (element != null && projectionDict.All(x => x.Name != projValue))
                        {
                            var prop = CreateDynamicProperty(element, proj, projType);
                            projectionDict.Add(prop);
                            //добавим 
                            if (prop.FieldType == FieldType.CalculatedField || prop.FieldType == FieldType.SqlCalculatedField)
                            {
                                var where = prop.WhereClause;
                                while (where.Contains("["))
                                {
                                    var field = where.GetSubstrBySeparator('[', ']');

                                    var relatedField = GetElement(prop.Alias.Contains(persistentType.Name) ? field : prop.Alias + "." + field);

                                    if (relatedField != null
                                        && projectionDict.All(x => x.Name != field) && projectionDict.All(x => x.Name != prop.Alias + "." + field))
                                        projectionDict.Add(CreateDynamicProperty(relatedField,
                                                                                 field.Contains(".") || prop.Alias.Contains(persistentType.Name) ? field : prop.Alias + "." + field,
                                                                                  null));
                                    where = where.Replace('[' + field + ']', "");
                                }
                            }
                        }
                    }
                    else
                    {
                        projectionDict.Add(new DynamicProperty("field" + i, typeof(String), proj));
                        i++;
                    }
                }
            }
            else
            {

                var persisterInfo = (DaoContainer.NHibernateSession.SessionFactory.GetClassMetadata(persistentType) as SingleTableEntityPersister);
                var fields = projs != null ? projs.ConvertAll(y => y.IndexOf('[') != -1 ? y.Substring(0, y.IndexOf('[')) : y) : null;
                persistentType.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                   .Where(x => !x.PropertyType.IsArray
                       && persisterInfo.PropertyNames.Contains(x.Name)
                       && !persisterInfo.PropertyTypes[persisterInfo.PropertyNames.ToList().IndexOf(x.Name)].IsCollectionType
                       && (projs == null || projs.Count == 0 || fields.Contains(x.Name))
                       && x.PropertyType != typeof(Guid)).ToList()
                   .ForEach(property =>
                   {
                       var displayAttr = property.GetCustomAttributes(typeof(DisplayNameAttribute), true).FirstOrDefault() as DisplayNameAttribute;
                       if (!property.PropertyType.IsSubclassOf(typeof(DomainObject<long>)))
                           projectionDict.Add(new DynamicProperty(property.Name, property.PropertyType, "[" + property.Name + "]", null,
                                                FieldType.MappedProperty, true, persistentType.Name, displayName: displayAttr != null ? displayAttr.DisplayName : null));
                       else
                       {
                           projectionDict.Add(new DynamicProperty(property.Name + "[ID]", typeof(Int64?), property.Name + "[ID]", null,
                                                FieldType.MappedProperty, true, persistentType.Name, null, propertyTypeName: property.PropertyType.AssemblyQualifiedName,
                                                displayName: displayAttr != null ? displayAttr.DisplayName : null));

                           if (property.PropertyType.IsSubclassOf(typeof(Reference<Int64>)))
                           {
                               projectionDict.Add(new DynamicProperty(property.Name + "[Code]", typeof(Int64?), property.Name + "[Code]", null,
                                                FieldType.MappedProperty, false, persistentType.Name, displayName: displayAttr != null ? displayAttr.DisplayName : null));
                               projectionDict.Add(new DynamicProperty(property.Name + "[Name]", typeof(String), property.Name + "[Name]", null,
                                                FieldType.MappedProperty, false, persistentType.Name, displayName: displayAttr != null ? displayAttr.DisplayName : null));
                               projectionDict.Add(new DynamicProperty(property.Name + "[ShortName]", typeof(String), property.Name + "[ShortName]", null,
                                                FieldType.MappedProperty, false, persistentType.Name, displayName: displayAttr != null ? displayAttr.DisplayName : null));
                           }
                       }
                   });
            }
            return projectionDict;
        }

        private static XmlElement GetElement(String field)
        {
            XmlElement element;
            if (field.Contains("."))
            {
                var relatedFields = new List<XmlElement>();
                var entity = Doc.GetElementById(field.Substring(0, field.IndexOf('.')).Replace("Alias", ""));
                if (entity == null)
                    element = _fields.Find(x => x.GetAttribute("name") == field);
                else
                {
                    foreach (XmlElement f in entity.GetElementsByTagName("field"))
                        relatedFields.Add(f);
                    element = relatedFields.Find(x => x.GetAttribute("name") == field.Substring(field.IndexOf('.') + 1));
                }
            }
            else
                element = _fields.Find(x => x.GetAttribute("name") == field);
            return element;
        }

        private static DynamicProperty CreateDynamicProperty(XmlElement element, String projName, String projType)
        {
            var name = projName;
            var where = element.GetAttribute("where");
            var type = element.GetAttribute("type");
            var fieldTypestr = element.GetAttribute("fieldType");
            var editable = !String.IsNullOrEmpty(element.GetAttribute("editable")) && Convert.ToBoolean(element.GetAttribute("editable"));
            var fieldType = !String.IsNullOrEmpty(fieldTypestr) ? (FieldType)Enum.Parse(typeof(FieldType), fieldTypestr, true) : FieldType.MappedProperty;

            return new DynamicProperty(name,
                Type.GetType(type),
                where,
                projType,
                fieldType,
                editable,
                element.ParentNode != null ? element.ParentNode.Attributes["name"].Value : null,
                element.GetAttribute("updateMethod"),
                element.HasAttribute("nvl") ? element.GetAttribute("nvl") : null,
                element.GetAttribute("onChangeValue"),
                element.ParentNode != null ? element.ParentNode.Attributes["type"].Value : null
                );
        }

        public static List<DynamicProperty> GetProjectionsByFieldSet(String persistentTypeName, String fieldSet)
        {
            return GetProjections(BOL.BolTypeLocator.GetType(persistentTypeName) ?? Type.GetType(persistentTypeName), GetProjectionString(fieldSet));
        }

        public static List<String> GetProjectionString(String fieldSet)
        {
            if (!String.IsNullOrEmpty(fieldSet))
            {
                //XmlDocument doc = new XmlDocument();
                //doc.LoadXml(File.ReadAllText("Translator.xml"));
                var fieldSetElement = Doc.GetElementById(fieldSet);
                var fields = fieldSetElement.GetAttribute("fields");
                var node = fieldSetElement.ParentNode;
                while (node != null && node.Name == "fieldSet")
                {
                    fields += "," + node.Attributes["fields"].Value;
                    node = node.ParentNode;
                }
                var res = fields.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList().ConvertAll(
                    x => x.Trim(new[] { ' ', '\r', '\n' }));
                return res;
            }

            return null;
        }

        public static String GetHint(String persistentTypeName)
        {
            var entity = Doc.GetElementById(persistentTypeName);
            if (entity == null) return null;
            var hint = new List<XmlElement>();
            foreach (XmlElement field in entity.GetElementsByTagName("hint"))
                hint.Add(field);

            return hint.Select(x => x.GetAttribute("value")).FirstOrDefault();
        }
    }
}
