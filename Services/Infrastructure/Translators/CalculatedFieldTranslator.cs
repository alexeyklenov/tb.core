﻿using System;
using TB.BOL.Descriptions;

namespace TB.Services.Infrastructure.Translators
{
    public static class CalculatedFieldTranslator
    {
        public static String GetEnumDescription(Enum value)
        {
            var d1 = typeof(DescribedEnum<>);
            Type[] typeArgs = { value.GetType() };
            var maketype = d1.MakeGenericType(typeArgs);
            var method = maketype.GetMethod("GetDescription");

            return method.Invoke(null, new object[] { value }).ToString();
        }
    }
}
