﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using TB.Configuration;

namespace TB.Services.Infrastructure.Translators
{
    public static class KnownTranslatorAssemblies
    {
        public static Assembly[] Collection;

        public static void InitializeCollection()
        {
            var result = new List<Assembly>();

            ConfigurationManager.RefreshSection("knownTranslatorAssemblies");

            var knownTranslatorAssembliesSection =
                ConfigurationManager.GetSection("knownTranslatorAssemblies") as KnownTranslatorAssembliesSection;

            if (knownTranslatorAssembliesSection != null)
                result.AddRange(from KnownAssemblyElement assembly in knownTranslatorAssembliesSection.KnownTranslatorAssemblies select Assembly.Load(assembly.KnownAssemblyName));

            Collection = result.ToArray();
        }
    }
}
