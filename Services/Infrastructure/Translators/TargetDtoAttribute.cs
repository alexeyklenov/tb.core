﻿using System;

namespace TB.Services.Infrastructure.Translators
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public sealed class TargetDtoAttribute : Attribute
    {
        public Type DtoType { get; set; }

        /// <summary>
        /// Конструктор атрибута
        /// </summary>
        /// <param name="dtoType">Тип DTO</param>
        public TargetDtoAttribute(Type dtoType)
        {
            DtoType = dtoType;
        }
    }
}
