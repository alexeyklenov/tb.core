﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using NCalc;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Impl;
using NHibernate.SqlCommand;
using TB.BOL;
using TB.DAL.Interface;
using TB.Services.Infrastructure.Dto;
using TB.Services.Infrastructure.Dto.Dynamic;
using TB.Services.Infrastructure.Translators.Cache;

namespace TB.Services.Infrastructure.Translators
{
    /// <summary>
    /// Базовый тип для трансляторов бизнес-объектов
    /// </summary>

    public class DtoTranslator
    {
        #region Fields

        private static readonly Dictionary<Type, DtoTranslator> Cache = new Dictionary<Type, DtoTranslator>();

        #endregion

        #region Properties
        /// <summary>
        /// Тип DTO, с которым работает транслятор
        /// </summary>
        protected Type DtoType { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Выполняет поиск и создание транслятора, ответсвенного за трансляцию определенного типа DTO
        /// </summary>
        /// <param name="dtoType">Тип DTO, для которого ищется транслятор</param>
        /// <returns>Транслятор DTO</returns>
        public static DtoTranslator GetTranslator(Type dtoType)
        {
            if (KnownTranslatorAssemblies.Collection == null)
                KnownTranslatorAssemblies.InitializeCollection();

            var assemblies = KnownTranslatorAssemblies.Collection;

            return GetTranslator(dtoType,
                                 assemblies != null && assemblies.Length > 0
                                     ? assemblies
                                     : new[] { Assembly.GetExecutingAssembly() });
        }

        /// <summary>
        /// Поиск в указанных модулях типа транслятора, 
        /// ответсвенного за трансляцию определенного типа DTO, и его создание
        /// </summary>
        /// <param name="dtoType">Тип DTO, для которого ищется транслятор</param>
        /// <param name="assemblies">Коллекция динамических библиотек</param>
        /// <returns>Транслятор DTO</returns>
        public static DtoTranslator GetTranslator(Type dtoType, params Assembly[] assemblies)
        {
            DtoTranslator result;
            if (!Cache.TryGetValue(dtoType, out result))
                lock (Cache)
                    if (!Cache.TryGetValue(dtoType, out result))
                    {
                        var targetDtoAttributeType = typeof(TargetDtoAttribute);
                        foreach (var asm in assemblies)
                        {
                            var asmTypes = asm.GetTypes();
                            var typeIndx = 0;
                            while (typeIndx < asmTypes.Length)
                            {
                                var attr = asmTypes[typeIndx].GetCustomAttributes(targetDtoAttributeType, false);
                                if (attr.Length != 0 && ((TargetDtoAttribute) attr[0]).DtoType == dtoType)
                                    return
                                        Cache[dtoType] = Activator.CreateInstance(asmTypes[typeIndx]) as DtoTranslator;
                                typeIndx++;
                            }
                        }
                    }
            return result;
        }
        /// <summary>
        /// Выполняет поиск и создание транслятора, ответсвенного за трансляцию определенного типа DTO
        /// </summary>
        /// <typeparam name="T">Тип DTO, для которого ищется транслятор</typeparam>
        /// <returns>Транслятор DTO</returns>
        public static DtoTranslator GetTranslator<T>() where T : DtoBase
        {
            return GetTranslator(typeof(T));
        }

        /// <summary>
        /// Транслирует DTO в бизнес-объект
        /// </summary>
        /// <param name="source">DTO бизнес-объекта</param>
        /// <returns>Бизнес-объект</returns>
        public virtual DomainObject<long> ToBusiness(DtoBase source)
        {
            return null;
        }

        /// <summary>
        /// Транслирует бизнес-объект в DTO
        /// </summary>
        /// <param name="source">Бизнес-объект</param>
        /// <returns>DTO бизнес-объекта</returns>
        protected virtual DtoBase ToDtoInternal(DomainObject<long> source)
        {
            return (DtoBase)Activator.CreateInstance(DtoType);
        }

        /// <summary>
        /// Транслирует объект в DTO
        /// </summary>
        /// <param name="source">Объект</param>
        /// <returns>DTO объекта</returns>
        protected virtual DtoBase ToDtoInternal(Object source)
        {
            return (DtoBase)Activator.CreateInstance(DtoType);
        }

        /// <summary>
        /// Транслирует бизнес-объект в DTO
        /// </summary>
        /// <param name="source">Бизнес-объект</param>
        /// <returns>DTO бизнес-объекта</returns>
        public DtoBase ToDto(DomainObject<long> source)
        {

            if (!TranslatorCache.IsEnabled)
                return ToDtoInternal(source);

            DtoBase dto;
            var key = new TranslatorCacheKey(source, DtoType);
            if (!TranslatorCache.Container.TryGetValue(key, out dto))
            {
                dto = ToDtoInternal(source);
                TranslatorCache.Container.Add(key, dto);
            }
            return dto;
        }

        /// <summary>
        /// Транслирует объект в DTO
        /// </summary>
        /// <param name="source">Объект</param>
        /// <returns>DTO объекта</returns>
        public DtoBase ToDto(Object source)
        {
            try
            {
                return ToDtoInternal(source);
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Ошибка трансляции объекта {0}. Type = {1}, DTO type = {2}",
                                                  source, source.GetType().FullName, DtoType.FullName), ex);
            }
        }

        /// <summary>
        /// Транслирует объекты  на основе ICriteria
        /// </summary>
        /// <param name="criteria">Исходный ICriteria</param>
        /// <param name="fieldList">Список полей для трансляции </param>
        /// <param name="hqlFilter"></param>
        /// <returns>DTO объекта</returns>
        public List<DtoBase> ToDtoByCriteria(ICriteria criteria, List<String> fieldList, String hqlFilter)
        {
            var method = typeof(DtoTranslator).GetMethod("ToDTOByCriteriaGeneric");
            var genericMethod = method.MakeGenericMethod(DtoType);
            var res = (genericMethod.Invoke(this, new object[] { criteria, fieldList, hqlFilter }) as IList).Cast<DtoBase>().ToList();
            return res;
        }

        /// <summary>
        /// Транслирует объекты  на основе ICriteria
        /// </summary>
        /// <param name="criteria">Исходный ICriteria</param>
        /// <param name="fieldList">Список полей для трансляции </param>
        /// <param name="hqlFilter"></param>
        /// <returns>DTO объекта</returns>
        public IList<T> ToDtoByCriteriaGeneric<T>(ICriteria criteria, List<String> fieldList, String hqlFilter) where T : DtoBase, new()
        {
            ToDtoByCriteriaInternal(criteria);

            var typeT = typeof(T);
            var dtoProperties = typeT.GetProperties(BindingFlags.Instance | BindingFlags.Public).Select(x => x.Name);

            //Получим проекции полей из ДТО
            var projectionsFromDto = Projections.ProjectionList();
            if (((CriteriaImpl)criteria).Projection != null)
                projectionsFromDto.Add(((CriteriaImpl)criteria).Projection);

            //если тип дто производный от DTODynamic и fieldList пустой, то не будем добавлять поля автоматом
            var persisterType = criteria.GetRootEntityTypeIfAvailable();
            var projectionDict = typeT != typeof(DtoDynamic) && fieldList == null ? new List<DynamicProperty>() :
                ProjectionProvider.GetProjections(persisterType, fieldList);

            projectionDict.ForEach(x => x.Name = x.Name.Replace(".", "$"));
            //Замапленные поля
            var mappedProjections = projectionDict.FindAll(x => x.FieldType == FieldType.MappedProperty);
            //Вычислимые поля
            var calcProjections = projectionDict.FindAll(x => x.FieldType == FieldType.CalculatedField)
                .FindAll(x => !dtoProperties.Contains(x.Name));

            //Вычислимые в запросе поля
            var sqlCalcProjections = projectionDict.FindAll(x => x.FieldType == FieldType.SqlCalculatedField);
            //Хинты
            var hint = ProjectionProvider.GetHint(persisterType.Name);
            if (((CriteriaImpl)criteria).Projection != null)
                ((CriteriaImpl)criteria).Projection.Aliases.ToList().RemoveAll(x => projectionDict.Exists(y => y.Name == x));

            //Получим проекции динамических полей
            //Если включен постраничный режим то сначала выполним запрос по фильтрам, чтобы получить список идов        
            Dictionary<String, String> aliases;
            SqlString sqlString;
            ISQLQuery sqlQuery;
            //Если включен постраничный режим то сначала выполним запрос по фильтрам, чтобы получить список идов     
            if ((criteria as CriteriaImpl).MaxResults > 0)
            {
                var pageCriteria = criteria.Clone() as ICriteria;
                pageCriteria.SetProjection(Projections.Constant(1));
                if (String.IsNullOrEmpty(hint))
                {
                    if (!(criteria as CriteriaImpl).IterateOrderings().Any())
                        pageCriteria.AddOrder(new Order("ID", false));
                }
                else if ((criteria as CriteriaImpl).IterateOrderings().ToList().All(x => x.Order.ToString().StartsWith("ID ")))
                    pageCriteria.ClearOrders();
               var proj = ProjectionProvider.GetProjections(persisterType, new List<string> { "ID" });
                var ids = new List<Int64>();
                sqlString = DynamicPropertiesParser.GetSqlString(pageCriteria, proj, hqlFilter, out aliases, hint);
                sqlQuery = DynamicPropertiesParser.GetSqlQuery(pageCriteria, sqlString);
                ids.AddRange(from object[] val in sqlQuery.List() select (Int64)val[0]);

                criteria.Add(Restrictions.In(criteria.Alias + ".ID", ids));
            }
            criteria.SetMaxResults(0);
            criteria.SetFirstResult(0);
            sqlString = DynamicPropertiesParser.GetSqlString(criteria, projectionDict, hqlFilter, out aliases);
            sqlQuery = DynamicPropertiesParser.GetSqlQuery(criteria, sqlString);

            var res = sqlQuery.List();
            mappedProjections.InsertRange(0, sqlCalcProjections);
            projectionDict.ForEach(x => x.Name = x.Name.Replace("$", "."));

            //TODO бесполезная строчка кода! Проанализировать и убрать
            var dao = DaoContainer.GetDao(criteria.GetRootEntityTypeIfAvailable());
            var dtos = new List<T>();
            if (res.Count > 0)
                if (res[0] != null && res[0].GetType().IsArray)
                {
                    var mappedProj = mappedProjections.FindAll(x => !projectionsFromDto.Aliases.Contains(x.Name));

                    foreach (object[] val in res)
                    {
                        var dto = new T();
                        // Заполним словарь DynamicProperties
                        val.Zip(
                            mappedProj,
                            (v, p) => dto.AddProperty(p.Name, p.Type, v, p.Nvl)
                            ).ToDictionary(p => p.Name);

                        //Заполним основные Property в ДТО
                        for (var i = 0; i < projectionsFromDto.Aliases.Count(); i++)
                        {
                            var prop = typeT.GetProperty(projectionsFromDto.Aliases[i]);
                            var value = val[i + mappedProj.Count];

                            if (prop.PropertyType == typeof(Boolean))
                                value = value != null && (value.ToString() == "1" || value.ToString().ToLower() == "true");
                            else
                            {
                                var underlyingType = Nullable.GetUnderlyingType(prop.PropertyType);
                                // if underlyingType has null value, it means the original type wasn't nullable
                                if (value != null && value.GetType() != (underlyingType ?? prop.PropertyType))
                                    value = Convert.ChangeType(value, underlyingType ?? prop.PropertyType);
                            }
                            prop.SetValue(dto, value, null);
                        }
                        dtos.Add(dto);
                    }
                }
                else
                {
                    foreach (var val in res)
                    {
                        var dto = new T();
                        //Заполним словарь DynamicProperties
                        mappedProjections.ForEach(x => dto.AddProperty(x.Name, x.Type, val));
                        //Заполним основные Property в ДТО
                        for (var i = 0; i < projectionsFromDto.Aliases.Count(); i++)
                        {
                            typeT.GetProperty(projectionsFromDto.Aliases[i]).SetValue(dto, val, null);
                        }
                        dtos.Add(dto);
                    }
                }

            //добавим вычислимые поля
            if (calcProjections.Count > 0)
                foreach (var dto in dtos)
                {
                    AddCalcFields(dto, calcProjections);
                    //dto.Properties = dto.Properties.OrderBy(x => x.Key).ToDictionary(x => x.Key, x => x.Value);
                }
            return dtos;
        }

        //Добавляет вычислимые поля
        private void AddCalcFields(DtoBase dto, IEnumerable<DynamicProperty> calcProjections)
        {
            //доб
            foreach (var calcProjection in calcProjections)
            {
                var e = new NCalc.Expression(calcProjection.WhereClause);
                e.EvaluateParameter += delegate(string name, ParameterArgs args)
                {
                    Boolean res;
                    if (Boolean.TryParse(name, out res))
                        args.Result = res;
                    else
                        args.Result = calcProjection.Type == typeof(String) ? dto[name] ?? "" : (dto[name]);
                };
                e.EvaluateFunction += delegate(string name, FunctionArgs args)
                {
                    if (name == "nvl")
                        args.Result = args.Parameters[0].Evaluate() ?? args.Parameters[1].Evaluate();
                    if (name == "GetEnumDescription")
                        args.Result = CalculatedFieldTranslator.GetEnumDescription((Enum)args.Parameters[0].Evaluate());
                };
                object val = null;
                try
                {
                    val = e.Evaluate();
                }
                catch (Exception ex)
                {
                    log4net.LogManager.GetLogger("AddCalcFields Error: ").Info(ex.Message + " " + ex.InnerException);
                }
                if (!dto.Properties.ContainsKey(calcProjection.Name))
                    dto.Properties.Add(calcProjection.Name, new DynamicProperty(calcProjection.Name, calcProjection.Type, val));
            }
        }

        /// <summary>
        /// Транслирует бизнес-объект в DTO
        /// </summary>
        /// <param name="criteria"> </param>
        public virtual void ToDtoByCriteriaInternal(ICriteria criteria)
        {

        }
        /// <summary>
        /// Проверяет, что параметр является не null и наследником определенного типа DTO
        /// </summary>
        /// <param name="paramName">Наименование исследуемого параметра</param>
        /// <param name="domainObjectType">Тип DTO, в иерархии которого производится поиск типа параметра</param>
        /// <param name="param">Исследуемый параметр</param>
        protected void ValidateParameters(String paramName, Type domainObjectType, Object param)
        {
            if (param == null)
                throw new ArgumentNullException(paramName, "Параметр равен null");
            var paramType = param.GetType();
            if (!(paramType == domainObjectType || paramType.IsSubclassOf(domainObjectType)))
                throw new ArgumentException(String.Format("Недопустимый тип параметра. " +
                    "Параметр должен иметь тип производный от {0}", domainObjectType.Name), paramName);
        }

        #endregion
    }

    /// <summary>
    /// Базовый тип для трансляторов бизнес-объектов открытого типа
    /// </summary>
    /// <typeparam name="T">Тип-параметр в типе бизнес-объекта</typeparam>
    public abstract class DtoTranslator<T> : DtoTranslator
    {
        struct GenericTranslatorKey
        {
            public RuntimeTypeHandle DtoRuntimeTypeHandle;
            public RuntimeTypeHandle GenericParamRuntimeTypeHandle;
        }

        private static readonly Dictionary<GenericTranslatorKey, Type> _cache = new Dictionary<GenericTranslatorKey, Type>();

        /// <summary>
        /// Выполняет поиск и создание обобщенного транслятора, ответсвенного за трансляцию определенного типа DTO
        /// </summary>
        /// <typeparam name="T">Тип, используемый в обобщеном трансляторе</typeparam>
        /// <param name="dtoType">Тип DTO, для которого ищется обощенный транслятор</param>
        /// <returns>Транслятор DTO</returns>
        public new static DtoTranslator<T> GetTranslator(Type dtoType)
        {
            var typeT = typeof(T);
            var cnvKey = new GenericTranslatorKey
            {
                DtoRuntimeTypeHandle = dtoType.TypeHandle,
                GenericParamRuntimeTypeHandle = typeT.TypeHandle
            };
            if (!_cache.ContainsKey(cnvKey))
                lock (_cache)
                    if (!_cache.ContainsKey(cnvKey))
                    {
                        var asm = Assembly.GetExecutingAssembly();
                        var asmTypes = asm.GetTypes();
                        var typeIndx = 0;
                        var targetDtoAttributeType = typeof(TargetDtoAttribute);
                        DtoTranslator<T> cnv = null;
                        while (cnv == null && typeIndx < asmTypes.Length)
                        {
                            var attr = asmTypes[typeIndx].GetCustomAttributes(targetDtoAttributeType, false);
                            if (attr.Length != 0 && ((TargetDtoAttribute)attr[0]).DtoType == dtoType
                                && asmTypes[typeIndx].IsGenericType && asmTypes[typeIndx].ContainsGenericParameters)
                            {
                                var genericArgs = asmTypes[typeIndx].GetGenericArguments();
                                if (genericArgs.Length == 1 && genericArgs[0].IsGenericParameter)
                                {
                                    Type[] typeParams = { typeT };
                                    Type translatorType = null;
                                    try
                                    {
                                        translatorType = asmTypes[typeIndx].MakeGenericType(typeParams);
                                    }
                                    catch (Exception)
                                    {
                                    }
                                    // Создание транслятора при нахождении подходящего обобщенного типа транслятора 
                                    // для данного DTO с одним открытым параметром, в качестве которого подходит T
                                    if (translatorType != null)
                                    {
                                        cnv = (Activator.CreateInstance(translatorType) as DtoTranslator<T>);
                                        var cacheKey = new GenericTranslatorKey
                                        {
                                            DtoRuntimeTypeHandle = dtoType.TypeHandle,
                                            GenericParamRuntimeTypeHandle = typeT.TypeHandle
                                        };
                                        _cache[cacheKey] = translatorType;
                                    }
                                }
                            }
                            typeIndx++;
                        }
                    }
            return Activator.CreateInstance(_cache[cnvKey]) as DtoTranslator<T>;
        }
    }
}
