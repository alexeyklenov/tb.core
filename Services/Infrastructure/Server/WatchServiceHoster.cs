﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.ServiceModel;
using log4net;
using TB.Services.Common;
using TB.Services.Common.Interface;
using TB.Services.Infrastructure.Client;
using Utils;

namespace TB.Services.Infrastructure.Server
{
    /// <summary>
    /// Осуществляет хостинг WCF сервисов из указанного каталога
    /// каталог может быть указан в конфигурационном файле 
    ///   <appSettings>
    ///      <add key="DropPath" value=".\Services"/>
    ///   </appSettings>
    /// либо передан параметром в метод Start
    /// если не найден ни по одному из этих мест используется текущий каталог
    /// Сборки со службами должны быть размещены в каталоге с сопутствующими сборками
    /// и с конфигурационным файлом Имя сборки.dll.config 
    /// </summary>
    public class WatchServiceHoster
    {
        static Dictionary<string, AppDomain> _appDomains = new Dictionary<string, AppDomain>();
        static ILog _log = LogManager.GetLogger("WatchServiceHoster");

        //ServiceHost _host;
        /*~WatchServiceHoster()
        {
            if (_host.State == CommunicationState.Opened)
                _host.Close();
        }*/

        /// <summary>
        /// Запуск хостера
        /// </summary>
        /// <param name="serviceAssemblyPath">Путь к каталогу со сборками сервисов</param>
        public static void Start(string serviceAssemblyPath = null)
        {
                
            const string pattern = "*.dll";
            var dropPath = "";
            if (serviceAssemblyPath != null)
            {
                dropPath =
                Path.GetFullPath(
                    string.IsNullOrEmpty(serviceAssemblyPath)
                        ? ConfigurationManager.AppSettings["DropPath"] ??
                          Environment.CurrentDirectory
                        : serviceAssemblyPath);
                _log.Debug(dropPath);

                if (Environment.UserInteractive)
                    Console.Title = dropPath + Path.DirectorySeparatorChar + pattern;
                if (!Directory.Exists(dropPath))
                    throw new DirectoryNotFoundException(dropPath);
            }

            // Служба - каталог служб
            var host = new ServiceHost(typeof (ServiceCatalog));
            var port = ConfigurationManager.AppSettings["ServerPort"];

            host.AddServiceEndpoint(typeof (IServiceCatalog),
                new NetTcpBinding(SecurityMode.None) {PortSharingEnabled = true},
                string.Format("net.tcp://localhost:{0}/ServiceCatalog", port));

            host.AddServiceEndpoint(typeof (IServiceCatalog),
                new NetNamedPipeBinding(NetNamedPipeSecurityMode.None),
                string.Format("net.pipe://localhost/ServiceCatalog{0}", port));

            host.Open();

            //Служба управления кэшами
            var hostC = new ServiceHost(typeof (CacheManagementService));

            //Публичная точка доступа (извне)
            var ep = hostC.AddServiceEndpoint(typeof (ICacheManagementService),
                new NetTcpBinding(SecurityMode.None) {PortSharingEnabled = true},
                string.Format("net.tcp://{0}:{1}/CacheManagementService", System.Net.Dns.GetHostName(), port));
            // Локальная точка доступа (для добавления подслужб)
            hostC.AddServiceEndpoint(typeof (ICacheManagementService),
                new NetNamedPipeBinding(NetNamedPipeSecurityMode.None),
                string.Format("net.pipe://localhost/CacheManagementService{0}", port));
            hostC.Open();
            // Добавить в службу
            using (
                var sc = new ClientProxy<IServiceCatalog>(new NetNamedPipeBinding(NetNamedPipeSecurityMode.None),
                    string.Format("net.pipe://localhost/ServiceCatalog{0}", port)))
            {
                sc.Channel.AddService(ep.Contract.ContractType.Name, ep.Address.ToString());
            }


            if (serviceAssemblyPath != null)
            {
                // sets up file system monitoring
                var dfsw = new DelayedFileSystemWatcher(dropPath, pattern);
                dfsw.Created += dfsw_CreatedOrChanged;
                dfsw.Changed += dfsw_CreatedOrChanged;
                dfsw.Deleted += dfsw_Deleted;

                // before start monitoring disk, load already existing assemblies
                foreach (var assemblyFile in Directory.GetFiles(dropPath, pattern))
                {
                    if (File.Exists(assemblyFile + ".config"))
                        Create(Path.GetFullPath(assemblyFile));
                }

                dfsw.EnableRaisingEvents = true;
            }
            else
            {
                try
                {
                    RemoteProxy.StartEmbedded();
                }
                catch (Exception ex)
                {
                    if (Environment.UserInteractive)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine(ex.Message);
                    }
                    _log.Error("Ошибка", ex);
                }
            }

            if (Environment.UserInteractive)
                Console.ResetColor();
        }
        /*
        public static void Stop()
        {
            foreach (var __d in appDomains)
            {
                Remove(__d.Key);
            }
            if (_host.State == CommunicationState.Opened)
                _host.Close();

        }*/

        #region DelayedFileSystemWatcher events

        static void dfsw_Deleted(object sender, FileSystemEventArgs e)
        {
            if (File.Exists(e.FullPath + ".config"))
            {
                if (Environment.UserInteractive)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("{0} : {1}", e.ChangeType, e.Name);
                }
                Remove(e.FullPath);
            }
        }

        static void dfsw_CreatedOrChanged(object sender, FileSystemEventArgs e)
        {
            if (File.Exists(e.FullPath + ".config"))
            {
                if (Environment.UserInteractive)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("{0} : {1}", e.ChangeType, e.Name);
                }
                Update(e.FullPath);
            }
        }

        #endregion

        #region Application domains management

        static void Create(string assemblyFile)
        {
            if (Environment.UserInteractive)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("Loading {0}", Path.GetFileName(assemblyFile));
            }
            _log.Info(string.Format("Загрузка {0}", Path.GetFileName(assemblyFile)));
            try
            {
                var appDomain = RemoteProxy.Start(assemblyFile, ConfigurationManager.AppSettings["LogConfigPath"]);
                if (appDomain != null)
                {
                    _appDomains.Add(assemblyFile, appDomain);
                }
            }
            catch (Exception ex)
            {
                if (Environment.UserInteractive)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(ex.Message);
                }
                _log.Error("Ошибка", ex);
            }
        }

        static void Remove(string assemblyFile)
        {
            if (_appDomains.ContainsKey(assemblyFile))
            {
                try
                {
                    if (Environment.UserInteractive)
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine("Unloading {0}", Path.GetFileName(assemblyFile));
                    }
                    _log.Info(string.Format("Выгрузка {0}", Path.GetFileName(assemblyFile)));
                    try
                    {
                        var p = ((RemoteProxy)_appDomains[assemblyFile].GetData("RemoteProxy"));
                        if (p != null)
                            p.Stop();

                        AppDomain.Unload(_appDomains[assemblyFile]);
                    }
                    catch (CannotUnloadAppDomainException ex)
                    {
                        _log.Error(string.Format("AppDomain {0} не может быть выгружен:", Path.GetFileName(assemblyFile)), ex);
                        // Попробую выгрузить его за несколько попыток с интервалами ожидания
                        for (int i = 0; i <= 10; i++)
                        {
                            System.Threading.Thread.Sleep(10000);
                            try
                            {
                                AppDomain.Unload(_appDomains[assemblyFile]);
                                break;
                            }
                            catch (CannotUnloadAppDomainException)
                            {
                            }

                        }

                    }
                    //Если не смог выгрузится за 10 попыток, попытаюсь о нем забыть...
                    _appDomains.Remove(assemblyFile);

                }
                catch (Exception ex)
                {
                    if (Environment.UserInteractive)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine(ex.Message);
                    }
                    _log.Error("Ошибка", ex);
                }
            }
        }

        static void Update(string assemblyFile)
        {
            Remove(assemblyFile);
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["UpdateWaitTime"]))
            {
                var slsec = int.Parse(ConfigurationManager.AppSettings["UpdateWaitTime"]);
                System.Threading.Thread.Sleep(slsec * 1000);
            }
            Create(assemblyFile);
        }

        #endregion
    }
}
