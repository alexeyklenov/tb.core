﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Channels;
using log4net;

namespace TB.Services.Infrastructure.Server
{
    public static class ErrorHandlerHelper
    {
        public static void PromoteException(Type serviceType, Exception error, MessageVersion version, ref Message fault)
        {
            //Is error in the form of FaultException<T> ? 
            if (error.GetType().IsGenericType && error is FaultException)
            {
                Debug.Assert(error.GetType().GetGenericTypeDefinition() == typeof(FaultException<>));
                return;
            }

            var inContract = ExceptionInContract(serviceType, error);
            if (inContract == false)
            {
                return;
            }
            try
            {
                var faultUnboundedType = typeof(FaultException<>);
                var faultBoundedType = faultUnboundedType.MakeGenericType(error.GetType());
                var newException = (Exception)Activator.CreateInstance(error.GetType(), error.Message);
                var faultException = (FaultException)Activator.CreateInstance(faultBoundedType, newException);
                var messageFault = faultException.CreateMessageFault();
                fault = Message.CreateMessage(version, messageFault, faultException.Action);
            }
            catch
            {
            }
        }

        //Can only be called inside a service
        public static void PromoteException(Exception error, MessageVersion version, ref Message fault)
        {
            var frame = new StackFrame(1);

            var serviceType = frame.GetMethod().ReflectedType;
            PromoteException(serviceType, error, version, ref fault);
        }

        public static void LogError(Exception error)
        {
            LogError(error, null);
        }

        private static bool ExceptionInContract(Type serviceType, Exception error)
        {
            var faultAttributes = new List<FaultContractAttribute>();
            var interfaces = serviceType.GetInterfaces();

            var serviceMethod = GetServiceMethod(error);

            foreach (var interfaceType in interfaces)
            {
                var methods = interfaceType.GetMethods();
                foreach (var methodInfo in methods)
                {
                    if (methodInfo.Name == serviceMethod) //Does not deal with overlaoded methods 
                    //or same method name on different contracts implemented explicitly 
                    {
                        var attributes = GetFaults(methodInfo);
                        faultAttributes.AddRange(attributes);
                        return FindError(faultAttributes, error);
                    }
                }
            }
            return false;
        }

        private static string GetServiceMethod(Exception error)
        {
            const string wcfPrefix = "SyncInvoke";
            var start = error.StackTrace.IndexOf(wcfPrefix, StringComparison.Ordinal);
            Debug.Assert(start != -1); //Did they change the prefix???

            var trimedTillMethod = error.StackTrace.Substring(start + wcfPrefix.Length);
            var parts = trimedTillMethod.Split('(');
            return parts[0];
        }

        private static FaultContractAttribute[] GetFaults(MethodInfo methodInfo)
        {
            var attributes = methodInfo.GetCustomAttributes(typeof(FaultContractAttribute), false);
            return attributes as FaultContractAttribute[];
        }

        private static bool FindError(List<FaultContractAttribute> faultAttributes, Exception error)
        {
            Predicate<FaultContractAttribute> sameFault = delegate(FaultContractAttribute fault)
            {
                var detailType = fault.DetailType;
                return detailType == error.GetType();
            };
            return faultAttributes.Exists(sameFault);
        }

        public static void LogError(Exception error, MessageFault fault)
        {
            try
            {
                if (error.TargetSite.DeclaringType != null)
                {
                    var log = LogManager.GetLogger(error.TargetSite.DeclaringType.Name);
                    log.Error(
                        string.Format("Метод {0} вызвал необрабатываемое исключение.{1}", error.TargetSite.Name,
                            error.InnerException != null ? string.Format(" (Внутреннее исключение: {0}).", error.InnerException) : null), error);
                }
            }
            catch (Exception ex)
            {
                var log = LogManager.GetLogger("ErrorHandlerHelper.LogError");

                if (log != null)
                {
                    log.Error(error == null
                                    ? "Error is null"
                                    : error.TargetSite == null
                                          ? "Error.TargetSite is null"
                                          : "Something more...", ex);
                    log.Error(new StackTrace().ToString());
                }
            }
        }
    }
}
