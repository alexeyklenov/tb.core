﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Description;
using log4net;
using TB.Services.Common;
using TB.Services.Common.Interface;
using TB.Services.Infrastructure.Behaviors;
using TB.Services.Infrastructure.Client;
using TB.Services.Infrastructure.Interface;

namespace TB.Services.Infrastructure.Server
{
    class RemoteProxy : MarshalByRefObject
    {
        static ILog _log = LogManager.GetLogger("RemoteProxy");
        // String _logConfigName; //= ConfigurationManager.AppSettings["LogConfigPath"];

        static public AppDomain StartEmbedded()
        {
            var proxy = new RemoteProxy();

            AppDomain.CurrentDomain.SetData("RemoteProxy", proxy);
            AppDomain.CurrentDomain.SetData("ServerIP", System.Net.Dns.GetHostName());
            AppDomain.CurrentDomain.SetData("ServerPort", ConfigurationManager.AppSettings["ServerPort"]);
            AppDomain.CurrentDomain.SetData("ServerName", ConfigurationManager.AppSettings["ServerName"]);
            AppDomain.CurrentDomain.SetData("OrmConfigPath", ConfigurationManager.AppSettings["OrmConfigPath"]);
            AppDomain.CurrentDomain.SetData("LogConfigPath", ConfigurationManager.AppSettings["LogConfigPath"]);
            AppDomain.CurrentDomain.SetData("DefaultCulture", ConfigurationManager.AppSettings["DefaultCulture"]);

          /*  var loadedAs = AppDomain.CurrentDomain.GetAssemblies().Select(a => a.GetName().Name).ToList();
            var names = Assembly.GetEntryAssembly().GetManifestResourceNames();
            foreach (var name in names)
            {
                using (var stream = Assembly.GetEntryAssembly().GetManifestResourceStream(name))
                {
                    if (stream == null || loadedAs.Any(a => name.Contains(a)))
                        continue;
                   
                    using (var reader = new BinaryReader(stream))
                        Assembly.Load(reader.ReadBytes((int)stream.Length));

                    loadedAs = AppDomain.CurrentDomain.GetAssemblies().Select(a => a.GetName().Name).ToList();
                    
                }
            }*/

            try
            {
                proxy.LoadServices();
            }
            catch (Exception ex)
            {
                if (Environment.UserInteractive)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(ex.Message);
                }
            }
            return AppDomain.CurrentDomain;
        }

        static public AppDomain Start(string assemblyFile, string configPath)
        {
            AppDomain domain = null;
            try
            {
                var info = new AppDomainSetup {ShadowCopyFiles = "true"};
                var dropPath = Path.GetFullPath(ConfigurationManager.AppSettings["DropPath"] ??
                                                   Environment.CurrentDirectory);
                //info.PrivateBinPath = Environment.CurrentDirectory + ";Workflows;" + dropPath;
                info.ShadowCopyDirectories = dropPath;
                // *service* assembly configuration file
                info.ConfigurationFile = assemblyFile + ".config";

                var appDomain =
                    AppDomain.CreateDomain(
                        assemblyFile, null, info);

                appDomain.AssemblyLoad += (sender, args) =>
                {
                    if (Environment.UserInteractive)
                    {
                        Console.ForegroundColor = ConsoleColor.Gray;
                        Console.WriteLine("   {0}", args.LoadedAssembly.ManifestModule.Name);
                    }
                };
                appDomain.DomainUnload += (sender, e) =>
                {
                    if (Environment.UserInteractive)
                    {
                        Console.ForegroundColor = ConsoleColor.Gray;
                        Console.WriteLine("   Unloading {0}", ((AppDomain) sender).FriendlyName);
                    }
                };
                appDomain.UnhandledException +=
                    (sender, e) => _log.Error("Необработанная ошибка", (Exception) e.ExceptionObject);

                // proxy runs *inside* new app domain;
                // there it can handle service hosting
                var proxy = (RemoteProxy)appDomain
                                                     .CreateInstanceAndUnwrap(
                                                     Assembly.GetExecutingAssembly().FullName,
                                                     typeof(RemoteProxy).FullName);
                appDomain.SetData("RemoteProxy", proxy);
                appDomain.SetData("ServerIP", System.Net.Dns.GetHostName());
                appDomain.SetData("ServerPort", ConfigurationManager.AppSettings["ServerPort"]);
                appDomain.SetData("ServerName", ConfigurationManager.AppSettings["ServerName"]);
                appDomain.SetData("OrmConfigPath", ConfigurationManager.AppSettings["OrmConfigPath"]);
                appDomain.SetData("LogConfigPath", ConfigurationManager.AppSettings["LogConfigPath"]);
                appDomain.SetData("DefaultCulture", ConfigurationManager.AppSettings["DefaultCulture"]);
                if (!proxy.LoadServices(assemblyFile))
                    AppDomain.Unload(appDomain);
                else
                    domain = appDomain;
                
            }
            catch (Exception ex)
            {
                if (Environment.UserInteractive)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(ex.Message);
                }
            }

           

            return domain;
        }

        public void Stop()
        {
            //Выгрузка служб WCF
            if (_hosts.Count > 0)
            {
                foreach (var host in _hosts)
                {
                    _log.Info(string.Format("Выгружается служба {0}", host.Description.Name));
                    host.Close();
                }
            }
            else
            {
                _log.ErrorFormat("Нет запущенных хостеров");

            }
        }


        #region Remote execution

        List<ServiceHost> _hosts = new List<ServiceHost>();
        bool _hasServices;

        public bool LoadServices(string assemblyFile = null)
        {
            //Инициализация конфигурации
            var configPath = (string)AppDomain.CurrentDomain.GetData("LogConfigPath");
            if (!string.IsNullOrEmpty(configPath))
                log4net.Config.XmlConfigurator.ConfigureAndWatch(new FileInfo(configPath));

            var defCulture = ConfigurationManager.AppSettings["DefaultCulture"];
            if (string.IsNullOrEmpty(defCulture))
                defCulture = (string)AppDomain.CurrentDomain.GetData("DefaultCulture");
            if (!string.IsNullOrEmpty(defCulture))
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo(defCulture);
                //_log.InfoFormat("Установлена культура CurrentUICulture: {0}", System.Threading.Thread.CurrentThread.CurrentUICulture);
            }

            try
            {
                // load assembly file with WCF services
                var serviceTypes = new Type[]{};
                if (assemblyFile != null)
                {
                    var assemblyRef = new AssemblyName {CodeBase = assemblyFile};
                    var assembly = Assembly.Load(assemblyRef);
                    serviceTypes = LocateServices(assembly.GetTypes());
                }
                else
                    serviceTypes =
                        LocateServices(AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.GetTypes()).ToArray());

                var faultTypes =
                   AppDomain.CurrentDomain.GetAssemblies().SelectMany(a=>a.GetTypes())
                        .Where(t => t.BaseType == typeof (FaultBase))
                        .Select(t => new FaultDescription(t.Name) { DetailType = t}).ToList();

                foreach (var serviceType in serviceTypes)
                {
                    try
                    {
                        Create(serviceType, faultTypes);
                    }
                    catch (Exception ex)
                    {
                        if (Environment.UserInteractive)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine(ex.Message);
                        }
                        _log.Error("Ошибка", ex);
                    }
                }

                //Служба управления кэшем

                var hostC = new ServiceHost(typeof(NHibernateCacheMngService));
                var port = (string)AppDomain.CurrentDomain.GetData("ServerPort");

                var ep = hostC.AddServiceEndpoint(typeof(ICacheManagementService),
                                   new NetNamedPipeBinding(NetNamedPipeSecurityMode.None),
                                  string.Format("net.pipe://localhost/CMS{0}{1}", Path.GetFileNameWithoutExtension(assemblyFile), port));
                hostC.Open();
                // Добавить в службу
                using (var sc = new ClientProxy<ICacheManagementService>(new NetNamedPipeBinding(NetNamedPipeSecurityMode.None), string.Format("net.pipe://localhost/CacheManagementService{0}", port)))
                {
                    sc.Channel.AddService(ep.Address.ToString());
                }

            }
            catch (ReflectionTypeLoadException ex)
            {
                if (Environment.UserInteractive)
                    Console.ForegroundColor = ConsoleColor.Red;
                foreach (var innerEx in ex.LoaderExceptions)
                {
                    if (Environment.UserInteractive)
                        Console.WriteLine(innerEx.Message);
                    else
                        _log.Error("Подробно:", innerEx);

                    _log.Error("Ошибка", ex);
                }

            }
            catch (Exception ex)
            {
                if (Environment.UserInteractive)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(ex.Message);
                }
                _log.Error("Ошибка", ex);
            }
            return _hasServices;
        }

        private void Create(Type serviceType, List<FaultDescription> faultTypes)
        {
            GlobalContext.Properties["appServerName"] = AppDomain.CurrentDomain.GetData("ServerName");
            if (Environment.UserInteractive)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("Starting {0}", serviceType.FullName);
            }
            try
            {
                var host = new ServiceHost(serviceType, new Uri[0]);
                var port = AppDomain.CurrentDomain.GetData("ServerPort");

                var serviceInterface =
                    serviceType.GetInterfaces()
                        .FirstOrDefault(i => i.GetCustomAttribute<ServiceContractAttribute>() != null);
                
                if (serviceInterface == null)
                {
                    _log.Info(string.Format("Не найден контракт для службы {0}", serviceType.FullName));
                    return;
                }

                host.AddServiceEndpoint(serviceInterface, new NetTcpBinding("customBinding"),
                    string.Format("net.tcp://{0}:{1}/{2}", System.Net.Dns.GetHostName(), port, serviceInterface.Name));

                var behaviour = host.Description.Behaviors.Find<ServiceBehaviorAttribute>();
                behaviour.InstanceContextMode = behaviour.InstanceContextMode == InstanceContextMode.Single
                    ? InstanceContextMode.Single
                    : InstanceContextMode.PerCall;

                if (host.Description.Endpoints.Count > 0)
                {
                    foreach (var endpoint in host.Description.Endpoints)
                    {
                        _log.Info(string.Format("Обработка endpoint {2} для службы {0}. Endpoint.Binding {1} ", serviceType, endpoint.Binding, endpoint));
                        // Увеличим количество объектов в сериализуемом графе до максимума
                        // Добавим кастомные исключения
                        foreach (var op in endpoint.Contract.Operations)
                        {
                            foreach (var faultDesc in faultTypes)
                            {
                                op.Faults.Add(faultDesc);
                            }

                            var dataContractBehavior =
                                op.Behaviors[typeof (DataContractSerializerOperationBehavior)]
                                    as DataContractSerializerOperationBehavior;

                            if (dataContractBehavior != null)
                            {
                                dataContractBehavior.MaxItemsInObjectGraph = int.MaxValue;
                            }
                        }

                        if (Environment.UserInteractive)
                        {
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.WriteLine("   {0}", endpoint.Address);
                        }
       
                        using (var sc = new ClientProxy<IServiceCatalog>(new NetNamedPipeBinding(NetNamedPipeSecurityMode.None), string.Format("net.pipe://localhost/ServiceCatalog{0}", port)))
                        {
                            try
                            {
                                sc.Channel.AddService(endpoint.Contract.ContractType.Name, endpoint.Address.ToString());

                            }
                            catch (Exception ex)
                            {
                                if (Environment.UserInteractive)
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine(ex.Message);
                                }
                                _log.Error("Ошибка: " + endpoint.Address, ex);
                            }
                        }
                        _log.Info(string.Format("Запущена служба {0}, культура: {1}", endpoint.Address, System.Threading.Thread.CurrentThread.CurrentUICulture));
                    }

                    #if DEBUG
                        IServiceBehavior errorHandler = new ErrorHandlerBehaviorAttribute();
                        host.Description.Behaviors.Add(errorHandler);
                    #endif
                    host.Open();
                    _hosts.Add(host);
                    _hasServices = true;
                }
                else
                {
                    host.Close();
                }
            }
            catch (Exception ex)
            {
                if (Environment.UserInteractive)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(ex.Message);
                }
                _log.Error("Ошибка", ex);
            }
        }

        private Type[] LocateServices(Type[] types)
        {
            var services = new List<Type>();
            foreach (var type in types)
            {
                if (type.IsClass && type.GetCustomAttribute<ServerServiceAttribute>()!=null && DefineServiceContract(type))
                {
                    services.Add(type);
                }
            }
            return services.ToArray();
        }

        private bool DefineServiceContract(Type type)
        {
            return
                CheckForServiceContractAttribute(type) ||
                DefineServiceContract(type.GetInterfaces());
        }

        private bool DefineServiceContract(Type[] types)
        {
            foreach (var type in types)
            {
                if (DefineServiceContract(type))
                    return true;
            }
            return false;
        }

        private bool CheckForServiceContractAttribute(Type type)
        {
            return type.IsDefined(typeof(ServiceContractAttribute), false);
        }

        #endregion

    }
}
