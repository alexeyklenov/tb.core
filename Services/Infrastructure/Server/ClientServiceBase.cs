﻿using System;
using System.Configuration;
using System.Globalization;
using log4net;
using TB.Services.Infrastructure.Behaviors;

namespace TB.Services.Infrastructure.Server
{
    [UserContextBehavior]
    [ServerService]
    public class ClientServiceBase
    {
        protected static ILog Log = LogManager.GetLogger("WCFServiceBase");

        private static CultureInfo _defCulture;
        protected static bool CultureInitialized = false;

        protected static CultureInfo DefCulture
        {
            get
            {
                if (CultureInitialized)
                    return _defCulture;

                var cultureStr = ConfigurationManager.AppSettings["DefaultCulture"];
                if (string.IsNullOrEmpty(cultureStr))
                    cultureStr = (string)AppDomain.CurrentDomain.GetData("DefaultCulture");
                if (!string.IsNullOrEmpty(cultureStr))
                {
                    _defCulture = new CultureInfo(cultureStr);
                }
                CultureInitialized = true;
                return _defCulture;
            }
        }

        public ClientServiceBase()
        {
            var culture = DefCulture;
            if (culture != null)
                System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
            //_log.DebugFormat("Установлена культура CurrentUICulture: {0}", System.Threading.Thread.CurrentThread.CurrentUICulture);
        }
    }
}
