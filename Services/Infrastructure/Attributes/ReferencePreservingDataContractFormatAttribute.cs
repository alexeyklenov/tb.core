﻿using System;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using TB.Services.Infrastructure.Interface;

namespace TB.Services.Infrastructure.Attributes
{
    /// <summary>
    /// Атрибут для установки для операции возможности при сериализации использовать указатель а не новый объект
    /// </summary>
    public class ReferencePreservingDataContractFormatAttribute : Attribute, IOperationBehavior
    {
        private string _surrogateName;

        public ReferencePreservingDataContractFormatAttribute()
        {
        }
        public ReferencePreservingDataContractFormatAttribute(string surrogateName)
        {
            _surrogateName = surrogateName;
        }
        #region IOperationBehavior Members
        public void AddBindingParameters(OperationDescription description, BindingParameterCollection parameters)
        {
        }

        public void ApplyClientBehavior(OperationDescription description, System.ServiceModel.Dispatcher.ClientOperation proxy)
        {
            IOperationBehavior innerBehavior = new ReferencePreservingDataContractSerializerOperationBehavior(description);
            innerBehavior.ApplyClientBehavior(description, proxy);
        }

        public void ApplyDispatchBehavior(OperationDescription description, System.ServiceModel.Dispatcher.DispatchOperation dispatch)
        {
            IOperationBehavior innerBehavior = new ReferencePreservingDataContractSerializerOperationBehavior(description, _surrogateName);
            innerBehavior.ApplyDispatchBehavior(description, dispatch);
        }

        public void Validate(OperationDescription description)
        {
        }

        #endregion
    }
}
