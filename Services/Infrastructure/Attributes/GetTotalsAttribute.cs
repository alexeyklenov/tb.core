﻿using System;

namespace TB.Services.Infrastructure.Attributes
{
    public class GetTotalsAttribute : ServiceAttributeBase
    {
        public GetTotalsAttribute()
        {

        }
        public GetTotalsAttribute(Type entityType)
            : base(entityType)
        {
        }
    }
}
