﻿using System;

namespace TB.Services.Infrastructure.Attributes
{
    public class GetPartDataAttribute : ServiceAttributeBase
    {
        public GetPartDataAttribute()
        {

        }
        public GetPartDataAttribute(Type entityType)
            : base(entityType)
        {
        }
    }
}
