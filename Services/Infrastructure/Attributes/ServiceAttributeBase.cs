﻿using System;

namespace TB.Services.Infrastructure.Attributes
{
    /// <summary>
    /// Базовый класс для атрибутов
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class ServiceAttributeBase : Attribute
    {
        public Type EntityType { get; set; }

        public ServiceAttributeBase()
        {
        }

        public ServiceAttributeBase(Type entityType)
        {
            EntityType = entityType;
        }

    }
}
