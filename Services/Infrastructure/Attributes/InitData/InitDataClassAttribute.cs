﻿using System;
namespace TB.Services.Infrastructure.Attributes.InitData
{
    /// <summary>
    /// Помечает класс для инициализации и заполнения первоначальных данных
    /// для новой БД
    /// </summary>
    public class InitDataClassAttribute : Attribute
    {
       public string Method;

        public InitDataClassAttribute(string method)
        {
            Method = method;
        }
    }
}
