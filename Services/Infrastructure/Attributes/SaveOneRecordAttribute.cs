﻿using System;

namespace TB.Services.Infrastructure.Attributes
{
    public class SaveRecordAttribute : ServiceAttributeBase
    {
        public SaveRecordAttribute()
        {

        }
        public SaveRecordAttribute(Type entityType)
            : base(entityType)
        {
        }
    }
}
