﻿using System;

namespace TB.Services.Infrastructure.Attributes
{
    public class GetUniqueValuesAttribute : ServiceAttributeBase
    {
        public GetUniqueValuesAttribute()
        {

        }
        public GetUniqueValuesAttribute(Type entityType)
            : base(entityType)
        {
        }
    }
}
