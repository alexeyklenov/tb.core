﻿using System;

namespace TB.Services.Infrastructure.Attributes
{
    public class GetOneRecordAttribute : ServiceAttributeBase
    {
        public GetOneRecordAttribute()
        {
        }

        public GetOneRecordAttribute(Type entityType)
            : base(entityType)
        {
        }
    }
}
