﻿using System;

namespace TB.Services.Infrastructure.Attributes
{
    public class GetAllDataAttribute : ServiceAttributeBase
    {
        public GetAllDataAttribute()
        {

        }
        public GetAllDataAttribute(Type entityType)
            : base(entityType)
        {
        }
    }
}
