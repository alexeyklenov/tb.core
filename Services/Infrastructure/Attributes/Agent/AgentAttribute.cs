﻿using System;

namespace TB.Services.Infrastructure.Attributes.Agent
{
    public class AgentAttribute: Attribute
    {
        public string Name { get; set; }

        /// <summary>
        /// </summary>
        /// <param name="name">Наименование агента</param>
        public AgentAttribute(string name)
        {
            Name = name;
        }
    }
}
