﻿using System;

namespace TB.Services.Infrastructure.Attributes.Agent
{
    public class AgentJobAttribute: Attribute
    {
        public TimeSpan DefaultPeriod;
        public string Name;

        /// <summary>
        /// </summary>
        /// <param name="name">Наименование задания</param>
        /// <param name="period"> Период по умолчанию в секундах</param>
        public AgentJobAttribute(string name, int period)
        {
            Name = name;
            DefaultPeriod = new TimeSpan(0, 0, period);
        }
    }
}
