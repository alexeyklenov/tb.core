﻿using System;

namespace TB.Services.Infrastructure.Attributes
{
    public class DeleteRecordAttribute : ServiceAttributeBase
    {
        public DeleteRecordAttribute()
        {

        }
        public DeleteRecordAttribute(Type entityType)
            : base(entityType)
        {
        }
    }
}
