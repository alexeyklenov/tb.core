﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using TB.Services.Infrastructure.Inspectors;

namespace TB.Services.Infrastructure.Attributes
{
    public class UserContextBehaviorAttribute : Attribute, IServiceBehavior
    {
        #region IServiceBehavior Members

        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            foreach (var endpointDispatcher in serviceHostBase.ChannelDispatchers.SelectMany(x => ((ChannelDispatcher)x).Endpoints))
            {
                endpointDispatcher.DispatchRuntime.MessageInspectors.Add(new UserContextMessageInspector());
                // на каждую операцию навешиваем инспектор логирования параметров
               /* foreach (DispatchOperation dispatchOperation in endpointDispatcher.DispatchRuntime.Operations)
                    dispatchOperation.ParameterInspectors.Add(new ParameterInspector());*/
            }
        }

        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }

        #endregion

    }
}
