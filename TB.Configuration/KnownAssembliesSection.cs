﻿using System;
using System.Configuration;
using System.Reflection;

namespace TB.Configuration
{
    public class KnownAssemblyElement : ConfigurationElement
    {
        public KnownAssemblyElement()
        {
        }
        public KnownAssemblyElement(Assembly knownAssembly)
        {
            KnownAssemblyName = knownAssembly.GetName().Name;
        }

        [ConfigurationProperty("assembly", IsRequired = true, IsKey = true)]
        [StringValidator]
        public String KnownAssemblyName
        {
            get
            {
                return (String)this["assembly"];
            }
            set
            {
                this["assembly"] = value;
            }
        }
    }

    [ConfigurationCollection(typeof(KnownAssemblyElement))]
    public class KnownAssemblyElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new KnownAssemblyElement();
        }

        protected override Object GetElementKey(ConfigurationElement element)
        {
            return ((KnownAssemblyElement)element).KnownAssemblyName;
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.AddRemoveClearMap;
            }
        }

        public void Add(KnownAssemblyElement knownAssembly)
        {
            BaseAdd(knownAssembly);
        }
    }

    public class KnownAssembliesSection : ConfigurationSection
    {
        public const string SectionName = "knownAssemblies";

        [ConfigurationProperty("assemblies")]
        public KnownAssemblyElementCollection KnownAssemblies
        {
            get
            {
                return (KnownAssemblyElementCollection)base["assemblies"];
            }
        }
    }
}
