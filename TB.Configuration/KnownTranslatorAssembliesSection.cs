﻿using System.Configuration;

namespace TB.Configuration
{
    public class KnownTranslatorAssembliesSection : ConfigurationSection
    {
        public const string SectionName = "knownTranslatorAssemblies";

        [ConfigurationProperty("assemblies")]
        public KnownAssemblyElementCollection KnownTranslatorAssemblies
        {
            get
            {
                return (KnownAssemblyElementCollection)base["assemblies"];
            }
        }
    }
}
