﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

namespace TB.Configuration
{
    public static class KnownAssemblies
    {
        public static IEnumerable<Type> GetKnownTypes(ICustomAttributeProvider provider)
        {
            var knownTypes = new List<Type>();

            ConfigurationManager.RefreshSection("knownAssemblies");
            var knownAssembliesSection = ConfigurationManager.GetSection("knownAssemblies") as KnownAssembliesSection;

            if (knownAssembliesSection == null)
                return knownTypes;
            foreach (KnownAssemblyElement knownAssembly in knownAssembliesSection.KnownAssemblies)
                knownTypes.AddRange(
                    Assembly.Load(knownAssembly.KnownAssemblyName)
                        .GetTypes()
                        .Where(x => x.GetCustomAttributes(typeof (KnownTypeAttribute), false).Any()));

            return knownTypes;
        }
    }
}
