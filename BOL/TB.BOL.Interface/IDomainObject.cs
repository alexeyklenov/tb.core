﻿using System;
using System.ComponentModel;

namespace TB.BOL.Interface
{
    public interface IDomainObject
    {
        object ObjectId { get; }

        //Boolean IsLoadingInProgress { get; set; }

        /// <summary>
        /// Свойство указывает, что класс изменен
        /// </summary>
        bool IsDirty { get; }

        //void PropertyHasChanged();
        void PropertyHasChanged(string propertyName);

        bool Equals(object value);

        /// <summary>
        /// Временные объекты - не сравнимы с обычными объектами в хранилище
        /// Transient objects are not associated with an item already in storage.  For instance,
        /// a <see cref="Customer" /> is transient if its ID is 0.
        /// </summary>
        bool IsTransient();

        /// <summary>
        /// Должен быть определен для сравнения двух объектов
        /// </summary>
        int GetHashCode();

        int CompareTo(object value);
        event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Для вызова события OnPropertyChanged для всех свойств
        /// </summary>
        void OnIsDirtyChanged();

        /// <summary>
        /// Для вызова события OnPropertyChanged для всех свойств
        /// </summary>
        void OnUnknownPropertyChanged();

        /// <summary>
        /// Метод для вызова события для конкретного свойства
        /// </summary>
        /// <param name="propertyName"></param>
        void OnPropertyChanged(string propertyName);

        /// <summary>
        /// Set IsLoadingFlag from outside
        /// </summary>
        /// <param name="loading"></param>
        void SetLoading(bool loading);
    }


    /// <summary>
    /// Базовый объект
    /// </summary>
    /// <typeparam name="IdType">Тип идентификатора</typeparam>
    public interface IDomainObject<IdType>
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        IdType Id { get; set; }

        /// <summary>
        /// Глобальный идентификатор
        /// </summary>
        Guid Guid { get; set; }
    }
}
