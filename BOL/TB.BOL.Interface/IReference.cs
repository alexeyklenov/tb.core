﻿using System;

namespace TB.BOL.Interface
{
    /// <summary>
    /// Базовый интерфейс справочника
    /// </summary>
    public interface IReference : IDomainObject
    {
        /// <summary>
        /// Код записи справочника
        /// </summary>
        long Code { get; set; }

        /// <summary>
        /// Краткое наименование
        /// </summary>
        string ShortName { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Дата начала действия
        /// </summary>
        DateTime? BeginDate { get; set; }

        /// <summary>
        /// Дата окончания действия
        /// </summary>
        DateTime? EndDate { get; set; }
    }
}
