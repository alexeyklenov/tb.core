﻿
namespace TB.BOL.Interface
{
    /// <summary>
    /// Объект предполагающий внешнее изменение
    /// </summary>
    public interface IExternallyChangeable : IDomainObject
    {
        /// <summary>
        /// Изменение свойств объекта
        /// </summary>
        /// <returns></returns>
        void Change();
    }
}
