﻿using System;
using System.Collections.Generic;
using TB.BOL.Descriptions;
using TB.BOL.Interfaces;

namespace TB.BOL.Materials
{    
    /// <summary>
    /// Шаблон материала
    /// </summary>
    public class MaterialTemplate : DomainObject<long>, IDescribable<MaterialTemplate>, ICloneable
    {
        #region Fields

        private MaterialType _materialType;
        private MaterialStatus _materialStatus;
        
        private String _no;
        private String _name;

        #endregion

        #region Properties

        /// <summary>
        /// Тип материала
        /// </summary>
        public virtual MaterialType MaterialType
        {
            get { return _materialType; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _materialType = value;
                }
                else
                    if (_materialType != value)
                    {
                        _materialType = value;
                        PropertyHasChanged("MaterialType");
                    }
            }
        }
        

        /// <summary>
        /// Назначение материала
        /// </summary>
        public virtual MaterialStatus MaterialStatus
        {
            get { return _materialStatus; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _materialStatus = value;
                }
                else
                    if (_materialStatus != value)
                    {
                        _materialStatus = value;
                        PropertyHasChanged("MaterialStatus");
                    }
            }
        }
        /// <summary>
        /// Номер материала
        /// </summary>
        public virtual String No
        {
            get { return _no; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _no = value;
                }
                else
                    if (_no != value)
                    {
                        _no = value;
                        PropertyHasChanged("No");
                    }
            }
        }

        /// <summary>
        /// Наименование материала
        /// </summary>
        public virtual String Name
        {
            get { return _name; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _name = value;
                }
                else
                    if (_name != value)
                    {
                        _name = value;
                        PropertyHasChanged("Name");
                    }
            }
        }        

        #endregion
        
        /// <summary>
        /// Конструктор
        /// </summary>
        public MaterialTemplate()
        {
            Descriptions = new List<Description<MaterialTemplate>>();
        }

        #region DomainObject

        /// <summary>
        /// Хэш объекта
        /// </summary>
        /// <returns></returns>
        public override Int32 GetHashCode()
        {
            return HashOf(Guid);
        }

        /// <summary>
        /// Преобразование в строку
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return MaterialType != null ? MaterialType.ToString() : "";
        }

        #endregion

        #region IDescribable<T> Members

        /// <summary>
        /// Список описаний шаблона материала
        /// </summary>
        public virtual ICollection<Description<MaterialTemplate>> Descriptions { get; set; }

        #endregion


        #region ICloneable Members

        /// <summary>
        /// Копирование
        /// </summary>
        /// <returns>Копия данного экземпляра MaterialTemplate</returns>        
        public virtual object Clone()
        {
            MaterialTemplate cloneTemplate = new MaterialTemplate
            {
                MaterialType = MaterialType,
                No = No,
                Name = Name
            };

            foreach (Description<MaterialTemplate> desc in Descriptions)
                desc.Clone(cloneTemplate);

            return cloneTemplate;
        }

        #endregion
    }
}
