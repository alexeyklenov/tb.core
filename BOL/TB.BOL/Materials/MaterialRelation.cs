﻿namespace TB.BOL.Materials
{
    /// <summary>
    /// Связь между материалами
    /// </summary>
    public class MaterialRelation : DomainObject<long>
    {
        #region Fields

        private Material _parentMaterial;
        private Material _childMaterial;

        #endregion

        #region Properties

        /// <summary>
        /// Материал-родитель
        /// </summary>
        public virtual Material ParentMaterial
        {
            get { return _parentMaterial; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _parentMaterial = value;
                }
                else
                    if (_parentMaterial != value)
                    {
                        if (_parentMaterial != null)
                            _parentMaterial.ChildMaterials.Remove(this);
                        _parentMaterial = value;
                        if (_parentMaterial != null)
                            _parentMaterial.ChildMaterials.Add(this);
                        PropertyHasChanged("ParentMaterial");
                    }
            }
        }

        /// <summary>
        /// Материал-потомок
        /// </summary>
        public virtual Material ChildMaterial
        {
            get { return _childMaterial; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _childMaterial = value;
                }
                else
                    if (_childMaterial != value)
                    {
                        if (_childMaterial != null)
                            _childMaterial.ParentMaterials.Remove(this);
                        _childMaterial = value;
                        if (_childMaterial != null)
                            _childMaterial.ParentMaterials.Add(this);
                        PropertyHasChanged("ChildMaterial");
                    }
            }
        }

        #endregion

        #region DomainObject

        /// <summary>
        /// Хэш объекта
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return HashOf(Guid);
        }

        /// <summary>
        /// Преобразование в строку
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return ParentMaterial + " " +
                ChildMaterial;
        }

        #endregion
    }
}
