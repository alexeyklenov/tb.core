﻿using System.Collections.Generic;
using TB.BOL.Descriptions;
using TB.BOL.Interfaces;

namespace TB.BOL.Materials
{
    /// <summary>
    ///     Тип материала
    /// </summary>
    public class MaterialType : Reference<long>, IDescribable<MaterialType>
    {
        #region Properties

        /// <summary>
        ///     Коэффициенты перевода
        /// </summary>
       // public virtual IList<QuantityConverter> QuantityConverters { get; set; }

        #endregion

        #region Methods

        /// <summary>
        ///     Конструктор
        /// </summary>
        public MaterialType()
        {
           // QuantityConverters = new List<QuantityConverter>()
            Descriptions = new List<Description<MaterialType>>();
        }

        #endregion

        #region IDescribable<T> Members

        public virtual ICollection<Description<MaterialType>> Descriptions { get; set; }

        #endregion
    }
}
