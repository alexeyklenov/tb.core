﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using TB.BOL.Descriptions;
using TB.BOL.Documents;
using TB.BOL.Interfaces;
using TB.BOL.RouteStep;

namespace TB.BOL.Materials
{
    /// <summary>
    /// Статус материала
    /// </summary>
    [Flags]
    [DataContract]
    public enum MaterialStatus
    {
        /// <summary>
        /// Отсутствует
        /// </summary>
        [EnumMember]
        None = 0x00,

        /// <summary>
        /// Архивирован
        /// </summary>
        [EnumMember]
        Archived = 0x01,

        /// <summary>
        /// На хранении
        /// </summary>
        [EnumMember]
        Stored = 0x04,

        /// <summary>
        /// Зарезервирован
        /// </summary>
        [EnumMember]
        Reserved = 0x08,


        /// <summary>
        /// Отгружен
        /// </summary>
        [EnumMember]
        Shipped = 0x20,

        /// <summary>
        /// Заблокирован
        /// </summary>
        [EnumMember]
        Blocked = 0x80
    }

    /// <summary>
    /// Материал
    /// </summary>
    public class Material : DomainObject<long>, ICloneable, IDescribable<Material>, IRouteStep<Material>
    {
        #region Fields

        private Material _parent;
        private MaterialStatus _materialStatus;
        private MaterialType _materialType;
        private string _no;
        private string _code;
        private string _name;

        #endregion

        #region Properties

        /// <summary>
        /// Признак активности
        /// </summary>
        public virtual bool IsActive
        {
            get
            {
                return MaterialStatus != MaterialStatus.Archived &&
                       MaterialStatus != MaterialStatus.Shipped;
            }
        }

        /// <summary>
        /// Родительский материал
        /// </summary>
        public virtual Material Parent
        {
            get { return _parent; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _parent = value;
                }
                else if (_parent != value)
                {
                    if (_parent != null)
                        _parent.Components.Remove(this);
                    _parent = value;
                    if (_parent != null)
                    {
                        _parent.Components.Add(this);

                    }

                    PropertyHasChanged("Parent");
                }
            }
        }

     
        /// <summary>
        /// Статус материала
        /// </summary>
        public virtual MaterialStatus MaterialStatus
        {
            get { return _materialStatus; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _materialStatus = value;
                }
                else if (_materialStatus != value)
                 {
                     _materialStatus = value;
                     PropertyHasChanged("MaterialStatus");
                 }
            }
        }

        /// <summary>
        /// Тип материала
        /// </summary>
        public virtual MaterialType MaterialType
        {
            get { return _materialType; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _materialType = value;
                }
                else if (_materialType != value)
                {
                    _materialType = value;
                    PropertyHasChanged("MaterialType");
                }
            }
        }

        /// <summary>
        /// Код материала
        /// </summary>
        public virtual string Code
        {
            get { return _code; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _code = value;
                }
                else if (_code != value)
                {
                    _code = value;
                    PropertyHasChanged("Code");
                }
            }
        }
        /// <summary>
        /// Номер материала
        /// </summary>
        public virtual string No
        {
            get { return _no; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _no = value;
                }
                else if (_no != value)
                {
                    _no = value;
                    PropertyHasChanged("No");
                }
            }
        }

        /// <summary>
        /// Наименование материала
        /// </summary>
        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _name = value;
                }
                else if (_name != value)
                {
                    _name = value;
                    PropertyHasChanged("Name");
                }
            }
        }

        /// <summary>
        /// Фактическое количество
        /// </summary>
       /* public virtual QuantityBase FactQuantity
        {
            get
            {
                if (_factQuantity == null)
                {
                    _factQuantity = new QuantityBase();
                    _factQuantity.Changed += Quantity_OnChanged;
                }
                return _factQuantity;
            }
            set
            {
                if (IsLoadingInProgress)
                {
                    _factQuantity = value;
                }
                else if (_factQuantity != value)
                {
                    _factQuantity = value;
                    PropertyHasChanged("FactQuantity");
                    Quantity_OnChanged(null, null);
                }
                if (_factQuantity != null)
                    _factQuantity.Changed += Quantity_OnChanged;
            }
        }*/

       
        /// <summary>
        /// Количество
        /// </summary>
       /* public virtual QuantityBase Quantity
        {
            get { return FactQuantity.GetValueOrDefault(ProportionalQuantity); }
        }*/

        /// <summary>
        /// Список составляющих материалов
        /// </summary>
        public virtual ICollection<Material> Components { get; set; }

        /// <summary>
        /// Список материалов-родителей
        /// </summary>
        public virtual ICollection<MaterialRelation> ParentMaterials { get; set; }

        /// <summary>
        /// Первый родительский материал
        /// </summary>
        public virtual Material FirstParentMaterial
        {
            get
            {
                MaterialRelation parent = ParentMaterials.OrderBy(x => x.Id).FirstOrDefault();
                return parent != null ? parent.ParentMaterial : null;
            }
        }

        // Получает список материалов-родителей
        private ICollection<MaterialRelation> GetAllParents(List<Material> childMaterials,
            Func<MaterialRelation, bool> predicate)
        {

            return ParentMaterials.Where(predicate).ToList()
                .SelectMany(y =>
                {
                    if (!childMaterials.Contains(y.ParentMaterial))
                    {
                        childMaterials.Add(y.ParentMaterial);
                        return y.ParentMaterial.GetAllParents(childMaterials, predicate);
                    }
                    return new List<MaterialRelation>();
                }).Concat(ParentMaterials.Where(predicate)).ToList();
        }

        /// <summary>
        /// Список активных материалов-родителей (до первого неактивного)
        /// </summary>
        public virtual ICollection<MaterialRelation> AllActiveParents
        {
            get { return GetAllParents(new List<Material> {this}, x => x.ParentMaterial.IsActive); }
            /* get
             {
                 return ParentMaterials.Where(x => x.ParentMaterial.IsActive).ToList()
                     .SelectMany(y => y.ParentMaterial.AllActiveParents)
                     .Concat(ParentMaterials.Where(x => x.ParentMaterial.IsActive)).ToList();
             }*/
        }

        /// <summary>
        /// Список материалов-потомков
        /// </summary>
        public virtual ICollection<MaterialRelation> ChildMaterials { get; set; }

        /// <summary>
        /// Полный список материалов-потомков
        /// </summary>
        public virtual ICollection<MaterialRelation> AllChilds
        {
            get { return GetAllChilds(new List<Material> {this}); }
            /*get
            {
                return ChildMaterials.ToList()
                    .SelectMany(y => y.ChildMaterial.AllChilds)
                    .Concat(ChildMaterials).Distinct().ToList();
            }*/
        }

        // Получает список материалов-потомков
        private ICollection<MaterialRelation> GetAllChilds(List<Material> parentMaterials)
        {

            List<MaterialRelation> result = ChildMaterials.ToList()
                .SelectMany(y =>
                {
                    if (!parentMaterials.Contains(y.ChildMaterial))
                    {
                        parentMaterials.Add(y.ChildMaterial);
                        return y.ChildMaterial.GetAllChilds(parentMaterials);
                    }
                    return new List<MaterialRelation>();
                }).Concat(ChildMaterials).ToList();
            /*
                        if (__result.Count >= __result.Select(x => x.ChildMaterial).Distinct().Count() && __result.Any(x => x.ChildMaterial.ID < x.ParentMaterial.ID))
                        {
                            // Возможно, зацикливание
                            log4net.LogManager.GetLogger("Material.GetAllChilds").ErrorFormat("Возможно обнаружено зацикливание MaterialRelation. MaterialRelationID: {0}",
                                String.Join(", ", __result.Where(x => x.ChildMaterial.ID < x.ParentMaterial.ID).Select(x => x.ID.ToString()).ToArray()));

                            String str = String.Join(", ",
                                                     __result.Where(x => x.ChildMaterial.ID < x.ParentMaterial.ID).Select(
                                                         x => x.ID.ToString()).ToArray());
                        }*/

            return result;

        }

        /// <summary>
        /// Полный список материалов-потомков
        /// </summary>
        public virtual ICollection<Material> AllChildMaterials
        {
            get { return AllChilds.Select(c => c.ChildMaterial).ToList(); }
        }

       
        /// <summary>
        /// Список шагов маршрута материала
        /// </summary>
        //public virtual IList<MaterialRouteStep> MaterialRouteSteps { get; set; }

      
        /// <summary>
        /// Список документов
        /// </summary>
        public virtual ICollection<DocumentLine> Documents { get; set; }

        #endregion

        #region IDescribable<T> Members

        /// <summary>
        /// Список описаний
        /// </summary>
        public virtual ICollection<Description<Material>> Descriptions { get; set; }
        
        #endregion

        #region Methods

        /// <summary>
        /// Аннуляция связок с родительскими материалами
        /// </summary>
        public virtual void RemoveParentRelations()
        {
            while (ParentMaterials.Count > 0)
            {
                ParentMaterials.First().ParentMaterial = null;
                ParentMaterials.First().ChildMaterial = null;
            }
        }

        /// <summary>
        /// Аннуляция связок с дочерними материалами
        /// </summary>
        public virtual void RemoveChildRelations()
        {
            while (ChildMaterials.Count > 0)
            {
                ChildMaterials.First().ChildMaterial = null;
                ChildMaterials.First().ParentMaterial = null;
            }
        }

        /// <summary>
        /// Очистка списка документов
        /// </summary>
        public virtual void ClearDocuments()
        {
            while (Documents.Any())
            {
                DocumentLine documentLine = Documents.First();
                documentLine.Material = null;
            }
        }

        /// <summary>
        /// Блокировка материала
        /// </summary>
        public virtual void Block()
        {
            if (MaterialStatus == MaterialStatus.Stored)
                MaterialStatus = MaterialStatus.Blocked;
        }

       
        /// <summary>
        /// Добавление документа к материалу
        /// </summary>
        /// <param name="document">Документ</param>
        public virtual DocumentLine AddDocument(Document document)
        {
            return (object) document == null ? null : document.AddDocumentLine(this);
        }

        /// <summary>
        /// Удаляет документ из документов материала
        /// </summary>
        /// <param name="document">Документ</param>
        public virtual void RemoveDocument(Document document)
        {
            if ((object) document == null)
                return;

            document.RemoveDocumentLine(this);
        }

        public Material()
        {
            _materialStatus = MaterialStatus.Stored;
           // MaterialState = MaterialState.None;

            Components = new List<Material>();
            ParentMaterials = new List<MaterialRelation>();
            ChildMaterials = new List<MaterialRelation>();
           // MaterialRouteSteps = new List<MaterialRouteStep>();
            Documents = new List<DocumentLine>();
            Descriptions = new List<Description<Material>>();
        }

        #endregion

        #region DomainObject

        /// <summary>
        /// Хэш объекта
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return HashOf(Guid);
        }

        /// <summary>
        /// Преобразование в строку
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Name;
        }

        #endregion

        //#region IBinaryConvertable Members

        //public virtual byte[] Bytes { get; set; }

        //#endregion

        #region ICloneable Members

        /// <summary>
        /// Копирование
        /// </summary>
        /// <returns>Копия данного экземпляра Material</returns>        
        public virtual object Clone(MaterialStatus? materialStatus,
            bool createRelation, Func<Description, bool> descriptionFilter)
        {
            var clonedMaterial = new Material
            {
                MaterialType = MaterialType,
                No = No,
                Name = Name
            };

            // Копирование описаний, согласно правилам
            this.CloneDescriptions(clonedMaterial, x => descriptionFilter(x) &&
                                                        !clonedMaterial.Descriptions.Select(d => d.DescriptionType)
                                                            .Contains(x.DescriptionType));

            // Проставление связки
            if (createRelation)
                new MaterialRelation
                {
                    ChildMaterial = clonedMaterial,
                    ParentMaterial = this
                };

            clonedMaterial.MaterialStatus = materialStatus ?? MaterialStatus;

            return clonedMaterial;
        }

        public virtual object Clone(MaterialStatus? materialStatus, bool createRelation)
        {
            return Clone(materialStatus, createRelation, x => true);
        }

        public virtual object Clone(Func<Description, bool> descriptionFilter)
        {
            return Clone(MaterialStatus, false, descriptionFilter);
        }

        public virtual object Clone()
        {
            return Clone(MaterialStatus, false, x => true);
        }

        #endregion

        public virtual Description this[long code]
        {
            get { return this.GetDescription(code); }
            set
            {
                this.SetDescription(code, value.MinValue, value.MaxValue, value.StringValue, value.BeginDate,
                    value.EndDate);
            }
        }

        public virtual ICollection<RouteStep<Material>> RouteSteps { get; set; }
    }
}
