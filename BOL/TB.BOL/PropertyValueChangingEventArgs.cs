﻿using System.ComponentModel;

namespace TB.BOL
{
    public delegate void PropertyValueChangingEventHandler(object sender, PropertyValueChangingEventArgs e);
    /// <summary>
    /// Данные события изменения свойства объекта
    /// </summary>
    public class PropertyValueChangingEventArgs : PropertyChangingEventArgs
    {
        #region Fields

        private object _oldValue;
        private object _newValue;
        private bool _isCanceled;

        #endregion

        #region Properties

        /// <summary>
        /// Старое значение
        /// </summary>
        public object OldValue
        {
            get { return _oldValue; }
        }

        /// <summary>
        /// Новое значение
        /// </summary>
        public object NewValue
        {
            get { return _newValue; }
        }

        /// <summary>
        /// Новое значение
        /// </summary>
        public bool IsCanceled
        {
            get { return _isCanceled; }
            set { _isCanceled = value; }
        }

        #endregion

        #region Methods

        public PropertyValueChangingEventArgs(string propertyName, object oldValue, object newValue)
            : base(propertyName)
        {
            _oldValue = oldValue;
            _newValue = newValue;
            _isCanceled = false;
        }

        #endregion
    }
}