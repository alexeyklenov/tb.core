﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.ObjectBuilder2;
using TB.BOL.Descriptions;
using TB.DAL.Interface;
using TB.DAL.Interface.Dao;

namespace TB.BOL.Interfaces
{
    /// <summary>
    /// Режим объединения описаний
    /// </summary>
    public enum DescriptionMergingMode
    {
        /// <summary>
        /// Все
        /// </summary>
        All,

        /// <summary>
        /// Только недостающие
        /// </summary>
        NewOnly
    }

    public enum DescriptionSearchMode
    {
        Type,
        Value,
        TypeAndStringValue
    }

    public interface IDescribable<T> where T : DomainObject<long>, IDescribable<T>
    {
        /// <summary>
        /// Список описаний
        /// </summary>
        ICollection<Description<T>> Descriptions { get; set; }
    }

    public static class ExtensionsIDescribable
    {
        /// <summary>
        /// Типы данных у описаний, которые объединяются всегда вне зависимости от mergingMode
        /// </summary>
        private static readonly List<DescriptionDataType> AlwaysMergableDataTypes = new List<DescriptionDataType>
        {
            DescriptionDataType.ReferenceQuantity
        };

        #region Сравнение описываемых объектов

        /// <summary>
        /// Сравнение описываемых объектов 
        /// </summary>
        /// <param name="source">Объект источник(что подбираем)</param>
        /// <param name="target">Целевой объект(к чему подбираем)</param>
        /// <param name="comparisonType">Тип сравнения</param>
        /// <param name="ignoreAbsence">Игнорировать отсутствие описания</param>
        /// <returns>В случае эквивалентности - колличество совпавших примечаний, иначе -1</returns>
        public static long CompareDescriptions<T, TU>(this IDescribable<T> source,
            IDescribable<TU> target, ComparisonType comparisonType, bool ignoreAbsence)
            where T : DomainObject<long>, IDescribable<T>
            where TU : DomainObject<long>, IDescribable<TU>
        {
            return DescriptionList.CompareDescriptions(source.Descriptions,
                target.Descriptions,
                comparisonType, ignoreAbsence);
        }

        public static long CompareDescriptions<T, TU>(this IDescribable<T> source,
            IDescribable<TU> target, ComparisonType comparisonType)
            where T : DomainObject<long>, IDescribable<T>
            where TU : DomainObject<long>, IDescribable<TU>
        {
            return CompareDescriptions(source, target, comparisonType, false);
        }

        public static long CompareDescriptions<T, TU>(this IDescribable<T> source, IDescribable<TU> target)
            where T : DomainObject<long>, IDescribable<T>
            where TU : DomainObject<long>, IDescribable<TU>
        {
            return CompareDescriptions(source, target, ComparisonType.Equality);
        }

        public static long CompareDescriptions<T>(this IDescribable<T> source,
            IEnumerable<Description> target, ComparisonType comparisonType)
            where T : DomainObject<long>, IDescribable<T>
        {
            return DescriptionList.CompareDescriptions(source.Descriptions, target, comparisonType);
        }

        public static long CompareDescriptions<T>(this IDescribable<T> source,
            IEnumerable<Description> target, ComparisonType comparisonType, bool ignoreAbsence)
            where T : DomainObject<long>, IDescribable<T>
        {
            return DescriptionList.CompareDescriptions(source.Descriptions, target, comparisonType, ignoreAbsence);
        }

        public static long CompareDescriptions<T>(this IDescribable<T> source,
            IEnumerable<Description> target, IEnumerable<DescriptionType> excludedDescs, ComparisonType comparisonType,
            bool ignoreAbsence)
            where T : DomainObject<long>, IDescribable<T>
        {
            return excludedDescs == null
                ? DescriptionList.CompareDescriptions(source.Descriptions, target, comparisonType, ignoreAbsence)
                : DescriptionList.CompareDescriptions(source.Descriptions, target, excludedDescs, comparisonType,
                    ignoreAbsence);
        }

        public static long CompareDescriptions<T>(this IDescribable<T> source, IEnumerable<Description> target)
            where T : DomainObject<long>, IDescribable<T>
        {
            return CompareDescriptions(source, target, ComparisonType.Equality);
        }

        #endregion

        #region Исключение описаний объекта

        /// <summary>
        /// Исключение описаний объекта по их наличию в списке описаний другого объекта
        /// </summary>
        /// <param name="source">Объект источник</param>
        /// <param name="target">Целевой объект</param>
        /// <param name="comparisonType">Тип сравнения</param>
        /// <returns>Результирующая коллекция</returns>
        public static IEnumerable<Description> ExceptDescriptions<T, TU>(this IDescribable<T> source,
            IDescribable<TU> target, ComparisonType comparisonType)
            where T : DomainObject<long>, IDescribable<T>
            where TU : DomainObject<long>, IDescribable<TU>
        {
            return DescriptionList.ExceptDescriptions(source.Descriptions,
                target.Descriptions,
                comparisonType);
        }

        public static IEnumerable<Description> ExceptDescriptions<T, TU>(this IDescribable<T> source,
            IDescribable<TU> target)
            where T : DomainObject<long>, IDescribable<T>
            where TU : DomainObject<long>, IDescribable<TU>
        {
            return DescriptionList.ExceptDescriptions(source.Descriptions,
                target.Descriptions,
                ComparisonType.Equality);
        }

        public static IEnumerable<Description> ExceptDescriptions<T>(this IDescribable<T> source,
            IEnumerable<Description> target, ComparisonType comparisonType)
            where T : DomainObject<long>, IDescribable<T>
        {
            return DescriptionList.ExceptDescriptions(source.Descriptions, target, comparisonType);
        }

        public static IEnumerable<Description> ExceptDescriptions<T>(this IDescribable<T> source,
            IEnumerable<Description> target)
            where T : DomainObject<long>, IDescribable<T>
        {
            return ExceptDescriptions(source, target, ComparisonType.Equality);
        }

        #endregion

        #region Пересечение описаний объекта

        /// <summary>
        /// Пересечение описаний объекта с описаниями другого объекта
        /// </summary>
        /// <param name="source">Объект источник</param>
        /// <param name="target">Целевой объект</param>
        /// <param name="comparisonType">Тип сравнения</param>
        /// <returns>Результирующая коллекция</returns>
        public static IEnumerable<Description> IntersectDescriptions<T, TU>(this IDescribable<T> source,
            IDescribable<TU> target, ComparisonType comparisonType)
            where T : DomainObject<long>, IDescribable<T>
            where TU : DomainObject<long>, IDescribable<TU>
        {
            return DescriptionList.IntersectDescriptions(source.Descriptions,
                target.Descriptions,
                comparisonType);
        }

        public static IEnumerable<Description> IntersectDescriptions<T, TU>(this IDescribable<T> source,
            IDescribable<TU> target)
            where T : DomainObject<long>, IDescribable<T>
            where TU : DomainObject<long>, IDescribable<TU>
        {
            return DescriptionList.IntersectDescriptions(source.Descriptions,
                target.Descriptions,
                ComparisonType.Equality);
        }

        public static IEnumerable<Description> IntersectDescriptions<T>(this IDescribable<T> source,
            IEnumerable<Description> target, ComparisonType comparisonType)
            where T : DomainObject<long>, IDescribable<T>
        {
            return DescriptionList.IntersectDescriptions(source.Descriptions, target, comparisonType);
        }

        public static IEnumerable<Description> IntersectDescriptions<T>(this IDescribable<T> source,
            IEnumerable<Description> target)
            where T : DomainObject<long>, IDescribable<T>
        {
            return IntersectDescriptions(source, target, ComparisonType.Equality);
        }

        #endregion

        #region Объединение описаний объекта

        /// <summary>
        /// Объединение описаний объекта с описаниями другого объекта
        /// </summary>
        /// <param name="source">Объект источник</param>
        /// <param name="descriptions">Список описаний</param>
        /// <param name="mergingMode">Режим объединения описаний</param>
        /// <returns>Результирующая коллекция</returns>
        public static IEnumerable<Description<T>> MergeDescriptions<T>(this IDescribable<T> source,
            IEnumerable<Description> descriptions, DescriptionMergingMode mergingMode)
            where T : DomainObject<long>, IDescribable<T>
        {
            // Если объект корректного типа, то выполняется модификация списка описаний
            if (source is T)
                foreach (var descPair in descriptions
                    .GroupJoin(source.Descriptions,
                        x => new {x.DescriptionType, x.IsEqual},
                        x => new {x.DescriptionType, x.IsEqual},
                        (x, y) => new {New = x, Olds = y})
                    .Where(
                        x =>
                            AlwaysMergableDataTypes.Contains(x.New.DescriptionType.DataType)
                                // описания, которые объединяются всегда вне зависимости от mergingMode
                            || !x.Olds.Any() // в результирующий список включаются описания отсутствующие в объекте
                            ||
                            mergingMode == DescriptionMergingMode.All &&
                            !x.Olds.Any(y => y.Equals(x.New, ComparisonType.Subvalue)))
                    .Select(
                        x => new {x.New, Old = x.Olds.FirstOrDefault(y => y.Equals(x.New, ComparisonType.Intersection))})
                    )
                {
                    if ((object) descPair.Old != null)
                        descPair.Old.Merge(descPair.New);
                    else
                        descPair.New.Clone(source as T);
                }

            return source.Descriptions;
        }

        public static IEnumerable<Description<T>> MergeDescriptions<T, TU>(this IDescribable<T> source,
            IDescribable<TU> target, DescriptionMergingMode mergingMode)
            where T : DomainObject<long>, IDescribable<T>
            where TU : DomainObject<long>, IDescribable<TU>
        {
            return source.MergeDescriptions(target.Descriptions, mergingMode);
        }

        public static IEnumerable<Description<T>> MergeDescriptions<T, TU>(this IDescribable<T> source,
            IDescribable<TU> target)
            where T : DomainObject<long>, IDescribable<T>
            where TU : DomainObject<long>, IDescribable<TU>
        {
            return source.MergeDescriptions(target, DescriptionMergingMode.All);
        }

        #endregion

        #region Замена описаний объекта

        /// <summary>
        /// Замена описаний объекта с описаниями другого объекта
        /// </summary>
        /// <param name="source">Объект источник</param>
        /// <param name="descriptions">Список описаний</param>
        /// <returns>Результирующая коллекция</returns>
        public static IEnumerable<Description<T>> ReplaceDescriptions<T>(this IDescribable<T> source,
            IEnumerable<Description> descriptions)
            where T : DomainObject<long>, IDescribable<T>
        {
            // Если объект корректного типа, то выполняется модификация списка описаний));
            if (source is T)
            {
                // Удаление неактуальных
                source.Descriptions
                    .Where(x => descriptions.Any(y => y.DescriptionType == x.DescriptionType) &&
                                !descriptions.Any(y => y.DescriptionType == x.DescriptionType &&
                                                       y.IsEqual == x.IsEqual &&
                                                       y.Equals(x, ComparisonType.Subvalue)))
                    .ToList()
                    .ForEach(x => x.DescribedObject = null);

                // Добавление новых
                descriptions
                    .Where(x => !source.Descriptions.Any(y => y.DescriptionType == x.DescriptionType &&
                                                              y.IsEqual == x.IsEqual &&
                                                              y.Equals(x, ComparisonType.Subvalue)))
                    .ToList()
                    .ForEach(x => x.Clone(source as T));
            }

            return source.Descriptions;
        }

        public static IEnumerable<Description<T>> ReplaceDescriptions<T, TU>(this IDescribable<T> source,
            IDescribable<TU> target, List<DescriptionType> descriptionTypes)
            where T : DomainObject<long>, IDescribable<T>
            where TU : DomainObject<long>, IDescribable<TU>
        {
            IEnumerable<Description> descriptions =
                target.Descriptions.Where(x => descriptionTypes == null ||
                                               descriptionTypes.Contains(x.DescriptionType));

            return source.ReplaceDescriptions(descriptions);
        }

        public static IEnumerable<Description<T>> ReplaceDescriptions<T, TU>(this IDescribable<T> source,
            IDescribable<TU> target)
            where T : DomainObject<long>, IDescribable<T>
            where TU : DomainObject<long>, IDescribable<TU>
        {
            return source.ReplaceDescriptions(target, null);
        }

        #endregion

        #region Описания объекта

        /// <summary>
        /// Список описаний объекта по списку типов
        /// </summary>
        /// <param name="source">Описываемый объект</param>
        /// <param name="descriptionTypeCodes">список кодов типов описаний</param>
        /// <returns>список описаний</returns>
        public static List<Description<T>> GetDescriptions<T>(this IDescribable<T> source,
            IEnumerable<long> descriptionTypeCodes)
            where T : DomainObject<long>, IDescribable<T>
        {
            return source.Descriptions
                .Join(descriptionTypeCodes, d => d.DescriptionType.Code, t => t, (d, t) => d)
                .ToList();
        }

        /// <summary>
        /// Список дефолтных описаний объекта по списку типов
        /// </summary>
        /// <param name="source">Описываемый объект</param>
        /// <param name="descriptionTypeCodes">список кодов типов описаний</param>
        /// <returns>список описаний</returns>
        public static List<Description<T>> GetDefaultDescriptions<T>(this IDescribable<T> source,
            IEnumerable<long> descriptionTypeCodes)
            where T : DomainObject<long>, IDescribable<T>
        {
            var result = new List<Description<T>>();
            descriptionTypeCodes.ForEach(p => result.Add(source.GetDescription(p)));
            result.RemoveAll(p => (object) p == null);

            return result;
        }

        /// <summary>
        /// Список описаний объекта по данному типу
        /// </summary>
        /// <param name="source">Описываемый объект</param>
        /// <param name="descriptionTypeCode">код типа описания</param>
        /// <returns>список описаний</returns>
        public static IEnumerable<Description<T>> GetDescriptions<T>(this IDescribable<T> source,
            long descriptionTypeCode)
            where T : DomainObject<long>, IDescribable<T>
        {
            return source.Descriptions.Where(p => p.DescriptionType.Code == descriptionTypeCode);
        }

        /// <summary>
        /// Описание объекта по данному типу
        /// </summary>
        /// <param name="source">Описываемый объект</param>
        /// <param name="descriptionTypeCode">код типа описания</param>
        /// <returns>описание объекта</returns>
        public static Description<T> GetDescription<T>(this IDescribable<T> source, long descriptionTypeCode)
            where T : DomainObject<long>, IDescribable<T>
        {
            Description<T> d = null;
            if (source != null)
            {
                foreach (var desc in source.Descriptions)
                {
                    if (desc.DescriptionType.Code != descriptionTypeCode)
                        continue;

                    if (desc.IsDefault)
                        return desc;

                    d = desc;
                }
            }

            return d;
        }

        /// <summary>
        /// Описание объекта по данному типу и справочному значению
        /// </summary>
        /// <param name="source">Описываемый объект</param>
        /// <param name="descriptionTypeCode">код типа описания</param>
        /// <param name="descriptionValueCode">Код справочного значения</param>
        /// <returns>описание объекта</returns>
        public static Description<T> GetDescription<T>(this IDescribable<T> source, long descriptionTypeCode,
            long descriptionValueCode)
            where T : DomainObject<long>, IDescribable<T>
        {
            return source.Descriptions
                .Where(p => p.DescriptionType == descriptionTypeCode && p.DescriptionValue == descriptionValueCode)
                .OrderBy(p => p.IsDefault).LastOrDefault();
        }

        public static Description<T> GetDescription<T>(this IDescribable<T> source, long descriptionTypeCode,
            long? descriptionValueCode)
            where T : DomainObject<long>, IDescribable<T>
        {
            return source.Descriptions
                .Where(p => p.DescriptionType == descriptionTypeCode &&
                            (descriptionValueCode.HasValue == false || p.DescriptionValue == descriptionValueCode.Value))
                .OrderBy(p => p.IsDefault).LastOrDefault();
        }

        public static Hashtable PrepareDesc<T>(this IDescribable<T> source)
            where T : DomainObject<long>, IDescribable<T>
        {
            var ht = new Hashtable(50);

            foreach (var desc in source.Descriptions)
            {
                var code = desc.DescriptionType.Code;
                if (desc.IsDefault)
                    ht[code] = desc;
                else
                {
                    if (ht[code] == null)
                        ht[code] = desc;
                }
            }
            return ht;
        }

        #endregion

        #region Методы получения данных по описанию объекта

        /// <summary>
        /// Минимальное числовое значение описания объекта по данному типу
        /// </summary>
        /// <param name="source">Описываемый объект</param>
        /// <param name="descriptionTypeCode">код типа описания</param>
        /// <returns>Минимальное числовое значение описания</returns>
        public static decimal? GetNumberDescription<T>(this IDescribable<T> source, long descriptionTypeCode)
            where T : DomainObject<long>, IDescribable<T>
        {
            var desc = source.GetDescription(descriptionTypeCode);
            return (object) desc != null ? desc.MinValue : null;
        }

        public static decimal? GetNumberDescription(this IEnumerable<Description> source, long descriptionTypeCode)
        {
            if (source.Any(x => x.DescriptionType == descriptionTypeCode))
                return source.First(x => x.DescriptionType == descriptionTypeCode).MinValue;

            return null;
        }

        /// <summary>
        /// Максимальное числовое значение описания объекта по данному типу
        /// </summary>
        /// <param name="source">Описываемый объект</param>
        /// <param name="descriptionTypeCode">код типа описания</param>
        /// <returns>Максимальное числовое значение описания</returns>
        public static decimal? GetMaxNumberDescription<T>(this IDescribable<T> source, long descriptionTypeCode)
            where T : DomainObject<long>, IDescribable<T>
        {
            var desc = source.GetDescription(descriptionTypeCode);
            return (object) desc != null ? desc.MaxValue : null;
        }

        public static decimal? GetMaxNumberDescription(this IEnumerable<Description> source, long descriptionTypeCode)
        {
            if (source.Any(x => x.DescriptionType == descriptionTypeCode))
                return source.FirstOrDefault(x => x.DescriptionType == descriptionTypeCode).MaxValue;

            return null;
        }

        /// <summary>
        /// Строковое значение описания объекта по данному типу
        /// </summary>
        /// <param name="source">Описываемый объект</param>
        /// <param name="descriptionTypeCode">код типа описания</param>
        /// <returns>cтроковое значение описания</returns>
        public static string GetStringDescription<T>(this IDescribable<T> source, long descriptionTypeCode)
            where T : DomainObject<long>, IDescribable<T>
        {
            var desc = source.GetDescription(descriptionTypeCode);
            return (object) desc != null ? desc.StringValue : string.Empty;
        }

        /// <summary>
        /// Строковое значение описания объекта по данному типу и справочному значению
        /// </summary>
        /// <param name="source">Описываемый объект</param>
        /// <param name="descriptionTypeCode">Код типа описания</param>
        /// <param name="descriptionValueCode">Код справочного значения</param>
        /// <returns>cтроковое значение описания</returns>
        public static string GetStringDescription<T>(this IDescribable<T> source, long descriptionTypeCode,
            long descriptionValueCode)
            where T : DomainObject<long>, IDescribable<T>
        {
            var desc = source.GetDescription(descriptionTypeCode, descriptionValueCode);
            return (object) desc != null ? desc.StringValue : string.Empty;
        }

        /// <summary>
        /// Значение описания объекта типа дата по данному типу
        /// </summary>
        /// <param name="source">Описываемый объект</param>
        /// <param name="descriptionTypeCode">код типа описания</param>
        /// <returns>cтроковое значение описания</returns>
        public static DateTime? GetDateTimeDescription<T>(this IDescribable<T> source, long descriptionTypeCode)
            where T : DomainObject<long>, IDescribable<T>
        {
            var desc = source.GetDescription(descriptionTypeCode);
            return (object) desc != null ? desc.BeginDate : null;
        }

        #endregion

        #region Методы получения данных по справочному описанию объекта

        /// <summary>
        /// Справочное описание объекта по данному типу
        /// </summary>
        /// <param name="source">Описываемый объект</param>
        /// <param name="descriptionTypeCode">код типа описания</param>
        /// <returns>наименование справочного значения</returns>
        public static DescriptionValue GetRefDescription<T>(this IDescribable<T> source, long descriptionTypeCode)
            where T : DomainObject<long>, IDescribable<T>
        {
            var desc = source.GetDescription(descriptionTypeCode);
            return (object) desc != null ? desc.DescriptionValue : null;
        }

        /// <summary>
        /// Описание объекта по данному типу и справочному значению
        /// </summary>
        /// <param name="source">Описываемый объект</param>
        /// <param name="descriptionTypeCode">Код типа описания</param>
        /// <param name="descriptionValueCode">Код справочного значения</param>
        /// <returns>описание объекта</returns>
        public static Description<T> GetRefDescription<T>(this IDescribable<T> source, long descriptionTypeCode,
            long descriptionValueCode)
            where T : DomainObject<long>, IDescribable<T>
        {
            Description<T> d = null;
            foreach (Description<T> desc in source.Descriptions)
            {
                if (
                    !(desc.DescriptionType.Code == descriptionTypeCode && desc.DescriptionValue != null &&
                      desc.DescriptionValue.Code == descriptionValueCode))
                    continue;

                if (desc.IsDefault)
                    return desc;

                d = desc;
            }

            return d;
        }

        /// <summary>
        /// Наименование справочного значения описания объекта по данному типу
        /// </summary>
        /// <param name="source">Описываемый объект</param>
        /// <param name="descriptionTypeCode">код типа описания</param>
        /// <returns>наименование справочного значения</returns>
        public static string GetRefDescriptionName<T>(this IDescribable<T> source, long descriptionTypeCode)
            where T : DomainObject<long>, IDescribable<T>
        {
            var desc = source.GetDescription(descriptionTypeCode);
            return (object) desc != null && (object) desc.DescriptionValue != null
                ? desc.DescriptionValue.Name
                : string.Empty;
        }

        /// <summary>
        /// Краткое наименование справочного значения описания объекта по данному типу
        /// </summary>
        /// <param name="source">Описываемый объект</param>
        /// <param name="descriptionTypeCode">код типа описания</param>
        /// <returns>краткое наименование справочного значения</returns>
        public static string GetRefDescriptionShortName<T>(this IDescribable<T> source, long descriptionTypeCode)
            where T : DomainObject<long>, IDescribable<T>
        {
            var desc = source.GetDescription(descriptionTypeCode);
            return (object) desc != null && (object) desc.DescriptionValue != null
                ? desc.DescriptionValue.ShortName
                : string.Empty;
        }

        /// <summary>
        /// Код справочного значения описания объекта по данному типу
        /// </summary>
        /// <param name="source">Описываемый объект</param>
        /// <param name="descriptionTypeCode">код типа описания</param>
        /// <returns>краткое наименование справочного значения</returns>
        public static long? GetRefDescriptionCode<T>(this IDescribable<T> source, long descriptionTypeCode)
            where T : DomainObject<long>, IDescribable<T>
        {
            var desc = source.GetDescription(descriptionTypeCode);
            return (object) desc != null && (object) desc.DescriptionValue != null
                ? (long?) desc.DescriptionValue.Code
                : null;
        }

        /// <summary>
        /// Идентификатор справочного значения описания объекта по данному типу
        /// </summary>
        /// <param name="source">Описываемый объект</param>
        /// <param name="descriptionTypeCode">код типа описания</param>
        /// <returns>Идентификатор справочного значения</returns>
        public static long? GetRefDescriptionId<T>(this IDescribable<T> source, long descriptionTypeCode)
            where T : DomainObject<long>, IDescribable<T>
        {
            var desc = source.GetDescription(descriptionTypeCode);
            return (object) desc != null
                ? ((object) desc.DescriptionValue != null ? (long?) desc.DescriptionValue.Id : null)
                : null;
        }

        #endregion

        #region Наличие описаний объекта

        /// <summary>
        /// Наличие описания объекта по данному типу
        /// </summary>
        /// <param name="source">Описываемый объект</param>
        /// <param name="descriptionTypeCode">код типа описания</param>
        /// <returns>Признак наличия</returns>
        public static bool HasDescription<T>(this IDescribable<T> source, long descriptionTypeCode)
            where T : DomainObject<long>, IDescribable<T>
        {
            return source.Descriptions.Any(p => p.DescriptionType == descriptionTypeCode);
        }

        /// <summary>
        /// Наличие описания объекта по данному типу и справочному значению
        /// </summary>
        /// <param name="source">Описываемый объект</param>
        /// <param name="descriptionTypeCode">код типа описания</param>
        /// <param name="descriptionValueCode">Код справочного значения</param>
        /// <returns>Признак наличия</returns>
        public static bool HasDescription<T>(this IDescribable<T> source, long descriptionTypeCode,
            long descriptionValueCode)
            where T : DomainObject<long>, IDescribable<T>
        {
            return
                source.Descriptions.Any(
                    p => p.DescriptionType == descriptionTypeCode && p.DescriptionValue == descriptionValueCode);
        }

        /// <summary>
        /// Наличие описания объекта по данному типу, справочному и строковому значению
        /// </summary>
        /// <param name="source">Описываемый объект</param>
        /// <param name="descriptionTypeCode">Код типа описания</param>
        /// <param name="descriptionValueCode">Код справочного значения</param>
        /// <param name="stringValue">Строковое значение описания</param>
        /// <returns>Признак наличия</returns>
        public static bool HasDescription<T>(this IDescribable<T> source, long descriptionTypeCode,
            long descriptionValueCode, string stringValue)
            where T : DomainObject<long>, IDescribable<T>
        {
            return source.Descriptions.Any(p => p.DescriptionType == descriptionTypeCode &&
                                                p.DescriptionValue == descriptionValueCode &&
                                                p.StringValue == stringValue);
        }

        #endregion

        #region Копирование описаний

        /// <summary>
        /// Клонирование списка описаний
        /// </summary>
        /// <param name="source">Объект источник</param>
        /// <returns></returns>
        public static List<Description> GetFlatDescriptions<T>(this IDescribable<T> source)
            where T : DomainObject<long>, IDescribable<T>
        {
            return source.Descriptions.Select(description => new Description().SetValue(description)).ToList();
        }


        //public static void CloneDescriptions<T, TU>(this IDescribable<T> source, TU describedObject, Func<Description, Boolean> descriptionFilter)
        //    where T : DomainObject<long>, IDescribable<T>
        //    where TU : DomainObject<long>, IDescribable<TU>
        //{
        //    source.Descriptions.Where(x => descriptionFilter(x)).ToList().ForEach(p => p.Clone(describedObject));
        //}

        /// <summary>
        /// Копирование всех описаний
        /// </summary>
        /// <param name="source">Описываемый объект</param>
        /// <param name="describedObject">объект к которому копируются описания</param>
        /// <param name="descriptionFilter">Фильтр описаний</param>
        public static void CloneDescriptions<T, TU>(this IDescribable<T> source, TU describedObject,
            Func<Description<T>, bool> descriptionFilter)
            where T : DomainObject<long>, IDescribable<T>
            where TU : DomainObject<long>, IDescribable<TU>
        {
            source.Descriptions.Where(descriptionFilter).ToList().ForEach(p => p.Clone(describedObject));
        }

        /// <summary>
        /// Копирование всех описаний
        /// </summary>
        /// <param name="source">Описываемый объект</param>
        /// <param name="describedObject">объект к которому копируются описания</param>
        public static void CloneDescriptions<T, TU>(this IDescribable<T> source, TU describedObject)
            where T : DomainObject<long>, IDescribable<T>
            where TU : DomainObject<long>, IDescribable<TU>
        {
            source.CloneDescriptions(describedObject, x => true);
        }

        /// <summary>
        /// Копирование описаний объекта по списку типов
        /// </summary>
        /// <param name="source">Описываемый объект</param>
        /// <param name="describedObject">Объект к которому копируются описания</param>
        /// <param name="descriptionTypeCodes">список кодов типов описаний</param>
        public static void CloneDescriptions<T, TU>(this IDescribable<T> source, TU describedObject,
            List<long> descriptionTypeCodes)
            where T : DomainObject<long>, IDescribable<T>
            where TU : DomainObject<long>, IDescribable<TU>
        {
            source.CloneDescriptions(describedObject, x => descriptionTypeCodes.Contains(x.DescriptionType.Code));
        }

        /// <summary>
        /// Копирование дефолтных описаний объекта по списку типов
        /// </summary>
        /// <param name="source">Описываемый объект</param>
        /// <param name="describedObject">объект к которому копируются описания</param>
        /// <param name="descriptionTypeCodes">список кодов типов описаний</param>
        public static void CloneDefaultDescriptions<T, TU>(this IDescribable<T> source, TU describedObject,
            List<long> descriptionTypeCodes)
            where T : DomainObject<long>, IDescribable<T>
            where TU : DomainObject<long>, IDescribable<TU>
        {
            source.GetDefaultDescriptions(descriptionTypeCodes).ForEach(p => p.Clone(describedObject));
        }

        #endregion

        #region Методы задания описаний

        /// <summary>
        /// Получает справочное значение
        /// </summary>
        /// <param name="descriptionTypeCode">Код типа справочного значения</param>
        /// <param name="descriptionValueId">Идентификатор справочного значения</param>
        /// <param name="descriptionValueCode">Код справочного значения</param>
        /// <param name="descriptionValueName">Наименование справочного значения</param>
        /// <returns></returns>
        private static DescriptionValue GetDescriptionValue(Int64 descriptionTypeCode, long? descriptionValueId,
                                                            Int64? descriptionValueCode, String descriptionValueName)
        {
            DescriptionValue descriptionValue = null;
            if (descriptionValueId != null)
                descriptionValue = DaoContainer.Dao<DescriptionValue>().GetById(descriptionValueId, false);
            else if (descriptionValueCode != null)
                descriptionValue =
                    DaoContainer.GetDao<IDescriptionValueDao<DescriptionValue>>().GetByCodeAndDescriptionTypeCodeEx(descriptionValueCode.Value,
                                                                                       descriptionTypeCode);
            else if (!String.IsNullOrEmpty(descriptionValueName))
                descriptionValue =
                     DaoContainer.GetDao<IDescriptionValueDao<DescriptionValue>>().GetByNameAndDescriptionTypeCodeEx(descriptionValueName,
                                                                                       descriptionTypeCode);
            return descriptionValue;
        }

        /// <summary>
        /// Устанавливалка всего
        /// </summary>
        /// <param name="source">Бизнес-объект</param>
        /// <param name="descriptionTypeCode">Код типа описания</param>
        /// <param name="descriptionValueId">ID справочного значения</param>
        /// <param name="descriptionValueCode">Код справочного значения</param>
        /// <param name="descriptionValueName">Наименование справочного значения</param>
        /// <param name="stringValue">Строковое значение</param>
        /// <param name="minValue">Минимальное значение</param>
        /// <param name="maxValue">Максимальное значение</param>
        /// <param name="beginDate">Начальная дата</param>
        /// <param name="endDate">Конечная дата</param>
        /// <param name="isDefault">Признак основного примечания</param>
        /// <param name="exist">Признак добавления-удаления логического примечания</param>
        /// <param name="searchMode">Режим поиска существующего примечания</param>
        public static Description<T> SetDescription<T>(this IDescribable<T> source, Int64 descriptionTypeCode,
                                              long? descriptionValueId = null, Int64? descriptionValueCode = null, String descriptionValueName = null,
                                              String stringValue = null,
                                              Decimal? minValue = null, Decimal? maxValue = null,
                                              DateTime? beginDate = null, DateTime? endDate = null,
                                              Boolean? isDefault = null,
                                              Boolean? exist = null,
                                              DescriptionSearchMode searchMode = DescriptionSearchMode.Type)
            where T : DomainObject<long>, IDescribable<T>
        {
            var descriptionValue = GetDescriptionValue(descriptionTypeCode, descriptionValueId,
                                                                    descriptionValueCode, descriptionValueName);
            Description<T> description;
            DescriptionType descriptionType = null;
            if (searchMode == DescriptionSearchMode.Type)
            {
                description = source.GetDescription(descriptionTypeCode);
            }
            else if(searchMode == DescriptionSearchMode.Value)
            {
                if (descriptionValue == null)
                {
                    return null;
                }

                descriptionType = DaoContainer.GetDao<IDescriptionTypeDao<DescriptionType>>().GetByCode(descriptionTypeCode);
                description = source.GetDescriptions(descriptionTypeCode)
                                        .Where(x => x.DescriptionValue == descriptionValue)
                                        .OrderByDescending(x => x.IsDefault)
                                        .FirstOrDefault();
            }
            else
            {
                description =
                    source.Descriptions
                        .FirstOrDefault(
                            d => d.DescriptionType.Code == descriptionTypeCode && d.StringValue == stringValue);
            }
            
            var isDescriptionAttributesChanged = IsDescriptionAttributesChanged(stringValue, minValue, maxValue,
                                                                    beginDate, endDate, searchMode,
                                                                    descriptionType, descriptionValue);

            if (exist != false && (isDescriptionAttributesChanged || exist == true))
            {
                if (description == null)
                {
                    descriptionType = descriptionType ?? DaoContainer.GetDao<IDescriptionTypeDao<DescriptionType>>().GetByCode(descriptionTypeCode);
                    //отстутствует тип
                    if (descriptionType == null) return null;

                    description = new Description<T>((T)source)
                    {
                        DescriptionType = descriptionType,
                        DescriptionValue = searchMode == DescriptionSearchMode.Value ? descriptionValue : null
                    };
                }

                if (searchMode == DescriptionSearchMode.Type)
                {
                    description.DescriptionValue = descriptionValue;
                }

                description.StringValue = stringValue;
                description.MinValue = (minValue != Decimal.MinValue ? minValue : description.MinValue);
                description.MaxValue = (maxValue != Decimal.MaxValue ? maxValue : description.MaxValue);
                description.BeginDate = beginDate;
                description.EndDate = endDate;
                if (isDefault.HasValue)
                {
                    description.IsDefault = isDefault.Value;
                }

                if (exist == true)
                {
                    var values =
                        DaoContainer.GetDao<IDescriptionValueDao<DescriptionValue>>()
                            .GetByDescriptionType(description.DescriptionType, false);

                    if (values.Count() == 1)
                    {
                        description.DescriptionValue = values[0];
                    }
                }
            }
            else if (description != null)
            {
                source.Descriptions.Remove(description);
                return null;
            }

            return description;
        }

        private static bool IsDescriptionAttributesChanged(string stringValue, decimal? minValue, decimal? maxValue,
                                                   DateTime? beginDate, DateTime? endDate, DescriptionSearchMode searchMode,
                                                   DescriptionType descriptionType, DescriptionValue descriptionValue)
        {
            var isStringValueDefined = String.IsNullOrEmpty(stringValue) == false;
            var isMinValueDefined = minValue.HasValue && (minValue != Decimal.MinValue);
            var isMaxValueDefined = maxValue.HasValue && (maxValue != Decimal.MaxValue);
            var isDatesDefined = beginDate.HasValue || endDate.HasValue;

            var isNeedToUpdateReferencedValue = (searchMode == DescriptionSearchMode.Value) &&
                                                 (descriptionType != null) &&
                                                 (descriptionType.DataType == DescriptionDataType.Reference);

            var isNeedToUpdateDescValue = (searchMode == DescriptionSearchMode.Type) && (descriptionValue != null);

            var isDescriptionAttributesChanged = isNeedToUpdateDescValue || isStringValueDefined ||
                                                  isMinValueDefined || isMaxValueDefined ||
                                                  isDatesDefined || isNeedToUpdateReferencedValue;
            return isDescriptionAttributesChanged;
        }


        /// <summary>
        /// Устанавливает текстовое описание
        /// </summary>
        /// <param name="source">Бизнес-объект</param>
        /// <param name="code">Код типа описания</param>
        /// <param name="stringValue">Текстовое значение</param>
        public static void SetDescription<T>(this IDescribable<T> source, Int64 code, String stringValue)
            where T : DomainObject<long>, IDescribable<T>
        {
            SetDescription(source, code, null, null, null, stringValue);
        }
        /// <summary>
        /// Устанавливает текстовое описание
        /// </summary>
        /// <param name="source">Бизнес-объект</param>
        /// <param name="code">Код типа описания</param>
        /// <param name="stringValue">Текстовое значение</param>
        /// <param name="isDefault">Установает на характеристике признак по-умолчанию</param>
        public static void SetDescription<T>(this IDescribable<T> source, Int64 code, String stringValue,
                                             Boolean isDefault)
            where T : DomainObject<long>, IDescribable<T>
        {
            SetDescription(source, code, null, null, null, stringValue, null, null, null, null, isDefault);
        }
        /// <summary>
        /// Устанавливает числовое описание
        /// </summary>
        /// <param name="source">Бизнес-объект</param>
        /// <param name="code">Код типа описания</param>
        /// <param name="minValue">Минимальное значение</param>
        public static void SetDescription<T>(this IDescribable<T> source, Int64 code, Decimal? minValue)
            where T : DomainObject<long>, IDescribable<T>
        {
            SetDescription(source, code, null, null, null, null, minValue);
        }
        /// <summary>
        /// Устанавливает описание диапазона значений
        /// </summary>
        /// <param name="source">Бизнес-объект</param>
        /// <param name="code">Код типа описания</param>
        /// <param name="minValue">Минимальное значение</param>
        /// <param name="maxValue">Максимальное значение</param>
        public static void SetDescription<T>(this IDescribable<T> source, Int64 code, Decimal? minValue,
                                             Decimal? maxValue)
            where T : DomainObject<long>, IDescribable<T>
        {
            SetDescription(source, code, null, null, null, null, minValue, maxValue);
        }
        /// <summary>
        /// Устанавливает описание диапазона значений
        /// </summary>
        /// <param name="source">Бизнес-объект</param>
        /// <param name="code">Код типа описания</param>
        /// <param name="minValue">Минимальное значение</param>
        /// <param name="maxValue">Максимальное значение</param>
        /// <param name="stringValue">Строковое значение</param>
        public static void SetDescription<T>(this IDescribable<T> source, Int64 code, Decimal? minValue,
                                             Decimal? maxValue, String stringValue)
            where T : DomainObject<long>, IDescribable<T>
        {
            SetDescription(source, code, null, null, null, stringValue, minValue, maxValue);
        }

        /// <summary>
        /// Устанавливает описание диапазона значений
        /// </summary>
        /// <param name="source">Бизнес-объект</param>
        /// <param name="code">Код типа описания</param>
        /// <param name="minValue">Минимальное значение</param>
        /// <param name="maxValue">Максимальное значение</param>
        /// <param name="stringValue">Строковое значение</param>
        /// <param name="beginDate"></param>
        /// <param name="endDate"></param>
        public static void SetDescription<T>(this IDescribable<T> source, Int64 code, Decimal? minValue,
                                             Decimal? maxValue, String stringValue, DateTime? beginDate, DateTime? endDate)
            where T : DomainObject<long>, IDescribable<T>
        {
            SetDescription(source, code, null, null, null, stringValue, minValue, maxValue, beginDate, endDate);
        }
        /// <summary>
        /// Устанавливает описание - период
        /// </summary>
        /// <param name="source">Бизнес-объект</param>
        /// <param name="code">Код типа описания</param>
        /// <param name="beginDate">Начальная дата</param>
        /// <param name="endDate">Конечная дата</param>
        public static void SetDescription<T>(this IDescribable<T> source, Int64 code, DateTime? beginDate, DateTime? endDate)
            where T : DomainObject<long>, IDescribable<T>
        {
            SetDescription(source, code, null, null, null, null, null, null, beginDate, endDate);
        }

        /// <summary>
        /// Устанавливает справочное описание
        /// </summary>
        /// <param name="source">Бизнес-объект</param>
        /// <param name="code">Код типа описания</param>
        /// <param name="descriptionValueId">Идентификатор справочного значения</param>
        /// <param name="searchMode"></param>
        public static Description<T> SetReferenceDescription<T>(this IDescribable<T> source, Int64 code, long? descriptionValueId, DescriptionSearchMode searchMode = DescriptionSearchMode.Type)
            where T : DomainObject<long>, IDescribable<T>
        {
            return SetDescription(source, code, descriptionValueId, null, null, null, null, null, null, null, null, null, searchMode);
        }

        /// <summary>
        /// Устанавливает справочное описание
        /// </summary>
        /// <param name="source">Бизнес-объект</param>
        /// <param name="code">Код типа описания</param>
        /// <param name="descriptionValueCode">Код справочного значения</param>
        public static void SetReferenceDescriptionEx<T>(this IDescribable<T> source, Int64 code, Int64? descriptionValueCode)
            where T : DomainObject<long>, IDescribable<T>
        {
            SetDescription(source, code, null, descriptionValueCode);
        }

        /// <summary>
        /// Устанавливает справочное описание по наименованию справочного значения, если не задан идентификатор
        /// </summary>
        /// <param name="source">Бизнес-объект</param>
        /// <param name="code">Код типа описания</param>
        /// <param name="descriptionValueId">Идентификатор справочного значения</param>
        /// <param name="descriptionValueName">Наименование справочного значения</param>
        public static void SetReferenceDescriptionEx<T>(this IDescribable<T> source, Int64 code, long? descriptionValueId,
            String descriptionValueName) where T : DomainObject<long>, IDescribable<T>
        {
            SetDescription(source, code, descriptionValueId, null, descriptionValueName);
        }

        /// <summary>
        /// Устанавливает справочное описание
        /// </summary>
        /// <param name="source">Бизнес-объект</param>
        /// <param name="code">Код типа описания</param>
        /// <param name="descriptionValueCode">Код справочного значения</param>
        /// <param name="stringValue">Текстовое значение</param>
        public static void SetReferenceDescription<T>(this IDescribable<T> source, Int64 code, Int64 descriptionValueCode,
            String stringValue) where T : DomainObject<long>, IDescribable<T>
        {
            SetDescription(source, code, null, descriptionValueCode, null, stringValue, null, null, null, null, null, null, DescriptionSearchMode.Value);
        }

        /// <summary>
        /// Устанавливает справочное описание
        /// </summary>
        /// <param name="source">Бизнес-объект</param>
        /// <param name="code">Код типа описания</param>
        /// <param name="descriptionValueCode">Код справочного значения</param>
        /// <param name="minValue">Минимальное значение</param>
        /// <param name="maxValue">Максимальное значение</param>
        public static void SetReferenceDescription<T>(this IDescribable<T> source, Int64 code, Int64 descriptionValueCode,
            Decimal? minValue, Decimal? maxValue) where T : DomainObject<long>, IDescribable<T>
        {
            SetDescription(source, code, null, descriptionValueCode, null, null, minValue, maxValue, null, null, null, null, DescriptionSearchMode.Value);
        }

        /// <summary>
        /// Устанавливака всего
        /// </summary>
        /// <param name="source">Бизнес-объект</param>
        /// <param name="code">Код типа описания</param>
        /// <param name="descriptionValueId">ID справочного значения</param>
        /// <param name="minValue">Минимальное значение</param>
        /// <param name="maxValue">Максимальное значение</param>
        /// <param name="stringValue">Строковое значение</param>
        /// <param name="exist"></param>
        /// <param name="searchMode">Режим поиска существующего примечания</param>
        /// <param name="beginDate"></param>
        /// <param name="endDate"></param>
        public static void SetReferenceDescription<T>(this IDescribable<T> source, Int64 code, long? descriptionValueId,
            Decimal? minValue, Decimal? maxValue, String stringValue, Boolean? exist, DescriptionSearchMode searchMode,
            DateTime? beginDate = null, DateTime? endDate = null)
            where T : DomainObject<long>, IDescribable<T>
        {
            SetDescription(source, code, descriptionValueId, null, null, stringValue, minValue, maxValue, beginDate, endDate,
                           null, exist, searchMode);
        }

        /// <summary>
        /// Добавляет/удаляет примечание
        /// </summary>
        /// <param name="source">Бизнес-объект</param>
        /// <param name="code">Код типа описания</param>
        /// <param name="descriptionValueCode">Код справочного значения</param>
        /// <param name="exist">Признак наличия примечания</param>
        public static void SetReferenceDescription<T>(this IDescribable<T> source, Int64 code, Int64? descriptionValueCode, Boolean exist)
            where T : DomainObject<long>, IDescribable<T>
        {
            SetDescription(source, code, null, descriptionValueCode, null, null, null, null, null, null, null, exist);
        }

        /// <summary>
        /// Добавляет/удаляет примечание
        /// </summary>
        /// <param name="source">Бизнес-объект</param>
        /// <param name="code">Код типа описания</param>
        /// <param name="exist">Признак наличия примечания</param>
        public static void SetDescription<T>(this IDescribable<T> source, Int64 code, Boolean exist)
            where T : DomainObject<long>, IDescribable<T>
        {
            SetDescription(source, code, null, null, null, null, null, null, null, null, null, exist);
        }

        #endregion
    }
}
