﻿using System.Collections.Generic;
using TB.BOL.RouteStep;

namespace TB.BOL.Interfaces
{
    public interface IRouteStep<T> where T : DomainObject<long>, IRouteStep<T>
    {
        ICollection<RouteStep<T>> RouteSteps { get; set; }
    }
}
