﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Xml.Serialization;
using TB.BOL.Interface;
using TB.Core.Reflection;

namespace TB.BOL
{
    /// <summary>
    /// Базовый класс сущности
    /// </summary>
    [Serializable]
    public abstract class DomainObject<TIdType> : IComparable, INotifyPropertyChanged, INotifyPropertyChanging, IDomainObject<TIdType>, IDomainObject
    {
        private TIdType _id = default(TIdType);

        public virtual TIdType Id
        {
            get { return _id; }
            set { _id = value; }
        }
        [XmlIgnore]
        public virtual object ObjectId
        {
            get { return Id; }
        }

        private Guid? _guid;

        public virtual Guid Guid
        {
            get
            {
                if (!_guid.HasValue)
                {
                    _guid = Guid.NewGuid();
                }
                return _guid.Value;
            }
            set
            {
                _guid = value;
            }

        }

        /// <summary>
        /// For lazy-loading
        /// </summary>
        public virtual DomainObject<TIdType> Self { get { return this; } }

        private string _typeName;
        public virtual string TypeName
        {
            get
            {
                if (string.IsNullOrEmpty(_typeName))
                {
                    _typeName = GetType().FullName;
                }
                return _typeName;
            }
            set
            {
                _typeName = value;
            }

        }

        [NonSerialized]
        protected bool IsLoadingInProgress = false;

        #region Modified

        [NonSerialized]
        private bool _isDirty = false;

        [NonSerialized]
        private PropertyChangedEventHandler _propertyChangedEventHandlers;

        [NonSerialized]
        private PropertyChangingEventHandler _propertyChangingEventHandlers;

        /// <summary>
        /// Свойство указывает, что класс изменен
        /// </summary>
        [XmlIgnore]
        public virtual bool IsDirty
        {
            get { return _isDirty; }
        }

        public virtual void MarkDirty()
        {
            MarkDirty(false);
        }
        public virtual void MarkDirty(bool suppressEvent)
        {
            _isDirty = true;
            if (!suppressEvent)
                OnUnknownPropertyChanged();
        }

        public virtual void PropertyHasChanged(System.Reflection.MethodBase callerMethod)
        {
            if (callerMethod == null)
                return;

            string __propertyName = callerMethod.Name.Substring(Math.Max(callerMethod.Name.IndexOf('_'), 0));

            if (!string.IsNullOrEmpty(__propertyName))
                PropertyHasChanged(__propertyName);
        }
        public virtual void PropertyHasChanged(string propertyName)
        {
            MarkDirty(true);
            OnPropertyChanged(propertyName);
        }
        public virtual void PropertyHasChanged(string propertyName, object oldValue, object newValue)
        {
            MarkDirty(true);
            OnPropertyChanged(propertyName, oldValue, newValue);
        }

        public virtual void PropertyIsChanging(string propertyName)
        {
            OnPropertyChanging(propertyName);
        }
        public virtual void PropertyIsChanging(string propertyName, object oldValue, object newValue)
        {
            OnPropertyChanging(propertyName, oldValue, newValue);
        }

        public virtual void MarkClean()
        {
            _isDirty = false;
            OnUnknownPropertyChanged();
        }

        #endregion

        public override bool Equals(object value)
        {
            if (value == null)
                return false;

            var compareTo = value as DomainObject<TIdType>;

            if (compareTo == null)
                return false;

            return (HasSameNonDefaultIdAs(compareTo) ||
                // Since the IDs aren't the same, either of them must be transient to 
                // compare business value signatures
                    (((IsTransient()) || compareTo.IsTransient()) &&
                     HasSameBusinessSignatureAs(compareTo)));
        }

        public static bool operator ==(DomainObject<TIdType> source, DomainObject<TIdType> target)
        {
            // If both are null, or both are same instance, return true.
            if (ReferenceEquals(source, target))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)source == null) || ((object)target == null))
            {
                return false;
            }

            // Return true if the fields match:
            return source.Equals(target);
        }

        public static bool operator !=(DomainObject<TIdType> source, DomainObject<TIdType> target)
        {
            return !(source == target);
        }

        /// <summary>
        /// Временные объекты - не сравнимы с обычными объектами в хранилище
        /// </summary>
        public virtual bool IsTransient()
        {
            return Id == null || Id.Equals(default(TIdType));
        }

        public virtual bool HasSameBusinessSignatureAs(DomainObject<TIdType> compareTo)
        {

            return GetHashCode().Equals(compareTo.GetHashCode());
        }

        /// <summary>
        /// Возвращает true если self и передаваемый объект имеют тот же ID 
        /// и ID не значение по умолчанию для типа ID
        /// </summary>
        public virtual bool HasSameNonDefaultIdAs(DomainObject<TIdType> compareTo)
        {
            return (Id != null && !Id.Equals(default(TIdType))) &&
                   (compareTo.Id != null && !compareTo.Id.Equals(default(TIdType))) &&
                   Id.Equals(compareTo.Id);
        }

        /// <summary>
        /// Должен быть определен для сравнения двух объектов
        /// </summary>
        public abstract override int GetHashCode();

        /// <summary>
        /// Переопределим преобразование класса в строку
        /// </summary>
        /// <returns></returns>
        public abstract override string ToString();

        #region IComparable Members

        public virtual int CompareTo(object value)
        {
            if (value != null && value is DomainObject<TIdType>)
                return ToString().CompareTo(value.ToString());

            throw new ArgumentException("object is not a Domain Object");
        }

        #endregion

        #region INotifyPropertyChanged Members

        public virtual event PropertyChangedEventHandler PropertyChanged
        {
            add
            {
                _propertyChangedEventHandlers += value;
            }
            remove
            {
                _propertyChangedEventHandlers -= value;
            }
        }

        #endregion

        #region INotifyPropertyChanging Members

        public virtual event PropertyChangingEventHandler PropertyChanging
        {
            add
            {
                _propertyChangingEventHandlers += value;
            }
            remove
            {
                _propertyChangingEventHandlers -= value;
            }
        }

        #endregion

        /// <summary>
        /// Для вызова события OnPropertyChanged для всех свойств
        /// </summary>
        public virtual void OnIsDirtyChanged()
        {
            OnUnknownPropertyChanged();
        }

        /// <summary>
        /// Для вызова события OnPropertyChanged для всех свойств
        /// </summary>
        public virtual void OnUnknownPropertyChanged()
        {
            OnPropertyChanged(string.Empty);
        }

        /// <summary>
        /// Метод для вызова события для конкретного свойства
        /// </summary>
        /// <param name="propertyName"></param>
        public virtual void OnPropertyChanged(string propertyName)
        {
            if (_propertyChangedEventHandlers != null)
                _propertyChangedEventHandlers.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public virtual void OnPropertyChanged(string propertyName, object oldValue, object newValue)
        {
            if (_propertyChangedEventHandlers != null)
                _propertyChangedEventHandlers.Invoke(this, new PropertyValueChangedEventArgs(propertyName, oldValue, newValue));
        }

        /// <summary>
        /// Метод для вызова события для конкретного свойства
        /// </summary>
        /// <param name="propertyName"></param>
        public virtual void OnPropertyChanging(string propertyName)
        {
            if (_propertyChangingEventHandlers != null)
                _propertyChangingEventHandlers.Invoke(this, new PropertyChangingEventArgs(propertyName));
        }
        public virtual void OnPropertyChanging(string propertyName, object oldValue, object newValue)
        {
            if (_propertyChangingEventHandlers != null)
                _propertyChangingEventHandlers.Invoke(this, new PropertyValueChangingEventArgs(propertyName, oldValue, newValue));
        }

        /// <summary>
        /// Возвращает Хэш введенных значений
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public virtual int HashOf(params object[] parameters)
        {
            var hashString = parameters.Where(parameter => parameter != null)
                .Aggregate(TypeName, (current, parameter) => current + "|" + parameter);

            return hashString.GetHashCode();
        }

        protected static void SetPropertyAndFireHasChangedEvent<TIn, TOut>(TIn marker, ref TOut field, TOut value, Expression<Func<TIn, TOut>> expression)
            where TIn : DomainObject<TIdType>
        {
            var propertyName = expression.PropertyName();

            if (marker.IsLoadingInProgress)
            {
                field = value;
            }
            else
            {
                if (Equals(field, value) == false)
                {
                    field = value;
                    marker.PropertyHasChanged(propertyName);
                }
            }
        }


        public virtual void SetLoading(bool loading)
        {
            IsLoadingInProgress = loading;
        }
    }
}
