using System.ComponentModel;

namespace TB.BOL
{
    public delegate void PropertyValueChangedEventHandler(object sender, PropertyValueChangedEventArgs e);
    /// <summary>
    /// ������ ������� ��������� �������� �������
    /// </summary>
    public class PropertyValueChangedEventArgs : PropertyChangedEventArgs
    {
        #region Fields

        #endregion

        #region Properties

        /// <summary>
        /// ������ ��������
        /// </summary>
        public object OldValue { get; private set; }

        /// <summary>
        /// ����� ��������
        /// </summary>
        public object NewValue { get; private set; }

        #endregion

        #region Methods

        public PropertyValueChangedEventArgs(string propertyName, object oldValue, object newValue)
            : base(propertyName)
        {
            OldValue = oldValue;
            NewValue = newValue;
        }

        #endregion
    }
}