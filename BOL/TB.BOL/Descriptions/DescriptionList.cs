﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TB.BOL.Descriptions
{
    public class JoinedDescriptionKey
    {
        protected bool Equals(JoinedDescriptionKey other)
        {
            return First.Equals(other.First) && Second.Equals(other.Second);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (First.GetHashCode() * 397) ^ Second.GetHashCode();
            }
        }

        public Guid First { get; set; }
        public Guid Second { get; set; }

        public JoinedDescriptionKey(Guid first, Guid second)
        {
            First = first;
            Second = second;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((JoinedDescriptionKey)obj);
        }
    }

    public class JoinedDescriptionValue
    {
        public bool IsEqual { get; set; }
        public Description First { get; set; }
        public Description Second { get; set; }

        public JoinedDescriptionValue(bool isEqual, Description first, Description second)
        {
            IsEqual = isEqual;
            First = first;
            Second = second;
        }
    }

    public class DescriptionsComparisonResult
    {
        public bool MatchFound { get; set; }
        public long MatchCount { get; set; }
        public List<IGrouping<JoinedDescriptionKey, JoinedDescriptionValue>> Descriptions { get; set; }

        public DescriptionsComparisonResult(bool matchFound, int matchCount, List<IGrouping<JoinedDescriptionKey, JoinedDescriptionValue>> descriptions)
        {
            MatchFound = matchFound;
            MatchCount = matchCount;
            Descriptions = descriptions;
        }
    }

    public static class EnumerableExtensions
    {
        public static IEnumerable<TResult> LeftJoin<TInner, TOuter, TKey, TResult>(this IEnumerable<TInner> inner, IEnumerable<TOuter> outer, Func<TInner, TKey> innerKeySelector, Func<TOuter, TKey> outerKeySelector, Func<TInner, TOuter, TResult> resultSelector)
            where TOuter : new()
        {
            return inner
                .GroupJoin(outer,
                    innerKeySelector,
                    outerKeySelector,
                    (s, targets) => new { s, targets = targets.DefaultIfEmpty(new TOuter()) })
                .SelectMany(pair => pair.targets, (pair, t) => resultSelector(pair.s, t));
        }
    }
    /// <summary>
    /// Коллекция описаний объекта
    /// </summary>
    public static class DescriptionList
    {
        /// <summary>
        /// Сравнение коллекций описаний
        /// </summary>
        /// <param name="source">Коллекция источник(что подбираем)</param>
        /// <param name="target">Целевая коллекция(к чему подбираем)</param>
        /// <param name="comparisonType">Тип сравнения</param>
        /// <param name="ignoreAbsence">Игнорировать отсутствие описания</param>
        /// <returns>В случае эквивалентности - колличество совпавших примечаний, иначе -1</returns>
        public static long CompareDescriptions(IEnumerable<Description> source,
            IEnumerable<Description> target, ComparisonType comparisonType, bool ignoreAbsence)
        {
            DescriptionsComparisonResult matchedDesc = GetMatchedDescriptions(source, target, comparisonType, ignoreAbsence);

            return matchedDesc.MatchFound == false ? -1 : matchedDesc.MatchCount;
        }

        /// <summary>
        /// Сравнение коллекций описаний c исключенными описаниями
        /// </summary>
        /// <param name="source">Коллекция источник(что подбираем)</param>
        /// <param name="target">Целевая коллекция(к чему подбираем)</param>
        /// <param name="excludedDescs">Исключаемые из сравнения описания по типу</param>
        /// <param name="comparisonType">Тип сравнения</param>
        /// <param name="ignoreAbsence">Игнорировать отсутствие описания</param>
        /// <returns>В случае эквивалентности - колличество совпавших примечаний, иначе -1</returns>
        public static long CompareDescriptions(IEnumerable<Description> source,
            IEnumerable<Description> target, IEnumerable<DescriptionType> excludedDescs, ComparisonType comparisonType, bool ignoreAbsence)
        {
            if (excludedDescs != null)
            {
                source = source.Where(s => s != null && !excludedDescs.Contains(s.DescriptionType)).ToList();
                target = target.Where(t => t != null && !excludedDescs.Contains(t.DescriptionType)).ToList();
            }
            DescriptionsComparisonResult matchedDesc = GetMatchedDescriptions(source, target, comparisonType, ignoreAbsence);

            return matchedDesc.MatchFound == false ? -1 : matchedDesc.MatchCount;
        }

        /// <summary>
        /// Сравнение коллекций описаний
        /// </summary>
        /// <param name="source">Коллекция источник(что подбираем)</param>
        /// <param name="target">Целевая коллекция(к чему подбираем)</param>
        /// <param name="comparisonType">Тип сравнения</param>
        /// <param name="ignoreAbsence">Игнорировать отсутствие описания</param>
        /// <param name="error">Сообщение о несоответствии примечаний </param>
        /// <returns>В случае эквивалентности - колличество совпавших примечаний, иначе -1</returns>
        public static long CompareDescriptions(IEnumerable<Description> source,
            IEnumerable<Description> target, ComparisonType comparisonType, bool ignoreAbsence, out string error)
        {
            DescriptionsComparisonResult matchedDesc = GetMatchedDescriptions(source, target, comparisonType, ignoreAbsence);
            error = FormatError(matchedDesc.Descriptions);

            return matchedDesc.MatchFound == false ? -1 : matchedDesc.MatchCount;
        }

        private static bool GetDescriptionsComparisonByContraint(Description source, Description target, ComparisonType comparisonType, bool? ignoreAbsence)
        {
            return source.IsEqual ^ target.IsEqual ^
                   ((target.DescriptionType != null || (ignoreAbsence.HasValue && ignoreAbsence.Value)) &&
                    (source.Equals(target, comparisonType) || source.IsEmpty));
        }

        private static List<IGrouping<JoinedDescriptionKey, JoinedDescriptionValue>> GetMatchedDescriptionsList(IEnumerable<Description> source, IEnumerable<Description> target, ComparisonType comparisonType, bool? ignoreAbsence)
        {
            var matchedDescriptionsByType = source.Where(x => x != null)
                .LeftJoin(target.Where(x => x != null),
                    s => s.DescriptionType,
                    t => t.DescriptionType, (s, t) => new { s, t })
                    .Where(x => !(ignoreAbsence.HasValue && ignoreAbsence.Value) || x.t.DescriptionType != null);

            var matchedDescriptionsByConstraint = matchedDescriptionsByType.GroupBy(
                key => key.s.IsEqual && key.t.IsEqual
                    ? new JoinedDescriptionKey(key.s.DescriptionType.Guid, key.s.DescriptionType.Guid)
                    : new JoinedDescriptionKey(key.s.Guid, key.t.Guid),
                value => new JoinedDescriptionValue(
                    GetDescriptionsComparisonByContraint(value.s, value.t, comparisonType, ignoreAbsence),
                    value.s, value.t)
            );

            return matchedDescriptionsByConstraint.ToList();
        }

        /// <summary>
        /// Сравнение коллекций описаний
        /// </summary>
        /// <param name="source">Коллекция источник(что подбираем)</param>
        /// <param name="target">Целевая коллекция(к чему подбираем)</param>
        /// <param name="comparisonType">Тип сравнения</param>
        /// <param name="ignoreAbsence">Игнорировать отсутствие описания</param>
        /// <returns>В случае эквивалентности - колличество совпавших примечаний, иначе -1</returns>
        private static DescriptionsComparisonResult GetMatchedDescriptions(IEnumerable<Description> source,
            IEnumerable<Description> target, ComparisonType comparisonType, bool ignoreAbsence)
        {
            // Стыковка операндов по типу описания
            var list = GetMatchedDescriptionsList(source, target, comparisonType, ignoreAbsence);

            // Анализ результатов стыковки
            bool matchedDescriptionFound = list.All(x => x.Any(y => y.IsEqual));

            return new DescriptionsComparisonResult(matchedDescriptionFound, list.Count(), list);
        }

        private static string FormatError(IEnumerable<IGrouping<JoinedDescriptionKey, JoinedDescriptionValue>> list)
        {
            string error = string.Join("," + Environment.NewLine,
                list.Where(
                    x => x.Any(y => y.IsEqual == false)).
                    Select(x => "Ожидается: "
                                + (x.First().First.IsEqual ? " наличие " : " отсутствие ")
                                + (x.Select(y => y.First).Distinct().Count() > 1 ? "примечаний " : "примечания ")
                                + DescToString(x.Select(y => y.First).Distinct(), " или ")
                                + ", имеется: "
                                + (x.First().Second.DescriptionType == null
                                    ? ("примечание отсутствует")
                                    : (x.First().Second.IsEqual ? " наличие " : " отсутствие ")
                                        + (x.Select(y => y.Second).Distinct().Count() > 1 ? "примечаний " : "примечания ")
                                        + DescToString(x.Select(y => y.Second).Distinct(), ", ")))
                    .ToArray());

            return error;
        }

        /// <summary>
        /// Преобразование примечания в строку
        /// </summary>
        /// <returns></returns>
        private static string DescToString(IEnumerable<Description> descs, string separator)
        {
            return string.Join(separator,
                descs
                .Select(desc =>
                {
                    DescriptionType descriptionType = desc.DescriptionType;
                    string descValue = desc.FormatString();

                    return string.Concat(descriptionType != null
                        ? (descriptionType.ShortName + " " + descriptionType.Name)
                        : string.Empty,
                        string.IsNullOrEmpty(descValue.Trim()) ? "" : string.Format(" ({0})", descValue));
                })
                .ToArray());
        }

        public static long CompareDescriptions(IEnumerable<Description> source,
            IEnumerable<Description> target, ComparisonType comparisonType)
        {
            return CompareDescriptions(source, target, comparisonType, false);
        }

        public static long CompareDescriptions(IEnumerable<Description> source,
            IEnumerable<Description> target, ComparisonType comparisonType, out string error)
        {
            return CompareDescriptions(source, target, comparisonType, false, out error);
        }

        public static long CompareDescriptions(IEnumerable<Description> source, IEnumerable<Description> target)
        {
            return CompareDescriptions(source, target, ComparisonType.Equality);
        }

        /// <summary>
        /// Исключение элементов коллекций описаний представленных в другой коллекции описаний
        /// </summary>
        /// <param name="source">Коллекция источник</param>
        /// <param name="target">Целевая коллекция</param>
        /// <param name="comparisonType">Тип сравнения</param>
        /// <returns>Результирующая коллекция</returns>
        public static IEnumerable<Description> ExceptDescriptions(IEnumerable<Description> source,
            IEnumerable<Description> target, ComparisonType comparisonType)
        {
            // Стыковка операндов по типу описания
            var list = GetMatchedDescriptionsList(source, target, comparisonType, null);

            // Анализ результатов стыковки
            return list.SelectMany(x => x.ToList())
                .Where(x => x.IsEqual == false)
                .Select(x => x.First);
        }

        public static IEnumerable<Description> ExceptDescriptions(IEnumerable<Description> source,
            IEnumerable<Description> target)
        {
            return ExceptDescriptions(source, target, ComparisonType.Equality);
        }

        /// <summary>
        /// Пересечение элементов коллекций описаний с элементами в другой коллекции описаний
        /// </summary>
        /// <param name="source">Коллекция источник</param>
        /// <param name="target">Целевая коллекция</param>
        /// <param name="comparisonType">Тип сравнения</param>
        /// <returns>Результирующая коллекция</returns>
        public static IEnumerable<Description> IntersectDescriptions(IEnumerable<Description> source,
            IEnumerable<Description> target, ComparisonType comparisonType)
        {
            // Стыковка операндов по типу описания
            var list = GetMatchedDescriptionsList(source, target, comparisonType, null);

            // Анализ результатов стыковки
            return list.SelectMany(x => x.ToList())
                .Where(x => x.IsEqual)
                .Select(x => x.First);
        }

        public static IEnumerable<Description> IntersectDescriptions(IEnumerable<Description> source,
            IEnumerable<Description> target)
        {
            return IntersectDescriptions(source, target, ComparisonType.Equality);
        }
    }
}
