﻿using System.Collections.Generic;
using System.Linq;
using TB.BOL.Interfaces;

namespace TB.BOL.Descriptions
{
    /// <summary>
    /// Значение описания
    /// </summary>
    public class DescriptionValue : Reference<long>, IDescribable<DescriptionValue>
    {
        #region Fields

        private DescriptionType _descriptionType;

        #endregion

        #region Properties


        /// <summary>
        /// Тип описания
        /// </summary>
        public virtual DescriptionType DescriptionType
        {
            get { return _descriptionType; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _descriptionType = value;
                }
                else
                    if (_descriptionType != value)
                    {
                        if (_descriptionType != null)
                            _descriptionType.DescriptionValues.Remove(this);
                        _descriptionType = value;
                        if (_descriptionType != null)
                            _descriptionType.DescriptionValues.Add(this);
                        PropertyHasChanged("DescriptionType");
                    }
            }
        }


        /// <summary>
        /// Дополнительная информация
        /// </summary>
        public virtual string AdditionalInfo
        {
            get
            {
                var descriptions = Descriptions
                    .Where(x => DescriptionType.Childs.Any(y => y.ChildDescriptionType == x.DescriptionType))
                    .Select(x => x.GetValue())
                    .ToArray();

                return string.Join("; ", descriptions);
            }
        }
        /// <summary>
        /// Краткое наименование с учетом наличия
        /// </summary>
        public virtual string ResultShortName
        {
            get { return ShortName ?? Name; }
        }


        #endregion


        #region IDescribable<T> Members

        /// <summary>
        /// Список описаний
        /// </summary>
        public virtual ICollection<Description<DescriptionValue>> Descriptions { get; set; }

        #endregion
    }
}
