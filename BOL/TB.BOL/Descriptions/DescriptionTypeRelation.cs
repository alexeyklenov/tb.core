﻿namespace TB.BOL.Descriptions
{
    /// <summary>
    /// Тип связки типов описаний
    /// </summary>
    public enum DescriptionTypeRelationType
    {
        /// <summary>
        /// Дополнительная информация
        /// </summary>
        AdditionalInfo
    }
    /// <summary>
    /// Cвязка типов описаний
    /// </summary>
    public class DescriptionTypeRelation : DomainObject<long>
    {
        #region Fields

        private DescriptionTypeRelationType _relationType;
        private DescriptionType _parentDescriptionType;
        private DescriptionType _childDescriptionType;

        #endregion

        #region Properties

        /// <summary>
        /// Тип связки типов описаний
        /// </summary>
        public virtual DescriptionTypeRelationType RelationType
        {
            get { return _relationType; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _relationType = value;
                }
                else
                    if (_relationType != value)
                    {
                        _relationType = value;
                        PropertyHasChanged("RelationType");

                    }
            }
        }

        /// <summary>
        /// Родительский тип описания
        /// </summary>
        public virtual DescriptionType ParentDescriptionType
        {
            get { return _parentDescriptionType; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _parentDescriptionType = value;
                }
                else
                    if (_parentDescriptionType != value)
                    {
                        if (_parentDescriptionType != null)
                            _parentDescriptionType.Childs.Remove(this);
                        _parentDescriptionType = value;
                        if (_parentDescriptionType != null)
                            _parentDescriptionType.Childs.Add(this);
                        PropertyHasChanged("ParentDescriptionType");
                    }
            }
        }

        /// <summary>
        /// Дочерний тип описания
        /// </summary>
        public virtual DescriptionType ChildDescriptionType
        {
            get { return _childDescriptionType; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _childDescriptionType = value;
                }
                else
                    if (_childDescriptionType != value)
                    {
                        if (_childDescriptionType != null)
                            _childDescriptionType.Parents.Remove(this);
                        _childDescriptionType = value;
                        if (_childDescriptionType != null)
                            _childDescriptionType.Parents.Add(this);
                        PropertyHasChanged("ChildDescriptionType");
                    }
            }
        }

        #endregion

        #region DomainObject

        /// <summary>
        /// Хэш объекта
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return HashOf(RelationType,
                ParentDescriptionType,
                ChildDescriptionType);
        }

        /// <summary>
        /// Преобразование в строку
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} {1} {2}",
                RelationType,
                ParentDescriptionType,
                ChildDescriptionType);
        }

        #endregion
    }
}
