﻿using System;
using System.Runtime.Serialization;
using TB.BOL.Interfaces;

namespace TB.BOL.Descriptions
{
       /// <summary>
    /// Тип сравнения
    /// </summary>
    public enum ComparisonType
    {
        /// <summary>
        /// Равенство
        /// </summary>
        Equality,

        /// <summary>
        /// Пересечение
        /// </summary>
        Intersection,

        /// <summary>
        /// Сожержит для строки, between для дат и minValue-maxValue
        /// </summary>
        Subvalue
    }
     /// <summary>
    /// Тип данных описания
    /// </summary>
    [DataContract]
    public enum DescriptionDataType : byte
    {
        /// <summary>
        /// Логический
        /// </summary>
        [EnumMember]
        Logic,

        /// <summary>
        /// Числовой
        /// </summary>
        [EnumMember]
        Numeric,

        /// <summary>
        /// Период времени
        /// </summary>
        [EnumMember]
        Period,

        /// <summary>
        /// Справочный
        /// </summary>
        [EnumMember]
        Reference,

        /// <summary>
        /// Справочно-числовой
        /// </summary>
        [EnumMember]
        ReferenceNumeric,

        /// <summary>
        /// Справочно-количественный
        /// </summary>
        [EnumMember]
        ReferenceQuantity,

        /// <summary>
        /// Справочно-периодный
        /// </summary>
        [EnumMember]
        ReferencePeriod,

        /// <summary>
        /// Справочно-текстовый
        /// </summary>
        [EnumMember]
        ReferenceText,

        /// <summary>
        /// Справочно-текстовый
        /// </summary>
        [EnumMember]
        ReferenceId,

        /// <summary>
        /// Текстовый
        /// </summary>
        [EnumMember]
        Text
    }

    /// <summary>
    ///     Описание объекта предметной области
    /// </summary>
    public class Description : DomainObject<long>
    {
        #region Fields

        private DescriptionType _descriptionType;
        private bool _isEqual;
        private decimal? _minValue;
        private decimal? _maxValue;
        private DateTime? _beginDate;
        private DateTime? _endDate;
        private DescriptionValue _descriptionValue;
        private string _stringValue;
        //private UnitMeasure _unitMeasure;
        private long? _no;
        private bool _isDefault;
        private bool _isEditable;

        #endregion

        #region Properties

        /// <summary>
        ///     Тип описания
        /// </summary>
        public DescriptionType DescriptionType
        {
            get { return _descriptionType; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _descriptionType = value;
                }
                else if (_descriptionType != value)
                {
                    _descriptionType = value;
                    MinValue = _minValue;
                    MaxValue = _maxValue;
                    BeginDate = _beginDate;
                    EndDate = _endDate;
                    DescriptionValue = _descriptionValue;
                    StringValue = _stringValue;
                    //UnitMeasure = _unitMeasure;
                    PropertyHasChanged("DescriptionType");
                }
            }
        }

        /// <summary>
        ///     Признак истинности описания
        /// </summary>
        public bool IsEqual
        {
            get { return _isEqual; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _isEqual = value;
                }
                else if (_isEqual != value)
                {
                    _isEqual = value;
                    PropertyHasChanged("IsEqual");
                }
            }
        }

        /// <summary>
        ///     Минимальное числовое значение
        /// </summary>
        public decimal? MinValue
        {
            get
            {
                if ((object)_descriptionType == null ||
                    _descriptionType.DataType == DescriptionDataType.Numeric ||
                    _descriptionType.DataType == DescriptionDataType.ReferenceNumeric ||
                    _descriptionType.DataType == DescriptionDataType.ReferenceQuantity)
                    return _minValue;

                return null;
            }
            set
            {
                if (IsLoadingInProgress)
                {
                    _minValue = value;
                }
                else if ((object)_descriptionType != null &&
                         _descriptionType.DataType != DescriptionDataType.Numeric &&
                         _descriptionType.DataType != DescriptionDataType.ReferenceNumeric &&
                         _descriptionType.DataType != DescriptionDataType.ReferenceQuantity)
                    _minValue = null;
                else if (_minValue != value)
                {
                    _minValue = value;
                    PropertyHasChanged("MinValue");
                }
            }
        }

        /// <summary>
        ///     Максимальное числовое значение
        /// </summary>
        public decimal? MaxValue
        {
            get
            {
                if ((object)_descriptionType == null ||
                    _descriptionType.DataType == DescriptionDataType.Numeric ||
                    _descriptionType.DataType == DescriptionDataType.ReferenceNumeric)
                    return _maxValue;

                return null;
            }
            set
            {
                if (IsLoadingInProgress)
                {
                    _maxValue = value;
                }
                else if ((object)_descriptionType != null &&
                         _descriptionType.DataType != DescriptionDataType.Numeric &&
                         _descriptionType.DataType != DescriptionDataType.ReferenceNumeric)
                    _maxValue = null;
                else if (_maxValue != value)
                {
                    _maxValue = value;
                    PropertyHasChanged("MaxValue");
                }
            }
        }

        /// <summary>
        ///     Дата начала периода
        /// </summary>
        public DateTime? BeginDate
        {
            get { return _beginDate; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _beginDate = value;
                }
                else if (_beginDate != value)
                {
                    _beginDate = value;
                    PropertyHasChanged("BeginDate");
                }
            }
        }

        /// <summary>
        ///     Дата окончания периода
        /// </summary>
        public DateTime? EndDate
        {
            get { return _endDate; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _endDate = value;
                }
                else if (_endDate != value)
                {
                    _endDate = value;
                    PropertyHasChanged("EndDate");
                }
            }
        }

        /// <summary>
        ///     Справочное значение
        /// </summary>
        public DescriptionValue DescriptionValue
        {
            get
            {
                if ((object)_descriptionType == null ||
                    _descriptionType.DataType == DescriptionDataType.Reference ||
                    _descriptionType.DataType == DescriptionDataType.ReferenceNumeric ||
                    _descriptionType.DataType == DescriptionDataType.ReferenceQuantity ||
                    _descriptionType.DataType == DescriptionDataType.ReferencePeriod ||
                    _descriptionType.DataType == DescriptionDataType.ReferenceText ||
                    _descriptionType.DataType == DescriptionDataType.ReferenceId)

                    return _descriptionValue;

                return null;
            }
            set
            {
                if (IsLoadingInProgress)
                {
                    _descriptionValue = value;
                }
                else if ((object)_descriptionType != null &&
                         _descriptionType.DataType != DescriptionDataType.Reference &&
                         _descriptionType.DataType != DescriptionDataType.ReferenceNumeric &&
                         _descriptionType.DataType != DescriptionDataType.ReferenceQuantity &&
                         _descriptionType.DataType != DescriptionDataType.ReferencePeriod &&
                         _descriptionType.DataType != DescriptionDataType.ReferenceText &&
                         _descriptionType.DataType != DescriptionDataType.ReferenceId)

                    _descriptionValue = null;
                else if (_descriptionValue != value)
                {
                    _descriptionValue = value;
                    PropertyHasChanged("DescriptionValue");
                }
            }
        }

        /// <summary>
        ///     Строковое значение
        /// </summary>
        public string StringValue
        {
            get { return _stringValue; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _stringValue = value;
                }
                else if (_stringValue != value)
                {
                    _stringValue = value;
                    PropertyHasChanged("StringValue");
                }
            }
        }

        /// <summary>
        ///     Единица измерения
        /// </summary>
        /*public UnitMeasure UnitMeasure
        {
            get
            {
                if ((Object)_descriptionType == null ||
                    _descriptionType.DataType == DescriptionDataType.Numeric ||
                    _descriptionType.DataType == DescriptionDataType.ReferenceNumeric)
                    return _unitMeasure;

                return null;
            }
            set
            {
                if (IsLoadingInProgress)
                {
                    _unitMeasure = value;
                }
                else if ((Object)_descriptionType != null &&
                         _descriptionType.DataType != DescriptionDataType.Numeric &&
                         _descriptionType.DataType != DescriptionDataType.ReferenceNumeric)
                    _unitMeasure = null;
                else if (_unitMeasure != value)
                {
                    _unitMeasure = value;
                    PropertyHasChanged("UnitMeasure");
                }
            }
        }*/

        /// <summary>
        ///     Порядковый номер
        /// </summary>
        public long? No
        {
            get { return _no; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _no = value;
                }
                else if (_no != value)
                {
                    _no = value;
                    PropertyHasChanged("No");
                }
            }
        }

        /// <summary>
        ///     Признак описания по-умолчанию
        /// </summary>
        public bool IsDefault
        {
            get { return _isDefault; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _isDefault = value;
                }
                else if (_isDefault != value)
                {
                    _isDefault = value;
                    PropertyHasChanged("IsDefault");
                }
            }
        }

        /// <summary>
        ///     Признак доступности описания для редактирования
        /// </summary>
        public bool IsEditable
        {
            get { return _isEditable; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _isEditable = value;
                }
                else if (_isEditable != value)
                {
                    _isEditable = value;
                    PropertyHasChanged("IsEditable");
                }
            }
        }

        /// <summary>
        ///     Признака заполнения значениями
        /// </summary>
        /// <returns></returns>
        public bool IsEmpty
        {
            get
            {
                return !MinValue.HasValue &&
                       !MaxValue.HasValue &&
                       !BeginDate.HasValue &&
                       !EndDate.HasValue &&
                       (object)DescriptionValue == null &&
                       string.IsNullOrEmpty(StringValue);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Конструктор
        /// </summary>
        public Description()
        {
            _isEqual = true;
            _isDefault = true;
        }

        /// <summary>
        ///     Получение значения описания
        /// </summary>
        /// <returns></returns>
        public string GetValue()
        {
            string result;
            switch (DescriptionType.DataType)
            {
                case DescriptionDataType.Reference:
                case DescriptionDataType.ReferencePeriod:
                case DescriptionDataType.ReferenceText:
                case DescriptionDataType.ReferenceQuantity:
                case DescriptionDataType.ReferenceNumeric:
                    result = ((object)DescriptionValue != null ? DescriptionValue.Name : "") +
                               (!string.IsNullOrEmpty(StringValue) ? " (" + StringValue + ")" : "") +
                               (BeginDate.HasValue && EndDate.HasValue ? " " : "") +
                               (BeginDate.HasValue ? "c " + BeginDate.Value.ToShortDateString() : "") +
                               (BeginDate.HasValue && EndDate.HasValue ? " " : "") +
                               (EndDate.HasValue ? "по " + EndDate.Value.ToShortDateString() : "");
                    break;
                case DescriptionDataType.Numeric:
                    if (MinValue.HasValue && MaxValue.HasValue)
                    {
                        if (MinValue.Value != int.MinValue)
                        {
                            if (MaxValue.Value != int.MaxValue)
                                result = MinValue + " - " + MaxValue;
                            else
                                result = "> " + MinValue;
                        }
                        else
                            result = "< " + MaxValue;
                    }
                    else
                        result = (MinValue.HasValue ? MinValue.ToString() : "") +
                                   (MaxValue.HasValue ? MaxValue.ToString() : "");
                    break;
                case DescriptionDataType.Period:
                    result = (BeginDate.HasValue ? "c " + BeginDate : "") +
                               (BeginDate.HasValue && EndDate.HasValue ? " " : "") +
                               (EndDate.HasValue ? "по " + EndDate : "");
                    break;
                default:
                    result = StringValue;
                    break;
            }

            return string.Concat(IsEqual ? "" : "<>", result);
        }

        /// <summary>
        ///     Установка значения описания
        /// </summary>
        /// <returns></returns>
        public Description SetValue(Description description)
        {
            // Если источник не задан, то ничего не изменяется
            if (description == null)
                return this;

            // Если тип описания неопределен, то присваивается целевой
            if (DescriptionType == null)
                DescriptionType = description.DescriptionType;

            // Если типы описаний равны, то присваивается значение
            if (DescriptionType == description.DescriptionType ||
                DescriptionType.DataType == description.DescriptionType.DataType &&
                DescriptionType.DataType != DescriptionDataType.Reference &&
                DescriptionType.DataType != DescriptionDataType.ReferenceNumeric &&
                DescriptionType.DataType != DescriptionDataType.ReferenceQuantity &&
                DescriptionType.DataType != DescriptionDataType.ReferencePeriod &&
                DescriptionType.DataType != DescriptionDataType.ReferenceText)
            {
                IsEqual = description.IsEqual;
                MinValue = description.MinValue;
                MaxValue = description.MaxValue;
                BeginDate = description.BeginDate;
                EndDate = description.EndDate;
                DescriptionValue = description.DescriptionValue;
                StringValue = description.StringValue;
                IsDefault = description.IsDefault;
                // TODO
                /*if (!Attribute.IsDefined(typeof(T), typeof(CanContainEmptyDescriptionAttribute)) &&
                    DescriptionType.DataType == DescriptionDataType.Reference &&
                    DescriptionValue == null)
                    DescribedObject = null;*/
            }

            return this;
        }

        /// <summary>
        ///     Сравнение описаний
        /// </summary>
        /// <param name="value"></param>
        /// <param name="comparisonType"></param>
        /// <returns></returns>
        public bool Equals(Description value, ComparisonType comparisonType)
        {
            if (value == null)
                return false;

            if (DescriptionType != value.DescriptionType)
                return false;

            switch (DescriptionType.DataType)
            {
                case DescriptionDataType.Logic:
                    return true;

                case DescriptionDataType.Numeric:
                case DescriptionDataType.ReferenceNumeric:
                    switch (comparisonType)
                    {
                        case ComparisonType.Equality:
                            return DescriptionValue == value.DescriptionValue &&
                                   MinValue == value.MinValue &&
                                   MaxValue == value.MaxValue;
                        case ComparisonType.Intersection:
                            return DescriptionValue == value.DescriptionValue &&
                                   ((MinValue ?? decimal.Zero) > (value.MinValue ?? decimal.Zero)
                                       ? MinValue ?? decimal.Zero
                                       : value.MinValue ?? decimal.Zero) <=
                                   ((MaxValue ?? (MinValue ?? decimal.Zero)) <
                                    (value.MaxValue ?? (value.MinValue ?? decimal.Zero))
                                       ? MaxValue ?? (MinValue ?? decimal.Zero)
                                       : value.MaxValue ?? (value.MinValue ?? decimal.Zero));
                        case ComparisonType.Subvalue:
                            return DescriptionValue == value.DescriptionValue &&
                                   (MinValue ?? decimal.Zero) <= (value.MinValue ?? decimal.Zero) &&
                                   (MaxValue ?? (MinValue ?? decimal.Zero)) >=
                                   (value.MaxValue ?? (value.MinValue ?? decimal.Zero));
                        default:
                            return false;
                    }

                case DescriptionDataType.Period:
                case DescriptionDataType.ReferencePeriod:
                    switch (comparisonType)
                    {
                        case ComparisonType.Equality:
                            return DescriptionValue == value.DescriptionValue &&
                                   BeginDate == value.BeginDate &&
                                   EndDate == value.EndDate &&
                                   StringValue == value.StringValue;
                        case ComparisonType.Intersection:
                            return DescriptionValue == value.DescriptionValue &&
                                   ((BeginDate ?? DateTime.MinValue) > (value.BeginDate ?? DateTime.MinValue)
                                       ? BeginDate ?? DateTime.MinValue
                                       : value.BeginDate ?? DateTime.MinValue) <=
                                   ((EndDate ?? DateTime.MaxValue) < (value.EndDate ?? DateTime.MaxValue)
                                       ? EndDate ?? DateTime.MaxValue
                                       : value.EndDate ?? DateTime.MaxValue);
                        case ComparisonType.Subvalue:
                            return DescriptionValue == value.DescriptionValue &&
                                   (BeginDate ?? DateTime.MinValue) <= (value.BeginDate ?? DateTime.MinValue) &&
                                   (EndDate ?? (BeginDate ?? DateTime.MinValue)) >=
                                   (value.EndDate ?? (value.BeginDate ?? DateTime.MinValue)) &&
                                   StringValue == value.StringValue;
                        default:
                            return false;
                    }

                case DescriptionDataType.Reference:
                    return DescriptionValue == value.DescriptionValue;

                case DescriptionDataType.Text:
                case DescriptionDataType.ReferenceText:
                    switch (comparisonType)
                    {
                        case ComparisonType.Equality:
                            return DescriptionValue == value.DescriptionValue &&
                                   StringValue == value.StringValue;
                        case ComparisonType.Intersection:
                            return DescriptionValue == value.DescriptionValue;
                        case ComparisonType.Subvalue:
                            return DescriptionValue == value.DescriptionValue &&
                                   (StringValue == value.StringValue ||
                                    StringValue != null &&
                                    value.StringValue != null &&
                                    StringValue.Contains(value.StringValue));
                        default:
                            return false;
                    }

                case DescriptionDataType.ReferenceQuantity:
                    switch (comparisonType)
                    {
                        case ComparisonType.Equality:
                            return DescriptionValue == value.DescriptionValue &&
                                   //UnitMeasure == value.UnitMeasure &&
                                   MinValue == value.MinValue;
                        case ComparisonType.Intersection:
                            return DescriptionValue == value.DescriptionValue;
                                  // UnitMeasure == value.UnitMeasure;
                        case ComparisonType.Subvalue:
                            return DescriptionValue == value.DescriptionValue &&
                                  // UnitMeasure == value.UnitMeasure &&
                                   (MinValue ?? value.MinValue ?? decimal.Zero) ==
                                   (value.MinValue ?? MinValue ?? decimal.Zero);
                        default:
                            return false;
                    }
                default:
                    return false;
            }
        }

        public override bool Equals(object value)
        {
            if (value is Description)
                return Equals(value as Description, ComparisonType.Equality);
            if (DescriptionType.DataType == DescriptionDataType.Numeric)
                return MinValue != null && MinValue.Equals(Convert.ToDecimal(value));
            return StringValue != null && StringValue.Equals(value);
        }

        public override int CompareTo(object value)
        {
            return MinValue != null ? MinValue.Value.CompareTo(Convert.ToDecimal(value)) : base.CompareTo(value);
        }

        /// <summary>
        ///     Объединение описаний
        /// </summary>
        public void Merge(Description source)
        {
            // Если описания не имеют пересечения, то объединение невозможно
            if (!Equals(source, ComparisonType.Intersection))
                return;

            // Объединение согласно типу данных
            switch (DescriptionType.DataType)
            {
                case DescriptionDataType.Logic:
                    break;

                case DescriptionDataType.Numeric:
                case DescriptionDataType.ReferenceNumeric:
                    MinValue = !MinValue.HasValue && !source.MinValue.HasValue
                        ? null
                        : !MinValue.HasValue
                            ? source.MinValue
                            : !source.MinValue.HasValue
                                ? MinValue
                                : Math.Min(MinValue.GetValueOrDefault(0), source.MinValue.GetValueOrDefault(0));
                    MaxValue = !MaxValue.HasValue && !source.MaxValue.HasValue
                        ? null
                        : !MaxValue.HasValue
                            ? source.MaxValue
                            : !source.MaxValue.HasValue
                                ? MaxValue
                                : Math.Max(MaxValue.GetValueOrDefault(0), source.MaxValue.GetValueOrDefault(0));
                    break;

                case DescriptionDataType.Period:
                case DescriptionDataType.ReferencePeriod:
                    BeginDate = !BeginDate.HasValue && !source.BeginDate.HasValue
                        ? null
                        : !BeginDate.HasValue
                            ? source.BeginDate
                            : !source.BeginDate.HasValue
                                ? BeginDate
                                : BeginDate <= source.BeginDate ? BeginDate : source.BeginDate;
                    EndDate = !EndDate.HasValue && !source.EndDate.HasValue
                        ? null
                        : !EndDate.HasValue
                            ? source.EndDate
                            : !source.EndDate.HasValue
                                ? EndDate
                                : EndDate >= source.EndDate ? EndDate : source.EndDate;
                    break;

                case DescriptionDataType.Reference:
                    break;

                case DescriptionDataType.Text:
                case DescriptionDataType.ReferenceText:
                    StringValue = string.Concat(StringValue, source.StringValue);
                    break;

                case DescriptionDataType.ReferenceQuantity:
                    MinValue = !MinValue.HasValue && !source.MinValue.HasValue
                        ? null
                        : !MinValue.HasValue
                            ? source.MinValue
                            : !source.MinValue.HasValue
                                ? MinValue
                                : MinValue + source.MinValue;
                    break;

                default:
                    break;
            }
        }

        #endregion

         #region DomainObject

        private static readonly string FullTypeName = typeof(Description).FullName;

        /// <summary>
        ///     Хэш объекта
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return string.Format("{0}|{1}", FullTypeName, ToString()).GetHashCode();
        }

        public string FormatString()
        {
            var minValue = MinValue;
            var maxValue = MaxValue;
            var beginDate = BeginDate;
            var endDate = EndDate;

            var descValue = string.Concat(minValue.HasValue ? minValue.ToString() : "",
                                          minValue.HasValue && maxValue.HasValue ? " - " : "",
                                          maxValue.HasValue ? maxValue.ToString() : "",
                                          (object)DescriptionValue != null ? DescriptionValue.ToString() : "",
                                          StringValue,
                                          beginDate.HasValue ? "c " + beginDate : "",
                                          beginDate.HasValue && endDate.HasValue ? " " : "",
                                          endDate.HasValue ? "по " + endDate : ""
                );

            return descValue;
        }

        /// <summary>
        ///     Преобразование в строку
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var descriptionType = DescriptionType;
            var descValue = FormatString();

            return string.Concat(IsEqual ? "" : "<>",
                                 descriptionType != null
                                     ? (descriptionType.ToString())
                                     : string.Empty,
                                 string.IsNullOrEmpty(descValue.Trim()) ? "" : string.Format(" ({0})", descValue));
        }

        #endregion

        public object Clone<TU>(TU describedObject)
           where TU : DomainObject<long>, IDescribable<TU>
        {
            var clonedDescription = new Description<TU>(describedObject) { DescriptionType = DescriptionType };

            clonedDescription.SetValue(this);
            clonedDescription.No = No;

            return clonedDescription;
        }
    }


    /// <summary>
    ///     Описание объекта предметной области
    /// </summary>
    public class Description<T> : Description, ICloneable
        where T : DomainObject<long>, IDescribable<T>
    {
        private T _describedObject;

        /// <summary>
        ///     Конструктор
        /// </summary>
        public Description()
        {
        }

        public Description(T describedObject)
            : this()
        {
            DescribedObject = describedObject;
        }

        /// <summary>
        ///     Ссылка на описываемый объект
        /// </summary>
        public T DescribedObject
        {
            get
            {                
                return _describedObject;
            }
            set
            {
                if (IsLoadingInProgress)
                {
                    _describedObject = value;
                }
                else if (_describedObject != value)
                {
                    if ((object)_describedObject != null)
                        _describedObject.Descriptions.Remove(this);

                    _describedObject = value;

                    if ((object)_describedObject != null)
                        _describedObject.Descriptions.Add(this);
                    
                    PropertyHasChanged("DescribedObject");
                }
            }
        }

        #region ICloneable Members

        /// <summary>
        ///     Копирование
        /// </summary>
        /// <returns>Копия данного экземпляра Description</returns>
        public object Clone()
        {
            return Clone(DescribedObject);
        }

        #endregion

        public override int CompareTo(object value)
        {
            return MinValue != null ? MinValue.Value.CompareTo(value) : base.CompareTo(value);
        }
    }
}
