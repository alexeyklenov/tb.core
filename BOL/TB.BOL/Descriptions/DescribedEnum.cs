﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Resources;
using log4net;

namespace TB.BOL.Descriptions
{
    /// <summary>
    /// Описание перечисления
    /// </summary>
    /// <typeparam name="T">Тип перечисления</typeparam>
    public class DescribedEnum<T>
    {
        /// <summary>
        /// Словарь значение-описание
        /// </summary>
        private static readonly IDictionary<T, string> Descriptions = new Dictionary<T, string>();

        /// <summary>
        /// Словарь описание-значение
        /// </summary>
        private static readonly IDictionary<string, T> Values = new Dictionary<string, T>();

        /// <summary> 
        /// Конструктор
        /// </summary> 
        static DescribedEnum()
        {
            // Менеджер ресурсов
            var resourceManager = new ResourceManager(Assembly.GetExecutingAssembly().GetName().Name + ".Resources", Assembly.GetExecutingAssembly());

            var type = typeof(T);

            // Если тип enum, то заполняем словари
            if (type.IsEnum)
            {
                foreach (T enumValue in Enum.GetValues(type))
                {
                    var desciption = enumValue.ToString();
                    try
                    {
                        desciption = resourceManager.GetString(string.Format("{0}_{1}", type.Name, desciption)) ??
                                     desciption;
                    }
                    catch (Exception ex)
                    {
                        LogManager.GetLogger("DescribedEnum").Error(
                            String.Format("Can't translate enum {0}={1}", type, enumValue), ex);
                    }
                    Descriptions[enumValue] = desciption;
                    Values[desciption] = enumValue;
                }
            }
        }

        /// <summary> 
        /// Получение описания по значению
        /// </summary> 
        /// <param name="value">Значение</param> 
        /// <returns>Описание</returns> 
        public static String GetDescription(T value)
        {
            String result;
            Descriptions.TryGetValue(value, out result);

            if (string.IsNullOrEmpty(result))
            {
                var splitted = value.ToString().Split(',');

                result = string.Join(" | ", splitted.Select(s => Descriptions.FirstOrDefault(i => i.Key.ToString() == s.Trim()).Value).ToArray());
            }

            return result;
        }

        /// <summary> 
        /// Получение значения по описанию
        /// </summary> 
        /// <param name="description">Описание</param> 
        /// <returns>Значение</returns> 
        public static T GetValue(String description)
        {
            T result;
            Values.TryGetValue(description, out result);

            return result;
        }
    }
}
