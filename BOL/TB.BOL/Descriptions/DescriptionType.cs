﻿using System.Collections.Generic;
using System.Linq;
using TB.BOL.Interfaces;

namespace TB.BOL.Descriptions
{
    /// <summary>
    /// Тип описания объекта предметной области
    /// </summary>
    public class DescriptionType : Reference<long>, IDescribable<DescriptionType>
    {
        #region Fields

        private DescriptionType _parent;
        private DescriptionDataType _dataType;
        //private UnitMeasure _unitMeasure;
        private decimal? _minValue;
        private decimal? _maxValue;

        #endregion

        #region Properties

      
        /// <summary>
        /// Родительская запись
        /// </summary>
        public virtual DescriptionType Parent
        {
            get { return _parent; }
            set
            {
                if (_parent != value)
                {
                    if (!IsLoadingInProgress)
                    {
                        if ((object)_parent != null)
                            _parent.DescriptionTypes.Remove(this);
                    }
                    _parent = value;
                    if (!IsLoadingInProgress)
                    {
                        if ((object)_parent != null)
                            _parent.DescriptionTypes.Add(this);
                        PropertyHasChanged("Parent");
                    }
                }
            }
        }

        /// <summary>
        /// Полный список родительских типов описаний
        /// </summary>
        /// <returns></returns>
        public virtual List<DescriptionType> AllParents
        {
            get
            {
                return Parent != null ? new List<DescriptionType> { Parent }.Concat(Parent.AllParents).ToList() : new List<DescriptionType>();
            }
        }

        /// <summary>
        /// Корневой тип описаний
        /// </summary>
        public virtual DescriptionType RootDescriptionType
        {
            get
            {
                return Parent != null ? AllParents.Where(x => x.Parent == null).First() : this;
            }
        }

        /// <summary>
        /// Тип данных описания
        /// </summary>
        public virtual DescriptionDataType DataType
        {
            get { return _dataType; }
            protected set
            {
                if (_dataType != value)
                {
                    _dataType = value;
                    if (!IsLoadingInProgress)
                    {
                        PropertyHasChanged("DataType");
                    }
                }
            }
        }

        /// <summary>
        /// Единица измерения продукции
        /// </summary>
       /* public virtual UnitMeasure UnitMeasure
        {
            get { return _unitMeasure; }
            set
            {
                if (_unitMeasure != value)
                {
                    _unitMeasure = value;
                    if (!IsLoadingInProgress)
                    {
                        PropertyHasChanged("UnitMeasure");
                    }
                }
            }
        }*/

        /// <summary>
        /// Минимальное числовое значение
        /// </summary>
        public virtual decimal? MinValue
        {
            get { return _minValue; }
            set
            {
                if (_minValue != value)
                {
                    _minValue = value;
                    if (!IsLoadingInProgress)
                    {
                        PropertyHasChanged("MinValue");
                    }
                }
            }
        }

        /// <summary>
        /// Максимальное числовое значение
        /// </summary>
        public virtual decimal? MaxValue
        {
            get { return _maxValue; }
            set
            {
                if (_maxValue != value)
                {
                    _maxValue = value;
                    if (!IsLoadingInProgress)
                    {
                        PropertyHasChanged("MaxValue");
                    }
                }
            }
        }

        /// <summary>
        /// Список дочерних типов описаний
        /// </summary>
        public virtual ICollection<DescriptionType> DescriptionTypes { get; set; }

        /// <summary>
        /// Список значение данного типа
        /// </summary>
        public virtual ICollection<DescriptionValue> DescriptionValues { get; set; }

        /// <summary>
        /// Список родительских типов описаний
        /// </summary>
        public virtual ICollection<DescriptionTypeRelation> Parents { get; set; }

        /// <summary>
        /// Список дочерних типов описаний
        /// </summary>
        public virtual ICollection<DescriptionTypeRelation> Childs { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Конструктор
        /// </summary>
        public DescriptionType()
        {
            DescriptionTypes = new List<DescriptionType>();
            DescriptionValues = new List<DescriptionValue>();
            Parents = new List<DescriptionTypeRelation>();
            Childs = new List<DescriptionTypeRelation>();
            Descriptions = new List<Description<DescriptionType>>();
        }

        public DescriptionType(DescriptionDataType type) : this()
        {
            DataType = type;
        }

        #endregion

        #region IDescribable<T> Members

        public virtual ICollection<Description<DescriptionType>> Descriptions { get; set; }

        #endregion
    }
}
