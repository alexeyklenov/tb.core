﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using TB.BOL.Administration;
using TB.BOL.Descriptions;
using TB.BOL.Interfaces;
using TB.BOL.Materials;

namespace TB.BOL.RouteStep
{
    /// <summary>
    /// Название системной операции
    /// </summary>
    [Flags]
    [DataContract]
    public enum SystemOperation : byte
    {
        Creation = 0x01,
        Processing = 0x04,
        Cancel = 0x08,
        Correction = 0x16,
        Moving = 0x32
    }

    /// <summary>
    /// Шаг маршрута
    /// </summary>
    [Description("Шаг маршрута")]
    public class RouteStep<T> :  DomainObject<long>, ICloneable, IDescribable<RouteStep<T>>
        where T : DomainObject<long>, IRouteStep<T>
    {

        #region Fields

        private Int64 _no;
        private SystemOperation _systemOperation;
        private TechnologicalOperation<T> _techOperation;
        private DateTime _operationDate;
        private Employee _employee;
        private RouteStep<T> _parent;
        private MaterialTemplate _materialTemplate;
        private T _routeStepObject;

        #endregion

        #region Properties


        /// <summary>
        /// Номер
        /// </summary>
        public Int64 No
        {
            get { return _no; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _no = value;
                }
                else
                    if (_no != value)
                    {
                        _no = value;
                        PropertyHasChanged("No");
                    }
            }
        }


        /// <summary>
        /// Системная операция
        /// </summary>
        public SystemOperation SystemOperation
        {
            get { return _systemOperation; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _systemOperation = value;
                }
                else
                    if (_systemOperation != value)
                    {
                        _systemOperation = value;
                        PropertyHasChanged("SystemOperation");
                    }
            }
        }

        /// <summary>
        /// Технологическая операция
        /// </summary>
        public TechnologicalOperation<T> TechOperation
        {
            get { return _techOperation; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _techOperation = value;
                }
                else
                    if (_techOperation != value)
                    {
                        _techOperation = value;
                        PropertyHasChanged("TechOperation");
                    }
            }
        }

        /// <summary>
        /// Дата регистрации операции
        /// </summary>
        public DateTime OperationDate
        {
            get { return _operationDate; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _operationDate = value;
                }
                else
                    if (_operationDate != value)
                    {
                        _operationDate = value;
                        PropertyHasChanged("OperationDate");
                    }
            }
        }

        /// <summary>
        /// Сотрудник
        /// </summary>
        public Employee Employee
        {
            get { return _employee; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _employee = value;
                }
                else
                    if (_employee != value)
                    {
                        _employee = value;
                        PropertyHasChanged("Employee");
                    }
            }
        }


        /// <summary>
        /// Родительская запись
        /// </summary>
        public RouteStep<T> Parent
        {
            get { return _parent; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _parent = value;
                }
                else
                    if (_parent != value)
                    {
                        _parent = value;
                        PropertyHasChanged("Parent");
                    }
            }
        }


        /// <summary>
        /// Состояние заказа на данном шаге маршрута
        /// </summary>
        public MaterialTemplate MaterialTemplate
        {
            get { return _materialTemplate; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _materialTemplate = value;
                }
                else
                    if (_materialTemplate != value)
                    {
                        _materialTemplate = value;
                        PropertyHasChanged("MaterialTemplate");
                    }
            }
        }

        #endregion

        /// <summary>
        ///     Конструктор
        /// </summary>
        public RouteStep()
        {
            Descriptions = new List<Description<RouteStep<T>>>();
        }

        public RouteStep(T routeStepObject)
            : this()
        {
            RouteStepObject = routeStepObject;
        }

        /// <summary>
        ///     Ссылка на описываемый объект
        /// </summary>
        public T RouteStepObject
        {
            get { return _routeStepObject; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _routeStepObject = value;
                }
                else if (_routeStepObject != value)
                {
                    if ((object) _routeStepObject != null)
                        _routeStepObject.RouteSteps.Remove(this);

                    _routeStepObject = value;

                    if ((object) _routeStepObject != null)
                        _routeStepObject.RouteSteps.Add(this);

                    PropertyHasChanged("RouteStepObject");
                }
            }
        }

        #region ICloneable Members


        public object Clone()
        {
            var cloneRouteStep = new RouteStep<T>
            {
                No = No,
                SystemOperation = SystemOperation,
                TechOperation = TechOperation,
                OperationDate = OperationDate
            };
            return cloneRouteStep;
        }


        public RouteStep<T> Clone(Material material)
        {
            var cloneRouteStep = Clone() as RouteStep<T>;

            return cloneRouteStep;
        }
        #endregion

        public override int GetHashCode()
        {
            return HashOf(Guid);
        }

        public override string ToString()
        {
            return string.Empty;
        }

        #region IDescribable<T> Members

        /// <summary>
        /// Список описаний шага маршрута
        /// </summary>
        public ICollection<Description<RouteStep<T>>> Descriptions { get; set; }

        #endregion
        
    }
}
