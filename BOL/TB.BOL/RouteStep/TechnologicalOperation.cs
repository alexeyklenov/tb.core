﻿using System.Collections.Generic;
using System.Linq;

namespace TB.BOL.RouteStep
{
    /// <summary>
    /// Технологическая операция
    /// </summary>
    public class TechnologicalOperation<T> : Reference<long>  where T : DomainObject<long>
    {        
           #region Fields

        private TechnologicalOperation<T> _parent;

        #endregion

        #region Properties


        /// <summary>
        /// Операция верхнего уровня
        /// </summary>
        public virtual TechnologicalOperation<T> Parent
        {
            get { return _parent; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _parent = value;
                }
                else
                    if (_parent != value)
                    {
                        if (_parent != null)
                            _parent.Childs.Remove(this);
                        _parent = value;
                        if (_parent != null)
                            _parent.Childs.Add(this);
                        PropertyHasChanged("Parent");
                    }
            }
        }

        /// <summary>
        /// Список дочерних операций
        /// </summary>
        public virtual IList<TechnologicalOperation<T>> Childs { get; set; }

        /// <summary>
        /// Полный список дочерних операций
        /// </summary>
        public virtual IList<TechnologicalOperation<T>> AllChilds
        {
            get
            {
                return Childs.ToList().SelectMany(y => y.AllChilds).Concat(Childs).ToList();
            }
        }

        #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        protected TechnologicalOperation()
        {
            Childs = new List<TechnologicalOperation<T>>();
        }
    }
  
}
