﻿using System;

namespace TB.BOL.Documents
{
    [Flags]
    public enum DocumentTypeSpecialCondition
    {
        /// <summary>
        /// Отсутствуют
        /// </summary>
        None = 0x00
    }
    [Flags]
    public enum DocumentLineMode
    {
        None = 0x00,
        /// <summary>
        /// Удалить предыдущую строку у материала по данному типу документа
        /// </summary>
        RemovePrev = 0x01,

        /// <summary>
        /// Анулировать предыдущую строку у материала по данному типу документа
        /// </summary>
        AnnulPrev = 0x02
    }

    public class DocumentType: Reference<long>
    {
        #region Fields

        private DocumentStatus? _documentStatus;

        private DocumentLineMode? _documentLineMode;

        private DocumentTypeSpecialCondition? _specialCondition;

        #endregion

        #region Properties

        /// <summary>
        /// Статус фиксации документа
        /// </summary>
        public virtual DocumentStatus? FixingDocumentStatus
        {
            get { return _documentStatus; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _documentStatus = value;
                }
                else
                    if (_documentStatus != value)
                    {
                        _documentStatus = value;
                        PropertyHasChanged("DocumentStatus");
                    }
            }
        }

        /// <summary>
        ///   Режим добавления строк однотипных документов на материал
        /// </summary>
        public virtual DocumentLineMode? DocumentLineMode
        {
            get { return _documentLineMode; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _documentLineMode = value;
                }
                else
                    if (_documentLineMode != value)
                    {
                        _documentLineMode = value;
                        PropertyHasChanged("DocumentLineMode");
                    }
            }
        }

        /// <summary>
        ///   Спец. условия
        /// </summary>
        public virtual DocumentTypeSpecialCondition? SpecialCondition
        {
            get { return _specialCondition; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _specialCondition = value;
                }
                else
                    if (_specialCondition != value)
                    {
                        _specialCondition = value;
                        PropertyHasChanged("SpecialCondition");
                    }
            }
        }
        /// <summary>
        /// Список активных документов
        /// </summary>
        //public virtual ICollection<Document> Documents { get; set; }

        #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        public DocumentType()
        {
           // Documents = new List<Document>();
        }
    }
}
