﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using TB.BOL.Descriptions;
using TB.BOL.Interfaces;
using TB.BOL.Materials;

namespace TB.BOL.Documents
{
    /// <summary>
    /// Тип строки документа
    /// </summary>
    [Flags]
    public enum DocumentLineType
    {
        /// <summary>
        /// Материал
        /// </summary>
        Material = 0x01
    }
    /// <summary>
    /// Статус строки документа
    /// </summary>
    [Flags]
    [DataContract]
    public enum DocumentLineStatus
    {
        /// <summary>
        /// Создан
        /// </summary>
        [EnumMember]
        Created = 0x01,

        /// <summary>
        /// Утвержден
        /// </summary>
        [EnumMember]
        Approved = 0x02,

        /// <summary>
        /// Закрыт
        /// </summary>
        [EnumMember]
        Closed = 0x04,

        /// <summary>
        /// Аннулирован
        /// </summary>
        [EnumMember]
        Annuled = 0x08,

        /// <summary>
        /// Архивирован
        /// </summary>
        [EnumMember]
        Archived = 0x10
    }

    public class DocumentLine : DomainObject<long>, IDescribable<DocumentLine>, ICloneable
    {
        #region Fields

        private DocumentLineType _documentLineType;
        private Document _document;
        private DocumentLineStatus _documentLineStatus;
        private long _no;
        private string _name;
        private Material _material;

        #endregion

        #region Properties

        /// <summary>
        /// Активные статусы строки. 
        /// </summary>
        public static DocumentLineStatus ActiveLineStatus
        {
            get
            {
                return DocumentLineStatus.Created | DocumentLineStatus.Approved |
                       DocumentLineStatus.Closed;
            }
        }

        /// <summary>
        /// Признак активности
        /// </summary>
        public virtual bool IsActive
        {
            get { return ActiveLineStatus.HasFlag(DocumentLineStatus); }
        }

        /// <summary>
        /// Тип строки
        /// </summary>
        public virtual DocumentLineType DocumentLineType
        {
            get { return _documentLineType; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _documentLineType = value;
                }
                else if (_documentLineType != value)
                {
                    _documentLineType = value;
                    PropertyHasChanged("DocumentLineType");
                }
            }
        }

        /// <summary>
        /// Документ
        /// </summary>
        public virtual Document Document
        {
            get { return _document; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _document = value;
                }
                else if (_document != value)
                {
                    if (_document != null)
                    {
                        /*if (NHibernateUtil.IsInitialized(_document.Lines))*/
                        _document.Lines.Remove(this);
                        //PropertyHasChanged("Document", Quantity, null);
                    }
                    _document = value;
                    if (_document != null)
                        _document.Lines.Add(this);
                    //PropertyHasChanged("Document", null, Quantity);
                }
            }
        }

        /// <summary>
        /// Номер строки
        /// </summary>
        public virtual DocumentLineStatus DocumentLineStatus
        {
            get { return _documentLineStatus; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _documentLineStatus = value;
                }
                else if (_documentLineStatus != value)
                {
                    DocumentLineStatus oldStatus = _documentLineStatus;
                    _documentLineStatus = value;
                    if (_documentLineStatus == DocumentLineStatus.Annuled &&
                        !Document.Lines.Any(x => x.DocumentLineStatus != DocumentLineStatus.Annuled &&
                                                 x.DocumentLineStatus != DocumentLineStatus.Archived))
                        //Document.AddRouteStep(Operation.Container.DocumentAnnulment);

                    if (_documentLineStatus == DocumentLineStatus.Approved &&
                        !Document.Lines.Any(x => x.DocumentLineStatus != DocumentLineStatus.Approved &&
                                                 x.DocumentLineStatus != DocumentLineStatus.Annuled))
                        //Document.AddRouteStep(Operation.Container.DocumentApproving);

                    if (_documentLineStatus == DocumentLineStatus.Closed &&
                        !Document.Lines.Any(x => x.DocumentLineStatus != DocumentLineStatus.Closed &&
                                                 x.DocumentLineStatus != DocumentLineStatus.Annuled))
                      //  Document.AddRouteStep(Operation.Container.DocumentClosing);

                    PropertyHasChanged("DocumentLineStatus", oldStatus, _documentLineStatus);
                }
            }
        }

        /// <summary>
        /// Номер строки
        /// </summary>
        public virtual long No
        {
            get { return _no; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _no = value;
                }
                else if (_no != value)
                {
                    _no = value;
                    PropertyHasChanged("No");
                }
            }
        }

        /// <summary>
        /// Наименование строки
        /// </summary>
        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _name = value;
                }
                else if (_name != value)
                {
                    _name = value;
                    PropertyHasChanged("Name");
                }
            }
        }

        /// <summary>
        /// Количество
        /// </summary>
        /*public virtual QuantityBase Quantity
        {
            get
            {
                if (_quantity == null)
                {
                    _quantity = new QuantityBase();
                    _quantity.OnPropertyChanged += QuantityChangedEventHandler;
                }

                return _quantity;
            }
            set
            {
                if (IsLoadingInProgress)
                {
                    _quantity = value;
                    if (_quantity != null)
                        _quantity.OnPropertyChanged += QuantityChangedEventHandler;
                }
                else if (_quantity != value)
                {
                    if (_quantity != null)
                        _quantity.OnPropertyChanged -= QuantityChangedEventHandler;
                    QuantityBase oldQuantity = _quantity;
                    _quantity = value;
                    if (_quantity != null)
                        _quantity.OnPropertyChanged += QuantityChangedEventHandler;

                    PropertyHasChanged("Quantity", oldQuantity, _quantity);
                }
            }
        }*/

        /// <summary>
        /// Материал
        /// </summary>
        public virtual Material Material
        {
            get { return _material; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _material = value;
                }
                else if (_material != value)
                {
                    if (_material != null /* && NHibernateUtil.IsInitialized(_material.Documents)*/)
                        _material.Documents.Remove(this);
                    _material = value;
                    if (_material != null)
                        _material.Documents.Add(this);

                    PropertyHasChanged("Material");
                }
            }
        }

        #endregion
        #region Methods

        /// <summary>
        /// Конструктор
        /// </summary>
        public DocumentLine()
        {
            DocumentLineStatus = DocumentLineStatus.Created;

            _documentLineType = DocumentLineType.Material;

            Descriptions = new List<Description<DocumentLine>>();
        }

        public DocumentLine(Document document)
            : this()
        {
            Document = document;

            No = _document.Lines.Max(p => p.No) + 1;
        }

        #endregion

        #region DomainObject

        public override int GetHashCode()
        {
            return HashOf(Guid);
        }

        public override string ToString()
        {
            return (Document != null ? Document.ToString() : "") + " стр." + No;
        }

        #endregion

        #region IDescribable<T> Members

        /// <summary>
        /// Список описаний строки документа
        /// </summary>
        public virtual ICollection<Description<DocumentLine>> Descriptions { get; set; }

        #endregion

        #region ICloneable Members

        public virtual object Clone(Document document)
        {
            DocumentLine documentLine = new DocumentLine
            {
                Document = document,
                DocumentLineStatus = DocumentLineStatus,
                Name = Name,
                No = No,
                //Quantity = Quantity
            };

            // Копирование описаний
            foreach (Description<DocumentLine> desc in Descriptions)
                desc.Clone(documentLine);

            //ObjectLoader.Save(documentLine);

            return documentLine;
        }

        public virtual object Clone()
        {
            return Clone(null);
        }

        #endregion

        public virtual Description this[long code]
        {
            get { return this.GetDescription(code); }
            set
            {
                this.SetDescription(code, value.MinValue, value.MaxValue, value.StringValue, value.BeginDate,
                    value.EndDate);
            }
        }
    }
}
