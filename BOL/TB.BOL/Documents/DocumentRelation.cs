﻿namespace TB.BOL.Documents
{
    /// <summary>
    /// Связка документов
    /// </summary>
    public class DocumentRelation : DomainObject<long>
    {
        #region Fields

        private Document _sourceDocument;
        private Document _derivedDocument;
        private bool _isDefault;

        #endregion

        #region Properties

        /// <summary>
        /// Документ-источник
        /// </summary>
        public virtual Document SourceDocument
        {
            get { return _sourceDocument; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _sourceDocument = value;
                }
                else
                    if (_sourceDocument != value)
                    {
                        if (_sourceDocument != null)
                            _sourceDocument.DerivedDocumentRltns.Remove(this);
                        _sourceDocument = value;
                        if (_sourceDocument != null)
                            _sourceDocument.DerivedDocumentRltns.Add(this);
                        PropertyHasChanged("SourceDocument");
                    }
            }
        }

        /// <summary>
        /// Производный документ
        /// </summary>
        public virtual Document DerivedDocument
        {
            get { return _derivedDocument; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _derivedDocument = value;
                }
                else
                    if (_derivedDocument != value)
                    {
                        if (_derivedDocument != null)
                            _derivedDocument.SourceDocumentRltns.Remove(this);
                        _derivedDocument = value;
                        if (_derivedDocument != null)
                            _derivedDocument.SourceDocumentRltns.Add(this);
                        PropertyHasChanged("DerivedDocument");
                    }
            }
        }

        /// <summary>
        /// Признак связки по-умолчанию
        /// </summary>
        public virtual bool IsDefault
        {
            get { return _isDefault; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _isDefault = value;
                }
                else
                    if (_isDefault != value)
                    {
                        _isDefault = value;
                        PropertyHasChanged("IsDefault");
                    }
            }
        }

        #endregion

        #region DomainObject

        /// <summary>
        /// Хэш объекта
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return HashOf(Guid);
        }

        /// <summary>
        /// Преобразование в строку
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} - {1} {2}", SourceDocument, DerivedDocument, IsDefault);
        }

        #endregion
    }
}
