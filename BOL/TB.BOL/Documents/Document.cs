﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using TB.BOL.Descriptions;
using TB.BOL.Interfaces;
using TB.BOL.Materials;
using TB.BOL.RouteStep;

namespace TB.BOL.Documents
{
    /// <summary>
    /// Статус документа
    /// </summary>
    [DataContract]
    public enum DocumentStatus
    {
        /// <summary>
        /// Создан
        /// </summary>
        [EnumMember]
        Created,

        /// <summary>
        /// Проверен
        /// </summary>
        [EnumMember]
        Checked,

        /// <summary>
        /// Отправлен
        /// </summary>
        [EnumMember]
        Sended,

        /// <summary>
        /// Утвержден
        /// </summary>
        [EnumMember]
        Approved,

        /// <summary>
        /// Закрыт
        /// </summary>
        [EnumMember]
        Closed,

        /// <summary>
        /// Аннулирован
        /// </summary>
        [EnumMember]
        Annuled,

        /// <summary>
        ///Отложен
        /// </summary>
        [EnumMember]
        Delayed
    }

    /// <summary>
    /// Тип связи документов
    /// </summary>
    public enum DocumentRelationType
    {
        /// <summary>
        /// Документ-источник
        /// </summary>
        Source,

        /// <summary>
        /// Производный документ
        /// </summary>
        Derived
    }


    public class DocumentBase : DomainObject<long>
    {
        #region Fields

        protected DocumentType _documentType;
        private long _no;
        private string _name;
        private DocumentStatus _documentStatus;
        private DateTime? _documentDate;

        #endregion

        #region Properties

        /// <summary>
        /// Тип документа
        /// </summary>
        public virtual DocumentType DocumentType
        {
            get { return _documentType; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _documentType = value;
                }
                else
                    if (_documentType != value)
                    {
                        _documentType = value;
                        PropertyHasChanged("DocumentType");
                    }
            }
        }

        /// <summary>
        /// Номер документа
        /// </summary>
        public virtual long No
        {
            get { return _no; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _no = value;
                }
                else
                    if (_no != value)
                    {
                        _no = value;
                        PropertyHasChanged("No");
                    }
            }
        }

        /// <summary>
        /// Наименование документа
        /// </summary>
        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _name = value;
                }
                else
                    if (_name != value)
                    {
                        _name = value;
                        PropertyHasChanged("Name");
                    }
            }
        }

        /// <summary>
        /// Статус документа
        /// </summary>
        public virtual DocumentStatus DocumentStatus
        {
            get { return _documentStatus; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _documentStatus = value;
                }
                else
                    if (_documentStatus != value)
                    {
                        _documentStatus = value;
                        PropertyHasChanged("DocumentStatus");
                    }
            }
        }

        /// <summary>
        /// Дата документа
        /// </summary>
        public virtual DateTime DocumentDate
        {
            get { return _documentDate ?? DateTime.Now; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _documentDate = value;
                }
                else
                    if (_documentDate != value)
                    {
                        _documentDate = value;
                        PropertyHasChanged("DocumentDate");
                    }
            }
        }

        /// <summary>
        /// Строки документа
        /// </summary>
        public virtual ICollection<DocumentLine> Lines { get; set; }

        #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        public DocumentBase()
        {
            Lines = new List<DocumentLine>();
        }


        /// <summary>
        /// Преобразование в строку
        /// </summary>
        /// <returns>Строковое представление документа</returns>
        public override string ToString()
        {
            return string.Format("№ {0} ({1} {2}) {3}", Name,"" /*DocumentType.ShortName*/, No, DocumentDate);
        }

        /// <summary>
        /// Хэш объекта
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return HashOf(Guid);
        }
    }

    public class Document : DocumentBase, IDescribable<Document>, ICloneable, IRouteStep<Document>
    {
        #region Fields

        //private DocumentType _documentType;
        //private DocumentPurpose _documentPurpose;
        private Document _parent;

        #endregion

        #region Properties

        /// <summary>
        /// Тип документа
        /// </summary>
        public override DocumentType DocumentType
        {
            get { return _documentType; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _documentType = value;
                }
                else
                    if (_documentType != value)
                    {
                        /*if (_documentType != null)
                            _documentType.Documents.Remove(this);*/
                        _documentType = value;
                        /*if (_documentType != null)
                            _documentType.Documents.Add(this);*/
                        PropertyHasChanged("DocumentType");
                    }
            }
        }
      
        /// <summary>
        /// Родительская запись
        /// </summary>
        public virtual Document Parent
        {
            get { return _parent; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _parent = value;
                }
                else
                    if (_parent != value)
                    {
                        if (_parent != null)
                            _parent.Childs.Remove(this);
                        _parent = value;
                        if (_parent != null)
                            _parent.Childs.Add(this);
                        PropertyHasChanged("Parent");
                    }
            }
        }
        /// <summary>
        /// Список описаний документа
        /// </summary>
        public virtual ICollection<Description<Document>> Descriptions { get; set; }

        /// <summary>
        /// Список дочерних документов
        /// </summary>
        public virtual ICollection<DocumentBase> Childs { get; set; }

        /// <summary>
        /// Список связок с производными документами
        /// </summary>
        public virtual ICollection<DocumentRelation> DerivedDocumentRltns { get; set; }

        /// <summary>
        /// Список производных документов
        /// </summary>
        public virtual ICollection<Document> DerivedDocuments
        {
            get { return DerivedDocumentRltns.ToList().ConvertAll(x => x.DerivedDocument); }
        }

        /// <summary>
        /// Список связок с исходными документами
        /// </summary>
        public virtual ICollection<DocumentRelation> SourceDocumentRltns { get; set; }

        /// <summary>
        /// Список исходных документов
        /// </summary>
        public virtual ICollection<Document> SourceDocuments
        {
            get { return SourceDocumentRltns.ToList().ConvertAll(x => x.SourceDocument); }
        }

        /// <summary>
        /// Шаги маршрута документа
        /// </summary>
        //public virtual IList<DocumentRouteStep> DocumentRouteSteps { get; set; }

        #endregion

        #region Methods
        
        /// <summary>
        /// Конструктор
        /// </summary>
        public Document()
        {

            Lines = new List<DocumentLine>();
            Descriptions = new List<Description<Document>>();
            DerivedDocumentRltns = new List<DocumentRelation>();
            SourceDocumentRltns = new List<DocumentRelation>();
        }

        /// <summary>
        /// Добавление связки документов
        /// </summary>
        public virtual void AddDocumentRelation(DocumentRelationType relationType, Document document, bool isDefault)
        {
            var documentRelations = relationType == DocumentRelationType.Source ?
                SourceDocumentRltns : DerivedDocumentRltns;
            var sourceDocument = relationType == DocumentRelationType.Source ?
                document : this;
            var derivedDocument = relationType == DocumentRelationType.Source ?
                this : document;

            if (!documentRelations.Any(x => x.SourceDocument == sourceDocument &&
                    x.DerivedDocument == derivedDocument))
                new DocumentRelation
                {
                    SourceDocument = sourceDocument,
                    DerivedDocument = derivedDocument,
                    IsDefault = isDefault
                };
        }
        /// <summary>
        /// Добавление связки документов
        /// </summary>
        public virtual void AddDocumentRelation(DocumentRelationType relationType, Document document)
        {
            AddDocumentRelation(relationType, document, false);
        }

        /// <summary>
        /// Добавление или замена связки документов. Перед добавлением удаляются связки с другими документами того же типа
        /// </summary>
        /// <param name="relationType">Тип связки</param>
        /// <param name="document">Документ, который требуется привязать</param>
        public virtual void ReplaceDocumentRelation(DocumentRelationType relationType, Document document)
        {
            var documents = relationType == DocumentRelationType.Source
                                              ? SourceDocuments
                                              : DerivedDocuments;
            documents.Where(x => x.DocumentType == document.DocumentType && x != document).ToList()
                .ForEach(x => RemoveDocumentRelation(relationType, x));
            AddDocumentRelation(relationType, document);
        }

        /// <summary>
        /// Удаление связки документов
        /// </summary>
        /// <param name="relationType">Тип связки</param>
        /// <param name="document">Связанный документ</param>
        public virtual void RemoveDocumentRelation(DocumentRelationType relationType, Document document)
        {
            var documentRelations = relationType == DocumentRelationType.Source ?
                SourceDocumentRltns : DerivedDocumentRltns;
            var sourceDocument = relationType == DocumentRelationType.Source ?
                document : this;
            var derivedDocument = relationType == DocumentRelationType.Source ?
                this : document;

            var documentRelation =
                documentRelations.FirstOrDefault(x => x.SourceDocument == sourceDocument &&
                                                        x.DerivedDocument == derivedDocument);

            if (documentRelation == null)
                return;

            documentRelation.SourceDocument = null;
            documentRelation.DerivedDocument = null;
        }

        /// <summary>
        /// Удаление всех связок документа по типу
        /// </summary>
        /// <param name="relationType">Тип связки</param>
        public virtual void RemoveDocumentRelations(DocumentRelationType? relationType)
        {
            Parent = null;

            if (relationType == null || relationType == DocumentRelationType.Derived)
                DerivedDocuments
                    .ToList()
                    .ForEach(x => RemoveDocumentRelation(DocumentRelationType.Derived, x));

            if (relationType == null || relationType == DocumentRelationType.Source)
                SourceDocuments
                    .ToList()
                    .ForEach(x => RemoveDocumentRelation(DocumentRelationType.Source, x));
        }
        public virtual void RemoveDocumentRelations()
        {
            RemoveDocumentRelations(null);
        }

        /// <summary>
        /// Добавление строки документа
        /// </summary>
        /// <param name="material">Материал</param>
        /// <param name="checkDesctiptions">Check descriptions compatibility between document and material</param>
        /// <returns>Строка документа</returns>
        public virtual DocumentLine AddDocumentLine(Material material, bool checkDesctiptions = true)
        {
            // Если не задан материал, то выход
            if (material == null)
                return null;

            var documentLine = Lines.FirstOrDefault(x => x.Material == material && x.DocumentLineStatus != DocumentLineStatus.Annuled);
            if (documentLine == null )
            {
                documentLine = new DocumentLine(this) { Material = material };
            }

            return documentLine;
        }

        /// <summary>
        /// Удаление строки документа
        /// </summary>
        /// <param name="material">Материал</param>
        public virtual void RemoveDocumentLine(Material material)
        {
            // Если не задан материал, то выход
            if (material == null)
                return;

            var documentLine = Lines.FirstOrDefault(x => x.Material == material &&
                x.DocumentLineStatus != DocumentLineStatus.Annuled);

            RemoveDocumentLine(documentLine);
        }

        /// <summary>
        /// Удаление строки документа
        /// </summary>
        /// <param name="documentLine">Строка</param>
        public virtual void RemoveDocumentLine(DocumentLine documentLine)
        {
            if (documentLine == null || documentLine.Document == null || documentLine.Document != this)
                return;

            // Отмена шага документирования
            if (documentLine.Material != null)
            {
                //MaterialRouteStep step = documentLine.Material.GetMaterialRouteStep(OperationConst.AddingDocument, this);
               // if (step != null)
                   // step.Cancel();
            }

            documentLine.Document = null;
            documentLine.Material = null;
        }

        #endregion

        #region ICloneable Members

        public virtual object Clone()
        {
            Document cloneDocument = new Document
            {
                No = No,
                Name = Name,
                DocumentDate = DateTime.Now,
                DocumentStatus = DocumentStatus.Created,
                DocumentType = DocumentType,
            };

            // Копирование описаний)
            foreach (Description<Document> desc in Descriptions)
                desc.Clone(cloneDocument);

            // Копирование связок с производными документами
            foreach (DocumentRelation relation in DerivedDocumentRltns)
                cloneDocument.AddDocumentRelation(DocumentRelationType.Derived, relation.DerivedDocument,
                                                    relation.IsDefault);

            // Копирование связок с исходными документами
            foreach (DocumentRelation relation in SourceDocumentRltns)
                cloneDocument.AddDocumentRelation(DocumentRelationType.Source, relation.SourceDocument,
                                                    relation.IsDefault);

            return cloneDocument;
        }

        #endregion

        public virtual Description this[long code]
        {
            get { return this.GetDescription(code); }
            set
            {
                this.SetDescription(code, value.MinValue, value.MaxValue, value.StringValue, value.BeginDate,
                    value.EndDate);
            }
        }

        public virtual ICollection<RouteStep<Document>> RouteSteps { get; set; }
    }
}
