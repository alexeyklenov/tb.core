﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace TB.BOL
{
    public sealed class BolTypeLocator
    {
        private static readonly Dictionary<string, Type> Cache = new Dictionary<string, Type>();

        public static Type GetType(string typeName)
        {
            return GetType(typeName, null);
        }

        public static Type GetType(string typeName, Type baseType)
        {
            Type result;
            if (Cache.TryGetValue(typeName, out result))
                return result;

            lock (Cache)
            {
                if (Cache.TryGetValue(typeName, out result))
                    return result;

                var assembly = Assembly.GetExecutingAssembly();
                var assemblyTypes = assembly.GetTypes()
                    .Where(x => baseType == null || x.BaseType == baseType)
                    .ToArray();

                var typeIndex = 0;
                while (typeIndex < assemblyTypes.Length)
                {
                    if (assemblyTypes[typeIndex].Name == typeName)
                        return Cache[typeName] = assemblyTypes[typeIndex];

                    typeIndex++;
                }
            }

            return null;
        }
    }
}
