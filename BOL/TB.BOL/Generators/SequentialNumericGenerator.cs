﻿using System;
using System.Runtime.Serialization;

namespace TB.BOL.Generators
{
    [KnownType(typeof(SequentialNumericGenerator))]
    [DataContract]
    public class SequentialNumericGenerator : SequentialGenerator
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public SequentialNumericGenerator()
        {
            ValueTypeName = typeof(Int64).FullName;
        }
    }
}
