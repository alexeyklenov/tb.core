﻿using System;
using System.Runtime.Serialization;

namespace TB.BOL.Generators
{
    /// <summary>
    ///     Генератор последовательных значений
    /// </summary>
    [KnownType(typeof(SequentialGenerator))]
    [DataContract]
    public abstract class SequentialGenerator : Generator
    {
        #region Fields

        private Int64 _no;
        private Int64 _minNo;
        private Int64? _maxNo;
        private Int64 _step;

        #endregion

        #region Properties

        /// <summary>
        ///     Номер
        /// </summary>
        [DataMember]
        public virtual Int64 No
        {
            get { return _no; }
            set
            {
                if (_no == value)
                    return;

                _no = value;
                if (!IsLoadingInProgress)
                    PropertyHasChanged("No");
            }
        }

        /// <summary>
        ///     Минимальный номер
        /// </summary>
        [DataMember]
        public virtual Int64 MinNo
        {
            get { return _minNo; }
            set
            {
                if (_minNo == value)
                    return;

                _minNo = value;
                if (!IsLoadingInProgress)
                    PropertyHasChanged("MinNo");
            }
        }

        /// <summary>
        ///     Максимальный номер
        /// </summary>
        [DataMember]
        public virtual Int64? MaxNo
        {
            get { return _maxNo; }
            set
            {
                if (_maxNo == value)
                    return;

                _maxNo = value;
                if (!IsLoadingInProgress)
                    PropertyHasChanged("MaxNo");
            }
        }

        /// <summary>
        ///     Шаг
        /// </summary>
        [DataMember]
        public virtual Int64 Step
        {
            get { return _step; }
            set
            {
                if (_step == value)
                    return;

                _step = value;
                if (!IsLoadingInProgress)
                    PropertyHasChanged("Step");
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Формирование текущего значения
        /// </summary>
        /// <returns></returns>
        public override Object CurrentValue()
        {
            return No;
        }

        #endregion

        /// <summary>
        ///     Конструктор
        /// </summary>
        public SequentialGenerator()
        {
            GeneratorType = GeneratorType.Sequential;
            Step = 1;
        }

        #region Generator

        public override void Change()
        {
            // Формирование следующего значения
            No = No + Step;

            // Если значение генератора превысило максимальное, то оно сбрасывается
            if (MaxNo!=null && No > MaxNo)
                No = MinNo;
        }

        #endregion
    }
}
