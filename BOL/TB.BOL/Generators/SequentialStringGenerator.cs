﻿using System;
using System.Runtime.Serialization;

namespace TB.BOL.Generators
{
     [KnownType(typeof(SequentialStringGenerator))]
    [DataContract]
    public class SequentialStringGenerator : SequentialGenerator
    {
        #region Fields

        private string _prefix;
        private string _suffix;

        #endregion

        #region Properties

        /// <summary>
        /// Префикс
        /// </summary>
        [DataMember]
        public string Prefix
        {
            get { return _prefix; }
            set
            {
                if (_prefix != value)
                {
                    _prefix = value;
                    if (!IsLoadingInProgress)
                    {
                        PropertyHasChanged("Prefix");
                    }
                }
            }
        }

        /// <summary>
        /// Суффикс
        /// </summary>
        [DataMember]
        public string Suffix
        {
            get { return _suffix; }
            set
            {
                if (_suffix != value)
                {
                    _suffix = value;
                    if (!IsLoadingInProgress)
                    {
                        PropertyHasChanged("Suffix");
                    }
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Формирование текущего значения
        /// </summary>
        /// <returns></returns>
        public override Object CurrentValue()
        {
            return Prefix + No + Suffix;
        }

        #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        public SequentialStringGenerator()
        {
            ValueTypeName = typeof(string).FullName;
        }
    }
}
