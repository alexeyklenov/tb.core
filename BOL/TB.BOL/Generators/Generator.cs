﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using TB.BOL.Descriptions;
using TB.BOL.Interface;
using TB.BOL.Interfaces;
using TB.Services.Infrastructure.Client;

namespace TB.BOL.Generators
{
    [DataContract]
    public enum GeneratorType
    {
        /// <summary>
        ///     Последовательный
        /// </summary>
        [EnumMember]
        Sequential
    }
    [KnownType(typeof(Generator))]
    [DataContract]
    public abstract class Generator : DomainObject<long>, IExternallyChangeable, IDescribable<Generator>
    {
        #region Fields

        private GeneratorType _generatorType;
        private string _name;
        private string _valueTypeName;
        private long _code;
        #endregion

        #region Properties

        /// <summary>
        ///     Тип цеховой единицы
        /// </summary>
        [DataMember]
        public virtual GeneratorType GeneratorType
        {
            get { return _generatorType; }
            set
            {
                if (_generatorType == value)
                    return;

                _generatorType = value;
                if (!IsLoadingInProgress)
                    PropertyHasChanged("GeneratorType");
            }
        }

        /// <summary>
        ///     Код
        /// </summary>
        [DataMember]
        public virtual long Code
        {
            get { return _code; }
            set
            {

                if (_code == value)
                    return;

                _code = value;
                if (!IsLoadingInProgress)
                    PropertyHasChanged("Code"); 
            }
        }

        /// <summary>
        ///     Наименование
        /// </summary>
        [DataMember]
        public virtual String Name
        {
            get { return _name; }
            set
            {
                if (_name == value)
                    return;

                _name = value;
                if (!IsLoadingInProgress)
                    PropertyHasChanged("Name");
            }
        }

        /// <summary>
        ///     Тип значения генератора
        /// </summary>
        [DataMember]
        public virtual String ValueTypeName
        {
            get { return _valueTypeName; }
            set
            {
                if (_valueTypeName == value)
                    return;

                _valueTypeName = value;
                if (!IsLoadingInProgress)
                    PropertyHasChanged("ValueTypeName");
            }
        }

        public virtual Type ValueType
        {
            get { return Type.GetType(ValueTypeName); }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Конструктор
        /// </summary>
        protected Generator()
        {
            Descriptions = new List<Description<Generator>>();
        }

        /// <summary>
        ///     Cледующее значение
        /// </summary>
        /// <returns></returns>
        public virtual Object NextValue
        {
            get { return ExternalChanger<Generator>.Change(this).CurrentValue(); }
        }

        /// <summary>
        ///     Формирование текущего значения
        /// </summary>
        /// <returns></returns>
        public abstract Object CurrentValue();

        #endregion

        #region DomainObject

        /// <summary>
        ///     Хэш объекта
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return HashOf(Name);
        }

        /// <summary>
        ///     Преобразование в строку
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Name;
        }

        #endregion

        /// <summary>
        ///     Изменение значения генератора
        /// </summary>
        public abstract void Change();

        /// <summary>
        ///     Список описаний генератора
        /// </summary>
        public virtual ICollection<Description<Generator>> Descriptions { get; set; }
    }
}
