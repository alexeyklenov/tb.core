﻿using System;

namespace TB.BOL.Administration
{
    public class ParameterValue : DomainObject<long>
    {

        public Parameter Parameter { get; set; }

        public long Code { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }

        public override int GetHashCode()
        {
            return HashOf(Code, Parameter.Name);
        }

        public override string ToString()
        {
            return String.Format("{0}={1}({2})", Parameter.Name, Code, Name);
        }
    }
}
