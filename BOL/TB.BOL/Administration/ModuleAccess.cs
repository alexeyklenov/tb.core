﻿using System.Collections.Generic;
using TB.Utils.Enums;

namespace TB.BOL.Administration
{
    public class ModuleAccess : DomainObject<long>
    {
        public Role Role { get; set; }
        public Module Module { get; set; }
        public AccessType AccessType { get; set; }
        public ICollection<ModuleAction> ModuleActions { get; set; }

        public ModuleAccess()
        {
            ModuleActions = new List<ModuleAction>();
        }

        public override int GetHashCode()
        {
            return HashOf(Role, Module);
        }

        public override string ToString()
        {
            return string.Format("{0}:{1}", Role.Name, Module.Name);
        }
    }
}
