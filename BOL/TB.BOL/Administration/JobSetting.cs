﻿using System;

namespace TB.BOL.Administration
{
    public class JobSetting
    {
        public long Id { get; set; }
        public string JobCode { get; set; }
        public string JobName { get; set; }
        public string AgentCode { get; set; }
        public string AgentName { get; set; }
        public TimeSpan Period { get; set; }
        public bool IsActive { get; set; }
    }
}
