﻿namespace TB.BOL.Administration
{
    public class ModuleAction : DomainObject<long>
    {
        public Module Module { get; set; }

        public Action Action { get; set; }

        public string MenuItemPath { get; set; }

        public string Parameters { get; set; }

        public ModuleAction Parent { get; set; }

        //public IList<ModuleAction> Childs { get; set; }

        public ModuleAction()
        {
            //    Childs = new List<ModuleAction>();
        }

        public override int GetHashCode()
        {
            var __h = HashOf(Module.Name, Action.Name, MenuItemPath) + (Parent != null ? Parent.GetHashCode() : 0);
            return __h;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2}", Module.Name, Action.Name, MenuItemPath);
        }
    }
}
