﻿namespace TB.BOL.Administration
{
    public class SessionContext : DomainObject<long>
    {
        /// <summary>
        /// Сессия
        /// </summary>
        public Session Session { get; set; }

        /// <summary>
        /// Переменная
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Значение
        /// </summary>
        public string Value { get; set; }

        public override int GetHashCode()
        {
            return HashOf(Session, ToString());
        }

        public override string ToString()
        {
            return string.Format("{0}:{1}", Name, Value);
        }
    }
}
