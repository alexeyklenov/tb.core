﻿using System;

namespace TB.BOL.Administration
{
    /// <summary>
    ///     Сотрудник
    /// </summary>
    public class Employee : DomainObject<long>
    {
        private string _surname;
        private string _name;
        private string _patname;
        private DateTime? _beginDate;
        private DateTime? _endDate;

        /// <summary>
        /// Фамилия
        /// </summary>
        public virtual string Surname
        {
            get { return _surname; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _surname = value;
                }
                else if (_surname != value)
                {
                    _surname = value;
                    PropertyHasChanged("Surname");
                }
            }
        }

        /// <summary>
        /// Имя
        /// </summary>
        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _name = value;
                }
                else if (_name != value)
                {
                    _name = value;
                    PropertyHasChanged("Name");
                }
            }
        }

        /// <summary>
        /// Отчество
        /// </summary>
        public virtual string Patname
        {
            get { return _patname; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _patname = value;
                }
                else if (_patname != value)
                {
                    _patname = value;
                    PropertyHasChanged("Patname");
                }
            }
        }

        /// <summary>
        /// Дата начала
        /// </summary>
        public virtual DateTime? BeginDate
        {
            get { return _beginDate; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _beginDate = value;
                }
                else if (_beginDate != value)
                {
                    _beginDate = value;
                    PropertyHasChanged("BeginDate");
                }
            }
        }

        /// <summary>
        /// Дата окончания
        /// </summary>
        public virtual DateTime? EndDate
        {
            get { return _endDate; }
            set
            {
                if (IsLoadingInProgress)
                {
                    _endDate = value;
                }
                else if (_endDate != value)
                {
                    _endDate = value;
                    PropertyHasChanged("EndDate");
                }
            }
        }

        /// <summary>
        /// Фамилия И.О.
        /// </summary>
        public virtual string FullName
        {
            get
            {
                return (!string.IsNullOrEmpty(Surname) ? Surname + " " : string.Empty) +
                    (!string.IsNullOrEmpty(Name) ? Name.Substring(0, 1).ToUpper() + "." : string.Empty) +
                    (!string.IsNullOrEmpty(Patname) ? Patname.Substring(0, 1).ToUpper() + "." : string.Empty);
            }
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        protected Employee()
        {

        }

        public override int GetHashCode()
        {
            return HashOf(Surname, Name, Patname);
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2}", Surname, Name, Patname);
        }
    }
}
