﻿using System.Globalization;

namespace TB.BOL.Administration
{
    /// <summary>
    /// Типы действий
    /// </summary>
    public class ActionType : DomainObject<long>
    {
        /// <summary>
        /// Наименование типа действий
        /// </summary>
        public string Name { get; set; }

        public override int GetHashCode()
        {
            return HashOf(Name);
        }

        public override string ToString()
        {
            return Name;
        }
        private static ActionType _menuItem;
        public static ActionType MenuItem
        {
            get { return _menuItem ?? (_menuItem = new ActionType() {Id = 1, Name = "Пункт меню"}); }
        }
        public static ActionType TypeById(long id)
        {
            switch (id)
            {
                case 1:
                    return MenuItem;
                default:
                    return new ActionType { Id = id, Name = id.ToString(CultureInfo.InvariantCulture) };
            }

        }


    }
}
