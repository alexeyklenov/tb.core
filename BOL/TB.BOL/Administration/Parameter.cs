﻿using System.Collections.Generic;
using TB.BOL.Descriptions;

namespace TB.BOL.Administration
{
    /// <summary>
    /// Параметр-тип
    /// </summary>
    public class Parameter : DomainObject<long>
    {
        public string Name { get; set; }

        public string DataType { get; set; }
        public string Note { get; set; }

        public DescriptionType ReferenceType { get; set; }

        public ICollection<ParameterValue> Values { get; set; }

        public Parameter()
        {
            Values = new List<ParameterValue>();
        }
        public override int GetHashCode()
        {
            return HashOf(Name, DataType);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
