﻿using System;
using System.Collections.Generic;

namespace TB.BOL.Administration
{
    public class Module : DomainObject<long>
    {
        public Module Parent { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
        public string FileName { get; set; }

        public ICollection<Module> Childs { get; set; }
        public ICollection<Module> Dependencies { get; set; }
        public ICollection<ModuleAction> Actions { get; set; }

        public ICollection<ModuleAccess> Access { get; set; }

        public DateTime BeginDate { get; set; }
        public DateTime? EndDate { get; set; }

        public Module()
        {
            Childs = new List<Module>();
            Dependencies = new List<Module>();
            Actions = new List<ModuleAction>();
            Access = new List<ModuleAccess>();
        }

        public override int GetHashCode()
        {
            return HashOf(Name);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
