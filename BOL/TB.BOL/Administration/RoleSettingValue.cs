﻿using System;
using TB.BOL.Descriptions;

namespace TB.BOL.Administration
{
    public class RoleSettingValue: DomainObject<long>
    {
        public Setting Setting { get; set; }

        public Role Role { get; set; }

        /// <summary>
        ///  Тип параметра
        /// </summary>
        public Parameter Parameter { get; set; }

        /// <summary>
        /// значение параметра
        /// </summary>
        public ParameterValue ParameterValue { get; set; }

        /// <summary>
        /// справочное значение параметра
        /// </summary>
        public DescriptionValue ReferenceValue { get; set; }

        /// <summary>
        /// Строковое значение
        /// </summary>
        public String Value { get; set; }

        public override int GetHashCode()
        {
            return HashOf(Guid);
        }

        public override string ToString()
        {
            return Role.ToString();
        }
    }
}
