﻿namespace TB.BOL.Administration
{
    /// <summary>
    /// Действие
    /// </summary>
    public class Action : DomainObject<long>
    {
        /// <summary>
        /// Тип действия
        /// </summary>
        public ActionType ActionType { get; set; }

        /// <summary>
        /// Наименование действия - уникальный ключ, по которому должна производится отработка
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Примечание 
        /// </summary>
        public string Note { get; set; }

        public override int GetHashCode()
        {
            return HashOf(Name);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
