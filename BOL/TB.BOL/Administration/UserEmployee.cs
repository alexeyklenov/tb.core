﻿namespace TB.BOL.Administration
{
    public class UserEmployee : DomainObject<long>
    {
        public long EmployeeId { get; set; }
        public Employee Employee { get; set; }
        public User User { get; set; }

        public override int GetHashCode()
        {
            return User.GetHashCode() + HashOf(EmployeeId);
        }

        public override string ToString()
        {
            return string.Format("{0} ({1})", User, EmployeeId);
        }
    }
}
