﻿using System.Collections.Generic;

namespace TB.BOL.Administration
{
    public class Role : DomainObject<long>
    {
        public string Name { get; set; }
        public string Note { get; set; }

        public ICollection<ModuleAccess> Modules { get; set; }
        public ICollection<User> Users { get; set; }

        public Role()
        {
            Modules = new List<ModuleAccess>();
            Users = new List<User>();
        }

        public override int GetHashCode()
        {
            return HashOf(Name);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
