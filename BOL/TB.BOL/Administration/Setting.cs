﻿using System;
using System.Collections.Generic;
using TB.BOL.Descriptions;

namespace TB.BOL.Administration
{
    /// <summary>
    /// Настройка
    /// </summary>
    public class Setting : DomainObject<long>
    {

        /// <summary>
        /// Порядковый номер настройки
        /// </summary>
        public Int32 No { get; set; }

        /// <summary>
        /// Код настройки
        /// </summary>
        public long Code { get; set; }

        /// <summary>
        /// Порядок в дереве зависимых настроек
        /// </summary>
        public Int32 Height { get; set; }

        /// <summary>
        /// Имя параметра
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        ///  Примечание
        /// </summary>
        public String Note { get; set; }

        /// <summary>
        ///  Тип параметра
        /// </summary>
        public Parameter Parameter { get; set; }

        /// <summary>
        /// значение параметра
        /// </summary>
        public ParameterValue ParameterValue { get; set; }

        /// <summary>
        /// справочное значение параметра
        /// </summary>
        public DescriptionValue ReferenceValue { get; set; }

        /// <summary>
        /// тип справочного значения
        /// </summary>
        public DescriptionType ReferenceType { get; set; }

        /// <summary>
        /// Строковое значение
        /// </summary>
        public String Value { get; set; }

        /// <summary>
        /// Модуль
        /// </summary>
        public Module Module { get; set; }

        /// <summary>
        /// Роль
        /// </summary>
        public Role Role { get; set; }
        
        /// <summary>
        /// Родительская запись
        /// </summary>
        public Setting Parent { get; set; }

        /// <summary>
        /// Общая настройка, не перегружается ролями
        /// </summary>
        public Boolean? IsGlobal { get; set; }

        /// <summary>
        /// Дочерние записи
        /// </summary>
        public ICollection<Setting> Children { get; set; }

        public override int GetHashCode()
        {
            return HashOf(Guid);
        }

        public override string ToString()
        {
            return Name;
        }

        public Setting()
        {
            Children = new List<Setting>();
        }

    }
}
