﻿using System;
using System.Collections.Generic;

namespace TB.BOL.Administration
{
    public class Session : DomainObject<Guid>
    {
        /// <summary>
        /// Дата и время начала сессии
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Терминал, с которого подключился пользователь
        /// </summary>
        public string Terminal { get; set; }

        /// <summary>
        /// Ид пользователя
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Наименование текущего клиентского модуля
        /// </summary>
        public string Module { get; set; }

        /// <summary>
        /// Примечания
        /// </summary>
        public string Description { get; set; }

        public ICollection<SessionContext> Context { get; set; }

        public Session()
        {
            Context = new List<SessionContext>();
            Guid = Id;
        }

        public override int GetHashCode()
        {
            return HashOf(UserId, Terminal, StartDate);
        }

        public override string ToString()
        {
            return string.Format("{0:d} {1}:{2}", StartDate, UserName, Terminal);
        }
    }
}
