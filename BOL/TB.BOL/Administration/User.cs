﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace TB.BOL.Administration
{
    /// <summary>
    /// Пользователь системы
    /// </summary>
    public class User : DomainObject<long>
    {
        public string Login { get; set; }
        public string DisplayName { get; set; }
        public string Phone { get; set; }
        public string NetworkName { get; set; }
        public string Email { get; set; }
        public string Note { get; set; }

        public string Hash { get; set; }

        public DateTime BeginDate { get; set; }
        public DateTime? EndDate { get; set; }

        [XmlIgnore]
        public ICollection<Role> Roles { get; set; }
        [XmlIgnore]
        public ICollection<UserEmployee> Employees { get; set; }

        public User()
        {
            Roles = new List<Role>();
            Employees = new List<UserEmployee>();
        }

        public override int GetHashCode()
        {
            return HashOf(Login, BeginDate);
        }

        public override string ToString()
        {
            return DisplayName;
        }
    }
}
