﻿using System;
using TB.BOL.Interface;

namespace TB.BOL
{
    /// <summary>
    /// Базовый класс справочника
    /// </summary>
    [Serializable]
    public class Reference<T> : DomainObject<T>, IReference
    {
        #region Fields

        private long _code;
        private string _outerCode;
        private string _shortName;
        private string _name;
        private DateTime? _beginDate;
        private DateTime? _endDate;

        #endregion

        #region IReference Members

        /// <summary>
        /// Код записи справочника
        /// </summary>
        public virtual long Code
        {
            get { return _code; }
            set
            {
                if (_code != value)
                {
                    _code = value;
                    if (!IsLoadingInProgress)
                    {
                        PropertyHasChanged("Code");
                    }
                }
            }
        }

        /// <summary>
        /// Внешний код записи справочника
        /// </summary>
        public virtual string OuterCode
        {
            get { return _outerCode; }
            set
            {
                if (_outerCode != value)
                {
                    _outerCode = value;
                    if (!IsLoadingInProgress)
                    {
                        PropertyHasChanged("OuterCode");
                    }
                }
            }
        }

        /// <summary>
        /// Краткое наименование
        /// </summary>
        public virtual string ShortName
        {
            get { return _shortName; }
            set
            {
                if (_shortName != value)
                {
                    _shortName = value;
                    if (!IsLoadingInProgress)
                    {
                        PropertyHasChanged("ShortName");
                    }
                }
            }
        }

        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    if (!IsLoadingInProgress)
                    {
                        PropertyHasChanged("Name");
                    }
                }
            }
        }

        /// <summary>
        /// Дата начала действия
        /// </summary>
        public virtual DateTime? BeginDate
        {
            get { return _beginDate; }
            set
            {
                if (_beginDate != value)
                {
                    _beginDate = value;
                    if (!IsLoadingInProgress)
                    {
                        PropertyHasChanged("BeginDate");
                    }
                }
            }
        }

        /// <summary>
        /// Дата окончания действия
        /// </summary>
        public virtual DateTime? EndDate
        {
            get { return _endDate; }
            set
            {
                if (_endDate != value)
                {
                    _endDate = value;
                    if (!IsLoadingInProgress)
                    {
                        PropertyHasChanged("EndDate");
                    }
                }
            }
        }

        #endregion

        public static bool operator ==(Reference<T> source, long code)
        {
            return (object)source != null && source.Code.Equals(code);
        }

        public static bool operator !=(Reference<T> source, long code)
        {
            return !(source == code);
        }

        public override int GetHashCode()
        {
            return HashOf(Code,
                (BeginDate.HasValue ? BeginDate : DateTime.MinValue),
                (EndDate.HasValue ? EndDate : DateTime.MaxValue));
        }

        public override string ToString()
        {
            return Name;
        }
    }


    [Serializable]
    public class Reference : Reference<long>
    {
    }
}
