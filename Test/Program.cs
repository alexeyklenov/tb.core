﻿using System;
using TB.BOL.Administration;
using TB.BOL.Descriptions;
using TB.BOL.Documents;
using TB.BOL.Generators;
using TB.BOL.Materials;
using TB.DAL.DAO.Descriptions;
using TB.DAL.Interface;
using TB.Services.Common;
using TB.Services.Common.Dto.Administration;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
           /* var config = new Configuration().AddProperties(new Dictionary<string, string>
                {
                    {Environment.ConnectionDriver, typeof (NHibernate.Driver.SqlServerCeDriver).FullName},
                    {Environment.Dialect, typeof (NHibernate.Dialect.MsSqlCeDialect).FullName},
                    {Environment.ConnectionString, "Data Source=Data.sdf;Max Database Size=256;Persist Security Info=False"},
                    {Environment.ShowSql,"true"},
                    {Environment.ReleaseConnections,  ConnectionReleaseModeParser.ToString(ConnectionReleaseMode.OnClose)},
                });
            config.EventListeners.PreLoadEventListeners = new IPreLoadEventListener[]{new NHibernateEventListener()};
            config.EventListeners.PostLoadEventListeners = new IPostLoadEventListener[] { new NHibernateEventListener() };


            DaoContainer.Init(config);

            DaoContainer.GetDao<Dao<DocumentType>>().SaveOrUpdate(new DocumentType() { Name = "TestName", ShortName = "TestName", Code = 1 });
            DaoContainer.GetDao<Dao<DocumentType>>().CommitChanges();

            DaoContainer.GetDao<DocumentDao>().SaveOrUpdate(new Document() { Name = "test", DocumentType = DaoContainer.GetDao<Dao<DocumentType>>().Linq().FirstOrDefault(x => x.Code == 1) });
            DaoContainer.GetDao<Dao<DocumentType>>().CommitChanges();*/
            //WatchServiceHoster.Start("");

            //var res = DaoContainer.GetDao<DocumentDao>().Linq().Where(d => d.Id == 5).ToList();
            /*var res1 = DaoContainer.GetDao<DocumentDao>().GetById(20L, false);
            var res2 = DaoContainer.GetDao<DocumentDao>().Linq().Fetch(d=>d.Descriptions).First(d=>d.Id == 20L);
            */
            Cashier();

            // Test();

        }

        static void Test()
        {
            var adms = new DbAdministrationService();
            adms.CreateUser(
                new UserDto()
                {
                    BeginDate = DateTime.Now,
                    DisplayName = "Администратор",
                    NetworkName = "Administrator",
                    Login = "Администратор"
                }, "QWErty123");

            adms.CreateUser(
               new UserDto()
               {
                   BeginDate = DateTime.Now,
                   DisplayName = "Пользователь",
                   NetworkName = "User",
                   Login = "Пользователь"
               }, "1");

            var placeCat = DaoContainer.GetDao<DescriptionTypeDao>().Save(new DescriptionType(DescriptionDataType.Reference) { Code = 1, Name = "Справочник мест" });


            DaoContainer.Dao<DocumentType>().Save(new DocumentType() { Code = 1, Name = "Запись инвентаризации" });

            DaoContainer.GetDao<DescriptionValueDao>()
               .Save(new DescriptionValue()
               {
                   Code = 1,
                   DescriptionType = placeCat,
                   Name = "Кабинет директора",
                   ShortName = "Директор"
               });

            DaoContainer.GetDao<DescriptionValueDao>()
              .Save(new DescriptionValue()
              {
                  Code = 1,
                  DescriptionType = placeCat,
                  Name = "Кабинет бухгалтера",
                  ShortName = "Бухгалтер"
              });

            var inventObj = DaoContainer.Dao<MaterialType>().Save(new MaterialType() { Code = 1, Name = "Объект инвентаризации" });

            var material1 = (new Material()
            {
                Code = "111",
                MaterialStatus = MaterialStatus.Stored,
                MaterialType = inventObj,
                Name = "МакБук",

            });

            DaoContainer.Dao<Material>().SaveOrUpdate(material1);
            var material2 = (new Material()
            {
                Code = "222",
                MaterialStatus = MaterialStatus.Stored,
                MaterialType = inventObj,
                Name = "Планшет",
            });

            DaoContainer.Dao<Material>().SaveOrUpdate(material2);
            var material3 = (new Material()
            {
                Code = "333",
                MaterialStatus = MaterialStatus.Stored,
                MaterialType = inventObj,
                Name = "Нетбук",
            });

            DaoContainer.Dao<Material>().SaveOrUpdate(material3);
            var material4 = (new Material()
            {
                Code = "444",
                MaterialStatus = MaterialStatus.Stored,
                MaterialType = inventObj,
                Name = "Пк",
            });

            DaoContainer.Dao<Material>().SaveOrUpdate(material4);

            DaoContainer.NHibernateSession.Flush();
        }

        static void Cashier()
        {
            var adms = new DbAdministrationService();
            adms.CreateUser(
                new UserDto
                {
                    BeginDate = DateTime.Now,
                    DisplayName = "Администратор",
                    NetworkName = "Administrator",
                    Login = "Администратор"
                }, "1");
            //var res = new DbAuthenticationService().Authenticate("Администратор", "1");

            DaoContainer.GetDao<DescriptionTypeDao>().Save(new DescriptionType(DescriptionDataType.Numeric) { Code = 1, Name = "Количество" });
            DaoContainer.GetDao<DescriptionTypeDao>().Save(new DescriptionType(DescriptionDataType.Numeric) { Code = 2, Name = "Цена" });
            DaoContainer.GetDao<DescriptionTypeDao>().Save(new DescriptionType(DescriptionDataType.Numeric) { Code = 3, Name = "Скидка" });

            var paymentTypeDescType =
                DaoContainer.GetDao<DescriptionTypeDao>()
                    .Save(new DescriptionType(DescriptionDataType.ReferenceNumeric) { Code = 4, Name = "Вид оплаты внутренний" });

            DaoContainer.GetDao<DescriptionValueDao>()
                .Save(new DescriptionValue
                {
                    Code = 1,
                    DescriptionType = paymentTypeDescType,
                    Name = "Наличный способ оплаты",
                    ShortName = "Наличные"
                });
            DaoContainer.GetDao<DescriptionValueDao>()
                .Save(new DescriptionValue
                {
                    Code = 2,
                    DescriptionType = paymentTypeDescType,
                    Name = "Безналичный способ оплаты",
                    ShortName = "Терминал"
                });

            DaoContainer.GetDao<DescriptionValueDao>()
                .Save(new DescriptionValue
                {
                    Code = 3,
                    DescriptionType = paymentTypeDescType,
                    Name = "Кредитный способ оплаты",
                    ShortName = "Кредит"
                });

            var partnerDescType = DaoContainer.GetDao<DescriptionTypeDao>().Save(new DescriptionType(DescriptionDataType.Reference) { Code = 5, Name = "Контрагент" });

            DaoContainer.GetDao<DescriptionValueDao>()
                .Save(new DescriptionValue
                {
                    Code = 1,
                    DescriptionType = partnerDescType,
                    Name = "Частное лицо",
                    ShortName = "Частное лицо"
                });

            DaoContainer.GetDao<DescriptionValueDao>()
                .Save(new DescriptionValue
                {
                    Code = 1,
                    DescriptionType = partnerDescType,
                    Name = "Юридическое лицо",
                    ShortName = "Юридическое лицо"
                });

            DaoContainer.GetDao<DescriptionTypeDao>().Save(new DescriptionType(DescriptionDataType.Period) { Code = 6, Name = "Дата смены" });
            DaoContainer.GetDao<DescriptionTypeDao>().Save(new DescriptionType(DescriptionDataType.Numeric) { Code = 7, Name = "Сумма клиента" });
            DaoContainer.GetDao<DescriptionTypeDao>().Save(new DescriptionType(DescriptionDataType.Text) { Code = 8, Name = "Склад" });
            DaoContainer.GetDao<DescriptionTypeDao>().Save(new DescriptionType(DescriptionDataType.Numeric) { Code = 9, Name = "Сумма наличности за смену" });
            DaoContainer.GetDao<DescriptionTypeDao>().Save(new DescriptionType(DescriptionDataType.Logic) { Code = 10, Name = "Индикатор выгрузки смены" });
            DaoContainer.GetDao<DescriptionTypeDao>().Save(new DescriptionType(DescriptionDataType.Numeric) { Code = 11, Name = "Сумма по чеку" });
            DaoContainer.GetDao<DescriptionTypeDao>().Save(new DescriptionType(DescriptionDataType.Text) { Code = 12, Name = "Характеристика" });
            DaoContainer.GetDao<DescriptionTypeDao>().Save(new DescriptionType(DescriptionDataType.Text) { Code = 13, Name = "Текст сообщения" });
            DaoContainer.GetDao<DescriptionTypeDao>().Save(new DescriptionType(DescriptionDataType.Numeric) { Code = 14, Name = "Ставка НДС" });

            /*Справочники --*/
            var cashBoxDesc = new DescriptionType(DescriptionDataType.Reference) {Code = 15, Name = "Кассы"};
            DaoContainer.GetDao<DescriptionTypeDao>().Save(cashBoxDesc);
            var shopDesc = new DescriptionType(DescriptionDataType.Reference) {Code = 16, Name = "Магазины"};
            DaoContainer.GetDao<DescriptionTypeDao>().Save(shopDesc);
            DaoContainer.GetDao<DescriptionTypeDao>().Save(new DescriptionType(DescriptionDataType.ReferenceNumeric) { Code = 17, Name = "Склады" });
            var orgDesc = new DescriptionType(DescriptionDataType.Reference) {Code = 18, Name = "Организации"};
            DaoContainer.GetDao<DescriptionTypeDao>().Save(orgDesc);
            DaoContainer.GetDao<DescriptionTypeDao>().Save(new DescriptionType(DescriptionDataType.Reference) { Code = 19, Name = "Производители" });
            DaoContainer.GetDao<DescriptionTypeDao>().Save(new DescriptionType(DescriptionDataType.Reference) { Code = 20, Name = "Единица измерения" });
            DaoContainer.GetDao<DescriptionTypeDao>().Save(new DescriptionType(DescriptionDataType.Reference) { Code = 21, Name = "Вид оплаты внешний" });
            DaoContainer.GetDao<DescriptionTypeDao>().Save(new DescriptionType(DescriptionDataType.Logic) { Code = 22, Name = "Индикатор выгрузки чека" });
            /*-- Справочники*/

            DaoContainer.GetDao<DescriptionTypeDao>().Save(new DescriptionType(DescriptionDataType.Text) { Code = 23, Name = "Штрихкод" });

            DaoContainer.Dao<DocumentType>().Save(new DocumentType { Code = 1, Name = "Смена" });
            DaoContainer.Dao<DocumentType>().Save(new DocumentType { Code = 2, Name = "Чек" });
            DaoContainer.Dao<DocumentType>().Save(new DocumentType { Code = 3, Name = "Скидка" });
            DaoContainer.Dao<DocumentType>().Save(new DocumentType { Code = 4, Name = "Внесение наличных" });
            DaoContainer.Dao<DocumentType>().Save(new DocumentType { Code = 5, Name = "Выдача наличных" });
            DaoContainer.Dao<DocumentType>().Save(new DocumentType { Code = 6, Name = "Возврат" });
            DaoContainer.Dao<DocumentType>().Save(new DocumentType { Code = 7, Name = "Сообщение" });


            /*SETTINGS --*/

            var stringParam = new Parameter
            {
                DataType = typeof(string).Name,
                Name = "Строковое значение",
                Note = "Строковое значение"
            };
            DaoContainer.Dao<Parameter>().Save(stringParam);

            var baseSet = new Setting {Name = "Общие", Note = "Общие настройки", No = 1, Code = 1};
            DaoContainer.Dao<Setting>().Save(baseSet);

            DaoContainer.Dao<Setting>().Save(new Setting { Name = "Касса", Note = "Наименование кассы", No = 1, Code = 11, Parent = baseSet, Parameter = stringParam });
            DaoContainer.Dao<Setting>().Save(new Setting { Name = "Магазин", Note = "Наименование магазина", No = 1, Code = 12, Parent = baseSet, Parameter = stringParam });
            DaoContainer.Dao<Setting>().Save(new Setting { Name = "Организация", Note = "Наименование организации", No = 1, Code = 13, Parent = baseSet, Parameter = stringParam });
            DaoContainer.Dao<Setting>().Save(new Setting { Name = "Склад", Note = "Наименование склада", No = 1, Code = 14, Parent = baseSet, Parameter = stringParam });

            var equipSet = new Setting
            {
                Name = "Оборудование",
                Note = "Настройки подключаемого оборудования",
                No = 2,
                Code = 2
            };
            DaoContainer.Dao<Setting>().Save(equipSet);

            var fastButtonsSet = new Setting
            {
                Name = "Горячие клавиши",
                Note = "Настройка горячих клавиш",
                No = 3,
                Code = 3
            };
            DaoContainer.Dao<Setting>().Save(fastButtonsSet);

            DaoContainer.Dao<Setting>()
                .Save(new Setting
                {
                    Name = "Количество рядов",
                    Note = "Количество рядов",
                    No = 1,
                    Code = 31,
                    Parent = fastButtonsSet,
                    Parameter = stringParam
                });

            var weightSet =
                DaoContainer.Dao<Setting>()
                    .Save(new Setting
                    {
                        Name = "весовые единицы измерения",
                        Note = "весовые единицы измерения",
                        No = 4,
                        Code = 4
                    });


                DaoContainer.Dao<Setting>()
                    .Save(new Setting
                    {
                        Name = "Чековые клавиши",
                        Note = "Настройка чековых клавиш",
                        No = 5,
                        Code = 5
                    });

            /*  DaoContainer.Dao<Setting>()
                  .Save(new Setting()
                  {
                      Name = "Принтер чеков",
                      Note = "Принтер чеков",
                      No = 1,
                      Code = 21,
                      Parent = equipSet,
                  });

              DaoContainer.Dao<Setting>()
                  .Save(new Setting()
                  {
                      Name = "Принтер чеков",
                      Note = "Принтер чеков",
                      No = 1,
                      Code = 21,
                      Parent = equipSet
                  });*/

            /*-- SETTINGS*/


            DaoContainer.Dao<Generator>()
                .Save(new SequentialNumericGenerator
                {
                    Code = 1,
                    Name = "Универсальный генератор кодов документов",
                    GeneratorType = GeneratorType.Sequential,
                    ValueTypeName = typeof(long).FullName,
                    Step = 1,
                    MinNo = 100
                });

            DaoContainer.Dao<Generator>()
                .Save(new SequentialNumericGenerator
                {
                    Code = 2,
                    Name = "Генератор кодов чеков",
                    GeneratorType = GeneratorType.Sequential,
                    ValueTypeName = typeof(long).FullName,
                    Step = 1,
                    MinNo = 1
                });


            DaoContainer.Dao<MaterialType>().Save(new MaterialType { Code = 1, Name = "Каталог" });
            var offerType = DaoContainer.Dao<MaterialType>().Save(new MaterialType { Code = 2, Name = "Предложение" });
            DaoContainer.Dao<MaterialType>().Save(new MaterialType { Code = 3, Name = "Папка" });

           /* var material1 = (new Material()
            {
                Code = "123",
                MaterialStatus = MaterialStatus.Stored,
                MaterialType = offerType,
                Name = "Тестовый товар",

            });
            material1.SetDescription(2, (Decimal)1000);
            material1.SetDescription(1, (Decimal)1);

            DaoContainer.Dao<Material>().SaveOrUpdate(material1);


            var material2 = DaoContainer.Dao<Material>()
              .Save(new Material()
              {
                  Code = "444",
                  MaterialStatus = MaterialStatus.Stored,
                  MaterialType = offerType,
                  Name = "Тестовый товар2",

              });
            material2.SetDescription(2, (Decimal)500);
            material2.SetDescription(1, (Decimal)1);

            DaoContainer.Dao<Material>().SaveOrUpdate(material2);*/

            DaoContainer.NHibernateSession.Flush();

            //var res = DaoContainer.Dao<Material>().Linq().First(x => x.Code == "123" && x.MaterialStatus == MaterialStatus.Stored);
        }
    }
}
